<?php

    // convert arrays

    if(isset($record->addon_hoodie) && strlen($record->addon_hoodie) > 0) {
        //$record->addon_hoodie = explode(',', $record->addon_hoodie);
    } else {
        $record->addon_hoodie = false;
    }

    if(isset($record->addon_polo) && strlen($record->addon_polo) > 0) {
        //$record->addon_polo = explode(',', $record->addon_polo);
    } else {
        $record->addon_polo = false;
    }

    //print_r($record->toArray()); exit;

    $addons = [

        [
            'key' => 'hoodie',
            'field' => 'addon_hoodie',
            'class' => 'hoodie',
            'name' => '2019 ISC Member\'s Hoodie',
            'description' => 'Gear up in our <strong>2019 Member exclusive</strong> ISC hoodie at our special Members Only price!*',
            'price' => 70,
            'monthly' => '(or $7 p/month)',
            'options' => ['Mens S','Mens M','Mens L','Mens XL','Mens 2XL','Mens 3XL','Mens 4XL','Mens 5XL','Ladies 8','Ladies 10','Ladies 12','Ladies 14','Ladies 16','Ladies 18'],
            'optionsLabel' => 'Size',
            'optionsDropdownLabel' => 'Choose your size...',
            'default' => [],
        ],

        [
            'key' => 'polo',
            'field' => 'addon_polo',
            'class' => 'polo',
            'name' => '2019 ISC Member\'s Polo',
            'description' => 'Get your <strong>2019 Member exclusive</strong> ISC polo for only $50!* Members Only price!*',
            'price' => 50,
            'monthly' => '(or $5 p/month)',
            'options' => ['Mens S','Mens M','Mens L','Mens XL','Mens 2XL','Mens 3XL','Mens 4XL','Mens 5XL','Ladies 8','Ladies 10','Ladies 12','Ladies 14','Ladies 16','Ladies 18'],
            'optionsLabel' => 'Size',
            'optionsDropdownLabel' => 'Choose your size...',
            'default' => [],
        ],

        [
            'key' => 'aflw_team',
            'field' => 'addon_aflw_team',
            'class' => 'aflw-team',
            'name' => '2019 Women\'s Team Membership',
            'description' => 'Get behind the Crows Women\'s Team in 2019! Member pack includes: member kit, handheld fan, wristband, temporary tattoos, & exclusive match day experiences.*',
            'price' => 60,
            'monthly' => '(or $6 p/month)',
            'options' => false,
            'default' => 0,
        ],

        [
            'key' => 'aflw_junior',
            'field' => 'addon_aflw_junior',
            'class' => 'aflw-junior',
            'name' => '2019 Junior Pack',
            'description' => '
                For little footy fans (4 to 14 years)<br>
                <span>Eddie’s Little Legends Junior Bronze</span> - Up to 3 Games General Admission Entry*, Junior pack incl. footy and customised lunchbox PLUS invite to Junior Member Clinic*<br>
                <span>Crows Women’s Team Junior</span> - Crows Women’s Team Junior pack incl. footy, boot bag and temporary tattoos PLUS invite to Junior Member Clinic.*
            ',
            'price' => 35,
            'monthly' => '(or $3.50 p/month)',
            'options' => ['Eddie’s Little Legends Junior Bronze', 'Crows Women’s Team Junior Supporter'],
            'optionsLabel' => 'Size',
            'optionsDropdownLabel' => 'Choose your junior pack...',
            'default' => [],
        ],

    ];

    $merch_items = [
        [
            'key' => 'cap',
            'field' => 'merch_cap',
            'name' => 'Member Cap',
            'label' => [
                'one' => 'I want a cap',
                'multiple' => ['singular' => 'Cap', 'plural' => 'Caps']
            ],
            'note' => '(Navy only)',
            'price' => 0,
        ],
        [
            'key' => 'scarf',
            'field' => 'merch_scarf',
            'name' => 'Member Scarf',
            'label' => [
                'one' => 'I want a scarf',
                'multiple' => ['singular' => 'Scarf', 'plural' => 'Scarves']
            ],
            'note' => '(Navy only)',
            'price' => 0,
        ],
        [
            'key' => 'beanie',
            'field' => 'merch_beanie',
            'name' => 'Beanie',
            'label' => [
                'one' => 'I want a beanie',
                'multiple' => ['singular' => 'Beanie', 'plural' => 'Beanies']
            ],
            'note' => '(Navy only)',
            'price' => 0,
        ],
    ];

    $app_data = [
        'campaign' => $campaign,
        'record' => $record,
        'fields' => $hidden_fields,
        'allowedSelections' => $record->number_of_choices,
        'processUrl' => $process_url,
        'addOns' => $addons,
        'merchItems' => $merch_items,
    ];

    //dd($app_data);
    //print_r($app_data); exit;

?><!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">

    <head>

    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    	<title>{{ $campaign->campaignTitle() }}</title>

    	<link href="{{ $campaign->asset('dist/bundle.css') }}" rel="stylesheet">
        <link rel="icon" href="{{ $campaign->asset('icon.ico') }}" />

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-33851616-15', 'auto');
          ga('send', 'pageview');

        </script>

        <link href="https://fonts.googleapis.com/css?family=Poppins:400,700" rel="stylesheet">
        <link rel="stylesheet" href="https://use.typekit.net/gks6kpg.css">

        <script>
            window._token = '{{ csrf_token() }}';
            window.appData = {!! json_encode($app_data) !!};
        </script>

    </head>

    <body>
        <div id="app"></div>
        <div class="incompatible" id="incompatible">
            <div class="incompatible__inner">
                <div class="incompatible__text">
                    <p class="title">Please upgrade your browser</p>
                    <p class="description">Unfortunately your browser doesn't support this site.<br>Please use one of the following broswers:<br>Google Chrome, Firefox, Safari, Internet Explorer 11 or Microsoft Edge</p>
                </div>
            </div>
        </div>
        <script src="{{ $campaign->asset('dist/bundle.js') }}"></script>
    </body>

</html>
