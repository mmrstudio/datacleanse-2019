import React from 'react';
import { plural, pushArray, popArray } from '../utils';

const DownArrow = (props) => (
    <svg viewBox="0 0 14 8" {...props}>
        <polygon points="7,8 0,1.3 1.4,0 7,5.4 12.6,0 14,1.3 	" />
    </svg>
);

class AddonTile extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            optionsVisible: false,
        }

        this.dropdownToggle = null;

        this.handleDropdownToggle = this.handleDropdownToggle.bind(this);
        this.handleDocumentClick = this.handleDocumentClick.bind(this);
        this.getDefaultSelections = this.getDefaultSelections.bind(this);
        this.incrementAddonQty = this.incrementAddonQty.bind(this);
        this.decrementAddonQty = this.decrementAddonQty.bind(this);
        this.renderOptionsDropdown = this.renderOptionsDropdown.bind(this);
    }

    componentDidMount() {
        document.addEventListener('click', this.handleDocumentClick);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleDocumentClick);
    }

    handleDropdownToggle(e) {
        this.setState({
            optionsVisible: !this.state.optionsVisible,
        });
    }

    handleDocumentClick(e) {
        if (e.target === this.dropdownToggle || e.target.classList.contains('add-ons__tile__dropdown__item__qty__button')) return;
        this.setState({
            optionsVisible: false,
        });
    }

    getDefaultSelections() {
        return this.props.addOn.options.reduce((optionsQty, optionKey) => {
            optionsQty[optionKey] = 0;
            return optionsQty;
        }, {});
    }

    incrementAddonQty(option) {
        const { addOn, addonQty, addonSelections, onUpdate } = this.props;

        if(addOn.options === false) {
            return onUpdate(addOn.field, addonQty + 1);
        } else {

            let newSelections = { ...this.getDefaultSelections(), ...addonSelections};

            if (typeof newSelections[option] !== 'undefined') {
                newSelections[option] = newSelections[option] + 1;
            }

            return onUpdate(addOn.field, newSelections);
        }
    }

    decrementAddonQty(option) {
        const { addOn, addonQty, addonSelections, onUpdate } = this.props;

        if(addOn.options === false) {
            if(addonQty >= 0) return onUpdate(addOn.field, addonQty - 1);
        } else {

            let newSelections = { ...this.getDefaultSelections(), ...addonSelections };

            if (typeof newSelections[option] !== 'undefined') {
                newSelections[option] = newSelections[option] > 0 ? newSelections[option] - 1 : 0;
            }

            return onUpdate(addOn.field, newSelections);
        }
    }

    renderOptionsDropdown() {
        const { addOn, addonSelections } = this.props;
        if (addOn.options === false) return null;

        return (
            <div className="add-ons__tile__dropdown">
                <button className="add-ons__tile__dropdown__toggle" onClick={this.handleDropdownToggle} ref={dropdownToggle => this.dropdownToggle = dropdownToggle}>
                    {addOn.optionsDropdownLabel}
                    <DownArrow />
                </button>
                <ul className="add-ons__tile__dropdown__list" data-visible={this.state.optionsVisible}>
                    {addOn.options.map((option) => {
                        const addonQty = typeof addonSelections[option] !== 'undefined' ? addonSelections[option] : 0;
                        return (
                            <li className="add-ons__tile__dropdown__item" key={option}>
                                <div className="add-ons__tile__dropdown__item__label">{option}</div>
                                <div className="add-ons__tile__dropdown__item__qty">
                                    <button className="add-ons__tile__dropdown__item__qty__button add-ons__tile__dropdown__item__qty__button--decrement" disabled={addonQty <= 0} onClick={e => this.decrementAddonQty(option)}>
                                        <svg viewBox="0 0 16 16"><path d="M15.7,6.1c-0.2-0.2-0.5-0.3-0.8-0.3H1.1c-0.3,0-0.6,0.1-0.8,0.3C0.1,6.3,0,6.6,0,6.9v2.2c0,0.3,0.1,0.6,0.3,0.8 c0.2,0.2,0.5,0.3,0.8,0.3h13.8c0.3,0,0.6-0.1,0.8-0.3C15.9,9.7,16,9.4,16,9.1V6.9C16,6.6,15.9,6.3,15.7,6.1z" /></svg>
                                    </button>
                                    <div className="add-ons__tile__dropdown__item__qty__number" data-has-val={addonQty > 0}>
                                        <span>{addonQty}</span>
                                    </div>
                                    <button className="add-ons__tile__dropdown__item__qty__button add-ons__tile__dropdown__item__qty__button--increment" onClick={e => this.incrementAddonQty(option)}>
                                        <svg viewBox="0 0 16 16"><path d="M15.7,6.1c-0.2-0.2-0.5-0.3-0.8-0.3h-4.7V1.1c0-0.3-0.1-0.6-0.3-0.8C9.7,0.1,9.4,0,9.1,0H6.9C6.6,0,6.3,0.1,6.1,0.3 C5.9,0.5,5.8,0.8,5.8,1.1v4.7H1.1c-0.3,0-0.6,0.1-0.8,0.3C0.1,6.3,0,6.6,0,6.9v2.2c0,0.3,0.1,0.6,0.3,0.8c0.2,0.2,0.5,0.3,0.8,0.3 h4.7v4.7c0,0.3,0.1,0.6,0.3,0.8C6.3,15.9,6.6,16,6.9,16h2.2c0.3,0,0.6-0.1,0.8-0.3c0.2-0.2,0.3-0.5,0.3-0.8v-4.7h4.7 c0.3,0,0.6-0.1,0.8-0.3C15.9,9.7,16,9.4,16,9.1V6.9C16,6.6,15.9,6.3,15.7,6.1z" /></svg>
                                    </button>
                                </div>
                            </li>
                        );
                    })}
                </ul>
            </div>
        );
    }

    render() {
        const { addOn, addonQty, addonSelections } = this.props;
        const addonTotalQty = addOn.options === false ? addonQty : addOn.options.reduce((totalQty, option) => {
            const addonOptionQty = typeof addonSelections[option] !== 'undefined' ? addonSelections[option] : 0;
            totalQty = totalQty + addonOptionQty;
            return totalQty;
        }, 0);

        return (
            <div className="add-ons__tile" data-selected={addonTotalQty > 0}>
                <div className="add-ons__tile__inner">
                    <div className="add-ons__tile__image">
                        <div className={`add-ons__tile__image__inner ${addOn.class}`}></div>
                    </div>
                    <div className="add-ons__tile__desc" dangerouslySetInnerHTML={{__html: addOn.description}}></div>
                    { addOn.price !== false ? (
                    <div className="add-ons__tile__options">

                        <div className="add-ons__tile__price">
                            <span className="add-ons__tile__price__total">${addOn.price} </span>
                            { addOn.monthly !== false ? <span className="add-ons__tile__price__monthly">{addOn.monthly}</span> : false }
                        </div>

                        {addOn.options === false ? (
                            <div className="add-ons__tile__qty">
                                <button className="add-ons__tile__qty__button add-ons__tile__qty__button--decrement" disabled={addonQty <= 0} onClick={e => this.decrementAddonQty()}>
                                    <svg viewBox="0 0 16 16"><path d="M15.7,6.1c-0.2-0.2-0.5-0.3-0.8-0.3H1.1c-0.3,0-0.6,0.1-0.8,0.3C0.1,6.3,0,6.6,0,6.9v2.2c0,0.3,0.1,0.6,0.3,0.8 c0.2,0.2,0.5,0.3,0.8,0.3h13.8c0.3,0,0.6-0.1,0.8-0.3C15.9,9.7,16,9.4,16,9.1V6.9C16,6.6,15.9,6.3,15.7,6.1z" /></svg>
                                </button>
                                <div className="add-ons__tile__qty__number">
                                    <span>{addonQty}</span>
                                </div>
                                <button className="add-ons__tile__qty__button add-ons__tile__qty__button--increment" onClick={e => this.incrementAddonQty()}>
                                    <svg viewBox="0 0 16 16"><path d="M15.7,6.1c-0.2-0.2-0.5-0.3-0.8-0.3h-4.7V1.1c0-0.3-0.1-0.6-0.3-0.8C9.7,0.1,9.4,0,9.1,0H6.9C6.6,0,6.3,0.1,6.1,0.3 C5.9,0.5,5.8,0.8,5.8,1.1v4.7H1.1c-0.3,0-0.6,0.1-0.8,0.3C0.1,6.3,0,6.6,0,6.9v2.2c0,0.3,0.1,0.6,0.3,0.8c0.2,0.2,0.5,0.3,0.8,0.3 h4.7v4.7c0,0.3,0.1,0.6,0.3,0.8C6.3,15.9,6.6,16,6.9,16h2.2c0.3,0,0.6-0.1,0.8-0.3c0.2-0.2,0.3-0.5,0.3-0.8v-4.7h4.7 c0.3,0,0.6-0.1,0.8-0.3C15.9,9.7,16,9.4,16,9.1V6.9C16,6.6,15.9,6.3,15.7,6.1z" /></svg>
                                </button>
                            </div>
                        ) : null}
                        {this.renderOptionsDropdown()}
                        {addonTotalQty > 0 && addOn.options !== false ? (
                            <div className="add-ons__tile__selections">
                                {addonTotalQty} {addonTotalQty > 1 ? 'selections' : 'selection'}
                            </div>
                        ) : null}
                    </div>
                    ) : false }
                </div>
            </div>
        );
    }

}

export default AddonTile;
