import React from 'react';

class Button extends React.Component {

    constructor(props){
        super(props);
    }

    render() {
        let buttonAttrs = {};
        let buttonClasses = ['button'];

        if(this.props.disabled === true) {
            buttonAttrs['disabled'] = 'disabled';
        }

        if(this.props.class) {
            buttonClasses.push(this.props.class);
        }

        let ButtonTag = this.props.anchor === true ? `a` : 'button';
        if(this.props.anchor) {
            if(this.props.target) buttonAttrs['target'] = this.props.target;
            if(this.props.href) buttonAttrs['href'] = this.props.href;
        }

        return (
            <ButtonTag className={buttonClasses.join(' ')} onClick={this.props.onClick} {...buttonAttrs}>
                {this.props.children}
            </ButtonTag>
        )
    }

}

export default Button;
