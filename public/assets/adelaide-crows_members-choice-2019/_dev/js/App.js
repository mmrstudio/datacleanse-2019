import React from 'react';
import MerchTile from './components/MerchTile';
import AddonTile from './components/AddonTile';
import Modal from './components/Modal';
import Checkbox from './components/Checkbox';
import { plural } from './utils';
import { fetchPOST } from './fetchUtils';

require('es6-promise').polyfill();
require('es6-object-assign').polyfill();

class App extends React.Component {

    constructor(props){
        super(props);

        let initialState = {
            campaign: window.appData.campaign,
            record: window.appData.record,
            fields: {},
            hiddenFields: window.appData.fields,
            addOns: window.appData.addOns,
            merchItems: window.appData.merchItems,
            memberPackage: window.appData.memberPackage,
            allowedSelections: parseInt(window.appData.allowedSelections),
            processUrl: window.appData.processUrl,
            totalSelections: 0,
            loading: false,
            submitted: false,
            errors: []
        }

        //console.log('initialState', initialState);

        // initial merch items
        initialState.merchItems.map((merchItem, i) => {
            if(merchItem.field !== false) {
                initialState.fields[merchItem.field] = initialState.record[merchItem.field] ? initialState.record[merchItem.field] : 0;
                initialState.totalSelections += initialState.fields[merchItem.field];
            }
        });

        // initial add-on selections
        initialState.addOns = initialState.addOns.map((addOn, i) => {
            const defValue = addOn.options === false ? 0 : {};
            initialState.fields[addOn.field] = initialState.record[addOn.field] ? initialState.record[addOn.field] : defValue;
            if (addOn.options !== false) {
                if (initialState.fields[addOn.field].length > 0) {
                    const fieldData = initialState.fields[addOn.field].split(',').reduce((options, option) => {
                        const [optionKey, optionValue] = option.split(':');
                        options[optionKey] = Number(optionValue);
                        return options;
                    }, {});
                    initialState.fields[addOn.field] = fieldData;
                } else {
                    initialState.fields[addOn.field] = {};
                }
            }
            return addOn;
        });

        // additional options
        initialState.fields.accept = initialState.record.accept ? initialState.record.accept : 'no';
        initialState.fields.optin_monthly = initialState.record.optin_monthly ? initialState.record.optin_monthly : 'no';
        initialState.fields.optin_upfront = initialState.record.optin_upfront ? initialState.record.optin_upfront : 'no';

        //console.log('initialState', initialState);

        this.state = initialState;

        this._modal = false;

        this._handleAddonSelection = this._handleAddonSelection.bind(this);
        this._handleToggleCheckbox = this._handleToggleCheckbox.bind(this);
        this._handleToggleRadio = this._handleToggleRadio.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        this._onSubmitResponse = this._onSubmitResponse.bind(this);
        this._renderMemberChoice = this._renderMemberChoice.bind(this);
        this._renderAddOns = this._renderAddOns.bind(this);
        this._handleDeleteAddon = this._handleDeleteAddon.bind(this);
        this._handleDeleteAddonOption = this._handleDeleteAddonOption.bind(this);

    }

    _handleMerchSelection(merchField, merchQty) {

        let fields = this.state.fields;

        if(this.state.allowedSelections === 1) {

            // toggle all selections
            this.state.merchItems.map((merchItem, i) => {
                if(merchItem.field !== false) fields[merchItem.field] = 0;
            });

        }

        // set current selection
        fields[merchField] = merchQty;

        // re-calculate total selections
        let totalSelections = 0;
        this.state.merchItems.map(merchItem => {
            if(merchItem.field !== false) totalSelections += fields[merchItem.field];
        });

        this.setState({
            fields: fields,
            totalSelections: totalSelections
        });
    }

    _handleAddonSelection(addonField, addonSelection) {
        let fields = { ...this.state.fields };
        fields[addonField] = addonSelection;
        this.setState({ fields: fields });
    }

    _handleToggleCheckbox(field) {
        let fields = { ...this.state.fields };
        fields[field] = fields[field] === 'no' ? 'yes' : 'no';
        this.setState({ fields: fields });
    }

    _handleToggleRadio(fieldName) {
        let fields = { ...this.state.fields };
        let field = fields[fieldName];

        if (fieldName === 'optin_monthly') {
            fields[fieldName] = field === 'yes' ? 'no' : 'yes';
            fields['optin_upfront'] = 'no';
        }

        if (fieldName === 'optin_upfront') {
            fields[fieldName] = field === 'yes' ? 'no' : 'yes';
            fields['optin_monthly'] = 'no';
        }

        this.setState({ fields: fields });
    }

    _handleSubmit() {
        const { record, fields, hiddenFields, allowedSelections, totalSelections, processUrl, addOns } = this.state;
        let submitErrors = [];

        // check if all selections made
        if(totalSelections < allowedSelections) {
            submitErrors.push(`Please make all your merchandise selections - you have ${allowedSelections - totalSelections} remaining.`);
        }

        // check if agree
        if(fields.accept === 'no') {
            submitErrors.push(`Please agree to the membership terms & conditions.`);
        }

        if(submitErrors.length > 0) return this.setState({ errors: submitErrors });

        const allFields = {...fields, ...hiddenFields};

        // flatten addon options
        function flattenAddonOptions(options) {
            return Object.keys(options).reduce((flattenedOptions, optionKey) => {
                const optionQty = options[optionKey];
                if (optionQty > 0) {
                    flattenedOptions.push(`${optionKey}:${optionQty}`);
                }
                return flattenedOptions;
            }, []).join(',');
        }

        const postData = {
            ...allFields,
            addon_aflw_junior: flattenAddonOptions(allFields.addon_aflw_junior),
            addon_hoodie: flattenAddonOptions(allFields.addon_hoodie),
            addon_polo: flattenAddonOptions(allFields.addon_polo),
            addon_cost: this._calculateAddonTotal(),
        }

        // update loading/submit state
        this.setState({ submitted: false, loading: true });

        //send data
        fetchPOST(
            processUrl,
            postData,
            this._onSubmitResponse
        );

    }

    _onSubmitResponse(response) {

        if(response === true) {

            this.setState({
                submitted: true,
                loading: false,
            });

            scroll(0,0);

        } else {

            let errors = [];

            Object.keys(response).map((key) => {
                let value = response[key];
                value.map((error) => {
                    errors.push(error);
                });
            });

            this.setState({ errors: errors });

            //alert(errors.join("\r\n"));

            this.setState({
                loading: false
            });

        }

    }

    _renderMemberChoice() {

        const { record, allowedSelections, totalSelections, fields, memberPackage } = this.state;
        const selectionsRemaining = allowedSelections - totalSelections;

        let paymentInfo = null;
        switch (record.payment_plan) {
            case 'monthly' :
                paymentInfo = 'By October you will receive a detailed summary of your 2019 Membership and your monthly Easy Pay payment plan rollover.';
                break;
            case 'upfront' :
                paymentInfo = 'By October you will receive a detailed summary of your 2019 Membership and your upfront Easy Pay payment plan rollover.';
                break;
            case 'na' :
            default :
                paymentInfo = 'By October you will receive a detailed summary of your 2019 Membership and your renewal payment details.';
                break;
        }

        return (
            <div className="member-choice">

                <h2>Back by popular demand!</h2>
                <h2>
                    After fantastic feedback this year we’re again offering you the option <br />
                    to choose the item you want to receive with your 2019 Membership pack(s).
                </h2>
                <h2 className="red">
                    For 2019 you can select a cap, scarf OR beanie <br />
                    to be included with your member pack.
                </h2>
                <p><strong>{record.name_first}</strong>, your account <strong>{record.acct_id}</strong> is eligible to choose <strong className="member-choice__allowed-selections">{allowedSelections}</strong> {plural('item', 'items', allowedSelections)}{allowedSelections > 1 ? ', one choice for each of your member packs' : ''}.</p>
                <p>
                    <strong>Choose your preferred {plural('item', 'items', allowedSelections)} now.</strong><br />
                    {paymentInfo}
                </p>

                <div className="member-choice__inner member-choice__tiles">
                    {this.state.merchItems.map(merchItem => <MerchTile key={merchItem.field} merchItem={merchItem} merchQty={fields[merchItem.field]} allowedSelections={allowedSelections} totalSelections={totalSelections} onUpdate={this._handleMerchSelection.bind(this)} />)}
                </div>

                <div className="member-choice__inner">

                    { allowedSelections > 1 ?
                        <div className="member-choice__selections-remaining">You have <span>{selectionsRemaining}</span> {plural('choice', 'choices', selectionsRemaining)} remaining</div>
                    : false }

                    <div className="member-choice__notes">
                        Please note: If you hold a Legends, Gold or Captain’s Club Membership you will automatically receive a Legends/Gold/Captains Club scarf, however you can select the second item to be included<br /> with your membership pack.<br />
                        <strong>Stock is strictly limited. Members are encouraged to select early to avoid missing out on preferred items.</strong>
                    </div>

                </div>

            </div>
        );

    }

    _renderAddOns() {

        if(this.state.addOns.length > 0) {

            const { fields } = this.state;

            return (
                <div className="add-ons">

                    <h2>
                        WANT TO ADD MORE VALUE TO YOUR MEMBERSHIP? <br />
                        SELECT YOUR MERCHANDISE AND ADD-ONS BELOW
                    </h2>
                    <p>
                        Simply make your selections below and we will update your 2019 Membership invoice &amp; payment(s) prior to you renewing.
                    </p>

                    <div className="add-ons__inner add-ons__tiles">
                        {this.state.addOns.map(addOn => <AddonTile key={addOn.key} addOn={addOn} addonSelections={fields[addOn.field]} addonQty={addOn.options === false ? fields[addOn.field] : fields[addOn.field].length} onUpdate={this._handleAddonSelection.bind(this)} />)}
                    </div>

                </div>
            );

        }

    }

    _handleAddonOptionChange(addonField, addonIndex, value) {
        let fields = Object.assign({}, this.state.fields);
        fields[addonField][addonIndex] = value;
        this.setState(fields: fields);
    }

    _handleDeleteAddon(addOn, addonIndex) {
        let fields = Object.assign({}, this.state.fields);
        this._handleAddonSelection(addOn.field, fields[addOn.field] - 1);
    }

    _handleDeleteAddonOption(addOn, addonOption) {
        let fields = Object.assign({}, this.state.fields);

        if (typeof fields[addOn.field][addonOption] !== 'undefined') {
            fields[addOn.field][addonOption] = fields[addOn.field][addonOption] > 0 ? fields[addOn.field][addonOption] - 1 : 0;
            this.setState(fields: fields);
        }

    }

    _calculateAddonTotal() {
        const { merchItems, addOns, fields } = this.state;
        let addonTotal = 0;

        // merch items
        merchItems.map(merchItem => {
            const merchItemField = fields[merchItem.field];
            addonTotal = addonTotal + (merchItemField * merchItem.price);
        });

        // addons
        addOns.map(addOn => {
            const addonField = fields[addOn.field];
            let addonQty = 0;

            if (addOn.options === false) {
                addonQty = addonField;
            } else {
                addonQty = Object.keys(addonField).reduce((qty, option) => {
                    qty = qty + Number(addonField[option]);
                    return qty;
                }, 0);
            }

            addonTotal = addonTotal + (addonQty * addOn.price);
        });

        return addonTotal;
    }

    _renderLineItems() {
        const { merchItems, addOns, fields } = this.state;

        let lineItems = [];

        merchItems.map(merchItem => {
            const merchItemField = fields[merchItem.field];

            for(let i=0; i < merchItemField; i++) {
                if(merchItem.price === 0) continue;
                lineItems.push(
                    <div className="line-items__item line-items__item--row" key={`${merchItem.key}.${i}`}>
                        <div className="line-items__item__name">{merchItem.name}</div>
                        <div className="line-items__item__options line-items__item__options--no-option"></div>
                        <div className="line-items__item__delete"></div>
                        <div className="line-items__item__price">{`$${merchItem.price}`}</div>
                    </div>
                );
            }

        });

        addOns.map((addOn, addonIndex) => {
            const addonField = fields[addOn.field];

            if(addOn.options === false) {
                if(addonField > 0) {
                    lineItems.push(
                        <div className="line-items__item line-items__item--row" key={`${addOn.key}.${addonIndex}`}>
                            <div className="line-items__item__name"><strong>{addOn.name}</strong></div>
                            <div className="line-items__item__options line-items__item__options--no-option">
                                <strong>{addonField}</strong>
                            </div>
                            <div className="line-items__item__price">
                                <strong>{`$${addonField * addOn.price}`}</strong>
                            </div>
                        </div>
                    );
                }
            } else {
                if (Object.keys(addonField).length > 0) {

                    const addonOptions = Object.keys(addonField).reduce((options, option) => {
                        const optionQty = addonField[option];
                        if (optionQty > 0) {
                            options.push([addOn, option, optionQty]);
                        }
                        return options;
                    }, []);

                    lineItems = addonOptions.reduce((lineItems, [addOn, option, optionQty], optionIndex) => {
                        lineItems.push(
                            <div className="line-items__item line-items__item--row" key={`${addOn.key}.${addonIndex}.${optionIndex}`}>
                                <div className="line-items__item__name">
                                    <span>
                                        <strong>{addOn.name}</strong>
                                            &nbsp;–&nbsp;
                                        {option}
                                    </span>
                                </div>
                                <div className="line-items__item__options line-items__item__options--no-option">
                                    <strong>{optionQty}</strong>
                                </div>
                                <div className="line-items__item__price">
                                    <strong>{`$${optionQty * addOn.price}`}</strong>
                                </div>
                            </div>
                        );
                        return lineItems;
                    }, lineItems);

                }
            }

        });

        if(lineItems.length > 0) {

            lineItems.unshift(
                <div className="line-items__item line-items__item--header" key="lineItemsHeader">
                    <div className="line-items__item__name">Item</div>
                    <div className="line-items__item__options line-items__item__options--no-option"></div>
                    <div className="line-items__item__delete"></div>
                    <div className="line-items__item__price">Amount</div>
                </div>
            );

        }

        return lineItems;
    }

    render() {
        const { record, fields } = this.state;

        let termsText = null;
        switch (record.payment_plan) {
            case 'monthly':
                termsText = <span>I understand that the Adelaide Crows will update my 2019 membership package and payments to reflect my above selections and will adjust the payments of my automatic renewal monthly payment plan, to be deducted from credit card on file. If I have any queries I can contact the Club on < a href = "tel:0884406690" > (08) 8440 6690</a > or via < a href = "mailto:membership@afc.com.au" > membership@afc.com.au</a ></span>;
                break;
            case 'upfront':
                termsText = <span>I understand that the Adelaide Crows will update my 2019 membership package and payments to reflect my above selections and will adjust the payments of my automatic renewal monthly payment plan, to be deducted from credit card on file. If I have any queries I can contact the Club on < a href="tel:0884406690" > (08) 8440 6690</a > or via < a href="mailto:membership@afc.com.au" > membership@afc.com.au</a ></span>;
                break;
            case 'npp':
            default:
                termsText = <span>I understand that the Adelaide Crows will update my 2019 membership package and payments to reflect my above selections and will adjust my invoice for my membership renewal. I understand my payment will need to be made before I receive my pack and other selections. If I have any queries I can contact the Club on < a href="tel:0884406690" > (08) 8440 6690</a > or via < a href="mailto:membership@afc.com.au" > membership@afc.com.au</a ></span>;
                break;
        }

        return (
            <div className="body-wrap">

                <header className="header">
                    <div className="header__bg"></div>
                </header>

                { this.state.submitted === true ? (
                    <div className="main">
                        <div className="thanks">
                            <div className="thanks__inner">
                                <div className="thanks__title">Thank You {record.name_first}!</div>
                                <div className="thanks__sub-title">For making your member's choice selection.</div>
                                <p>You will receive further communication in October regarding the details and pricing of your 2019 Membership. If you have any queries please contact Member Services on <a href="tel:0884406690">(08) 8440 6690</a>, email <a href="mailto:membership@afc.com.au">membership@afc.com.au</a> or visit our <a href="https://www.19thman.com.au/2019-members-choice-0" target="_blank">frequently asked questions page</a>.</p>
                            </div>
                        </div>
                    </div>
                ) : (
                    <form className="main" onSubmit={e => e.preventDefault()}>

                        {this._renderMemberChoice()}
                        {this._renderAddOns()}

                        <div className="line-items">
                            <div className="line-items__inner">
                                {this._renderLineItems()}
                            </div>
                        </div>

                        <div className="total-addons">
                            <div className="total-addons__title">Your total add-on cost for 2019 will be:</div>
                            <div className="total-addons__amount">${this._calculateAddonTotal()}</div>
                        </div>

                        <div className="form__submit">
                            <div className="form__submit__inner">
                                <div className="form__submit__accept">
                                    <Checkbox class="form__submit__accept__checkbox" checked={fields.accept === 'yes'} onClick={e => this._handleToggleCheckbox('accept')} /> {termsText}
                                </div>
                                {record.payment_plan === 'npp' ? (
                                    <div className="form__submit__accept">
                                        <Checkbox class="form__submit__accept__checkbox" checked={fields.optin_monthly === 'yes'} onClick={e => this._handleToggleRadio('optin_monthly')} />
                                        (Optional) I would like to opt in to the <strong>Monthly</strong> Easy-Pay Payment Plan commencing in October, to pay my membership in 10 easy instalments. Please contact me for my credit card details.
                                    </div>
                                ) : null}
                                {record.payment_plan === 'npp' ? (
                                    <div className="form__submit__accept">
                                        <Checkbox class="form__submit__accept__checkbox" checked={fields.optin_upfront === 'yes'} onClick={e => this._handleToggleRadio('optin_upfront')} />
                                            (Optional) I would like to opt in to the <strong>Upfront</strong> Easy-Pay Payment Plan commencing in October. Please contact me for my credit card details.
                                    </div>
                                ) : null}
                                <button className="form__submit__button" onClick={this._handleSubmit} disabled={this.state.loading}>{ this.state.loading ? 'Loading...' : 'Submit' }</button>
                            </div>
                        </div>

                    </form>
                )}

                <footer className="footer">

                    <div className="footer__disclaimer">
                        <div className="footer__inner">
                            <p>
                                <strong>Please note: If you have multiple membership accounts linked to the same email address, you may need to make your selections over the phone with Member Services to ensure all choices are registered.</strong><br />
                                *Full entitlements and terms and conditions available at <a href="http://www.afc.com.au/" target="_blank">19thman.com.au</a>.<br />
                                Membership packs will be sent in November, following Membership payments commencing in October. <em>Merchandise and membership add-ons may be sent separately.</em>
                            </p>
                        </div>
                    </div>

                    <div className="footer__links">
                        <div className="footer__inner">
                            <div className="disclaimer">For any queries please contact Member Services at <a href="mailto:membership@afc.com.au">membership@afc.com.au</a> or <a href="tel:0884406690">(08) 8440 6690</a>.</div>
                            <div className="links">
                                <a href="http://www.afc.com.au/privacy" target="_blank">Privacy Policy</a><span>|</span><br />
                                <a href="https://www.19thman.com.au/terms-conditions-7" target="_blank">Terms &amp; Conditions</a>
                            </div>
                            <br />
                            &copy; Adelaide Football Club 2018
                        </div>
                    </div>

                    <div className="footer__sponsors">
                        <div className="footer__sponsors__lockup">
                            <a href="http://www.afc.com.au/" target="_blank">19thman.com.au</a>
                        </div>
                    </div>

                </footer>

                <Modal visible={this.state.errors.length > 0} button={'Ok'} buttonAction={() => { this.setState({ errors: [] }) }} buttonDisabled={false} ref={(modal) => { this._modal = modal; }}>
                    {this.state.errors.map(error => <p>{error}</p>)}
                </Modal>

            </div>
        );
    }

}

export default App;
