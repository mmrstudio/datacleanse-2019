@extends($campaign->view('main'))

@section('content')

<header class="header">

    @include($campaign->view('common.header'))

    <div class="header__title">
        <div class="header__title__inner header__title__inner--secondary"></div>
    </div>

    <div class="header__player"></div>

</header>

<div class="main">

    <form action="{{ $process_url }}" method="post" class="form">

        @include($campaign->view('common.errors'))

        <div class="form__inner">

            <div class="hospitality-question">

                <p><strong>Please let us know if you're interested in hearing about future corporate hospitality options, including entertainment and match day dining.</strong></p>

                @include($campaign->view('fields.hidden'), [
                    'key' => 'hospitality',
                    'value' => 'Yes',
                ])

                <div class="form__submit">

                    @foreach($hidden_fields as $name=>$value)
                    <input type="hidden" name="{{ $name }}" value="{{ $value }}">
                    @endforeach

                    <button class="form__submit__button" type="submit">Yes, I am interested</button>

                </div>

                <p>Click 'yes' and you’re automatically in the draw to win a <strong>2016 Collingwood team signed guernsey</strong> *</p>

                <p class="small">* Terms and Conditions apply</p>

            </div>

        </div>

    </form>

</div>

<footer class="footer">

    @include($campaign->view('common.footer'))

</footer>

@endsection
