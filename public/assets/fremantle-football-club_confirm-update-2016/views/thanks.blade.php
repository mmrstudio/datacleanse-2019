@extends($campaign->view('main'))

@section('content')

<header class="header header--thanks">

    @include($campaign->view('common.header'))

    <div class="header__thanks-title">
        <div class="header__thanks-title__inner"></div>
    </div>

    <div class="header__players"></div>

</header>

<footer class="footer">

    @include($campaign->view('common.footer'))

</footer>

@endsection
