
    <p>Fremantle Football Club, Fremantle Oval, Parry Street, Fremantle WA 6160</p>

    <ul class="footer__links">
        <li><a href="http://www.fremantlefc.com.au/" target="_blank">Fremantle Football Club</a></li>
        <li><a href="http://www.fremantlefc.com.au/privacy" target="_blank">Privacy Policy</a></li>
        <li><a href="{{ $campaign->asset('Fremantle-Dockers-Terms-and-Conditions.pdf') }}" target="_blank">Terms &amp; Conditions</a></li>
        <li><a href="http://datacleanse.com.au" target="_blank">Product of Datacleanse</a></li>
    </ul>
