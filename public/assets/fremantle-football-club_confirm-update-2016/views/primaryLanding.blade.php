@extends($campaign->view('main'))

@section('content')

<header class="header">

    @include($campaign->view('common.header'))

    <div class="header__landing-title">
        <div class="header__landing-title__inner"></div>
    </div>

    <div class="header__players"></div>

</header>

<div class="main">

    <form action="{{ $process_url }}" method="post" class="form">

        @include($campaign->view('common.errors'))

        <div class="form__inner">

            <div class="form__intro">
                <div class="form__intro__logo">
                    <div class="form__intro__logo__inner"></div>
                </div>
                <div class="form__intro__title">
                    Dear {{ $record->name_first }},<br>
                    Are your details correct?
                </div>
                <div class="form__intro__text">Confirm or update your details now for your chance to win one of the great prizes on offer **. Please ensure your personal details are correct in order to receive all relevant information. Competition closes Thursday 21 July, 2016.</div>
            </div>

            <div class="form__title">
                <h1>Your Details</h1>
            </div>

            @include($campaign->view('fields.text'), [
                'key' => 'acct_id',
                'props' => ['readonly'],
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'name_first',
                'props' => ['readonly'],
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'name',
                'props' => ['readonly'],
            ])

            @include($campaign->view('fields.address'), [
                'keys' => ['street_addr_1', 'street_addr_2'],
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'city',
                'id' => 'suburbField',
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'zip',
                'id' => 'postcodeField',
            ])

            @include($campaign->view('fields.country'), [
                'key' => 'country',
            ])

            @include($campaign->view('fields.state'), [
                'key' => 'state',
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'phone_work',
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'phone_mobile',
            ])

            @include($campaign->view('fields.email'), [
                'key' => 'email_addr',
            ])

            @if($record->birthdate !== 'Supplied')
                @include($campaign->view('fields.dob'), [
                    'key' => 'birthdate',
                    'not_required' => false,
                ])
            @endif

            @if($record->gender !== 'Supplied')
                @include($campaign->view('fields.dropdown'), [
                    'key' => 'gender',
                ])
            @endif

            <div class="form__submit">

                @foreach($hidden_fields as $name=>$value)
                <input type="hidden" name="{{ $name }}" value="{{ $value }}">
                @endforeach

                <button class="form__submit__button" type="submit">Update</button>

            </div>

            <div class="form__sponsors">
                <img src="{{ $campaign->asset('images/forever-freo.png') }}">
                <img src="{{ $campaign->asset('images/sponsors.png') }}">
            </div>

        </div>

    </form>

</div>

<footer class="footer">

    <p>
        <span class="required">*</span> Compulsory fields.<br>
        <span class="required">**</span> Selection of prizes includes a 2016 signed Fremantle Dockers Guernsey, 6 person Freo Fan Box at the Rd 23 v Western Bulldogs match (catering not included), Table for 10 at the Rd 23 Members’ Function, Merchandise valued at $100, and a 2016 Members Cap and Scarf.
    </p>

    @include($campaign->view('common.footer'))

</footer>

@endsection
