import React from 'react';
import { post, get } from '../utils/fetch';
import { track } from '../utils/ga';
import VideoPlayer from './VideoPlayer';
import FieldInput from './FieldInput';
import FieldSelect from './FieldSelect';
import Button from './Button';

class VideoGenerator extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            fields: {
                name: { value: (this.props.name || ''), valid: false, touched: false, filled: false },
                animal: { value: '', valid: false, touched: false, filled: false },
            },
            videoLoading: false,
            videoLoaded: false,
            videoURL: false,
            videoPlaying: false,
            videoId: false
        }

        this._onChange = this._onChange.bind(this);
        this._fetchVideo = this._fetchVideo.bind(this);
        this._onVideoUrlResponse = this._onVideoUrlResponse.bind(this);
        this._onVideoInit = this._onVideoInit.bind(this);
        this._onVideoPlay = this._onVideoPlay.bind(this);
        this._onVideoPause = this._onVideoPause.bind(this);
        this._onVideoEnded = this._onVideoEnded.bind(this);
        this._handlePlayClick = this._handlePlayClick.bind(this);
        this._handleReset = this._handleReset.bind(this);

    }

    _onChange(fieldKey, fieldData) {

        let stateFields = this.state.fields;
            stateFields[fieldKey] = Object.assign(stateFields[fieldKey], fieldData);

        this.setState({
            fields: stateFields
        });

    }

    _handlePlayClick() {

        if(this.state.videoLoaded) {
            this.videoPlayer.play();
        }

    }

    _handleReset() {

        this.setState({
            videoLoaded: false,
            videoURL: false,
            fields: {
                name: { value: '', valid: false, touched: false, filled: false },
                animal: { value: '', valid: false, touched: false, filled: false },
            },
        });

    }

    _fetchVideo() {

        this.setState({ videoLoading: true });

        //const videoUrl = 'https://balancer.creativa.com.au/201709-mmr-victoriazoo/?name_to=' + this.state.fields.name.value + '&name_from=' + this.state.fields.animal.value + '&GUID=' + this.state.fields.name.value + '_' + this.state.fields.animal.value + '&sectionid=sc1,sc2,sc3,sc4&audiosectionid=1';

        //return console.log('videoUrl', videoUrl);

        // TEST
        //return this._onVideoUrlResponse({ status: 'ok', video: { id: 1 }, video_url: 'https://s3-ap-southeast-2.amazonaws.com/mmr-hosting/Preview2.mp4' });

        let postData = {
            to: this.state.fields.name.value,
            from: this.state.fields.animal.value,
        };

        return post('/video', postData).then(response => {

            this.setState({ videoLoading: false });

            if(response !== false) {
                this.setState({ videoURL: response, videoId: response });
                this.props.onVideoLoaded(response);
                //track('event', { category: 'Video', action: 'play', label: 'Non-Member Video', value: response.video.id });
            }

        });

        //json => this._onVideoUrlResponse
    }

    _onVideoUrlResponse(response) {

        this.setState({ videoLoading: false });

        if(response !== false) {
            this.setState({ videoURL: response, videoId: response });
            this.props.onVideoLoaded(response);
            //track('event', { category: 'Video', action: 'play', label: 'Non-Member Video', value: response.video.id });
        } else {
            alert(response.errors);
        }

    }

    _onVideoInit(player) {
        this.videoPlayer = player;
        this.setState({ videoLoaded: true });
    }

    _onVideoPlay() {

        // if(isMobile.any) {
        //     if(this.videoPlayer.webkitRequestFullscreen) this.videoPlayer.webkitRequestFullscreen();
        //     if(this.videoPlayer.mozRequestFullScreen) this.videoPlayer.mozRequestFullScreen();
        //     if(this.videoPlayer.msRequestFullscreen) this.videoPlayer.msRequestFullscreen();
        // }

        this.setState({ videoPlaying: true });
    }

    _onVideoPause() {
        this.setState({ videoPlaying: false });
    }

    _onVideoEnded() {

        // if(isMobile.any) {
        //     if(this.videoPlayer.webkitExitFullScreen) this.videoPlayer.webkitExitFullScreen();
        //     if(this.videoPlayer.mozCancelFullScreen) this.videoPlayer.mozCancelFullScreen();
        //     if(this.videoPlayer.msExitFullscreen) this.videoPlayer.msExitFullscreen();
        // }

        //track('event', { category: 'Video', action: 'ended', label: 'Non-Member Video', value: this.state.videoId });
    }

    render() {

        let allValid = true;
        let allTouched = true;
        // Object.keys(this.state.fields).map((fieldKey) => {
        //     if(this.state.fields[fieldKey].valid === false) allValid = false;
        //     if(this.state.fields[fieldKey].touched === false) allTouched = false;
        //     if(this.state.signup === '0') allValid = false;
        // });

        const animalOptions = [
            { key: 'Thembi', label: 'Thembi' },
            { key: 'Si Si', label: 'Si Si' },
            { key: 'Binjai', label: 'Binjai' },
            { key: 'Yuska', label: 'Yuska' },
            { key: 'Mali', label: 'Mali' },
        ]

        return (
            <div className="video-generator" data-loading={this.state.videoLoading} data-video-loaded={this.state.videoLoaded} data-playing={this.state.videoPlaying}>

                <div className="video-generator__main">

                    <div className="video-generator__main__inner">
                        <div className="video-generator__loading" data-visible={this.state.videoLoading}><div className="loading__spinner"><div className="spinner"><div className="bounce1"></div><div className="bounce2"></div><div className="bounce3"></div></div></div></div>

                        <div className="video-generator__form" data-visible={this.state.videoLoading === false && this.state.videoLoaded === false}>
                            <div className="video-generator__form__inner">
                                <div className="video-generator__form__title">Enter your first name and press play</div>
                                <div className="video-generator__form__fields">
                                    <div className="video-generator__form__field">
                                        <FieldInput type="text" name="name" label="Your First Name" field={this.state.fields.name} required={true} onChange={this._onChange} onBlur={this._onChange} />
                                    </div>
                                    <div className="video-generator__form__field">
                                        <FieldSelect name="animal" label="Select An Animal" placeholder="Select..." field={this.state.fields.animal} options={animalOptions} required={true} onChange={this._onChange} onBlur={this._onChange} />
                                    </div>
                                </div>
                                <div className="video-generator__form__submit">
                                    <Button class={['button--green', 'button--medium', 'button--arrow']} disabled={!allValid} onClick={this._fetchVideo}>Play</Button>
                                    {/*<div className="form__submit__validation" data-visible={allTouched && allValid === false}>Please enter your name and a valid email address and accept our terms to continue...</div>*/}
                                </div>
                            </div>
                        </div>

                        <div className="video-generator__play-again" data-visible={this.state.videoLoading === false && this.state.videoLoaded === true && this.state.videoPlaying === false}>
                            <div className="video-generator__play-again__inner">
                                <Button class={['button--green', 'button--medium', 'button--arrow', 'button--play-again']} disabled={!allValid} onClick={this._handlePlayClick}>Replay</Button>
                                <Button class={['button--green', 'button--medium', 'button--arrow', 'button--change-name']} disabled={!allValid} onClick={this._handleReset}>Change Name</Button>
                                <Button class={['button--green', 'button--medium', 'button--arrow', 'button--change-name']} disabled={!allValid} onClick={this._handleReset}>Download</Button>
                            </div>
                        </div>

                        {this.state.videoURL !== false ? <VideoPlayer src={this.state.videoURL} type="video/mp4" autoplay={true} onInit={this._onVideoInit} onPlay={this._onVideoPlay} onPause={this._onVideoPause} onEnd={this._onVideoEnded} /> : false}

                    </div>

                </div>

                <div className="video-generator__extra">
                    <div className="video-generator__extra__share">
                        <span>Share</span>
                        <a href="#" className="video-generator__extra__share__button facebook">Facebook</a>
                        <a href="#" className="video-generator__extra__share__button twitter">Twitter</a>
                        <button onClick={this.props.openSend} className="video-generator__extra__share__button email">Email</button>
                    </div>
                    {this.state.videoURL !== false ? (
                        <div className="video-generator__extra__download">
                            <span>Download</span>
                            <a href={this.state.videoURL} target="_blank" className="video-generator__extra__download__button">Download</a>
                        </div>
                    ) : false}
                </div>

            </div>
        )
    }

}

export default VideoGenerator;
