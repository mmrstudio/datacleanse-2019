import React from 'react';

class VideoPlayer extends React.Component {

    constructor(props){
        super(props);

        this.player = false;

        this._onVideoClick = this._onVideoClick.bind(this);
    }

    _onVideoClick(e) {

        if(this.player.paused) {
            this.player.play();
        } else {
            this.player.pause();
        }

    }

    componentDidMount() {

        this.player = this.refs.video;

        if(this.props.onInit) this.props.onInit(this.player);

        if(this.props.onPlay) this.player.addEventListener('play', this.props.onPlay);
        if(this.props.onPause) this.player.addEventListener('pause', this.props.onPause);
        if(this.props.onEnd) this.player.addEventListener('ended', this.props.onEnd);
    }

    render() {

        let videoAttrs = Object.assign({
            autoPlay: this.props.autoplay,
            controls: false
        }, this.props.videoAttrs);

        return (
            <video {... videoAttrs} ref="video" onClick={this._onVideoClick} controls>
              <source src={this.props.src} type={this.props.type} />
            </video>
        )
    }

}

export default VideoPlayer;
