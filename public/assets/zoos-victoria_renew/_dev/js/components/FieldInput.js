import React from 'react';

class FieldInput extends React.Component {

    constructor(props){
        super(props);

        this._onChange = this._onChange.bind(this);
        this._onBlur = this._onBlur.bind(this);
        this._validate = this._validate.bind(this);
    }

    _onChange(event) {

        let fieldData = {
            value: event.target.value,
            valid: this._validate(event.target.value),
            filled: event.target.value.length > 0
        };

        this.props.onChange(this.props.name, fieldData);
    }

    _onBlur(event) {

        let fieldData = {
            touched: true
        };

        if(this.props.onBlur !== undefined) this.props.onBlur(this.props.name, fieldData);
    }

    _validate(value) {

        let fieldValid = false;

        switch(this.props.type) {

            case 'text' : case 'textarea' : default :
                fieldValid = value.trim().length > 0;
            break;

            case 'email' :
                let emailRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;
                fieldValid = emailRegex.exec(value) !== null;
            break;

            case 'tel' :
                let phoneRegex = { standard: /^\D*([0][23478]|[1][389])(\D*\d){8}\D*$/, international: /^\D*(?:[+]?[6][1])\D*[23478](\D*\d){8}\D*$/, short: /^\D*([1][3])(\D*\d){4}\D*$/ };
                fieldValid = (phoneRegex.standard.exec(value) !== null || phoneRegex.international.exec(value) !== null || phoneRegex.short.exec(value) !== null);
            break;

        }

        // return true if not required (and not filled out)
        if(fieldValid === false && this.props.required !== true && value.trim().length === 0) {
            fieldValid = true;
        }

        return fieldValid;

    }

    componentDidMount() {

        if(this.refs.field !== undefined) {

            let fieldData = {
                value: this.refs.field.value,
                valid: this._validate(this.refs.field.value),
                filled: this.refs.field.value.length > 0
            };

            this.props.onChange(this.props.name, fieldData);

        }

    }

    render() {

        let fieldGroupAttrs = {
            'data-validate': this.props.validate !== undefined ? this.props.validate : true,
            'data-touched': this.props.field.touched,
            'data-filled': this.props.field.filled,
            'data-valid': this.props.field.valid,
            'data-required': this.props.required
        };

        let fieldAttrs = {};
        if(this.props.type === 'number') {
            fieldAttrs['pattern'] = '\d*';
        }

        return (
            <div className="form__group form__group--input" {...fieldGroupAttrs}>
                <label htmlFor="">{this.props.label}</label>
                { this.props.type === 'textarea' ? (
                    <textarea id={this.props.name} name={this.props.name} className="form__input" value={this.props.field.value} placeholder={this.props.placeholder} onChange={this._onChange} onBlur={this._onBlur} ref="field" {...fieldAttrs}></textarea>
                ) : (
                    <input type={this.props.type} id={this.props.name} name={this.props.name} className="form__input" value={this.props.field.value} placeholder={this.props.placeholder} onChange={this._onChange} onBlur={this._onBlur} ref="field" {...fieldAttrs} />
                )}
            </div>
        )
    }

}

export default FieldInput;
