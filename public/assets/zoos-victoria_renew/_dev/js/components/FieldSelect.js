import React from 'react';

class FieldSelect extends React.Component {

    constructor(props){
        super(props);

        this._onChange = this._onChange.bind(this);
        this._onBlur = this._onBlur.bind(this);
    }

    _onChange(event) {

        let optionValue = event.target.value;

        let fieldData = {
            value: optionValue,
            label: optionValue,
            filled: optionValue.length > 0 ? true : false,
            valid: optionValue.length > 0 ? true : false,
            touched: true
        };

        this.props.onChange(this.props.name, fieldData, optionValue[1]);
    }

    _onBlur(event) {
        this.props.onBlur(this.props.name, { touched: true });
    }

    render() {

        let fieldGroupAttrs = {
            'data-touched': this.props.field.touched,
            'data-filled': this.props.field.filled,
            'data-valid': this.props.field.valid
        };

        let fieldAttrs = {};

        let fieldOptions = false;
        if(this.props.options !== false) {

            fieldOptions = [];

            this.props.options.map((option, i) => {
                fieldOptions.push(<option key={i} value={option.key}>{option.label}</option>);
            });

        }

        if(fieldOptions === false || fieldOptions.length === 0) {
            fieldGroupAttrs['data-disabled'] = true;
            fieldAttrs['disabled'] = 'disabled';
        } else {
            fieldGroupAttrs['data-disabled'] = false;
            fieldAttrs = {};
        }

        let loadingLabel = this.props.loading === true ? ' (loading)' : '';

        return (
            <div className="form__group form__group--select" {...fieldGroupAttrs}>
                { this.props.label ? (<label>{ this.props.label }</label>) : false }
                <select className="form__select" id={this.props.name} value={this.props.field.value} onChange={this._onChange} onBlur={this._onBlur} {...fieldAttrs}>
                    <option value="">{this.props.placeholder}{loadingLabel}</option>
                    {fieldOptions}
                </select>
            </div>
        )
    }

}

export default FieldSelect;
