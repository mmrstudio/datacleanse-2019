import React from 'react';
import Modal from './Modal';
import Button from './Button';
import { post } from '../utils/fetch';

class SendModal extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            sent: true,
            sending: false,
        };

        this._renderFriends = this._renderFriends.bind(this);
    }

    _updateMe(field, value) {
        const { data, update } = this.props;

        let newData = Object.assign({}, data);
        newData.me[field] = value;

        return update(newData);
    }

    _updateFriend(index, field, value) {
        const { data, update } = this.props;

        let newData = Object.assign({}, data);
        newData.friends[index][field] = value;

        return update(newData);
    }

    _updateComments(value) {
        const { data, update } = this.props;

        let newData = Object.assign({}, data);
        newData.comments = value;

        return update(newData);
    }

    _addFriend() {
        const { data, update } = this.props;
        let newData = Object.assign({}, data);
        newData.friends.push({ name: '', email: '' });
        return update(newData);
    }

    _deleteFriend(index) {
        const { data, update } = this.props;
        let newData = Object.assign({}, data);
        newData.friends.splice(index, 1);
        return update(newData);
    }

    _renderFriends() {
        const { data } = this.props;

        return data.friends.map((friend, i) => (
            <div key={`friend_${i}`} className="send-modal__details-row">
                <label htmlFor={`friendName_${i}`}>{ i === 0 ? 'Your friend details' : '' }</label>
                <div className="send-modal__details-row__fields">
                    <input id={`friendName_${i}`} type="text" placeholder="Name" value={friend.name} onChange={e => this._updateFriend(i, 'name', e.target.value)} />
                    <input id={`friendEmail_${i}`} type="email" placeholder="Email" value={friend.email} onChange={e => this._updateFriend(i, 'email', e.target.value)} />
                </div>
                { data.friends.length > 1 ? <Button class={['send-modal__details-row__delete']} tabIndex="-1" onClick={event => this._deleteFriend(i)}>Delete</Button> : false }
            </div>
        ));

    }

    _submit() {
        const { data } = this.props;

        this.setState({ sending: true });

        post(window.sendFriendsUrl, data).then(response => {

            if(response === true) {
                this.setState({ sent: true });
            }

            this.setState({ sending: false });

        });

    }

    componentWillReceiveProps(nextProps) {

        if(this.state.sent === true && this.props.visible === false && nextProps.visible === true) {

            // reset modal
            this.props.reset();
            this.setState({ sent: false });
        }

    }

    _renderContents() {
        const { data, close, visible } = this.props;

        if(this.state.sent === false) {

            return (
                <div className="send-modal__inner">
                    <div className="send-modal__header">
                        <h2>Send your friend a letter from the animals too!</h2>
                        <p>Simply fill out the form below and they will receive an email with a link to watch their personalised video.</p>
                        <button className="send-modal__header__close" onClick={close}>Close</button>
                    </div>
                    <div className="send-modal__form">
                        <div className="send-modal__details-row">
                            <label htmlFor="meName">Your details</label>
                            <div className="send-modal__details-row__fields">
                                <input type="text" id="meName" placeholder="Name" value={data.me.name} onChange={e => this._updateMe('name', e.target.value)} />
                                <input type="email" id="meEmail" placeholder="Email" value={data.me.email} onChange={e => this._updateMe('email', e.target.value)} />
                            </div>
                        </div>
                        {this._renderFriends()}
                        <div className="send-modal__form__add">
                            <Button class={['send-modal__form__add__button']} onClick={this._addFriend.bind(this)}>Add more</Button>
                        </div>
                        <div className="send-modal__submit">
                            <Button class={['button--green', 'button--medium', 'button--arrow']} onClick={this._submit.bind(this)} disabled={this.state.sending}>{this.state.sending ? 'Sending...' : 'Send Invite'}</Button>
                        </div>
                    </div>
                </div>
            );

        } else {

            return (
                <div className="send-modal__inner">
                    <div className="send-modal__thanks">
                        <h2>Your email has been sent!</h2>
                        <p>Click on the button below to find out what upcoming events are on that you might also be interested in doing.</p>
                        <div className="send-modal__thanks__button">
                            <Button element="a" href={'https://www.zoo.org.au/whats-on'} target="_blank" class={['button--green', 'button--medium', 'button--arrow']}>What&rsquo;s On</Button>
                        </div>
                    </div>
                </div>
            );

        }

    }

    render() {
        const { data, close, visible } = this.props;

        return (
            <Modal class={['send-modal']} visible={visible} close={close}>
                {this._renderContents()}
            </Modal>
        )
    }

}

export default SendModal;
