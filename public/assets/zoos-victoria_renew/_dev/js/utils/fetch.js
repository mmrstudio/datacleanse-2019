import 'whatwg-fetch';
import { getCookie } from './helpers';

export function post(url, postData={}) {

    postData._token = window._token;
    postData._fetch = true;

    // configure fetch for POST
    let fetchConfig = {
        credentials: 'same-origin',
        //mode: 'no-cors',
        method: 'POST',
        headers: {
            "Content-type": "application/json; charset=UTF-8",
            "Accept": "application/json",
            "X-CSRF-TOKEN": window._token,
        },
        //body: Object.keys(postData).map((key) => { return key + '=' + postData[key]; }).join('&')
        body: JSON.stringify(postData),
    };

    //console.log('postData', postData);
    //console.log('fetchConfig', fetchConfig);

    // do fetch
    return fetch(url, fetchConfig)
        .then((response) => { return response.json(); })
        //.then(callback)
        .catch((error) => {
            //console.log('There has been a problem with your fetch operation: ' + error.message);
        });

}

export function get(url, callback) {

    console.log('fetchGET', url);

    // do fetch
    fetch(url)
        .then((response) => { return response.json(); })
        .then(callback)
        .catch((error) => {
            //console.log('There has been a problem with your fetch operation: ' + error.message);
        });

}

export function fetchUpload(url, file, callback) {

    let postData = new FormData();
    postData.append('file', file);
    postData.append('_token', mvb.token);

    // configure fetch for POST
    let fetchConfig = {
        method: 'POST',
        body: postData
    };

    //console.log('fetchConfig', fetchConfig);

    // do fetch
    fetch(url, fetchConfig)
        .then((response) => { return response.json(); })
        .then(callback);

}
