import { post } from './fetch';
import 'whatwg-fetch';

require('es6-promise').polyfill();
require('es6-object-assign').polyfill();

export function track(type, params) {

    let postData = {};
    let utm_source = '';
    let utm_content = '';
    let utm_medium = '';
    let member_id = '';

    if(typeof mvb != 'undefined') {
        utm_source = mvb.utmTracking.utm_source !== false ? mvb.utmTracking.utm_source : '';
        utm_content = mvb.utmTracking.utm_content !== false ? mvb.utmTracking.utm_content : '';
        utm_medium = mvb.utmTracking.utm_medium !== false ? mvb.utmTracking.utm_medium : '';
        member_id = mvb.member !== false ? mvb.member.member_number : '';
    }

    switch(type) {

        case 'pageview' :

            postData = {
                utm_source: utm_source,
                utm_content: utm_content,
                utm_medium: utm_medium,
                member_id: member_id,
                type: 'pageview',
                location_pathname: window.location.pathname,
                location_search: window.location.search
            };

            fetchPOST('/ga-track', postData);
            ga('send', 'pageview');

        break;

        case 'event' :

            postData = {
                utm_source: utm_source,
                utm_content: utm_content,
                utm_medium: utm_medium,
                member_id: member_id,
                type: 'event',
                category: params.category !== undefined ? params.category : '',
                action: params.action !== undefined ? params.action : '',
                label: params.label !== undefined ? params.label : '',
                value: params.value !== undefined ? params.value : '',
                location_pathname: window.location.pathname,
                location_search: window.location.search
            };

            fetchPOST('/ga-track', postData);
            ga('send', 'event', postData.category, postData.action, postData.label);

        break;

    }

}
