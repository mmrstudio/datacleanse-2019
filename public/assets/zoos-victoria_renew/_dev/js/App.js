import React from 'react';
import VideoGenerator from './components/VideoGenerator';
import Modal from './components/Modal';
import SendModal from './components/SendModal';
import Button from './components/Button';
import { plural } from './utils/helpers';
import { post } from './utils/fetch';

require('es6-promise').polyfill();
require('es6-object-assign').polyfill();

class App extends React.Component {

    constructor(props){
        super(props);

        let initialState = {
            campaign: window.appData.campaign,
            record: window.appData.record,
            fields: {},
            hiddenFields: window.appData.fields,
            processUrl: window.appData.processUrl,
            totalSelections: 0,
            loading: false,
            submitted: false,
            errors: [],
            fetchVideo: false,
            videoDownloadURL: false,
            videoId: false,
            shareUrl: false,
            animalVisible: false,
            currentAnimal: false,
            sendVisible: false,
            sendData: {
                me: { name: '', email: '' },
                friends: [{ name: '', email: '' }],
                comments: '',
            }
        }

        this._animals = {
            giraffe: {
                name: 'Thembi',
                born: 'Born 2007',
                bio: 'Thembi is very motivated by food and is the ‘greedy guts’ of the group. He eats everything in sight including sunglasses etc. if the keepers are not careful! He is the tallest (only just!) and is the leader of the herd.',
                facts: [
                    'The giraffe is the tallest of all mammals, males reaching a height of about 5.5 metres and females about 5 metres',
                    'To circulate blood all through this tall body, the giraffe has the biggest heart of any animal, weighing about 12kg!',
                    'A giraffe’s tongue can reach up to 45cm long and 8cm wide! The tongue’s dark colour prevents blistering in the sun',
                    'Always alert for danger, giraffes sleep for approximately two hours a day, and catch quick 5–6 minute naps for the rest of the time',
                    'Close to 2 metres long, the giraffe’s neck has seven vertebrae like other mammals, just longer. The long neck means great changes in blood pressure as a giraffe lowers or raises its head, so special blood vessels and valves compensate. Without this adaptation, giraffes would faint when blood pressure increased',
                ],
                btsHeader: 'Giraffe Feed Encounter',
                btsDescription: 'You will have the opportunity to get up close and personal with the astonishing and fascinating giraffe. Participants will help feed the Zoo’s herd of giraffe and learn about their curious habits and daily care. A Giraffe Feed Encounter at Werribee Open Range Zoo is the perfect gift and an unbeatable adventure with friends or colleagues.',
                btsLink: 'https://tickets.zoo.org.au/event/giraffe-feed-encounter',
            },
            rhino: {
                name: 'Si Si',
                born: 'Born 1998',
                bio: 'SiSi is the dominant female of the group and keeps everyone in line – be they Rhino, Eland, Ostrich or Zebra!',
                facts: [
                    'A rhinoceros has three toes on each foot',
                    'Some animals such as the eland and kudu will roll in rhinoceros poo to disguise their scent. This reduces their chances of being attacked by predators such as lions',
                    'The collective noun for a group of rhinos is a "crash" of rhinos',
                    'The rhinoceros’ ears can move independently allowing it to hear in different directions at the same time',
                    'Rhinoceros horn is made of keratin, the same substance of human hair and nails',
                    'A pile of rhinoceros poo is called a ‘midden’. Rhinos use middens to help them navigate in their territory',
                ],
                btsHeader: 'Slumber Safari',
                btsDescription: 'Enjoy an overnight experience at the Zoo in our luxury safari camp. Your stay includes amazing close-up animal encounters, drinks and dips at sunset, a sumptuous dinner, unique night-time activities and breakfast. An ideal getaway for couples, groups, families or colleagues.',
                btsLink: 'https://www.zoo.org.au/werribee/wild-encounters/slumber-safari',
            },
            tiger: {
                name: 'Binjai',
                born: 'Female, born 2002',
                bio: 'Binjai is super smart and really enjoys interactions with her keepers. She is highly observant and really quick to learn new things.',
                facts: [
                    'Tigers are proficient swimmers and can cross rivers that are 7–8 km wide without any difficulty',
                    'Tigers will attack their prey from the rear; only 1 in 10–20 attempts succeed in a kill',
                    'To seek out prey and defend a home range it is not uncommon for a tiger to travel 10–20 km a day',
                ],
                btsHeader: 'Tiger Behind The Scenes',
                btsDescription: 'In this exclusive experience you will have the opportunity to meet these incredible yet critically endangered animals. With the tigers safely back of house you will head into the exhibit with our keeper to set up enrichment activities. Then head to a VIP viewing area to see our tiger up close and engaging with the scents and treats you left for their entertainment. ',
                btsLink: 'https://tickets.zoo.org.au/event/tiger-behind-the-scenes',
            },
            gorilla: {
                name: 'Yuska',
                born: 'Female, born 1971',
                bio: '',
                facts: [
                    'All are critically endangered and are in need of further conservation action.',
                    'When excited and pleased, gorillas giggle in almost the same way that humans do',
                    'Young gorillas are very playful, tickling and chasing each other. At the zoo they play with special ‘gorilla proof’ toys',
                    'Baby gorillas are quite similar to human babies and have the same needs, such as physical interaction, security and lots of opportunities to play',
                    'When gorillas are upset or threatened they show it by coughing',
                    'Melbourne Zoo’s gorillas are usually asleep by 7.00pm and wake at about 7.00am the next morning',
                ],
                btsHeader: 'Gorilla Behind The Scenes',
                btsDescription: 'Go behind-the-scenes with our keepers to meet the largest of all the primates and learn what it means to be part of the troop!',
                btsLink: 'https://www.zoo.org.au/melbourne/wild-encounters/gorilla-behind-the-scenes',
            },
            elephant: {
                name: 'Mali',
                born: 'Born 16 January 2010',
                bio: 'The first offspring for her mother Dokkoon and father Bong Su. Mali means ‘jasmine’ in Thai.',
                facts: [
                    'The female Asian Elephant reaches sexual maturity at 10 years of age',
                    'Elephant tusks are a modified form of upper incisors: the front teeth that many animals use for cutting food',
                    'The ears of the Asian Elephant assist in cooling the animal. Heat is circulated to blood vessels located close to the skin of the ears and is diffused into the air',
                    'The trunk of the Asian Elephant is used for feeding, watering, smelling, touching, communicating, lifting, dusting and fighting',
                    'The trunk of an Asian Elephant can hold up to 8.5L of water',
                    'In the wild the Asian Elephant eats leaves, flowers, fruits, shrubs, grasses and roots. An adult elephant may eat up to 170kg of food, drink 90L of water, and produce up to 75kg of faeces per day.',
                ],
                btsHeader: 'Guided Tours',
                btsDescription: 'FREE guided tours of Trail of the Elephants are run by our talented Volunteer Guides. Enquire at the Memberships, Sales and Encounters Office at the Main Entrance, or the Information Office at the Railgate Entrance. Our award-winning trail of the elephants invites visitors into an Asian village and garden setting which is home to our Asian Elephants. You’ll be overwhelmed with a sense of theatre, discovery and wonder, while learning about the survival of the endangered elephant in its natural habitat. Set in lush, spacious surrounds, Trail of Elephants provides a comfortable and stimulating environment for our majestic elephants, Bong Su, Mek Kapah, Dokkoon, Num-Oi, Kulab, Mali, Ongard and Man Jai.',
                btsLink: 'https://www.zoo.org.au/melbourne/animals/asian-elephant',
            },
        };

        console.log('initialState', initialState);

        this.state = initialState;

    }

    _handleSubmit() {
        const { record, fields, hiddenFields, allowedSelections, totalSelections, processUrl } = this.state;

        if(totalSelections < allowedSelections) {
            this.setState({ errors: [`Please make all your merchandise selections - you have ${allowedSelections - totalSelections} remaining.`] })
            return;
            //return alert(`Please make all your merchandise selections - you have ${allowedSelections - totalSelections} remaining.`);
        }

        if(fields.accept === 'no') {
            this.setState({ errors: [`Please agree to the membership terms & conditions.`] })
            return;
            //return alert('Please agree to the membership terms & conditions');
        }

        this.setState({
            submitted: false,
            loading: true,
        });


        // send data
        fetchPOST(
            processUrl,
            Object.assign(fields, hiddenFields),
            this._onSubmitResponse
        );

    }

    _onSubmitResponse(response) {

        if(response === true) {

            this.setState({
                submitted: true,
                loading: false,
            });

            scroll(0,0);

        } else {

            let errors = [];

            Object.keys(response).map((key) => {
                let value = response[key];
                value.map((error) => {
                    errors.push(error);
                });
            });

            this.setState({ errors: errors });

            //alert(errors.join("\r\n"));

            this.setState({
                loading: false
            });

        }

    }

    _showAnimal(animal) {
        this.setState({ currentAnimal: animal, animalVisible: true });
    }

    _hideAnimal() {
        this.setState({ animalVisible: false });
    }

    _renderAnimalModal() {

        const { currentAnimal } = this.state;

        if(currentAnimal !== false) {

            return (
                <div className="animal-modal__inner">
                    <div className="animal-modal__header">
                        Did {this._animals[currentAnimal].name} deliver your letter?
                        <button className="animal-modal__header__close" onClick={this._hideAnimal.bind(this)}>Close</button>
                    </div>
                    <div className={`animal-modal__image ${currentAnimal}`}></div>
                    <div className="animal-modal__main">
                        <div className="animal-modal__name">{this._animals[currentAnimal].name}</div>
                        <div className="animal-modal__born">{this._animals[currentAnimal].born}</div>
                        <div className="animal-modal__bio">{this._animals[currentAnimal].bio}</div>
                        <div className="animal-modal__did-you-know__title">Did you know?</div>
                        <ul className="animal-modal__did-you-know">
                            {this._animals[currentAnimal].facts.map((fact, i) => <li key={`fact${i}`}>{fact}</li>)}
                        </ul>
                        <div className="animal-modal__behind-the-scenes">
                            <div className="animal-modal__behind-the-scenes__header">{this._animals[currentAnimal].btsHeader}</div>
                            <div className="animal-modal__behind-the-scenes__inner">
                                <div className="animal-modal__behind-the-scenes__description">{this._animals[currentAnimal].btsDescription}</div>
                                <div className="animal-modal__behind-the-scenes__book">
                                    <Button element="a" href={this._animals[currentAnimal].btsLink} target="_blank" class={['button--large', 'button--green', 'button--large-arrow', 'button--book']}>Book Now</Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );

        }

    }

    _openSend() {
        this.setState({ sendVisible: true });
    }

    _closeSend() {
        this.setState({ sendVisible: false });
    }

    _resetSend() {
        let sendData = Object.assign({}, this.state.sendData);

        sendData.friends = [{ name: '', email: '' }];
        sendData.comments = '';

        this.setState({
            sendData: sendData,
        });
    }

    _updateSendData(data) {
        this.setState({ sendData: data });
    }

    _onVideoLoaded(response) {

        return;

        let videoDownloadURL = response.video_data.download_url;
        if(isMobile.any) videoDownloadURL = videoDownloadURL + '/mobile';

        this.setState({
            videoDownloadURL: videoDownloadURL,
            videoId: response.video.id,
            shareUrl: response.share_url
        });
    }

    render() {

        const { record, fields, currentAnimal, animalVisible, sendVisible } = this.state;

        let errors = [];

        return (
            <div className="body-wrap">

                <header className="header">

                    <div className="header__inner">

                        <div className="header__banner">
                            <h1>
                                <span>There&rsquo;s a letter for you,</span><br />
                                from the animals at the zoo!
                            </h1>
                        </div>

                    </div>

                </header>

                <div className="main">
                    <div className="main__inner">
                        <VideoGenerator onVideoLoaded={this._onVideoLoaded.bind(this)} name={window.appData.name} ref="video" openSend={this._openSend.bind(this)} />

                        <div className="intro">
                            <div className="intro__inner">
                                <h2>It&rsquo;s time to renew for the zoo!</h2>
                                <h3>Thanks for your support as a member of Zoos Victoria.</h3>
                                <p>
                                    We hope you have enjoyed the past 12 months and made use of all the member benefits.<br />
                                    Being a Zoos Victoria member provides you unlimited access to three great zoos and helps to fight animal extinction.
                                </p>
                                <div className="intro__price">Only $9 a month</div>
                                <h4>Your member benefits include:</h4>
                                <ul className="intro__benefits">
                                    <li>Your membership helps save wildlife</li>
                                    <li>Unlimited entry to Melbourne Zoo, Healesville Sanctuary and Werribee Open Range Zoo</li>
                                    <li>Express entry 365 days a year</li>
                                    <li>Member events and exhibit previews</li>
                                    <li>Quarterly Zoo News magazine</li>
                                    <li>Discount on Zoo Twilights tickets, animal experiences and at zoo shops</li>
                                    <li>Free reciprocal entry to select interstate zoos*</li>
                                </ul>
                                <div className="intro__button">
                                    {/*<Button class={['button--large', 'button--green', 'button--large-arrow']}>Renew</Button>*/}
                                    <Button element="a" href="https://members.zoo.org.au/#dashboard" class={['button--large', 'button--green', 'button--large-arrow']}>Renew</Button>
                                    <Button element="a" href="https://www.zoo.org.au/members" class={['button--large', 'button--green', 'button--large-arrow']}>Join Now</Button>
                                </div>
                                <div className="zoos-logo">
                                    <a href="https://www.zoo.org.au/?utm_source=renewforthezoo" target="_blank" className="zoos-logo__inner">
                                        Zoos Victoria
                                    </a>
                                </div>

                            </div>
                        </div>

                        <div className="interact">

                            <div className="interact__send">
                                <h2>Send a letter to your friend too!</h2>
                                <p>Send your friend a special letter from the animals too. Whether it’s a wild encounter, enjoying a picnic or playing all day at ‘Keeper Kids’, there’s something for everyone to enjoy. </p>
                                <div className="interact__send__button">
                                    <Button class={['button--medium', 'button--green', 'button--arrow', 'button--send']} onClick={this._openSend.bind(this)}>Send Your Friend a Letter</Button>
                                </div>
                            </div>

                            <div className="interact__animals">
                                <h2>Who delivered your letter?</h2>
                                <p>Find out more about the animal that delivered your letter by clicking on the images below.</p>
                                <div className="interact__animals__tiles">
                                    <div className="interact__animals__tile" onClick={e => this._showAnimal('giraffe')}>
                                        <div className="interact__animals__tile__inner">
                                            <div className="interact__animals__tile__image giraffe"></div>
                                            <div className="interact__animals__tile__overlay">
                                                <h3>Thembi</h3>
                                                <p>Werribee Open Range Zoo</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="interact__animals__tile" onClick={e => this._showAnimal('rhino')}>
                                        <div className="interact__animals__tile__inner">
                                            <div className="interact__animals__tile__image rhino"></div>
                                            <div className="interact__animals__tile__overlay">
                                                <h3>Si Si</h3>
                                                <p>Werribee Open Range Zoo</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="interact__animals__tile" onClick={e => this._showAnimal('tiger')}>
                                        <div className="interact__animals__tile__inner">
                                            <div className="interact__animals__tile__image tiger"></div>
                                            <div className="interact__animals__tile__overlay">
                                                <h3>Binjai</h3>
                                                <p>Melbourne Zoo</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="interact__animals__tile" onClick={e => this._showAnimal('gorilla')}>
                                        <div className="interact__animals__tile__inner">
                                            <div className="interact__animals__tile__image gorilla"></div>
                                            <div className="interact__animals__tile__overlay">
                                                <h3>Yuska</h3>
                                                <p>Melbourne Zoo</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="interact__animals__tile" onClick={e => this._showAnimal('elephant')}>
                                        <div className="interact__animals__tile__inner">
                                            <div className="interact__animals__tile__image elephant"></div>
                                            <div className="interact__animals__tile__overlay">
                                                <h3>Mali</h3>
                                                <p>Melbourne Zoo</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <footer className="footer">
                    <div className="footer__main">
                        <div className="footer__inner">
                            <h2>Need Help? Contact the Membership Team:</h2>
                            <ul className="footer__contacts">
                                <li className="footer__contacts__contact phone">
                                    <a href="tel:0393402780">03 9340 2780</a>
                                    <span>(9am-5pm daily)</span>
                                </li>
                                <li className="footer__contacts__contact email">
                                    <a href="mailto:members@zoo.org.au">members@zoo.org.au</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="footer__bottom">
                        <div className="footer__inner">
                            <ul className="footer__links">
                                <li>&copy; <a href="https://www.zoo.org.au/?utm_source=renewforthezoo" target="_blank">Zoos Victoria</a></li>
                                <li><a href="https://www.zoo.org.au/healesville/highlights/australian-wildlife-health-centre/emergency-help-for-wildlife?utm_source=renewforthezoo" target="_blank">Emergency wildlife help</a></li>
                                <li><a href="https://www.zoo.org.au/zoos-victoria/governance/accessibility?utm_source=renewforthezoo" target="_blank">Accessibility</a></li>
                                <li><a href="https://www.zoo.org.au/contact-us?utm_source=renewforthezoo" target="_blank">Contact us</a></li>
                                <li><a href="https://www.zoo.org.au/about-us/governance/zoos-victoria-privacy-policy?utm_source=renewforthezoo" target="_blank">Privacy</a></li>
                                <li><a href="http://mmr.com.au/?utm_medium=clientcampaign&utm_source=zoos&utm_campaign=renewforthezoo" target="_blank">Site by MMR</a></li>
                            </ul>
                        </div>
                    </div>
                </footer>

                <Modal class={['animal-modal']} visible={animalVisible} close={this._hideAnimal.bind(this)}>
                    {this._renderAnimalModal()}
                </Modal>

                <SendModal data={this.state.sendData} visible={sendVisible} close={this._closeSend.bind(this)} update={this._updateSendData.bind(this)} reset={this._resetSend.bind(this)} />

            </div>
        );
    }

}

export default App;
