<?php

    function sendToFriends() {

        $request = request();

        $me = $request->get('me', false);
        $friends = $request->get('friends', false);
        $comments = $request->get('comments', false);

        //print_r($request->all()); exit;

        foreach($friends as $friend) {

            \Mail::send('emails.zoos-friend', ['name' => $friend['name'], 'comments' => $comments, 'sender' => $me['name'], 'sender_email' => $me['email']], function ($message) use ($me, $friend) {
                $message->from($me['email'], $me['name']);
                $message->to($friend['email'], 'Nick Morton')->subject('There\'s a letter for you from the animals at the zoo');
            });

        }

        return true;

    }

    function renderVideo() {

        $request = request();

        $to = $request->get('to', false);
        $from = $request->get('from', false);
        $videoName = 'To' . trim(ucfirst(strtolower($to))) . 'From' . trim(ucfirst(strtolower($from)));

        $videoUrl = false;
        $videoRequestUrl = 'https://balancer.creativa.com.au/201709-mmr-victoriazoo/?name_to=' . urlencode($to) . '&name_from=' . urlencode($from) . '&GUID=' . urlencode($videoName) . '&sectionid=sc1,sc2,sc3,sc4&audiosectionid=1';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $videoRequestUrl);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        if (preg_match('~Location: (.*)~i', $result, $match)) {
            $videoUrl = trim($match[1]);
        }

        return $videoUrl;
    }
