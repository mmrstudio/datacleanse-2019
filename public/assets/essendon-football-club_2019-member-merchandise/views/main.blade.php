<?php

    function hasPackage($package, $package_name) {
        return strpos($package_name, $package) !== false;
    }

    //print_r($record->toArray()); exit;

    $addons = [
        'grandfinal' => [
            'key' => 'grandfinal',
            'field' => 'addon_grandfinal',
            'class' => 'rollover',
            'name' => false,
            'description' => 'I definitely want guaranteed access to a Grand Final ticket if Essendon play.',
            'price' => 70,
            'monthly' => false,
            'options' => false,
            'default' => 0,
        ],
    ];

    $member_addons = [$addons['grandfinal']];

    $merch_items = [
        [
            'key' => 'polo',
            'field' => 'merch_polo',
            'name' => 'Polo Adult',
            'price' => 30,
            'sizes' => ['XX-Small', 'X-Small', 'Small', 'Medium', 'Large', 'X-Large', '2X-Large', '3X-Large', '4X-Large', '5X-Large', '7X-Large'],
            'sizeLabel' => 'Size',
            'sizeField' => 'merch_polo_size',
        ],
        [
            'key' => 'hood_adult',
            'field' => 'merch_hood_adult',
            'name' => 'Hood Adult',
            'price' => 50,
            'sizes' => ['XX-Small', 'X-Small', 'Small', 'Medium', 'Large', 'X-Large', '2X-Large', '3X-Large', '4X-Large', '5X-Large', '7X-Large'],
            'sizeLabel' => 'Size',
            'sizeField' => 'merch_hood_adult_size',
        ],
        [
            'key' => 'hood_youth',
            'field' => 'merch_hood_youth',
            'name' => 'Hood Youth',
            'price' => 45,
            'sizes' => ['6','8','10','12','14','16'],
            'sizeLabel' => 'Size',
            'sizeField' => 'merch_hood_youth_size',
        ],
    ];

    $app_data = [
        'campaign' => $campaign,
        'record' => $record,
        'fields' => $hidden_fields,
        'allowedSelections' => 1,
        'processUrl' => $process_url,
        'addOns' => $member_addons,
        'merchItems' => $merch_items,
    ];

    //dd($app_data);
    //print_r($app_data); exit;

?><!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">

    <head>

    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    	<title>{{ $campaign->campaignTitle() }}</title>

    	<link href="{{ $campaign->asset('dist/bundle.css') }}" rel="stylesheet">
        <link rel="icon" href="{{ $campaign->asset('icon.ico') }}" />

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-33851616-15', 'auto');
          ga('send', 'pageview');

        </script>

        <link href="https://fonts.googleapis.com/css?family=Poppins:400,700" rel="stylesheet">

        <script>
            window._token = '{{ csrf_token() }}';
            window.appData = {!! json_encode($app_data) !!};
        </script>

    </head>

    <body>
        <div id="app"></div>
        <div class="incompatible" id="incompatible">
            <div class="incompatible__inner">
                <div class="incompatible__text">
                    <p class="title">Please upgrade your browser</p>
                    <p class="description">Unfortunately your browser doesn't support this site.<br>Please use one of the following broswers:<br>Google Chrome, Firefox, Safari, Internet Explorer 11 or Microsoft Edge</p>
                </div>
            </div>
        </div>
        <script src="{{ $campaign->asset('dist/bundle.js') }}"></script>
    </body>

</html>
