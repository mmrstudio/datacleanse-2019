@extends($campaign->view('main'))

<?php

    //dd($record);

    $memberships = [
        'gold' => ['17RH-A/A6AA','17RH-A/A6AP','17RH-A/A6A','17RHA-A/A6A','17RH-A/A6K','17RH-A/A6KA','17RH-A/A6KP','17RHA-A/A6KP','17RH-A/A6V','17RHA-A/A6X','17RHA-A/A6W','17RH-A/A6Z','17RH-A/A6Y','17RHA-A/A6U','17RH-A/A6R','17RHA-A/A6S'],
        'silver' => ['17RH-A/F4A','17RH-A/F4AA','17RH-A/F4K','17RH-A/F4KA','17RH-A/F4V','17RHA-A/F4X','17RHA-A/F4W','17RH-A/F4Y','17RH-A/F4U','17RHA-A/F5X','17RHA-A/F5W','17RHA-A/F5V','17RHA-A/F5U','17RHA-A/F5S','17RH-A/F4R','17RHA-A/F4S','17RH-A/F5A','17RH-A/F5AA','17RHA-A/F5AL','17RH-A/F5K','17RH-A/F5KA','17RH-A/F5R','17RH-A/C4A','17RH-A/C4AA','17RH-A/C4K','17RH-A/C4KA','17RH-A/C4V','17RHA-A/C4V','17RHA-A/C4X','17RHA-A/C4W','17RHA-A/C4Z','17RHA-A/C4Y','17RH-A/C4U','17RHA-A/C4U','17RH-A/C4R','17RH-A/C4S','17RH-A/C5A','17RH-A/C5AA','17RH-A/C5AP','17RHA-A/C5AL','17RH-A/C5K','17RH-A/C5KP','17RH-A/C5V','17RHA-A/C5X','17RHA-A/C5W','17RH-A/C5U','17RH-A/C5R','17RH-A/C5S'],
        'bronze' => ['17RH-A/E4A','17RHA-A/E4A','17RHA-A/E4K','17RH-A/E4V','17RHA-A/E4W','17RH-A/E4Y','17RH-A/E4U','17RH-A/E4R','17RH-A/E4S','17RHA-A/E5A','17RH-A/E5K','17RH-A/E5V','17RHA-A/E5W','17RH-A/E5U'],
        'footscray_end' => ['17RH-A/D4A','17RH-A/D4K','17RH-A/D4V','17RHA-A/D4X','17RHA-A/D4W','17RH-A/D4Y','17RHA-A/D4Y','17RH-A/D4U','17RH-A/D4R','17RH-A/D4S','17RH-A/D5AA','17RH-A/D5A','17RHA-A/D5A','17RHA-A/D5K','17RH-A/D5U','17RH-A/D5V','17RH-A/D5Y','17RH-A/D5S','17RHA-A/D5X','17RHA-A/D5W','17RH-A/D5R'],
        'ga' => ['17GH/G4A','17GHA/G4A','17GH/G4K','17GH/G4V','17GHA/G4X','17GHA/G4W','17GH/G4Y','17GHA/G4Z','17GH/G4U','17GH/G4R','17GH/G4S','17GH/P4A','17GH/P4K','17GH/P4V','17GHA/P4W','17GH/P4U','17GH/P4S','17GH/G5A','17GH/G5AA','17GH/G5AP','17GHA/G5A','17GHA/G5AP','17GH/G5K','17GH/G5KA','17GH/G5KP','17GHA/G5K','17GH/G5V','17GHA/G5V','17GHA/G5X','17GHA/G5W','17GHA/P5W','17GH/G5Z','17GH/G5Y','17GH/G5U','17GHA/G5U','17GH/G5R','17GHA/G5R','17GH/G5S','17GHA/G5Y','17GHA/G5S','17GH/P5A','17GHA/P5A','17GH/P5K','17GH/P5V','17GH/P5U','17GH/P5S','17GHA/P5S'],
        'social' => ['17RHA-A/F5X','17RHA-A/F5W','17RHA-A/F5Y','17RHA-A/F5V','17RHA-A/F5U','17RHA-A/F5S','17RH-A/F5A','17RH-A/F5AA','17RHA-A/F5AL','17RH-A/F5K','17RH-A/F5KA','17RH-A/F5R','17G5H/G5A','17G5H/G5AP','17G5H/G5X','17G5H/G5U','17G5H/G5W','17G5H/G5V','17G3HB/G5A','17RHA-A/E5A','17RH-A/E5K','17RH-A/E5V','17RHA-A/E5W','17RH-A/E5U','17GHACS/G5A','17GHACS/G5AP','17GHCS/G5K','17GHACS/G5V','17GHACS/G5W','17GHACS/G5U','17GHCS/G5S','17RH-A/D5AA','17RH-A/D5A','17RHA-A/D5A','17RHA-A/D5K','17RH-A/D5U','17RH-A/D5V','17RH-A/D5Y','17RH-A/D5S','17RHA-A/D5X','17RHA-A/D5W','17RH-A/D5R','17GH/G5A','17GH/G5AA','17GH/G5AP','17GHA/G5A','17GHA/G5AP','17GH/G5K','17GH/G5KA','17GH/G5KP','17GHA/G5K','17GH/G5V','17GHA/G5V','17GHA/G5X','17GHA/G5W','17GHA/P5W','17GH/G5Z','17GH/G5Y','17GH/G5U','17GHA/G5U','17GH/G5R','17GHA/G5R','17GH/G5S','17GHA/G5Y','17GHA/G5S','17G5H/I5A','17G5H/I5AP','17G5H/I5U','17G5H/I5V','17G5H/I5X','17G5H/I5W','17GH/P5A','17GHA/P5A','17GH/P5K','17GH/P5V','17GH/P5U','17GH/P5S','17GHA/P5S','17RH-A/C5A','17RH-A/C5AA','17RH-A/C5AP','17RHA-A/C5AL','17RH-A/C5K','17RH-A/C5KP','17RH-A/C5V','17RHA-A/C5X','17RHA-A/C5W','17RH-A/C5Y','17RHA-A/C5Y','17RH-A/C5U','17RH-A/C5R','17RH-A/C5S'],
        'charles_sutton' => ['17APREMC/AA','17APREMC/AK'],
    ];

    $membership_type = [];

    $membership_type['gold'] = false;
    $membership_type['silver'] = false;
    $membership_type['bronze'] = false;
    $membership_type['footscray_end'] = false;
    $membership_type['ga'] = false;
    $membership_type['social'] = false;
    $membership_type['charles_sutton'] = false;

?>

@section('content')

<div class="main">

    <form action="{{ $process_url }}" method="post" class="form">

        @include($campaign->view('common.errors'))

        <div class="form__inner">

            <div class="member-choice">

                <h2>Choose what YOU want in your 2018 membership pack!</h2>
                <h3>Hurry, stock is limited and you don’t want to miss out on your preferred item.</h3>

                <div class="member-choice__inner">

                    <div class="member-choice__tile">
                        <div class="member-choice__tile__image scarf"></div>
                        <label class="member-choice__tile__label wb-checkbox">
                            <input type="radio" name="merch_choice" value="scarf" {{ old('merch_choice', $record->merch_choice) === 'scarf' ? 'checked' : '' }}>
                            <span class="member-choice__tile__label__bg">
                                <span class="member-choice__tile__label__bg__inner">
                                    <span class="member-choice__tile__label__checkbox">
                                        <svg viewBox="0 0 40 40"><polygon points="14.1,31.7 6,23.3 8.6,20.9 14.2,26.7 31.7,9.1 34.2,11.6"/></svg>
                                    </span>
                                    <span class="member-choice__tile__label__text">I want a Scarf</span>
                                </span>
                            </span>
                        </label>
                    </div>

                    <div class="member-choice__tile">
                        <div class="member-choice__tile__image cap"></div>
                        <label class="member-choice__tile__label wb-checkbox">
                            <input type="radio" name="merch_choice" value="cap" {{ old('merch_choice', $record->merch_choice) === 'cap' ? 'checked' : '' }}>
                            <span class="member-choice__tile__label__bg">
                                <span class="member-choice__tile__label__bg__inner">
                                    <span class="member-choice__tile__label__checkbox">
                                        <svg viewBox="0 0 40 40"><polygon points="14.1,31.7 6,23.3 8.6,20.9 14.2,26.7 31.7,9.1 34.2,11.6"/></svg>
                                    </span>
                                    <span class="member-choice__tile__label__text">I want a Cap</span>
                                </span>
                            </span>
                        </label>
                    </div>

                    <div class="member-choice__tile">
                    <div class="member-choice__tile__image beanie"></div>
                    <label class="member-choice__tile__label wb-checkbox">
                        <input type="radio" name="merch_choice" value="beanie" {{ old('merch_choice', $record->merch_choice) === 'beanie' ? 'checked' : '' }}>
                        <span class="member-choice__tile__label__bg">
                            <span class="member-choice__tile__label__bg__inner">
                                <span class="member-choice__tile__label__checkbox">
                                    <svg viewBox="0 0 40 40"><polygon points="14.1,31.7 6,23.3 8.6,20.9 14.2,26.7 31.7,9.1 34.2,11.6"/></svg>
                                </span>
                                <span class="member-choice__tile__label__text">I want a Beanie</span>
                            </span>
                        </span>
                    </label>
                </div>

                </div>

            </div>

            <div class="add-ons">

                <h2>WANT TO ADD EXCLUSIVE MEMBER MERCHANDISE,<br> UPGRADE YOUR MEMBERSHIP OR CONTRIBUTE TO OUR COMMUNITY PROGRAMS?</h2>
                <h3>Simply make your selection below and we will update your 2018 membership package.</h3>

                <div class="add-ons__inner">

<?php

    $debug = false;

    foreach(range(1,8) as $i)
    {
        $pcs_concat = $record->{'event_name_' . $i} . '/' . $record->{'pc_' . $i};

        if($pcs_concat != '/')
        {

            if($debug) echo $pcs_concat . '<br>';
            if($debug) echo $record->{'pc_info_' . $i} . ' / ' . $record->{'pc_desc_' . $i} . '<br>';

            // levels
            if(in_array($pcs_concat, $memberships['gold'])) $membership_type['gold'] = true;
            if(in_array($pcs_concat, $memberships['silver'])) $membership_type['silver'] = true;
            if(in_array($pcs_concat, $memberships['bronze'])) $membership_type['bronze'] = true;
            if(in_array($pcs_concat, $memberships['footscray_end'])) $membership_type['footscray_end'] = true;
            if(in_array($pcs_concat, $memberships['ga'])) $membership_type['ga'] = true;
            if(in_array($pcs_concat, $memberships['charles_sutton'])) $membership_type['charles_sutton'] = true;

            // social club
            if(in_array($pcs_concat, $memberships['social'])) $membership_type['social'] = true;

        }

    }

    //dd($membership_type);

    foreach($membership_type as $key => $val)
    {
        if($debug) echo $key . ': ' . ($val ? 'true' : 'false') . '<br>';
    }

    $display = '';

    // screen 1 - Gold OR Social OR Charles Sutton
    if( $membership_type['gold'] || $membership_type['social'] || $membership_type['charles_sutton'] )
    {
        $display = '1';
    }
    // screen 2 - Silver, Bronze OR Footscray End AND NO Social OR Charles Sutton
    elseif( ( $membership_type['silver'] || $membership_type['bronze'] || $membership_type['footscray_end'] ) && !$membership_type['social'] && !$membership_type['charles_sutton'] )
    {
        $display = '2';
    }
    // screen 3 - All other packages AND NO Social OR Charles Sutton
    elseif( !$membership_type['social'] && !$membership_type['charles_sutton'] )
    {
        $display = '3';
    }

    if($debug) echo 'Display: ' . $display . '<br>';

    //dd($display);

    //$display = 2;

?>

                    @include($campaign->view('fields.wb-aflw-membership'))
                    @include($campaign->view('fields.wb-aflw-ambassador'))
                    @include($campaign->view('fields.wb-baby'))
                    @include($campaign->view('fields.wb-backyard'))
                    @include($campaign->view('fields.wb-cap'))
                    @include($campaign->view('fields.wb-mug'))
                    @include($campaign->view('fields.wb-pet'))
                    @include($campaign->view('fields.wb-polo'))
                    @include($campaign->view('fields.wb-turbo'))
                    @include($campaign->view('fields.wb-vest'))
                    @include($campaign->view('fields.wb-vfl'))

                    {{--

                    @if($display == '1')

                        @include($campaign->view('fields.wb-turbo'))
                        @include($campaign->view('fields.wb-vfl'))
                        @include($campaign->view('fields.wb-pet'))
                        @include($campaign->view('fields.wb-backyard'))
                        @include($campaign->view('fields.wb-baby'))
                        @include($campaign->view('fields.wb-mug'))

                    @elseif($display == '2')

                        @include($campaign->view('fields.wb-pet'))
                        @include($campaign->view('fields.wb-turbo'))
                        @include($campaign->view('fields.wb-backyard'))
                        @include($campaign->view('fields.wb-polo'))

                    @elseif($display == '3')

                        @include($campaign->view('fields.wb-pet'))
                        @include($campaign->view('fields.wb-turbo'))
                        @include($campaign->view('fields.wb-vfl'))
                        @include($campaign->view('fields.wb-baby'))

                    @endif

                    --}}

                </div>

            </div>

            <div class="form__submit">

                <div class="form__submit__inner">

                    @foreach($hidden_fields as $name=>$value)
                    <input type="hidden" name="{{ $name }}" value="{{ $value }}">
                    @endforeach

                    <div class="form__submit__accept">
                        <label class="wb-checkbox">
                            <input type="checkbox" name="accept" value="yes" {{ old('accept', $record->accept) === 'yes' ? 'checked' : '' }}>
                            <span></span>
                        </label>
                        I understand that the Western Bulldogs will update my 2018 membership package to reflect the choices I have made with my above selections and will adjust the payments to be deducted from my credit card on file. If I have any queries I can contact the club on 1300 46 36 47 or via membership@westernbulldogs.com.au.
                    </div>

                    <button class="form__submit__button" type="submit">Submit</button>

                </div>

            </div>

        </div>

    </form>

</div>

@endsection
