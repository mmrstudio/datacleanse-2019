import React from 'react';
import MerchTile from './components/MerchTile';
import AddonTile from './components/AddonTile';
import Modal from './components/Modal';
import Checkbox from './components/Checkbox';
import { plural } from './utils';
import { fetchPOST } from './fetchUtils';

require('es6-promise').polyfill();
require('es6-object-assign').polyfill();

class App extends React.Component {

    constructor(props){
        super(props);

        let initialState = {
            campaign: window.appData.campaign,
            record: window.appData.record,
            fields: {},
            hiddenFields: window.appData.fields,
            addOns: window.appData.addOns,
            merchItems: window.appData.merchItems,
            memberPackage: window.appData.memberPackage,
            allowedSelections: parseInt(window.appData.allowedSelections),
            processUrl: window.appData.processUrl,
            totalSelections: 0,
            loading: false,
            submitted: false,
            termsVisible: false,
            noThanksVisible: false,
            thanksMessage: '',
            errors: []
        }

        //console.log('initialState', initialState);

        // initial merch items
        initialState.merchItems.map((merchItem, i) => {
            if(merchItem.field !== false) {
                initialState.fields[merchItem.field] = initialState.record[merchItem.field] ? initialState.record[merchItem.field] : 0;
                initialState.totalSelections += initialState.fields[merchItem.field];

                if(merchItem.sizeField !== false) {
                    initialState.fields[merchItem.sizeField] = initialState.record[merchItem.sizeField] ? initialState.record[merchItem.sizeField] : '';
                }
            }
        });

        // initial add-on selections
        initialState.addOns.map((addOn, i) => {
            const defValue = addOn.options === false ? 0 : [];
            initialState.fields[addOn.field] = initialState.record[addOn.field] ? initialState.record[addOn.field] : defValue;
        });

        // change details option
        initialState.fields.change_details = initialState.record.change_details ? initialState.record.change_details : 'no';

        //console.log('initialState', initialState);

        this.state = initialState;

        this._modal = false;

        this._handleAddonSelection = this._handleAddonSelection.bind(this);
        this._handleAcceptChange = this._handleAcceptChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        this._onSubmitResponse = this._onSubmitResponse.bind(this);
        this._renderMemberChoice = this._renderMemberChoice.bind(this);
        this._renderAddOns = this._renderAddOns.bind(this);

    }

    _showTerms(e) {
        e.preventDefault();
        this.setState({ termsVisible: true });
    }

    _showNoThanks() {
        this.setState({ noThanksVisible: true });
    }

    _handleMerchSelection(merchField, merchQty) {

        let fields = this.state.fields;

        // set current selection
        fields[merchField] = merchQty;

        // re-calculate total selections
        let totalSelections = 0;
        this.state.merchItems.map(merchItem => {
            if(merchItem.field !== false) totalSelections += fields[merchItem.field];
        });

        this.setState({
            fields: fields,
            totalSelections: totalSelections
        });
    }

    _handleAddonSelection(addonField, addonSelection) {
        let fields = this.state.fields;
        fields[addonField] = addonSelection;
        this.setState({ fields: fields });
    }

    _handleAcceptChange() {
        let fields = this.state.fields;
        fields.accept = fields.accept === 'no' ? 'yes' : 'no';
        this.setState({ fields: fields });
    }

    _handleSubmit(purchasing, thanksMessage) {
        const { fields, hiddenFields, merchItems, processUrl } = this.state;
        let submitErrors = [];

        this.setState({ thanksMessage: thanksMessage });

        if (purchasing) {

            // check if all add options selected
            merchItems.map(merchItem => {
                if (merchItem.sizes !== false && merchItem.sizeField !== false) {
                    if (fields[merchItem.field] === 1 && fields[merchItem.sizeField] === '') {
                        submitErrors.push(`Please choose ${merchItem.sizeLabel.toLowerCase()} for ${merchItem.name.toLowerCase()}`);
                    }
                }
            });

            if (submitErrors.length > 0) return this.setState({ errors: submitErrors });

        }

        const postData = {
            ...fields,
            ...hiddenFields,
            purchase_total: this._calculatePurchaseTotal(),
        };

        console.log('postData', postData);

        // send data
        fetchPOST(
            processUrl,
            postData,
            this._onSubmitResponse
        );

    }

    _onSubmitResponse(response) {

        if(response === true) {

            this.setState({
                submitted: true,
                loading: false,
            });

            scroll(0,0);

        } else {

            let errors = [];

            Object.keys(response).map((key) => {
                let value = response[key];
                value.map((error) => {
                    errors.push(error);
                });
            });

            this.setState({ errors: errors });

            //alert(errors.join("\r\n"));

            this.setState({
                loading: false
            });

        }

    }

    _toggleChangeDetails() {
        let fields = Object.assign({}, this.state.fields);
        fields.change_details = fields.change_details === 'yes' ? 'no' : 'yes';
        this.setState({ fields: fields });
    }

    _renderMemberChoice() {

        const { record, allowedSelections, totalSelections, fields, memberPackage } = this.state;
        const selectionsRemaining = allowedSelections - totalSelections;

        return (
            <div className="member-choice">

                <div className="member-choice__inner">

                    <h2>ADD EXCLUSIVE 2019 MEMBER’S MERCHANDISE</h2>
                    <p>Click on an option below to add to your membership.</p>

                    <div className="member-choice__tiles">
                        {this.state.merchItems.map(merchItem => <MerchTile key={merchItem.field} merchItem={merchItem} merchQty={fields[merchItem.field]} allowedSelections={allowedSelections} totalSelections={totalSelections} sizeField={merchItem.sizeField} selectedSize={merchItem.sizeField ? fields[merchItem.sizeField] : false} onUpdate={this._handleMerchSelection.bind(this)} />)}
                    </div>

                </div>

            </div>
        );

    }

    _renderAddOns() {

        if(this.state.addOns.length > 0) {

            const { fields } = this.state;

            return (
                <div className="add-ons">
                    <div className="add-ons__inner">
                        <h2>Additional Upgrades</h2>
                        <div className="add-ons__tiles">
                            {this.state.addOns.map(addOn => <AddonTile key={addOn.key} addOn={addOn} addonSelections={fields[addOn.field]} addonQty={addOn.options === false ? fields[addOn.field] : fields[addOn.field].length} onUpdate={this._handleAddonSelection.bind(this)} />)}
                        </div>
                    </div>
                </div>
            );

        }

    }

    _calculatePurchaseTotal() {
        const { merchItems, addOns, fields, record } = this.state;

        const merchTotal = merchItems.reduce((merchTotal, merchItem) => {
            const merchItemField = fields[merchItem.field];
            return merchTotal + (merchItemField * merchItem.price);
        }, 0);

        const addonTotal = addOns.reduce((addonTotal, addOn) => {
            const addonField = fields[addOn.field];
            const addonQty = addOn.options === false ? addonField : addonField.length;
            return addonTotal + (addonQty * addOn.price);
        }, 0);

        let totalCost = merchTotal + addonTotal;
        if (Math.round(totalCost) !== totalCost) {
            totalCost = totalCost.toFixed(2);
        }

        return totalCost;
    }

    render() {
        const { record, fields } = this.state;

        return (
            <div className="body-wrap">

                { this.state.submitted === true ? (
                    <div className="main">
                        <div className="thanks">
                            <div className="thanks__inner">
                                <div className="thanks__title">Thank You!</div>
                                <p dangerouslySetInnerHTML={{ __html: this.state.thanksMessage }}></p>
                                <div className="thanks__banner">
                                    <div className="thanks__banner__inner"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                ) : (
                    <div>

                        <div className="player-bg"></div>

                        <header className="header">
                            <h1>Buy Your 2019<br />Member’s Merchandise</h1>
                        </header>

                        <form className="main" onSubmit={e => e.preventDefault()}>

                            {this._renderMemberChoice()}

                            {this._renderAddOns()}

                            <div className="form__submit">
                                <div className="form__submit__inner">
                                    <div className="form__submit__note">We have your credit card ending in <strong>{record.cc_num}</strong> on file. To add member’s merchandise to your membership payment using this credit card, click “Buy Now”. If this card is incorrect, please indicate below and we will contact you.</div>
                                    <div className="form__submit__change-details">
                                        <Checkbox class="form__submit__change-details__checkbox white" checked={fields.change_details === 'yes'} onClick={this._toggleChangeDetails.bind(this)} />
                                        <span>Please contact me to update my credit card details</span>
                                    </div>
                                    <button className="form__submit__button" onClick={e => this._handleSubmit(true, 'Thank you! We have received your order. Your membership pack and Member’s merchandise will arrive during December.')} disabled={this.state.loading}>{ this.state.loading ? 'Loading...' : 'Buy Now >' }</button>
                                </div>
                            </div>

                            <div className="total-addons">
                                <h2 className="total-addons__title">Merchandise Total</h2>
                                <div className="total-addons__amount">
                                    <div className="total-addons__amount__main">${this._calculatePurchaseTotal()}</div>
                                </div>
                            </div>

                            <div className="no-thanks">
                                <button className="no-thanks__button" onClick={e => this._handleSubmit(true, 'No worries! You can purchase Member’s merchandise anytime during the season from BomberShop HQ or <a href="https://www.bombershop.com.au/" target="_blank">bombershop.com.au</a>')}>No Thanks</button>
                            </div>


                        </form>

                        <footer className="footer">
                            <div className="footer__inner">
                                <a href="#" target="_blank" className="footer__don-the-sash">Don The Sash</a>
                            </div>
                        </footer>

                    </div>
                )}

                <Modal classes="errors-modal" visible={this.state.errors.length > 0} title="Please check the following errors:" button={'Ok'} buttonAction={() => { this.setState({ errors: [] }) }} onClose={() => { this.setState({ errors: [] }) }} ref={(modal) => { this._modal = modal; }}>
                    <div className="errors-modal__list">
                        {this.state.errors.map((error, i) => <p key={`error_${i}`}>{error}</p>)}
                    </div>
                </Modal>

            </div>
        );
    }

}

export default App;
