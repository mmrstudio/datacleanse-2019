import React from 'react';
import { plural, pushArray, popArray } from '../utils';
import Checkbox from './Checkbox';

class AddonTile extends React.Component {

    constructor(props){
        super(props);
    }

    _toggleAddOnQty() {
        const { addOn, addonQty, onUpdate } = this.props;
        onUpdate(addOn.field, addonQty === 0 ? 1 : 0);
    }

    render() {
        const { addOn, addonQty, addonSelections } = this.props;

        return (
            <div className="add-ons__tile" data-selected={addonQty > 0}>
                { addOn.name !== false ? (
                <div className="add-ons__tile__name">{addOn.name}</div>
                ) : false }
                <div className="add-ons__tile__checkbox">
                    <Checkbox class="" checked={addonQty > 0} onClick={this._toggleAddOnQty.bind(this)} />
                </div>
                <div className="add-ons__tile__desc" dangerouslySetInnerHTML={{__html: addOn.description}}></div>
                { addOn.price !== false ? (
                <div className="add-ons__tile__price">${addOn.price}</div>
                ) : false }
            </div>
        );
    }

}

export default AddonTile;
