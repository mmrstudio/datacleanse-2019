<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Confirm or update your details</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0 " />
<meta name="format-detection" content="telephone=no" />
<base href="<?php echo url('assets/fremantle-football-club_confirm-update-2019/edm/test'); ?>">
<style type="text/css">
body {
	margin: 0 !important;
	padding: 0 !important;
	-webkit-text-size-adjust: 100% !important;
	-ms-text-size-adjust: 100% !important;
	-webkit-font-smoothing: antialiased !important;
}
.visible-mobile {
  max-height: 0;
  width: 0;
  display: none;
}
img {
	border: 0 !important;
	outline: none !important;
}
p {
	Margin: 0px !important;
	Padding: 0px !important;
}
table {
	border-collapse: collapse;
	mso-table-lspace: 0px;
	mso-table-rspace: 0px;
}
td, a, span {
	border-collapse: collapse;
	mso-line-height-rule: exactly;
}
.ExternalClass * {
	line-height: 100%;
}
.em_defaultlink a {
	color: inherit !important;
	text-decoration: none !important;
}
span.MsoHyperlink {
	mso-style-priority: 99;
	color: inherit;
}
span.MsoHyperlinkFollowed {
	mso-style-priority: 99;
	color: inherit;
}
.em_text { font-family:Arial,sans-serif; font-size:14px; line-height:16px;  color:#828282;}
.em_text a { text-decoration:none; color:#828282;}
.em_text1, .em_text1 a { font-family: Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; color: #ffffff; text-decoration: none;   }
.em_text2, .em_text2 a { font-family: Helvetica, Arial, sans-serif; font-size: 22px; line-height: 26px; color:#A5ACAF; text-decoration: none; font-weight:bold; text-transform: uppercase;
	}
.em_text3, .em_text3 a { font-family: Helvetica, Arial, sans-serif; font-size: 18px; line-height: 20px; color: #ffffff; text-decoration: none; }
.em_text4, .em_text4 a { font-family: Helvetica, Arial, sans-serif; font-size: 11px; line-height: 16px; color: #ffffff; text-decoration: none; }
.em_text5, .em_text5 a { font-family: Helvetica, Arial, sans-serif; font-size: 20px; line-height: 28px; color: #A5ACAF; text-decoration: none; }
.preheader {
    display: none;
}
/*Stylesheet for the devices width between 481px to 660px*/
@media only screen and (min-width:481px) and (max-width:659px) {
.em_main_table { width:100% !important; }
.em_wrapper { width: 100% !important; }
.em_side{ padding: 0 50px !important; }
.em_aside { padding: 0 20px !important; /*Update the values as required*/ }
.em_aside2 { padding: 0 0px !important; /*Update the values as required*/ }
.em_aside3 { padding-left:20px!important;padding-right:20px!important; /*Update the values as required*/ }
.em_hide { display: none !important; }
.em_full_img { margin:auto; height: auto !important; max-width:none !important; }
.em_align_cent { text-align: center !important; }
.em_height { height:20px !important; font-size:0px !important; line-height:0px !important;}
.em_background { background-image:none  !important; height:auto !important; }
.em_pad { padding-top:20px !important;}
.em_side2 { width:20px !important;}
.em_side10 { width: 10px !important; /*Update the values as required*/ }
.em_text1, .em_text1 a {  font-size: 14px; line-height: 20px;	}
.em_text5, .em_text5 a { font-family: Helvetica, Arial, sans-serif; font-size: 17px; line-height: 24px; color: #A5ACAF; text-decoration: none; }		
.em_pad_bottom {
	padding-bottom: 14px !important;
}
.em_center1{
	text-align: center!important;
	height: inherit!important;
	border-bottom: none!important;
	padding-top: 8px!important;
}
.em_center2{
	text-align: center!important;
	height: inherit!important;
	padding-bottom: 8px!important;
}
.visible-mobile {
		display: block !important;
		max-height: none !important;
		width:100%!important;
		max-width: 100% !important;
		height: auto !important;
	  }
}
/*Stylesheet for the devices width between 0px to 480px*/
@media only screen and (max-width:480px) {
.em_main_table { width: 100% !important; }
.em_wrapper { width: 100% !important; }
.em_side{ padding: 0 50px !important; }
.em_aside { padding: 0 20px !important; /*Update the values as required*/ }
.em_aside2 { padding: 0 0px !important; /*Update the values as required*/ }
.em_aside3 { padding-left:20px!important;padding-right:20px!important; /*Update the values as required*/ }
.em_hide { display: none !important; }
.em_full_img { margin: auto; height: auto !important; max-width:none !important; }
.em_align_cent { text-align: center !important; }
.em_height { height:20px !important; font-size:0px !important; line-height:0px !important;}
.em_background { background-image:none  !important; height:auto !important; }
.em_pad { padding-top:20px !important;}
.em_side2 { width:20px !important;}
.em_side10 { width: 10px !important; /*Update the values as required*/ }
.em_text1, .em_text1 a {  font-size: 14px; line-height: 20px;	}	
.em_text5, .em_text5 a { font-family: Helvetica, Arial, sans-serif; font-size: 17px; line-height: 24px; color: #A5ACAF; text-decoration: none; }	
.em_br {
	display: block;
}
.em_pad_bottom {
	padding-bottom: 14px !important;
}
.em_center1{
	text-align: center!important;
	height: inherit!important;
	border-bottom: none!important;
	padding-top: 8px!important;
}
.em_center2{
	text-align: center!important;
	height: inherit!important;
	padding-bottom: 8px!important;
}
.visible-mobile {
	display: block !important;
	max-height: none !important;
	width:100%!important;
	max-width: 100% !important;
	height: auto !important;
  }
}
</style>
<!--[if gte mso 9]>
<xml>
  <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
 </o:OfficeDocumentSettings>
</xml>
<![endif]-->
</head>
<body style="margin: 0px;padding: 0px;-webkit-text-size-adjust: 100% !important;-ms-text-size-adjust: 100% !important;-webkit-font-smoothing: antialiased !important;" bgcolor="#828282"><span class="preheader" style="display: none;">Confirm or update your details for your chance to win!</span>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#828282" style="border-collapse: collapse;mso-table-lspace: 0px;mso-table-rspace: 0px;">
    	<tr>
        	<td>
            	<table width="700" cellpadding="0" cellspacing="0" border="0" align="center" class="em_main_table">
                	<tr>
                    	<td valign="top" align="center">
                    		<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                       			<tr>
										<td height="20" class="em_height">&nbsp;</td>
								</tr>
                        		<tr>
                        			<td align="center" class="em_text4">
                    					If you are having trouble viewing this email,<span class="em_br" style="border-collapse: collapse;mso-line-height-rule: exactly;"></span> please <webversion>click here</webversion> to view it as a webpage.
                    				</td>
								</tr>
                  				<tr>
										<td height="20" class="em_height">&nbsp;</td>
								</tr>
                   			</table>
                    	</td>
                    </tr>
                    <tr>
                    	<td valign="top" colspan="1">
                    		<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                        		<tr>
                        			<td align="center">
                    					<a href="http://www.fremantlefc.com.au"><img src="images/img_03.jpg" editable="true" width="700" border="0" style="display:block; max-width: 700px;" class="em_hide" alt="Confirm or update your details and WIN"></a>
                    				</td>
                    				<!--[if !mso]><!-->
									<td valign="top" colspan="1">
											<a href="http://www.fremantlefc.com.au"><img src="images/mobile_01.jpg" width="320" height="250" border="0" style="-moz-hyphens:none;-ms-hyphens:none;-ms-interpolation-mode:bicubic;-webkit-hyphens:none;border:none;clear:both;display:none;hyphens:none;max-height:0;max-width:100%;mso-hide:all;outline:0;text-decoration:none;width:0;word-break:keep-all" alt="Confirm or update your details and WIN" class="visible-mobile"></a>
									</td>
									<!--<![endif]-->
								</tr>
                   			</table>
                    	</td>
                    </tr>
                    <tr>
                    	<td valign="top" colspan="1" bgcolor="#000000" class="em_aside" style="padding-left: 87px; padding-right: 87px;">
                    		<table width="526" style="width:526px;" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
								<tr>
									<td valign="top" colspan="1" bgcolor="#000000">
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="center" style="padding: 35px 0 30px 0; line-height: 24px;">
													 <span class="em_text1"><strong>Hello <?php echo $record->name_first; ?>,</strong></span><br>
<br>
			<span class="em_text1">Please ensure your details are correct to enter the draw.<br>
Competition closes Wednesday 3 July, 2019.</span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td valign="top" colspan="1" bgcolor="#000000">
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="center" style="padding-bottom: 20px;">
													 <span class="em_text2">ARE YOUR <br class="visible-mobile">
 DETAILS CORRECT?</span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								 <tr>
									<td valign="top" colspan="1" bgcolor="#000000">
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="center" style="padding: 0px 0 15px; line-height: 24px;">
													 <span class="em_text">Account ID</span><br>
													<span class="em_text3"><?php echo $record->acct_id ? $record->acct_id : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 15px; line-height: 24px;">
													 <span class="em_text">First Name*</span><br>
													<span class="em_text3"><?php echo $record->name_first ? $record->name_first : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 15px; line-height: 24px;">
													 <span class="em_text">Surname*</span><br>
													<span class="em_text3"><?php echo $record->name ? $record->name : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 15px; line-height: 24px;">
													 <span class="em_text">Address*</span><br>
													<span class="em_text3"><?php echo $record->street_addr_1 ? $record->street_addr_1 : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 15px; line-height: 24px;">
													 <span class="em_text">Suburb*</span><br>
													<span class="em_text3"><?php echo $record->city ? $record->city : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 15px; line-height: 24px;">
													 <span class="em_text">State*</span><br>
													<span class="em_text3"><?php echo $record->state ? $record->state : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 15px; line-height: 24px;">
													 <span class="em_text">Postcode*</span><br>
													<span class="em_text3"><?php echo $record->zip ? $record->zip : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 15px; line-height: 24px;">
													 <span class="em_text">Day Phone Number</span><br>
													<span class="em_text3"><?php echo $record->phone_work ? $record->phone_work : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 15px; line-height: 24px;">
													 <span class="em_text">Mobile Number</span><br>
													<span class="em_text3"><?php echo $record->phone_mobile ? $record->phone_mobile : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 15px; line-height: 24px;">
													 <span class="em_text">Email*</span><br>
													<span class="em_text3"><a href="mailto:[email]" style="text-decoration:none;"><?php echo $record->email_addr ? $record->email_addr : '-'; ?></a></span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								 <tr>
									<td align="center" valign="top" colspan="1" bgcolor="#000000" style="padding-bottom:35px; padding-top:33px;">
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td>
													<table width="251" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="left">
														<tr>
															<td valign="top" align="right" style="" class="em_pad_bottom">
																<a href="<?php echo $record->confirm_url; ?>"><img src="images/img_07.jpg" width="247" height="58" border="0" style="display:block; max-width: 251px;" class="em_full_img"></a>
															</td>
														</tr>
													</table>
													<table width="251" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="right">
														<tr>
															<td valign="top" align="left">
																  <a href="<?php echo $record->update_url; ?>"><img src="images/img_09.jpg" width="247" height="58" border="0" style="display:block; max-width: 251px;" class="em_full_img"></a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
                  				<tr>
									<td valign="top" colspan="1" bgcolor="#000000">
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="center" style="padding: 0px 0 35px 0; line-height: 20px;">
													<span class="em_text1">Click on the buttons to confirm or update your details.<br class="em_hide">
Do not reply to this email.</span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
                   			</table>
                    	</td>
                    </tr>
				</table>
     		</td>
        </tr>
         <tr>
        	<td>
            	<table width="700" cellpadding="0" cellspacing="0" border="0" align="center" class="em_main_table">
                     <tr>
                    	<td valign="top" colspan="1" bgcolor="#000000">
                    		<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                        		<tr>
                        			<td align="center" style="padding: 0px 50px 10px 50px;" class="em_aside3">
                    					 <span class="em_text4">*Compulsory fields.<br>
<br>
											 <span class="em_text5">**Selection of prizes includes; two lucky people can win their 2020 Fremantle Dockers membership FREE!*
</span><br>
<br>
*Please refer to the <a href="https://membership.fremantlefc.com.au/pdf/update-your-details-tcs">terms and conditions</a> for further information.</span>
                    				</td>
								</tr>
                  				<tr>
                        			<td align="center" style="padding: 0px 50px 10px 50px;" class="em_aside3">
<span class="em_text4">Email sent by: Fremantle Football Club, 31 Veterans Parade, Cockburn Central WA 6164<br>
<br>
<a href="http://www.fremantlefc.com.au/privacy">PRIVACY POLICY</a>     <span class="em_hide">&nbsp;&nbsp;I&nbsp;&nbsp;</span><br class="visible-mobile">
     <unsubscribe>UNSUBSCRIBE</unsubscribe>     <span class="em_hide">&nbsp;&nbsp;I&nbsp;&nbsp;</span><br class="visible-mobile">
     <a href="https://membership.fremantlefc.com.au/pdf/update-your-details-tcs">TERMS &amp; CONDITIONS</a>
</span>
                    				</td>
								</tr>
                   			</table>
                    	</td>
                    </tr>
                     <tr>
                    	<td valign="top" colspan="1" class="em_aside" style="padding: 0px 50px 15px 45px;" bgcolor="#000000">
                    		<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                    			<tr>
									<td valign="top" align="center">
										<table width="323" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="center" style="padding-top: 12px; padding-bottom: 8px;">
													<table width="260" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
														<tr>
															<td align="left">
																<a href="https://www.programmed.com.au/"><img src="images/pic_15.jpg" width="130" height="38" border="0" style="display:block; max-width: 130px;" class="em_full_img"></a>
															</td>
															<td align="left">
																<a href="http://www.woodside.com.au/Pages/home.aspx"><img src="images/pic_16.jpg" width="130" height="38" border="0" style="display:block; max-width: 130px;" class="em_full_img"></a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
                   				</tr>
                   				<tr>
									<td valign="top" align="center" style="padding-top: 15px;">
										<span class="em_text4">Proud Major Sponsors</span>
									</td>
                   				</tr>
							</table>
                    	</td>
                    </tr>
                    <tr>
                    	<td valign="top" colspan="1" class="em_aside2" style="padding: 0px 0px 0px 0px;" bgcolor="#000000">
                    		<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                    			<tr>
									
                   				</tr>
							</table>
                    	</td>
                    </tr>
                 </table>
            </td>
       	</tr>
    </table>
   
<div class="em_hide" style="white-space:nowrap;font:20px courier;color:#ffffff;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
</td></tr></table></body>
</html>
