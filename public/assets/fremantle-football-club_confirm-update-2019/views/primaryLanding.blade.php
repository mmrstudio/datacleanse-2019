@extends($campaign->view('main'))

@section('content')
<div class="body-wrap__inner">

	<div class="main">

        <header class="header">
            <div class="header__logo">
                <a href="{{ $campaign->client_url }}/?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank"><img src="{{ $campaign->asset('images/header-logo.png') }}"></a>
            </div>
        </header>

		<form action="{{ $process_url }}" method="post" class="form">

			@include($campaign->view('common.errors'))

			<div class="form__inner">

				<div class="form__top">
					<h1 class="form__top__title title">{{ $campaign->title }}</h1>
                    @if($campaign->creative && $campaign->creative->prize_image)
					<div class="form__top__prize"><img src="{{ $campaign->asset('images/prize-image.jpg') }}"></div>
                    @endif
                    @if($campaign->intro)
					<div class="form__top__intro intro">{{ $campaign->intro }}</div>
                    @endif
				</div>

				<div class="form__fields">

                    <h2 class="form__heading heading">Are your details correct?</h2>


                     @include($campaign->view('fields.text'), [
						'key' => 'acct_id',
                        'width' => 'half',
                        'props' => ['readonly']
					])

					<div class="form__group hidemobile" data-width="half" >

					</div>

					@include($campaign->view('fields.text'), [
						'key' => 'name_first',
                        'width' => 'half',
						'props' => ['readonly']
					])

                    @include($campaign->view('fields.text'), [
						'key' => 'name',
                        'width' => 'half',
						'props' => ['readonly']
					])


                    @include($campaign->view('fields.address'), [
						'keys' => ['street_addr_1', 'street_addr_2'],
					])


					@include($campaign->view('fields.country'), [
					 'key' => 'country',

					])


                   @include($campaign->view('fields.text'), [
						'key' => 'city',
						'id' => 'suburbField',
                        'width' => 'third',
					])

                    @include($campaign->view('fields.state'), [
						'key' => 'state',
                        'width' => 'third',
					])

                    @include($campaign->view('fields.text'), [
						'key' => 'zip',
						'id' => 'postcodeField',
                       'width' => 'third',
					])

                   @include($campaign->view('fields.text'), [
						'key' => 'phone_work',
                        'width' => 'third',
					])

					@include($campaign->view('fields.text'), [
						'key' => 'phone_mobile',
                        'width' => 'third',
					])

					@include($campaign->view('fields.email'), [
						'key' => 'email_addr',
                        'width' => 'third',
					])


                   @include($campaign->view('fields.dob'), [
						'key' => 'birthdate',
						'placeholder' => 'Please provide your DOB...',
                        'width' => 'half',
                        'not_required' => $input->birthdate == 'Supplied'
					])

					@include($campaign->view('fields.gender'), [
						'key' => 'gender',
						'placeholder' => 'Please choose your gender...',
                        'width' => 'half',
                        'not_required' => $input->gender == 'Supplied'
					])

				{{-- 	@include($campaign->view('fields.checkbox'), [
						'key' => 'extra1',
                        'width' => 'full',
                        'control_label' => 'Would you like to go green and opt out of all paper correspondence?',

					]) --}}

					@include($campaign->view('fields.dropdown'), [
						 'label' => false,
                        'key' => 'extra1',
                        'width' => 'full',
						'control_label' => 'From the below, who is your favourite player?',
                        'options' => [
                            'Nat Fyfe' => 'Nat Fyfe',
                            'Brad Hill' => 'Brad Hill',
                            'Stephen Hill' => 'Stephen Hill',
                            'Jesse Hogan' => 'Jesse Hogan',
                            'Rory Lobb' => 'Rory Lobb',
                            'Michael Walters' => 'Michael Walters',
                            'Cam McCarthy' => 'Cam McCarthy',
                        ],
					])

					@include($campaign->view('fields.dropdown'), [
						 'label' => false,
                        'key' => 'extra2',
                        'width' => 'full',
						'control_label' => 'Please select what form of renewal you will prefer to use for the 2020 season',
                        'options' => [
                            'Monthly Payment Plan' => 'Monthly Payment Plan',
                            'Upfront Payment Plan' => 'Upfront Payment Plan',
                            'Full payment when it suits me' => 'Full payment when it suits me',
                            'Bank Transfer' => 'Bank Transfer',
                            'Other (Please specify)' => 'Other (Please specify)',
                           
                        ],
					])

					@include($campaign->view('fields.text'), [
						'key' => 'extra3',
                        'width' => 'half',
                        'control_label' => 'Please specify',
						
					])


				</div>

				<div class="form__submit">

					@foreach($hidden_fields as $name=>$value)
					<input type="hidden" name="{{ $name }}" value="{{ $value }}">
					@endforeach

					<button class="form__submit__button" type="submit">{{ $campaign->creative->button_text }}</button>

				</div>

			</div>

		</form>

	</div>

    @include($campaign->view('common.footer'))

</div>


@endsection
