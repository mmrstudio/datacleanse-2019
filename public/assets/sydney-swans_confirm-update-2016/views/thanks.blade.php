@extends($campaign->view('main'))

@section('content')

<header class="header">

    <img src="{{ $campaign->asset('images/thanks.jpg') }}">

    @include($campaign->view('common.header'))

</header>

<footer class="footer">

    @include($campaign->view('common.footer'))

</footer>

@endsection
