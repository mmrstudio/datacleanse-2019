@extends($campaign->view('main'))

@section('content')

<header class="header">

    <img src="{{ $campaign->asset('images/header.jpg') }}">

    @include($campaign->view('common.header'))

</header>

<div class="main">

    <form action="{{ $process_url }}" method="post" class="form">

        @include($campaign->view('common.errors'))

        <div class="form__inner">

            <div class="form__intro">
                @if($record->name_first != '')
                <div class="form__intro__greeting">Dear {{ $record->name_first }},</div>
                @endif
                <div class="form__intro__text">
                    <p><strong>Please check your details below.</strong><br>If we've got everything correct, please click <strong>CONFIRM</strong> to let us know we've got the right information for you. If any of the details aren't quite right, please type the correct information in and click <strong>UPDATE</strong>.</p>
                    <p>Just by confirming or updating your details, you'll be in the running to <strong>WIN</strong> one of five <strong>2017 SYDNEY SWANS MEMBERSHIP GIFT VOUCHERS TO THE VALUE OF $100</strong>.</p>
                </div>
            </div>

            <div class="form__title">
                <h1>Your Details</h1>
            </div>

            @include($campaign->view('fields.text'), [
                'key' => 'name_first',
                'props' => ['readonly'],
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'name',
                'props' => ['readonly'],
            ])

            @include($campaign->view('fields.email'), [
                'key' => 'email_addr',
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'phone_mobile',
            ])

            @include($campaign->view('fields.dob'), [
                'key' => 'birthdate',
                'placeholder' => 'Please provide your DOB...',
                'not_required' => false
            ])

            <div class="form__group form__get-updates form__group--receive_updates" id="receiveUpdatesField">
                <div class="form__get-updates__text {{ $errors->has('receive_updates') ? 'form__get-updates__text--error' : '' }}">I am happy to receive important membership and finals information via email, including my 2017 membership renewal.</div>
                <ul class="form__get-updates__options">
                    <li><label><input type="radio" name="receive_updates" value="yes" {{ $record->receive_updates === 'yes' ? 'checked' : '' }}><span></span> Yes</label></li>
                    <li><label><input type="radio" name="receive_updates" value="no" {{ $record->receive_updates === 'no' ? 'checked' : '' }}><span></span> No</label></li>
                </ul>
            </div>

            <div class="form__submit">

                @foreach($hidden_fields as $name=>$value)
                <input type="hidden" name="{{ $name }}" value="{{ $value }}">
                @endforeach

                <button class="form__submit__button form__submit__button--confirm" type="submit">Confirm</button>
                <button class="form__submit__button" type="submit">Update</button>

            </div>

        </div>

    </form>

</div>

<footer class="footer">

    <img src="{{ $campaign->asset('images/footer.png') }}">

    @include($campaign->view('common.footer'))

</footer>

@endsection
