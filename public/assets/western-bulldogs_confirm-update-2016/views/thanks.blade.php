@extends($campaign->view('main'))

@section('content')

<header class="header">

    @include($campaign->view('common.header'))

    <div class="header__bottom">
        <div class="header__bottom__player"></div>
        <div class="header__bottom__thanks">
            <div class="header__bottom__thanks__title">Thank<br>You!</div>
            <div class="header__bottom__thanks__desc">You are now entered into the draw to win a 2017 Membership.</div>
        </div>
    </div>

</header>

<footer class="footer">

    @include($campaign->view('common.footer'))

</footer>

@endsection
