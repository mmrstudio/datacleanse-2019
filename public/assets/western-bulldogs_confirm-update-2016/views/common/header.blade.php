
    <div class="header__top">
        <div class="header__top__title">Membership</div>
        <ul class="header__top__social">
            <li><a href="http://twitter.com/#!/westernbulldogs" target="_blank"><img src="{{ $campaign->asset('images/social-twitter.png') }}"></a></li>
            <li><a href="http://www.facebook.com/Western.Bulldogs?" target="_blank"><img src="{{ $campaign->asset('images/social-facebook.png') }}"></a></li>
            <li><a href="http://www.youtube.com/user/BulldogsTVOfficial" target="_blank"><img src="{{ $campaign->asset('images/social-youtube.png') }}"></a></li>
            <li><a href="https://plus.google.com/115330438065491963007" target="_blank"><img src="{{ $campaign->asset('images/social-google.png') }}"></a></li>
        </ul>
    </div>
