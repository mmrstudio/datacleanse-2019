
    <div class="footer__banner">
        <img src="{{ $campaign->asset('images/footer-banner.jpg') }}">
    </div>

    <div class="footer__bottom">
        <p>Western Bulldogs, 417 Barkly St, Footscray West 3012. Email: <a href="mailto:membership@westernbulldogs.com.au">membership@westernbulldogs.com.au</a></p>
        <ul>
            <li><a href="http://www.westernbulldogs.com.au/membership/terms-and-conditions" target="_blank">Terms &amp; Conditions</a></li>
            <li><a href="http://www.westernbulldogs.com.au/privacy" target="_blank">Privacy Policy</a></li>
            <li><a href="http://datacleanse.com.au" target="_blank">Product of Datacleanse</a></li>
        </ul>
    </div>
