@extends($campaign->view('main'))

@section('content')

<header class="header">

    @include($campaign->view('common.header'))

    <div class="header__bottom">
        <div class="header__bottom__player"></div>
        <div class="header__bottom__text">
            <div class="header__bottom__title">
                <span>Confirm Your Details</span><br>
                And WIN!
            </div>
            <div class="header__bottom__desc">Confirm or update your details by 5pm FRIDAY JULY 1, 2016 and you’ll go into the draw to win your 2017 membership for free*.</div>
        </div>
    </div>

</header>

<div class="main">

    <form action="{{ $process_url }}" method="post" class="form">

        @include($campaign->view('common.errors'))

        <div class="form__inner">

            <h1>Your Details</h1>

            @include($campaign->view('fields.text'), [
                'key' => 'name_first',
                'props' => ['readonly'],
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'name',
                'props' => ['readonly']
            ])

            @include($campaign->view('fields.address'), [
                'keys' => ['street_addr_1', 'street_addr_2'],
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'city',
                'id' => 'suburbField',
                'width' => 'two-thirds',
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'zip',
                'id' => 'postcodeField',
                'width' => 'third',
            ])

            @include($campaign->view('fields.country'), [
                'key' => 'country',
            ])

            @include($campaign->view('fields.state'), [
                'key' => 'state',
            ])

            @include($campaign->view('fields.email'), [
                'key' => 'email_addr',
            ])

            @include($campaign->view('fields.dropdown'), [
                'key' => 'other_language',
                'label' => 'Question 1&#41;',
                'control_label' => 'Do you speak a language other than English as your first language?',
                'show_other' => true,
            ])

            @include($campaign->view('fields.dropdown'), [
                'key' => 'favourite_player',
                'label' => 'Question 2&#41;',
                'control_label' => 'Who is your favourite player?',
            ])

            <div class="form__submit">

                @foreach($hidden_fields as $name=>$value)
                <input type="hidden" name="{{ $name }}" value="{{ $value }}">
                @endforeach

                <button class="form__submit__button" type="submit">Update</button>

            </div>

        </div>

    </form>

</div>

<footer class="footer">

    <div class="footer__disclaimer">
        <p>* Based on membership held in 2016, individual membership only. Family memberships not included. If no membership purchased in 2016, an 11 Game Home membership will be offered to the value of $205.00. All personal information you provide will be used by the Western Bulldogs Football Club, AFL and on behalf of selected third parties in accordance with our Privacy Policy, this may include for promotional and direct marketing purposes and other disclosures as specified in our Privacy Policy found at <a href="http://www.westernbulldogs.com.au/privacy" target="_blank">www.westernbulldogs.com.au/privacy</a>. By providing your personal information, you agree to such use. Your personal information will be kept securely and confidentially. Only those who confirm or update their details prior to 5pm AEST, Friday July 1 will be entered into the draw to win a 2017 membership. The winner will be drawn at Western Bulldogs Football Club at on Tuesday July 5, 2016.</p>
    </div>

    @include($campaign->view('common.footer'))

</footer>

@endsection
