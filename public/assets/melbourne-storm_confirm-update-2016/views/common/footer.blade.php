
    

    <div class="footer__sponsors">
        <img src="{{ $campaign->asset('images/sponsors.png') }}">
    </div>

    <p>Melbourne Storm Rugby League Club | 1300 786 767 | <a href="mailto:membership@melbournestorm.com.au">membership@melbournestorm.com.au</a></p>

    <ul class="footer__links">
        <li><a href="https://www.melbournestorm.com.au/" target="_blank">Melbourne Storm</a></li>
        <li><a href="http://membership.melbournestorm.com.au/terms-and-conditions/privacy/" target="_blank">Privacy Policy</a></li>
        <li><a href="http://membership.melbournestorm.com.au/terms-and-conditions/terms-conditions/" target="_blank">Terms &amp; Conditions</a></li>
        <li><a href="http://datacleanse.com.au" target="_blank">Product of Datacleanse</a></li>
    </ul>
