<?php
    if(! isset($fields[$key])) return '';
    $field = $fields[$key];
    $id = isset($id) ? $id : camel_case($field->name . '_field');
    $label = isset($label) ? $label : $field->front_label;
    $control_label = isset($control_label) ? $control_label : false;
    $not_required = isset($not_required) ? $not_required : true;
    $placeholder = isset($placeholder) ? $placeholder : $field->front_placeholder;
    $props = isset($props) ? implode(' ', $props) : false;
    $form_group_classes = ['form__group', 'form__group--' . $field->name];
    if($errors->has($field->name)) $form_group_classes[] = 'form__group--error';
    if(isset($width)) $form_group_classes[] = 'form__group--width-' . $width;
    $field_classes = ['form__control', 'form__control--text', 'form__control--' . $field->name];
?>

<div class="{{ implode(' ', $form_group_classes) }}" @if($id)id="{{ $id }}"@endif>
    <label class="form__group__label" for="{{ $field->name }}">
        {{ $label }}
        @if($field->required && !$record->{$field->name})<span class="required">*</span>@endif
    </label>
    <div class="form__group__controls">
        @if(! $record->{$field->name} || $not_required === false)
        <input id="{{ $field->name }}" name="{{ $field->name }}" type="text" placeholder="{{ $placeholder }}" class="{{ implode(' ', $field_classes) }} datepicker-here" value="{{ old($field->name, $record->{$field->name}) }}" data-language='en' data-date-format='dd/mm/yyyy' data-position='bottom center' data-view="years" readonly>
        @else
        <input type="text" class="{{ implode(' ', $field_classes) }}" value="Not required" disabled>
        <input type="hidden" name="{{ $field->name }}" value="{{ $record->{$field->name} }}">
        @endif
    </div>
</div>
