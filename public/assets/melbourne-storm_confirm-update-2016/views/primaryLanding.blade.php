@extends($campaign->view('main'))

@section('content')

<header class="header">

    @include($campaign->view('common.header'))

    <div class="header__landing-title">
        <div class="header__landing-title__inner"></div>
    </div>
    
    <div class="header__logo">
    <div class="header__logo__inner"></div>
    
    </div>

</header>

<div class="main">

    <form action="{{ $process_url }}" method="post" class="form">

        @include($campaign->view('common.errors'))

        <div class="form__inner">

           
            <div class="form__title">
                <h1>Your Details</h1>
            </div>
            
            @include($campaign->view('fields.text'), [
                'key' => 'name_prefix1',
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'first_name',
               
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'last_name',
                
            ])

            

            @include($campaign->view('fields.address'), [
                'keys' => ['street_addr_1', 'street_addr_2'],
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'city',
                'id' => 'suburbField',
            ])

            
            
            @include($campaign->view('fields.state'), [
                'key' => 'state',
            ])
            
            @include($campaign->view('fields.text'), [
                'key' => 'postcode',
                'id' => 'postcodeField',
            ])

            @include($campaign->view('fields.country'), [
                'key' => 'country',
            ])
            
            @include($campaign->view('fields.text'), [
                'key' => 'phone_work',
            ])

            

            @include($campaign->view('fields.text'), [
                'key' => 'phone_mobile',
            ])

            @include($campaign->view('fields.email'), [
                'key' => 'email_addr',
            ])
            
             @include($campaign->view('fields.dob'), [
                'key' => 'birth_date',
                'not_required' => false,
            ])
            
            @if($record->gender !== 'Supplied')
                @include($campaign->view('fields.dropdown'), [
                    'key' => 'gender',
                ])
            @endif

            <div class="form__submit">

                @foreach($hidden_fields as $name=>$value)
                <input type="hidden" name="{{ $name }}" value="{{ $value }}">
                @endforeach

                <button class="form__submit__button" type="submit">Update</button>

            </div>

        </div>

    </form>

</div>

<footer class="footer">

    @include($campaign->view('common.footer'))

</footer>

@endsection
