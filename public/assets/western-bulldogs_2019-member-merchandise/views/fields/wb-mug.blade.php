<div class="add-ons__tile">

    <div class="add-ons__tile__inner">

        <div class="add-ons__tile__image mug"></div>
        <div class="add-ons__tile__desc">Let your tea or coffee #bemorebulldog with this exclusive member-only mug.</div>
        <div class="add-ons__tile__note">Monthly payment option available.</div>
        <div class="add-ons__tile__options">

            <label class="add-ons__tile__options__tick add-ons__tile__label">
                <input type="checkbox" name="addon_mug" value="yes" {{ old('addon_mug', $record->addon_mug) === 'yes' ? 'checked' : '' }}>
                <span class="add-ons__tile__label__bg">
                    <span class="add-ons__tile__label__bg__inner">
                        <span class="add-ons__tile__label__text">$15</span>
                        <span class="add-ons__tile__label__checkbox">
                            <svg viewBox="0 0 40 40"><polygon points="14.1,31.7 6,23.3 8.6,20.9 14.2,26.7 31.7,9.1 34.2,11.6"/></svg>
                        </span>
                    </span>
                </span>
            </label>

        </div>

    </div>

</div>
