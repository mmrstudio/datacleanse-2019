<!DOCTYPE html>
<!--
<?php print_r($record->toArray()); ?>
-->
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>2019 Member Merchandise Available Now</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0 " />
        <meta name="format-detection" content="telephone=no"/>
        <style>
        
    .font-arial { font-family: Helvetica, Arial, sans-serif; line-height: 1.2; }

	.font-bold { font-weight: bold; }
	.font-italic { font-style: italic; }

	.font-10 { font-size: 10px; }
	.font-12 { font-size: 12px; }
	.font-13 { font-size: 13px; }
	.font-14 { font-size: 14px; }
	.font-16 { font-size: 16px; }
	.font-18 { font-size: 18px; }
	.font-20 { font-size: 20px; }
	.font-22 { font-size: 22px; }
	.font-24 { font-size: 24px; }
	.font-26 { font-size: 26px; }
	.font-28 { font-size: 28px; }

	.font-uppercase { text-transform: uppercase; }

	.color-black { color: #000000; }
	.color-white { color: #ffffff; }
	.color-red { color: #c50b2f; }
	.color-blue { color: #0039a6; }
	.color-grey { color: #464646; }

	.bg-black { color: #000000; }
	.bg-white { color: #ffffff; }
	.bg-red { color: #c50b2f; }
	.bg-blue { color: #0039a6; }
	.bg-grey { color: #464646; }
	.bg-light-blue { background-color: #f2f5fa; }

	.text-center { text-align: center; }
	.text-left { text-align: left; }
	.text-right { text-align: right; }

	.gutters { padding: 40px; }

    .border-top-blue { border-top: 1px solid #0039a6; }

    .button {
        background-color:#c50b2f;
        color:#ffffff;
        display:inline-block;
        font-family:sans-serif;
        font-size:21px;
        font-weight:bold;
        line-height:60px;
        text-align:center;
        text-decoration:none;
        width:480px;
        -webkit-text-size-adjust:none;
    }
        
        </style>
        <base href="<?php echo url('assets/western-bulldogs_2019-member-merchandise/edm/test'); ?>">
    </head>


<body bgcolor="#fff"><span class="preheader" style="display: none;"><?php echo $record->name_first; ?>, your loyalty to the Club through your membership is greatly appreciated. Thank you for your continued support.</span>

    <!--Full width table start-->
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="border-collapse: collapse;mso-table-lspace: 0px;mso-table-rspace: 0px;">
    	<tr>
        	<td>
            	<table width="700" cellpadding="0" cellspacing="0" border="0" align="center" class="em_main_table">

                	<tr>
                    	<td valign="top" align="center">
                    		<table width="100%"  class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                       			<tr>
									<td height="10"></td>
								</tr>
                        		<tr>
                        			<td align="center" class="font-arial font-12 color-grey">If you are having trouble viewing this email, <span class="em-br"></span> please <webversion class="color-grey">click here</webversion> to view it as a webpage.</td>
								</tr>
                  				<tr>
									<td height="10"></td>
								</tr>
                   			</table>
                    	</td>
                    </tr>

                    <tr>
                    	<td valign="top" colspan="1">
                    		<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                        		<tr>
                                    <td valign="top" align="center">
                    					<a href="<?php echo $record->update_url; ?>"><img src="images/header.jpg" border="0" style="display:block;" width="700" height="410" alt="2019 Member Merchandise Available Now" ></a>
                                    </td>
								</tr>
                   			</table>
                    	</td>
                    </tr>

                    <tr>
                    	<td valign="top" colspan="1" bgcolor="#ffffff" class="gutters">
							<span class="font-arial font-24 font-bold color-red font-uppercase">Hi <?php echo $record->name_first; ?>,</span>
							<br><br>
							<span class="font-arial font-14">
                                In 2019, we have once again designed an exclusive member merchandise range for our members to purchase. 
                                <br /><br />
                                With the Committed Bulldog payment plan, it’s easy to add in the latest merchandise to your current payments. 
                                <br /><br />
                                Check out our new range below.
                                <br />
                                <br />
                                <br />
                                <div style="text-align: center;"><!--[if mso]>
                                <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="<?php echo $record->update_url; ?>" style="height:60px;v-text-anchor:middle;width:480px;" stroke="f" fillcolor="#c50b2f">
                                    <w:anchorlock/>
                                    <center>
                                <![endif]-->
                                    <a href="<?php echo $record->update_url; ?>" class="button" target="_blank">BROWSE ITEMS</a>
                                <!--[if mso]>
                                    </center>
                                </v:rect>
                                <![endif]--></div>
								<singleline />
							</span>
                    	</td>
                    </tr>
                    

                    <tr>
                    	<td valign="top" colspan="1" class="font-arial font-13 color-blue bg-light-blue border-top-blue gutters">
                            * Once items are selected, they will be added to your current 2019 Committed Bulldog payment plan. If you are on a monthly plan the due balance will be taken before being divided over remaining months of the plan. Sizes are subject to availability. 
                    	</td>
                    </tr>

                    <tr>
                    	<td valign="top" colspan="1" bgcolor="#ffffff" align="center" class="border-top-blue">
							<a href="http://westernbulldogs.com.au"><img src="images/sponsors.png" width="700" height="170" border="0" style="display:block; max-width: 700px;" alt="" ></a>
                    	</td>
                    </tr>
                    <tr>
                    	<td valign="top" colspan="1" bgcolor="#011e46" align="center" style="padding-top: 20px; padding-bottom: 20px;">

							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td>

										<table width="150" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td><a href="http://www.facebook.com/Western.Bulldogs?"><img src="images/facebook.png" width="30" height="30" border="0" style="display:block;" alt="Facebook" ></a></td>
												<td width="10"></td>
												<td><a href="https://www.instagram.com/westernbulldogs"><img src="images/instagram.png" width="30" height="30" border="0" style="display:block;" alt="Instagram" ></a></td>
												<td width="10"></td>
												<td><a href="http://twitter.com/#!/westernbulldogs"><img src="images/twitter.png" width="30" height="30" border="0" style="display:block;" alt="Twitter" ></a></td>
												<td width="10"></td>
												<td><a href="http://www.youtube.com/user/BulldogsTVOfficial"><img src="images/youtube.png" width="30" height="30" border="0" style="display:block;" alt="YouTube" ></a></td>
											</tr>
										</table>

									</td>
								</tr>
								<tr>
									<td height="20"></td>
								</tr>
								<tr>
									<td align="center">
                                        <span class="font-arial color-white font-10">
										    Email sent by: Western Bulldogs, 417 Barkly St, Footscray West 3012<br>
                                            <a href="https://membership.westernbulldogs.com.au/terms-conditions-2" class="color-white">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a href="http://www.westernbulldogs.com.au/privacy" class="color-white">Privacy Policy</a> &nbsp;|&nbsp; To unsubscribe from future receipt please <unsubscribe class="color-white">click here</unsubscribe>
                                        </span>
									</td>
								</tr>
							</table>

                    	</td>
                    </tr>


				</table>
     		</td>
        </tr>
    </table>
<div class="hidden-mobile" style="white-space:nowrap;font:20px courier;color:#ffffff;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
</body>
</html>

