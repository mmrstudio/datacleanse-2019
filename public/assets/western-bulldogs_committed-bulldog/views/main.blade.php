<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">

    <head>

    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    	<title>{{ $campaign->campaignTitle() }}</title>

    	<link href="{{ $campaign->asset('css/campaign.css') }}" rel="stylesheet">

        <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
        <link rel="icon" href="{{ $campaign->asset('icon.ico') }}" />

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-33851616-15', 'auto');
          ga('send', 'pageview');

        </script>

        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700|Arvo:400italic,400,700,700italic' rel='stylesheet' type='text/css'>

        <script src="{{ $campaign->asset('js/modernizr.js') }}"></script>

    </head>

    <body>

        <div class="body-wrap">

            <header class="header">

                <div class="header__top">

                    <div class="header__inner">

                        <div class="header__top__logo"><a href="http://www.westernbulldogs.com.au/" target="_blank">Western Bulldogs</a></div>

                        <nav class="header__top__nav">
                            <ul>
                                <li><a href="http://www.westernbulldogs.com.au/" target="_blank">Bulldogs Home</a></li>
                                <li><a href="http://membership.westernbulldogs.com.au/" target="_blank">Membership</a></li>
                                <li><a href="http://membership.westernbulldogs.com.au/" target="_blank">Which Package</a></li>
                            </ul>
                        </nav>

                        <ul class="header__top__social">
                            <li><a href="https://instagram.com/westernbulldogs/?hl=en" target="_blank" class="instagram">Instagram</a></li>
                            <li><a href="http://twitter.com/#!/westernbulldogs" target="_blank" class="twitter">Twitter</a></li>
                            <li><a href="http://www.facebook.com/Western.Bulldogs?" target="_blank" class="facebook">Facebook</a></li>
                        </ul>

                    </div>

                </div>

                <div class="header__bottom">

                    <div class="header__inner">
                        <div class="header__be-more-bulldog"><a href="http://www.westernbulldogs.com.au/" target="_blank">Western Bulldogs</a></div>
                    </div>

                </div>

                @include($campaign->view('common.header'))

            </header>

            <div class="body-wrap__inner">

                @yield('content')

            </div>

            <footer class="footer">

                <div class="footer__sponsors">
                    <div class="footer__inner">
                        <img src="{{ $campaign->asset('images/logo-lockup.png') }}">
                    </div>
                </div>

                <div class="footer__disclaimer">
                    <div class="footer__inner">
                        <p>All personal information you provide will be used by the Western Bulldogs Football Club, AFL and on behalf of selected third parties in accordance with our Privacy Policy, this may include for promotional and direct marketing purposes and other disclosures as specified in our Privacy Policy found at <a href="http://www.westernbulldogs.com.au/privacy" target="_blank">http://www.westernbulldogs.com.au/privacy</a>.</p>
                        <p>By providing your personal information, you agree to such use. Your personal information will be kept securely and confidentially. Although you may choose not to provide us with certain information, please be aware that this choice will affect the type of and quality of service we can provide to you.</p>
                    </div>
                </div>

                <div class="footer__links">

                    <div class="footer__inner">

                        <div class="links">
                            <a href="http://www.westernbulldogs.com.au" target="_blank">www.westernbulldogs.com.au</a> <span>/</span><br>
                            <a href="http://membership.westernbulldogs.com.au/" target="_blank">Members Home</a> <span>/</span><br>
                            <a href="http://www.westernbulldogs.com.au/privacy" target="_blank">Privacy Policy</a> <span>/</span><br>
                            <a href="http://www.westernbulldogs.com.au/membership/buy-membership/membership-terms-and-conditions" target="_blank">Terms & Conditions</a> <span>/</span><br>
                            <a href="http://www.westernbulldogs.com.au/club/contact-us" target="_blank">Contact Us</a>
                        </div>

                        <br>
                        &copy; Western Bulldogs Football Club 2016
                    </div>

                </div>

            </footer>

        </div>

        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    	<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
        <script src="{{ $campaign->asset('js/campaign.js') }}"></script>

    </body>

</html>
