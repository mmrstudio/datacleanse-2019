
<div class="package-tile">

    <div class="package-tile__inner">

        <div class="package-tile__top package-tile__top--vfl"></div>

        <div class="package-tile__main">

            <div class="package-tile__main__desc">Support the Footscray Bulldogs VFL team and get access to home games played at Victoria University Whitten Oval. Includes a VFL Member pack with retro membership card, key chain and certificate.</div>

        </div>

        <div class="package-tile__options">
            <div class="package-tile__options__note">Monthly payment option available.</div>
            <div class="package-tile__options__checkbox">
                <label>
                    <input type="checkbox" name="addon_vfl" value="yes" {{ old('addon_vfl', $record->addon_vfl) === 'yes' ? 'checked' : '' }}>
                    <span></span>
                    $60
                </label>
            </div>
        </div>

    </div>

</div>
