
<div class="package-tile">

    <div class="package-tile__inner">

        <div class="package-tile__top package-tile__top--social"></div>

        <div class="package-tile__main">

            <div class="package-tile__main__desc">Close to capacity! Guarantee yourself access to purchase a Grand Final ticket should the Bulldogs participate, plus access to the Social Club room at Etihad Stadium. You also get an invitation to an end of year event to meet the players.</div>

        </div>

        <div class="package-tile__options">
            <div class="package-tile__options__note">Monthly payment option available.</div>
            <div class="package-tile__options__checkbox">
                <label>
                    <input type="checkbox" name="addon_social" value="yes" {{ old('addon_social', $record->addon_social) === 'yes' ? 'checked' : '' }}>
                    <span></span>
                    $120
                </label>
            </div>
        </div>

    </div>

</div>
