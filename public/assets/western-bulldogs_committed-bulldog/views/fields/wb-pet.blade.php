
<div class="package-tile">

    <div class="package-tile__inner">

        <div class="package-tile__top package-tile__top--pet"></div>

        <div class="package-tile__main">

            <div class="package-tile__main__desc">Is your pet a Bulldog in disguise? Sign your pet up to the Kennel and receive an exclusive mat, and pet tag and certificate. Woof Woof!</div>

        </div>

        <div class="package-tile__options">
            <div class="package-tile__options__note">Monthly payment option available.</div>
            <div class="package-tile__options__checkbox">
                <label>
                    <input type="checkbox" name="addon_pet" value="yes" {{ old('addon_pet', $record->addon_pet) === 'yes' ? 'checked' : '' }}>
                    <span></span>
                    $50
                </label>
            </div>
        </div>

    </div>

</div>
