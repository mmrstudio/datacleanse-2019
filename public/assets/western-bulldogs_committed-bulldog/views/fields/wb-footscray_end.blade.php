
<div class="package-tile">

    <div class="package-tile__inner">

        <div class="package-tile__top package-tile__top--footscray_end"></div>

        <div class="package-tile__main">

            <div class="package-tile__main__desc"><strong>Experience the action and the noise</strong><br> This family friendly place is for our fans who like to get loud and enjoy the footy while surrounded by fellow passionate fans. The Footscray End is also home to our cheer squad and has plenty of activities for the kids.</div>

        </div>

        <div class="package-tile__options">
            <div class="package-tile__options__note">Adult price. Monthly payment option available.</div>
            <div class="package-tile__options__checkbox">
                <label>
                    <input type="checkbox" name="addon_footscray_end" value="yes" {{ old('addon_footscray_end', $record->addon_footscray_end) === 'yes' ? 'checked' : '' }}>
                    <span></span>
                    $325
                </label>
            </div>
        </div>

    </div>

</div>
