
<div class="package-tile">

    <div class="package-tile__inner">

        <div class="package-tile__top package-tile__top--premiership"></div>

        <div class="package-tile__main">

            <div class="package-tile__main__desc"><strong>Best seats in the house</strong><br>The best seats in the house among your fellow Western Bulldogs members located on the wing from the elevated position of level 2. Only the coaches have a better view! Includes EJ Whitten Social Club.</div>

        </div>

        <div class="package-tile__options">
            <div class="package-tile__options__note">Adult price. Monthly payment option available.</div>
            <div class="package-tile__options__checkbox">
                <label>
                    <input type="checkbox" name="addon_premiership" value="yes" {{ old('addon_premiership', $record->addon_premiership) === 'yes' ? 'checked' : '' }}>
                    <span></span>
                    $70
                </label>
            </div>
        </div>

    </div>

</div>
