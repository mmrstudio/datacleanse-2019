
<div class="package-tile">

    <div class="package-tile__inner">

        <div class="package-tile__top package-tile__top--mug"></div>

        <div class="package-tile__main">

            <div class="package-tile__main__desc">Let your tea or coffee #bemorebulldog with this exclusive member-only mug.</div>

        </div>

        <div class="package-tile__options">
            <div class="package-tile__options__note">Monthly payment option available.</div>
            <div class="package-tile__options__checkbox">
                <label>
                    <input type="checkbox" name="addon_mug" value="yes" {{ old('addon_mug', $record->addon_mug) === 'yes' ? 'checked' : '' }}>
                    <span></span>
                    $15
                </label>
            </div>
        </div>

    </div>

</div>
