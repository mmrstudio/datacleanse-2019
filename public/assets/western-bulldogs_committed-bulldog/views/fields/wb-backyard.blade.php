
<div class="package-tile">

    <div class="package-tile__inner">

        <div class="package-tile__top package-tile__top--backyard"></div>

        <div class="package-tile__main">

            <div class="package-tile__main__desc">Your donation of just $55 provides a seat in the Bulldogs Backyard for nine children throughout the season. Each donor receives a special certificate of appreciation and makes an excellent gift idea or add-on to an existing membership.</div>

        </div>

        <div class="package-tile__options">
            <div class="package-tile__options__note">Monthly payment option available.</div>
            <div class="package-tile__options__checkbox">
                <label>
                    <input type="checkbox" name="addon_backyard" value="yes" {{ old('addon_backyard', $record->addon_backyard) === 'yes' ? 'checked' : '' }}>
                    <span></span>
                    $55
                </label>
            </div>
        </div>

    </div>

</div>
