
<div class="package-tile">

    <div class="package-tile__inner">

        <div class="package-tile__top package-tile__top--baby"></div>

        <div class="package-tile__main">

            <div class="package-tile__main__desc"><strong>Perfect for the young Pups!</strong><br>You’re never too young to #bemorebulldog – this membership starts the Bulldog journey and comes with a plush football and baby milestone cards.</div>

        </div>

        <div class="package-tile__options">
            <div class="package-tile__options__note">Monthly payment option available.</div>
            <div class="package-tile__options__checkbox">
                <label>
                    <input type="checkbox" name="addon_baby" value="yes" {{ old('addon_baby', $record->addon_baby) === 'yes' ? 'checked' : '' }}>
                    <span></span>
                    $40
                </label>
            </div>
        </div>

    </div>

</div>
