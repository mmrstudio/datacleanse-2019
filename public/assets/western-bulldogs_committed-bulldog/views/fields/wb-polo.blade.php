
<div class="package-tile">

    <div class="package-tile__inner">

        <div class="package-tile__top package-tile__top--polo"></div>

        <div class="package-tile__main">

            <div class="package-tile__main__desc">It’s back due to popular demand in 2016 – this season’s member polo features a new design. Sizes XXS to 7XL. Get in fast to secure your size before they sell out!</div>

        </div>

        <div class="package-tile__options">
            <div class="package-tile__options__note">Monthly payment option available.</div>
            <div class="package-tile__options__select">
                <select name="addon_polo_size">
                    <option value="">Choose size...</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == 'XXS') selected @endif>XXS</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == 'XS') selected @endif>XS</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == 'S') selected @endif>S</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == 'M') selected @endif>M</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == 'L') selected @endif>L</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == 'XL') selected @endif>XL</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == '2XL') selected @endif>2XL</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == '3XL') selected @endif>3XL</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == '4XL') selected @endif>4XL</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == '5XL') selected @endif>5XL</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == '6XL') selected @endif>6XL</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == '7XL') selected @endif>7XL</option>
                </select>
            </div>
            <div class="package-tile__options__checkbox has-select">
                <label>
                    <input type="checkbox" name="addon_polo" value="yes" {{ old('addon_polo', $record->addon_polo) === 'yes' ? 'checked' : '' }}>
                    <span></span>
                    $40
                </label>
            </div>
        </div>

    </div>

</div>
