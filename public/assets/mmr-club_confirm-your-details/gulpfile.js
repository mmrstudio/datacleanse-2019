
var originalGulpPath = process.cwd();

// change gulp path to reference parent node_modules
process.chdir('../../../');

var gulp = require('gulp');
var sass = require('gulp-sass');
var fileinclude = require('gulp-file-include');
var uglify = require('gulp-uglify');
var cssmin = require('gulp-cssmin');
var autoprefixer = require('gulp-autoprefixer');

// change gulp path back to current directory
process.chdir(originalGulpPath);

// WATCH

// gulp.task('watch', [], function () {
//     gulp.watch('_dev/scss/**/*.scss', ['css']);
//     gulp.watch('_dev/js/**/*.js', ['js']);
// });

// TASKS

function buildCss() {

    return gulp.src('_dev/scss/campaign.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({ browsers: ['last 2 versions'], cascade: false }))
        //.pipe(cssmin())
        .pipe(gulp.dest('css'));

};

function buildJs() {

    return gulp.src(['_dev/js/campaign.js'])
        .pipe(fileinclude({ prefix: ' @@', basepath: '@file' }))
        .pipe(uglify())
        .pipe(gulp.dest('js'));

};

function publishCss() {

    return gulp.src('css/campaign.css')
        .pipe(autoprefixer({ browsers: ['last 2 versions'], cascade: false }))
        .pipe(cssmin())
        .pipe(gulp.dest('css'));

}

function publishJs() {

    return gulp.src(['js/campaign.js'])
        .pipe(uglify())
        .pipe(gulp.dest('js'));

}

exports.css = gulp.series(buildCss);
exports.js = gulp.series(buildJs);
exports.build = gulp.parallel(buildCss, buildJs);
exports.publish = gulp.series(publishCss, publishJs);

