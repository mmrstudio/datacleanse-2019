<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Confirm or update your details</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0 " />
<meta name="format-detection" content="telephone=no" />
<base href="<?php echo url('assets/mmr-club_confirm-your-details/edm/test'); ?>">
<style type="text/css">
body {
	margin: 0 !important;
	padding: 0 !important;
	-webkit-text-size-adjust: 100% !important;
	-ms-text-size-adjust: 100% !important;
	-webkit-font-smoothing: antialiased !important;
}
.visible-mobile {
  max-height: 0;
  width: 0;
  display: none;
}
img {
	border: 0 !important;
	outline: none !important;
}
p {
	Margin: 0px !important;
	Padding: 0px !important;
}
table {
	border-collapse: collapse;
	mso-table-lspace: 0px;
	mso-table-rspace: 0px;
}
td, a, span {
	border-collapse: collapse;
	mso-line-height-rule: exactly;
}
.ExternalClass * {
	line-height: 100%;
}
.em_defaultlink a {
	color: inherit !important;
	text-decoration: none !important;
}
span.MsoHyperlink {
	mso-style-priority: 99;
	color: inherit;
}
span.MsoHyperlinkFollowed {
	mso-style-priority: 99;
	color: inherit;
}
.em_text { font-family:Arial,sans-serif; font-size:14px; line-height:16px;  color:#ee4049;}
.em_text a { text-decoration:none; color:#E0E0E0;}
.em_text1, .em_text1 a { font-family: Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; color: #1D252D; text-decoration: none;   }
.em_text2, .em_text2 a { font-family: Helvetica, Arial, sans-serif; font-size: 22px; line-height: 26px; color:#1D252D; text-decoration: none; font-weight:bold; text-transform: uppercase;
	}
.em_text3, .em_text3 a { font-family: Helvetica, Arial, sans-serif; font-size: 18px; line-height: 20px; color: #1D252D; text-decoration: none; }
.em_text4, .em_text4 a { font-family: Helvetica, Arial, sans-serif; font-size: 11px; line-height: 16px; color: #1D252D; text-decoration: none; }
.em_text5, .em_text5 a { font-family: Helvetica, Arial, sans-serif; font-size: 20px; line-height: 28px; color: #1D252D; text-decoration: none; }
.preheader {
    display: none;
}
/*Stylesheet for the devices width between 481px to 660px*/
@media only screen and (min-width:481px) and (max-width:659px) {
.em_main_table { width:100% !important; }
.em_wrapper { width: 100% !important; }
.em_side{ padding: 0 50px !important; }
.em_aside { padding: 0 20px !important; /*Update the values as required*/ }
.em_aside2 { padding: 0 0px !important; /*Update the values as required*/ }
.em_aside3 { padding-left:20px!important;padding-right:20px!important; /*Update the values as required*/ }
.em_hide { display: none !important; }
.em_full_img { width: 100% !important; height: auto !important; max-width:none !important; }
.em_align_cent { text-align: center !important; }
.em_height { height:20px !important; font-size:0px !important; line-height:0px !important;}
.em_background { background-image:none  !important; height:auto !important; }
.em_pad { padding-top:20px !important;}
.em_side2 { width:20px !important;}
.em_side10 { width: 10px !important; /*Update the values as required*/ }
.em_text1, .em_text1 a {  font-size: 14px; line-height: 20px;	}
.em_text5, .em_text5 a { font-family: Helvetica, Arial, sans-serif; font-size: 17px; line-height: 24px; color: #A5ACAF; text-decoration: none; }		
.em_pad_bottom {
	padding-bottom: 14px !important;
}
.em_center1{
	text-align: center!important;
	height: inherit!important;
	border-bottom: none!important;
	padding-top: 8px!important;
}
.em_center2{
	text-align: center!important;
	height: inherit!important;
	padding-bottom: 8px!important;
}
.visible-mobile {
		display: block !important;
		max-height: none !important;
		width:100%!important;
		max-width: 100% !important;
		height: auto !important;
	  }
}
/*Stylesheet for the devices width between 0px to 480px*/
@media only screen and (max-width:480px) {
.em_main_table { width: 100% !important; }
.em_wrapper { width: 100% !important; }
.em_side{ padding: 0 50px !important; }
.em_aside { padding: 0 20px !important; /*Update the values as required*/ }
.em_aside2 { padding: 0 0px !important; /*Update the values as required*/ }
.em_aside3 { padding-left:20px!important;padding-right:20px!important; /*Update the values as required*/ }
.em_hide { display: none !important; }
.em_full_img { width: 100% !important; height: auto !important; max-width:none !important; }
.em_align_cent { text-align: center !important; }
.em_height { height:20px !important; font-size:0px !important; line-height:0px !important;}
.em_background { background-image:none  !important; height:auto !important; }
.em_pad { padding-top:20px !important;}
.em_side2 { width:20px !important;}
.em_side10 { width: 10px !important; /*Update the values as required*/ }
.em_text1, .em_text1 a {  font-size: 14px; line-height: 20px;	}	
.em_text5, .em_text5 a { font-family: Helvetica, Arial, sans-serif; font-size: 17px; line-height: 24px; color: #A5ACAF; text-decoration: none; }	
.em_br {
	display: block;
}
.em_pad_bottom {
	padding-bottom: 14px !important;
}
.em_center1{
	text-align: center!important;
	height: inherit!important;
	border-bottom: none!important;
	padding-top: 8px!important;
}
.em_center2{
	text-align: center!important;
	height: inherit!important;
	padding-bottom: 8px!important;
}
.visible-mobile {
	display: block !important;
	max-height: none !important;
	width:100%!important;
	max-width: 100% !important;
	height: auto !important;
  }
}
</style>
<!--[if gte mso 9]>
<xml>
  <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
 </o:OfficeDocumentSettings>
</xml>
<![endif]-->
</head>
<body style="margin: 0px;padding: 0px;-webkit-text-size-adjust: 100% !important;-ms-text-size-adjust: 100% !important;-webkit-font-smoothing: antialiased !important;" bgcolor="#E0E0E0"><span class="preheader" style="display: none;">Simply update or confirm your details for a chance to win lunch for two people in The Promenade by Julian Robertshaw at Caulfield racecourse.</span>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#E0E0E0" style="border-collapse: collapse;mso-table-lspace: 0px;mso-table-rspace: 0px;">
    	<tr>
        	<td>
            	<table width="700" cellpadding="0" cellspacing="0" border="0" align="center" class="em_main_table">
                	<tr>
                    	<td valign="top" align="center">
                    		<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                       			<tr>
										<td height="20" class="em_height">&nbsp;</td>
								</tr>
                        		<tr>
                        			<td align="center" class="em_text4">
                    					If you are having trouble viewing this email,<span class="em_br" style="border-collapse: collapse;mso-line-height-rule: exactly;"></span> please <webversion>click here</webversion> to view it as a webpage.
                    				</td>
								</tr>
                  				<tr>
										<td height="20" class="em_height">&nbsp;</td>
								</tr>
                   			</table>
                    	</td>
                    </tr>
                    <tr>
                    	<td valign="top" colspan="1">
                    		<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                        		<tr>
                        			<td align="center">
                    					<a href="https://mmr.com.au/"><img src="images/img_03.jpg" editable="true" width="700" border="0" style="display:block; max-width: 700px;" class="em_hide" alt="Confirm or update your details"></a>
                    				</td>
                    				<!--[if !mso]><!-->
									<td valign="top" colspan="1">
											<a href="https://mrc.racing.com/"><img src="images/mobile_01.jpg" width="320" height="250" border="0" style="-moz-hyphens:none;-ms-hyphens:none;-ms-interpolation-mode:bicubic;-webkit-hyphens:none;border:none;clear:both;display:none;hyphens:none;max-height:0;max-width:100%;mso-hide:all;outline:0;text-decoration:none;width:0;word-break:keep-all" alt="Confirm or update your details" class="visible-mobile"></a>
									</td>
									<!--<![endif]-->
								</tr>
                   			</table>
                    	</td>
                    </tr>
                    <tr>
                    	<td valign="top" colspan="1" bgcolor="#ffffff" class="em_aside" style="padding-left: 87px; padding-right: 87px;">
                    		<table width="526" style="width:526px;" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
								<tr>
									<td valign="top" colspan="1" bgcolor="#ffffff">
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="center" style="padding: 35px 0 30px 0; line-height: 24px;">
													 <span class="em_text1"><strong>Hello <?php echo $record->FirstName; ?>,</strong></span><br>
<br>
			<span class="em_text1">Simply update or confirm your details for a chance to win lunch for two people in The Promenade by Julian Robertshaw at Caulfield racecourse.</span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td valign="top" colspan="1" bgcolor="#ffffff">
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="center" style="padding-bottom: 20px;">
													 <span class="em_text2">ARE YOUR <br class="visible-mobile">
 DETAILS CORRECT?</span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								 <tr>
									<td valign="top" colspan="1" bgcolor="#ffffff">
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="center" style="padding: 0px 0 15px; line-height: 24px;">
													 <span class="em_text">First Name</span><br>
													<span class="em_text3"><?php echo $record->FirstName ? $record->FirstName : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 15px; line-height: 24px;">
													 <span class="em_text">Surname</span><br>
													<span class="em_text3"><?php echo $record->LastName ? $record->LastName : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 15px; line-height: 24px;">
													 <span class="em_text">Address</span><br>
													<span class="em_text3"><?php echo $record->Address ? $record->Address : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 15px; line-height: 24px;">
													 <span class="em_text">Suburb</span><br>
													<span class="em_text3"><?php echo $record->City ? $record->City : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 15px; line-height: 24px;">
													 <span class="em_text">State</span><br>
													<span class="em_text3"><?php echo $record->State ? $record->State : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 15px; line-height: 24px;">
													 <span class="em_text">Postcode</span><br>
													<span class="em_text3"><?php echo $record->PostCode ? $record->PostCode : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 15px; line-height: 24px;">
													 <span class="em_text">Phone</span><br>
													<span class="em_text3"><?php echo $record->PersonalPhone ? $record->PersonalPhone : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 15px; line-height: 24px;">
													 <span class="em_text">Mobile</span><br>
													<span class="em_text3"><?php echo $record->Mobile ? $record->Mobile : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 15px; line-height: 24px;">
													 <span class="em_text">Birthday</span><br>
													<span class="em_text3"><?php echo $record->Birthday ? 'Supplied' : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 15px; line-height: 24px;">
													 <span class="em_text">Email</span><br>
													<span class="em_text3"><a href="mailto:<?php echo $record->Email; ?>" style="text-decoration:none;"><?php echo $record->Email; ?></a></span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								 <tr>
									<td align="center" valign="top" colspan="1" bgcolor="#ffffff" style="padding-bottom:35px; padding-top:33px;">
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td>
													<table width="251" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="left">
														<tr>
															<td valign="top" align="right" style="" class="em_pad_bottom">
																<a href="<?php echo $record->confirm_url; ?>"><img src="images/img_07.jpg"  border="0" style="display:block; max-width: 251px;" class="em_full_img"></a>
															</td>
														</tr>
													</table>
													<table width="251" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="right">
														<tr>
															<td valign="top" align="left">
																  <a href="<?php echo $record->update_url; ?>"><img src="images/img_09.jpg" border="0" style="display:block; max-width: 251px;" class="em_full_img"></a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
                  				<tr>
									<td valign="top" colspan="1" bgcolor="#ffffff">
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="center" style="padding: 0px 0 35px 0; line-height: 20px;">
													<span class="em_text1">Click on the buttons to confirm or update your details.<br class="em_hide">
Do not reply to this email.</span>
													<span class="em_text1" style="color: #C5B783"><br><br>If you have any questions please contact us at <a href="mailto:membership@mrc.net.au" style="color: #C5B783; text-decoration: underline;">membership@mrc.net.au</a></span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
                   			</table>
                    	</td>
                    </tr>
				</table>
     		</td>
        </tr>
         <tr>
        	<td>
            	<table width="700" cellpadding="0" cellspacing="0" border="0" align="center" class="em_main_table">
                   	     <tr>
          <td height="10" style="line-height: 1px;font-size: 1px;border-collapse: collapse;mso-line-height-rule: exactly;" bgcolor="#ee4049">&nbsp;</td>
        </tr>
        <!-- == Footer Section -->
        <tr>
          <td align="center" valign="top" bgcolor="#ffffff" style="border-collapse: collapse;mso-line-height-rule: exactly;"><table width="700" border="0" cellspacing="0" cellpadding="0" align="center" style="width: 700px;border-collapse: collapse;mso-table-lspace: 0px;mso-table-rspace: 0px;" class="em_wrapper">
              <tr>
                <td width="70" style="width: 70px;border-collapse: collapse;mso-line-height-rule: exactly;" class="em_side">&nbsp;</td>
                <td align="center" valign="top" style="border-collapse: collapse;mso-line-height-rule: exactly;"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse: collapse;mso-table-lspace: 0px;mso-table-rspace: 0px;">
                    <tr>
                      <td height="49" style="height: 49px;border-collapse: collapse;mso-line-height-rule: exactly;">&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="center" valign="top" style="border-collapse: collapse;mso-line-height-rule: exactly;"><a href="http://www.mmr.com.au/" target="_blank" style="text-decoration: none;border-collapse: collapse;mso-line-height-rule: exactly;"><img src="images/footer_logo.jpg" width="145" height="44" alt="MMR" border="0" style="display: block;font-family: Arial, sans-serif;font-size: 25px;line-height: 44px;text-align: center;color: #000000;font-weight: bold;border: 0 !important;outline: none !important;"></a></td>
                    </tr>
                    <tr>
                      <td height="30" style="height: 30px;border-collapse: collapse;mso-line-height-rule: exactly;">&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="center" valign="top" class="footer_text1" style="border-collapse: collapse;mso-line-height-rule: exactly;font-family: Arial, sans-serif;font-size: 18px;line-height: 21px;color: #000000;font-weight: bold;"><a href="#" target="_blank" style="text-decoration: none;color: #000000;border-collapse: collapse;mso-line-height-rule: exactly;">Melbourne</a>&nbsp;&nbsp; | &nbsp;&nbsp;<a href="#" target="_blank" style="text-decoration: none;color: #000000;border-collapse: collapse;mso-line-height-rule: exactly;">Sydney</a></td>
                    </tr>
                    
                    <!-- Mobile Show --> 
                    <!--[if !mso 9]><!-->
                    <tr>
                      <td valign="top" align="center" height="0" style="border-collapse: collapse;mso-line-height-rule: exactly;"><div class="em_hide_desktop" style="display:none;width:0;overflow:hidden;max-height:0!important">
                          <table align="center" width="100%" border="0" cellspacing="0" cellpadding="0" class="em_hide_desktop" style="border-collapse: collapse;mso-table-lspace: 0px;mso-table-rspace: 0px;">
                            <tr>
                              <td height="7" style="font-size: 1px;line-height: 1px;border-collapse: collapse;mso-line-height-rule: exactly;">&nbsp;</td>
                            </tr>
                            <tr>
                              <td align="center" valign="top" class="footer_text3" style="border-collapse: collapse;mso-line-height-rule: exactly;font-family: Arial, sans-serif;font-size: 18px;line-height: 21px;color: #000000;"><span style="font-weight: bold;border-collapse: collapse;mso-line-height-rule: exactly;">T:</span> <a href="tel:+61390907000" style="text-decoration: none;color: #000000;border-collapse: collapse;mso-line-height-rule: exactly;">+61 3 9090 7000</a></td>
                            </tr>
                            <tr>
                              <td height="5" style="font-size: 1px;line-height: 1px;border-collapse: collapse;mso-line-height-rule: exactly;">&nbsp;</td>
                            </tr>
                            <tr>
                              <td align="center" valign="top" class="footer_text1" style="border-collapse: collapse;mso-line-height-rule: exactly;font-family: Arial, sans-serif;font-size: 18px;line-height: 21px;color: #000000;font-weight: bold;">E: <span style="color: #ee4049;border-collapse: collapse;mso-line-height-rule: exactly;"><a href="mailto:firstcontact@mmr.com.au" style="text-decoration: none;color: #ee4049;border-collapse: collapse;mso-line-height-rule: exactly;">firstcontact@mmr.com.au</a></span></td>
                            </tr>
                          </table>
                        </div></td>
                    </tr>
                    <!--<![endif]--> 
                    <!-- //Mobile Show -->
                    <tr>
                      <td height="28" class="em_h20" style="height: 28px;border-collapse: collapse;mso-line-height-rule: exactly;">&nbsp;</td>
                    </tr>
                    <tr>
                      <td valign="top" align="center" style="border-collapse: collapse;mso-line-height-rule: exactly;"><table border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse: collapse;mso-table-lspace: 0px;mso-table-rspace: 0px;">
                          <tr>
                            <td width="26" valign="top" align="center" style="border-collapse: collapse;mso-line-height-rule: exactly;"><a href="https://www.facebook.com/mmragency/" target="_blank" style="text-decoration: none;border-collapse: collapse;mso-line-height-rule: exactly;"><img src="images/fb.jpg" width="26" height="26" alt="Fb" border="0" style="display: block;font-family: Arial, sans-serif;font-size: 15px;line-height: 30px;font-weight: bold;text-align: center;color: #000000;border: 0 !important;outline: none !important;"></a></td>
                            <td width="13" style="width: 13px;border-collapse: collapse;mso-line-height-rule: exactly;">&nbsp;</td>
                            <td width="27" valign="top" align="center" style="border-collapse: collapse;mso-line-height-rule: exactly;"><a href="https://www.instagram.com/mmragency/" target="_blank" style="text-decoration: none;border-collapse: collapse;mso-line-height-rule: exactly;"><img src="images/insgt.jpg" width="27" height="26" alt="ins" border="0" style="display: block;font-family: Arial, sans-serif;font-size: 15px;line-height: 30px;font-weight: bold;text-align: center;color: #000000;border: 0 !important;outline: none !important;"></a></td>
                            <td width="13" style="width: 13px;border-collapse: collapse;mso-line-height-rule: exactly;">&nbsp;</td>
                            <td width="27" valign="top" align="center" style="border-collapse: collapse;mso-line-height-rule: exactly;"><a href="https://www.linkedin.com/company/mmr-studio" target="_blank" style="text-decoration: none;border-collapse: collapse;mso-line-height-rule: exactly;"><img src="images/in.jpg" width="27" height="26" alt="In" border="0" style="display: block;font-family: Arial, sans-serif;font-size: 15px;line-height: 30px;font-weight: bold;text-align: center;color: #000000;border: 0 !important;outline: none !important;"></a></td>
                            <td width="13" style="width: 13px;border-collapse: collapse;mso-line-height-rule: exactly;">&nbsp;</td>
                            <td width="26" valign="top" align="center" style="border-collapse: collapse;mso-line-height-rule: exactly;"><a href="http://www.mmr.com.au/the-den/" target="_blank" style="text-decoration: none;border-collapse: collapse;mso-line-height-rule: exactly;"><img src="images/img_3.jpg" width="26" height="26" alt="" border="0" style="display: block;font-family: Arial, sans-serif;font-size: 15px;line-height: 30px;font-weight: bold;text-align: center;color: #000000;border: 0 !important;outline: none !important;"></a></td>
                          </tr>
                        </table></td>
                    </tr>
                    <tr>
                      <td height="28" style="height: 28px;border-collapse: collapse;mso-line-height-rule: exactly;">&nbsp;</td>
                    </tr>
                    <tr>
						<td align="center" class="footer_text2" style="border-collapse: collapse;mso-line-height-rule: exactly;font-family: Arial, sans-serif;font-size: 12px;line-height: 17px;color: #000000;"><unsubscribe style="text-decoration: none;color: #000000;border-collapse: collapse;mso-line-height-rule: exactly;">Unsubscribe</unsubscribe> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="http://www.mmr.com.au/privacy-policy/" target="_blank" style="text-decoration: none;color: #000000;border-collapse: collapse;mso-line-height-rule: exactly;">Privacy Policy</a><br>
                        Copyright &copy; 2019 MMR. All rights reserved.</td>
                    </tr>
                    <tr>
                      <td height="49" class="em_h40" style="height: 49px;border-collapse: collapse;mso-line-height-rule: exactly;">&nbsp;</td>
                    </tr>
                  </table></td>
                <td width="67" style="width: 67px;border-collapse: collapse;mso-line-height-rule: exactly;" class="em_side">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
                  
                 </table>
            </td>
       	</tr>
    </table>
   
<div class="em_hide" style="white-space:nowrap;font:20px courier;color:#ffffff;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
</td></tr></table></body>
</html>
