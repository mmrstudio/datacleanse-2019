@extends($campaign->view('main'))

@section('content')
<div class="body-wrap__inner">

	<div class="main">

        <header class="header">
            <div class="header__logo">
            </div>
        </header>

		<form action="{{ $process_url }}" method="post" class="form">

			@include($campaign->view('common.errors'))

			<div class="form__inner">

				<div class="form__top">
					<h1 class="form__top__title title">{{ $campaign->title }}</h1>
                    @if($campaign->creative && $campaign->creative->prize_image)
					<div class="form__top__prize"><img src="{{ $campaign->asset('images/prize-image.jpg') }}"></div>
                    @endif
                    @if($campaign->intro)
					<div class="form__top__intro intro">{!! $campaign->getIntro($record) !!}</div>
                    @endif
				</div>

				<div class="form__fields">

                    <h2 class="form__heading heading">Are your details correct?</h2>

					@include($campaign->view('fields.text'), [
						'key' => 'FirstName',
                        'width' => 'half',
                        'props' => []
					])

                    @include($campaign->view('fields.text'), [
						'key' => 'LastName',
                        'width' => 'half',
                        'props' => []
					])


                    @include($campaign->view('fields.address'), [
						'keys' => ['Address'],
					])


					@include($campaign->view('fields.country'), [
				        'key' => 'Country',
                    ])

                    @include($campaign->view('fields.text'), [
						'key' => 'City',
						'id' => 'suburbField',
                        'width' => 'third',
					])

                    @include($campaign->view('fields.state'), [
						'key' => 'State',
                        'width' => 'third',
					])

                    @include($campaign->view('fields.text'), [
						'key' => 'PostCode',
						'id' => 'postcodeField',
                       'width' => 'third',
					])

                   @include($campaign->view('fields.text'), [
						'key' => 'PersonalPhone',
                        'width' => 'half',
					])

					@include($campaign->view('fields.text'), [
						'key' => 'Mobile',
                        'width' => 'half',
					])

					@include($campaign->view('fields.email'), [
						'key' => 'Email',
                        'width' => 'full',
                    ])
                    
                    @include($campaign->view('fields.text'), [
						'key' => 'Birthday',
                        'width' => 'full',
                        'not_required' => false,
					])
                    
                    @include($campaign->view('fields.dropdown'), [
						'key' => 'AflTeam',
                        'width' => 'full',
                    ])

                    @include($campaign->view('fields.file'), [
						'key' => 'Photo',
                        'width' => 'full',
                        'props' => []
					])

                  

				</div>

				<div class="form__submit">

					@foreach($hidden_fields as $name=>$value)
					<input type="hidden" name="{{ $name }}" value="{{ $value }}">
					@endforeach

					<button class="form__submit__button" type="submit">{{ $campaign->creative->button_text }}</button>

				</div>

			</div>

		</form>

	</div>

</div>

@include($campaign->view('common.footer'))

@endsection
