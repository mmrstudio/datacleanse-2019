<?php
    if(! isset($fields[$key])) return '';
    $field = $fields[$key];
    $id = isset($id) ? $id : camel_case($field->name . '_field');
    $label = isset($label) ? $label : $field->front_label;
    $control_label = isset($control_label) ? $control_label : false;
    $placeholder = isset($placeholder) ? $placeholder : $field->front_placeholder;
    $props = isset($props) ? implode(' ', $props) : false;
    $form_group_classes = ['form__group', 'form__group--' . $field->name];
    if($errors->has($field->name)) $form_group_classes[] = 'form__group--error';
    $field_classes = ['form__control', 'form__control--file', 'form__control--' . $field->name];
    $width = isset($width) ? $width : 'full';
?>

<div class="{{ implode(' ', $form_group_classes) }}" data-width="{{ $width }}" @if($id)id="{{ $id }}"@endif>
    <label class="form__group__label" for="{{ $field->name }}">
        {!! $label !!}
        @if($field->required)<span class="required">*</span>@endif
    </label>
    <div class="form__group__controls">
        @if($control_label)
        <label class="form__group__controls__label" for="{{ $field->name }}">
            {{ $control_label }}
        </label>
        @endif
        <input id="{{ $field->name }}" name="{{ $field->name }}" type="file" placeholder="{{ $placeholder }}" class="{{ implode(' ', $field_classes) }}" value="{{ old($field->name, $record->{$field->name}) }}" {{ $props }}>
        <span class="form__group__border"></span>
    </div>
</div>
