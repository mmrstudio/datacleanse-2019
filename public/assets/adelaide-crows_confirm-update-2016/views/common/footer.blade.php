
    <div class="footer__sponsors">
        <img src="{{ $campaign->asset('images/sponsors.png') }}">
    </div>

    <p>Adelaide Football Club | (08) 8440 6666 | <a href="mailto:membership@afc.com.au">membership@afc.com.au</a></p>

    <ul class="footer__links">
        <li><a href="http://www.afc.com.au/" target="_blank">Adelaide Football Club</a></li>
        <li><a href="http://www.afc.com.au/privacy" target="_blank">Privacy Policy</a></li>
        <li><a href="{{ $campaign->asset('AFC_ConfirmUpdate_TermsConditions.pdf') }}" target="_blank">Terms &amp; Conditions</a></li>
        <li><a href="http://datacleanse.com.au" target="_blank">Product of Datacleanse</a></li>
    </ul>
