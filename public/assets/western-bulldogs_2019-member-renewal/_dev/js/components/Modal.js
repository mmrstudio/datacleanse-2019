import React from 'react';
import ReactDOM from 'react-dom';
import Button from './Button.js';

class Modal extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            visible: false
        };

        this._renderModal = this._renderModal.bind(this);
        this._showModal = this._showModal.bind(this);
        this._hideModal = this._hideModal.bind(this);
        this._buttonAction = this._buttonAction.bind(this);
    }

    componentDidMount() {

        if(this.props.visible) {
            this.setState({ visible: this.props.visible });
        }

        this.modal = document.createElement("div");
        this.modal.className = 'modal__wrap ' + this.props.classes;
        document.body.appendChild(this.modal);
        this._renderModal();
    }

    componentWillUnmount() {
        ReactDOM.unmountComponentAtNode(this.modal);
        document.body.removeChild(this.modal);
    }

    componentDidUpdate() {
        this._renderModal();
    }

    _showModal() {
        this.setState({ visible: true });
        //document.body.classList.add(this.props.classes + '-visible');

        if(this.props.onShow) this.props.onShow();

    }

    _hideModal() {
        this.setState({ visible: false });
        //document.body.classList.remove(this.props.classes + '-visible');

        if(this.props.onHide) this.props.onHide();

    }

    _buttonAction() {
        this.props.buttonAction();
    }

    _renderModal() {

        let modalButton = false;
        if(this.props.button) {
            modalButton = (
                <div className="modal__button">
                    <Button onClick={this._buttonAction} disabled={this.props.buttonDisabled}>{this.props.button}</Button>
                </div>
            );
        }

        let modal = (
            <div className="modal" data-visible={this.props.visible}>
                <div className="modal__body" data-button-visible={modalButton !== false}>
                    <div className="modal__body__inner">
                        <h2>Please check the following:</h2>
                        {this.props.children}
                    </div>
                    {modalButton}
                </div>
                <div className="modal__overlay"></div>
            </div>
        );

        ReactDOM.render(modal, this.modal);
    }

    render() {
        return (<div></div>);
    }

}

export default Modal;
