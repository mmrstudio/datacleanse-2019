<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">

    <head>

    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    	<title>{{ strip_tags($campaign->campaignTitle()) }}</title>

    	<link href="{{ $campaign->asset('css/campaign.css') }}" rel="stylesheet">
    	<link href="{{ $campaign->asset('fonts/font.css') }}" rel="stylesheet">

        <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
        <link rel="icon" href="{{ $campaign->asset('icon.ico') }}" />

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-33851616-15', 'auto');
          ga('send', 'pageview');

        </script>

        <script src="{{ $campaign->asset('js/modernizr.js') }}"></script>

        @if($campaign->creative)
        <style>

            body {
                font-family: {{ $campaign->creative->body_font }}, Helvetica, Arial, sans-serif;
                background: url({{ $campaign->asset('images/hero-banner.jpg') }}) no-repeat center top/1800px auto {{ $campaign->creative->body_background_color }};
                color: {{ $campaign->creative->body_color }};
            }

            .title {
                font-family: {{ $campaign->creative->title_font }}, Helvetica, Arial, sans-serif;
                font-size: {{ $campaign->creative->title_size['desktop'] }}rem;
                line-height: {{ $campaign->creative->title_line_height }};
                color: {{ $campaign->creative->title_color }};
            }
			
			 .title span{
                font-family: {{ $campaign->creative->heading_font }}, Helvetica, Arial, sans-serif;
                font-size:2.375rem;
                line-height: 1.3;
				display: block;
                color: {{ $campaign->creative->body_background_color }};
            }

            .heading {
                font-family: {{ $campaign->creative->heading_font }}, Helvetica, Arial, sans-serif;
                font-size: {{ $campaign->creative->heading_size['desktop'] }}rem;
                line-height: {{ $campaign->creative->heading_line_height }};
                color: {{ $campaign->creative->heading_color }};
            }

            .form__inner { background-color: {{ $campaign->creative->form_background_color }}; }
            .form__group { font-size: {{ $campaign->creative->field_size['desktop'] }}rem; }
            .form__group__border { background-color: {{ $campaign->creative->field_border_color }}; }
            .form__group__label {  font-family: {{ $campaign->creative->heading_font }}, Helvetica, Arial, sans-serif; color: {{ $campaign->creative->field_label_color }}; }
            .form__control { color: {{ $campaign->creative->field_text_color }}; }
            .form__control:focus { color: {{ $campaign->creative->field_focus_color }}; }
            .form__control:focus + .form__group__border { background-color: {{ $campaign->creative->field_focus_border_color }}; }
            .form__group--error .form__group__label { color: {{ $campaign->creative->field_error_color }}; }
            .form__group--error .form__group__border { background-color: {{ $campaign->creative->field_error_color }}; }
            .form__errors { background-color: {{ $campaign->creative->field_error_color }}; }
            .form__control--checkbox + span { border-color: {{ $campaign->creative->button_background_color }}; }
            .form__control--checkbox + span:before { color: {{ $campaign->creative->button_background_color }}; }

            ::-webkit-input-placeholder { color: {{ $campaign->creative->field_placeholder_color }}; }
            ::-moz-placeholder { color: {{ $campaign->creative->field_placeholder_color }}; }
            :-ms-input-placeholder { color: {{ $campaign->creative->field_placeholder_color }}; }
            :-moz-placeholder { color: {{ $campaign->creative->field_placeholder_color }}; }

            .form__submit__button {
				background: url({{ $campaign->asset('images/submit_button.png') }}) no-repeat center top/640px auto;
                font-family: {{ $campaign->creative->button_font }}, Helvetica, Arial, sans-serif;
                font-size: 0;
			    text-indent:-9999;
			    width:640px;
			    height:61px;
			    display:block;
			    margin:0 auto;
                color: {{ $campaign->creative->button_foreground_color }};
              
            }

            .form__submit__button:hover {
                opacity: 0.7;
            }

            .footer {
                font-size: {{ $campaign->creative->footer_size['desktop'] }}rem;
                color: {{ $campaign->creative->footer_color }}
            }

            .footer svg path { fill: {{ $campaign->creative->footer_color }} }
            .footer__datacleanse { border-color: {{ $campaign->creative->footer_color }} }
            .footer__datacleanse span { background-color: {{ $campaign->creative->footer_color }}; }
            .footer__links > li + li { border-color: {{ $campaign->creative->footer_color }} }
			
			.footer__text a{ 
				 
				color: #464646;
			}
			
			.footer__text a strong{ 
				font-family: {{ $campaign->creative->heading_font }}, Helvetica, Arial, sans-serif; 
				color: #464646;
			}
			
			.footer__text strong{ 
				font-family: {{ $campaign->creative->heading_font }}, Helvetica, Arial, sans-serif; 
				color: #464646;
			}
			 
			

            .thanks__title {
                font-family: {{ $campaign->creative->thanks_title_font }}, Helvetica, Arial, sans-serif;
                font-size: {{ $campaign->creative->thanks_title_size['desktop'] }}rem;
                color: {{ $campaign->creative->thanks_title_color }};
            }

            .thanks__sub-title {
                font-family: {{ $campaign->creative->thanks_sub_title_font }}, Helvetica, Arial, sans-serif;
                font-size: {{ $campaign->creative->thanks_sub_title_size['desktop'] }}rem;
                color: {{ $campaign->creative->thanks_sub_title_color }};
            }

            .thanks__body {
                font-family: {{ $campaign->creative->thanks_body_font }}, Helvetica, Arial, sans-serif;
                font-size: {{ $campaign->creative->thanks_body_size['desktop'] }}rem;
                color: {{ $campaign->creative->thanks_body_color }};
            }
			
			@media screen and (max-width: 1800px) {
				
				background: url({{ $campaign->asset('images/hero-banner.jpg') }}) no-repeat left top/1800px auto {{ $campaign->creative->body_background_color }};
			}
			
			@media screen and (max-width: 1400px) {
				body{
                background: url({{ $campaign->asset('images/hero-banner.jpg') }}) no-repeat left top/1400px auto {{ $campaign->creative->body_background_color }};}
			
			
				.header__logo{

					max-width: 663px;
				}
				
			}
			
			
			@media screen and (max-width: 1100px) {
				body{font-family: {{ $campaign->creative->body_font }}, Helvetica, Arial, sans-serif;
                background: url({{ $campaign->asset('images/hero-banner.jpg') }}) no-repeat left top/1100px auto {{ $campaign->creative->body_background_color }};}
			
			
				.header__logo{

					max-width: 520px;
				}
				
			}
			
			
			@media screen and (max-width: 900px) {
				body{font-family: {{ $campaign->creative->body_font }}, Helvetica, Arial, sans-serif;
                background: url({{ $campaign->asset('images/hero-banner.jpg') }}) no-repeat left top/900px auto {{ $campaign->creative->body_background_color }};}
			
			
				.header__logo{

					max-width: 426px;
				}
				
			}
			
			@media screen and (max-width: 750px) {
				
				.form__submit__button {
                    font-size: 0;
					max-width: 100%;
					width: 100%;
					background-size: 100% auto;
                }
			}
			
			@media screen and (max-width: 700px) {
				body {
                    background: url({{ $campaign->asset('images/mobile-banner.jpg') }}) no-repeat center top;
					background-size:100% auto;
					padding-top:47%;
                }
			
				body.thanks {
                    background: url({{ $campaign->asset('images/mobile-thankyou.jpg') }}) no-repeat center top;
					background-size:100% auto;
					padding-top:47%;
                }
			
				.header__logo{

					display: none;
				}
				
				
			}

            @media screen and (max-width: 660px) {

                

                /*.header {
                    background-color: {{ $campaign->creative->body_background_color_mobile }};
                }*/

                .title {
                    font-size: {{ $campaign->creative->title_size['mobile'] }}rem;
                }

                .heading {
                    font-size: {{ $campaign->creative->heading_size['mobile'] }}rem;
                }

                .footer {
                    font-size: {{ $campaign->creative->footer_size['mobile'] }}rem;
                }

                .form__group {
                    font-size: {{ $campaign->creative->field_size['mobile'] }}rem;
                }

                

                .thanks__title {
                    font-size: {{ $campaign->creative->thanks_title_size['mobile'] }}rem;
                }

                .thanks__sub-title {
                    font-size: {{ $campaign->creative->thanks_sub_title_size['mobile'] }}rem;
                }

                .thanks__body {
                    font-size: {{ $campaign->creative->thanks_body_size['mobile'] }}rem;
                }

            }

        </style>
        @endif

    </head>

    <body>

        <div class="body-wrap">


                @yield('content')


        </div>

        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    	<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
        <script src="{{ $campaign->asset('js/campaign.js') }}"></script>

    </body>

</html>
