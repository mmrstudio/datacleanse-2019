<?php
    if(! isset($fields[$key])) return '';
    $field = $fields[$key];
    $id = isset($id) ? $id : 'stateField';
    $label = isset($label) ? $label : $field->front_label;
    $control_label = isset($control_label) ? $control_label : false;
    $placeholder = isset($placeholder) ? $placeholder : $field->front_placeholder;
    $options = isset($options) ? $options : $field->getOptions();
    $props = isset($props) ? implode(' ', $props) : false;
    $form_group_classes = ['form__group', 'form__group--select', 'form__group--' . $field->name];
    if($errors->has($field->name)) $form_group_classes[] = 'form__group--error';
    $field_classes = ['form__control--select', 'form__control--' . $field->name];
    $width = isset($width) ? $width : 'full';
?>

<div class="{{ implode(' ', $form_group_classes) }}" data-width="{{ $width }}" data-field-name="{{ $field->name }}" @if($id)id="{{ $id }}"@endif>
    <label class="form__group__label" for="{{ $field->name }}">
        {{ $label }}
        @if($field->required)<span class="required">*</span>@endif
    </label>
    <div class="form__group__controls">
        @if($control_label)
        <label class="form__group__controls__label" for="{{ $field->name }}">
            {{ $control_label }}
        </label>
        @endif

        <div class="state-field-au">
            {!! Form::select($field->name, select('Select state...', australian_states()), old($field->name, $record->{$field->name}), ['class'=>implode(' ', $field_classes), 'data-placeholder' => $field->front_placeholder]) !!}
            <span class="form__group__border"></span>
        </div>
        <div class="state-field-nz">
            {!! Form::select($field->name, select('Select...', nz_states()), old($field->name, $record->{$field->name}), ['class'=>implode(' ', $field_classes), 'data-placeholder' => $field->front_placeholder]) !!}
            <span class="form__group__border"></span>
        </div>
        <div class="state-field-other">
            <input name="{{ $field->name }}_other" type="text" placeholder="Enter state/province..." class="form__control form__control--text form__control--{{ $field->name }}" value="{{ old($field->name . '_other', $record->{$field->name}) }}">
            <span class="form__group__border"></span>
        </div>

    </div>
</div>
