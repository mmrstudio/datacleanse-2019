@extends($campaign->view('main'))

@section('content')

<script>
	document.body.className += 'thanks';
</script>	

<header class="header">
	<div class="header__logo">
		<a href="{{ $campaign->client_url }}/?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank"><img src="{{ $campaign->asset('images/thankyou-header.png') }}"></a>
	</div>
</header>
<div class="body-wrap__inner ">

	<div class="main">

       

        <div class="form__inner">

            

        </div>

	</div>

    @include($campaign->view('common.footer'))

</div>


@endsection
