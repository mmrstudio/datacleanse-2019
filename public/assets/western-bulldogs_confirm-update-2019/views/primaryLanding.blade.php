@extends($campaign->view('main'))

@section('content')

<header class="header">

		<a class="desktop_banner" href="{{ $campaign->client_url }}/?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank"><img src="{{ $campaign->asset('images/hero-banner.jpg') }}"></a>

		<a class="mobile_banner" href="{{ $campaign->client_url }}/?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank"><img src="{{ $campaign->asset('images/mobile-banner.jpg') }}"></a>

</header>
<div class="body-wrap__inner">

	<div class="main">



		<form action="{{ $process_url }}" method="post" class="form">

			@include($campaign->view('common.errors'))

			<div class="form__inner">

				<div class="form__top">
					<h1 class="form__top__title title">{!! $campaign->title !!}</h1>
                    @if($campaign->creative && $campaign->creative->prize_image)
					<div class="form__top__prize"><img src="{{ $campaign->asset('images/prize-image.jpg') }}"></div>
                    @endif
                    @if($campaign->intro)
					<div class="form__top__intro intro">{{ $campaign->intro }}</div>
                    @endif
				</div>

				<div class="form__fields">

                    <h2 class="form__heading heading">Are your details correct?</h2>




					@include($campaign->view('fields.text'), [
						'key' => 'name_first',
                        'width' => 'half',

					])

                    @include($campaign->view('fields.text'), [
						'key' => 'name',
                        'width' => 'half',

					])


					@include($campaign->view('fields.text'), [
						'key' => 'phone_mobile',
                        'width' => 'half',
					])



					@include($campaign->view('fields.text'), [
						'key' => 'phone_home',
                        'width' => 'half',
					])




                    @include($campaign->view('fields.address'), [
						'keys' => ['street_addr_1', 'street_addr_2'],
						'width' => 'two-thirds'
					])

					@if($record->birthdate !== 'Supplied')
						 @include($campaign->view('fields.dob'), [
							 'key' => 'birthdate',
							 'placeholder' => 'Please provide your DOB...',
														 'width' => 'third',
														 'not_required' => false
						 ])
					 @endif


					@include($campaign->view('fields.country'), [
				 'key' => 'country',

				])


                   @include($campaign->view('fields.text'), [
						'key' => 'city',
						'id' => 'suburbField',
                        'width' => 'third',
					])

                    @include($campaign->view('fields.state'), [
						'key' => 'state',
                        'width' => 'third',
					])

                    @include($campaign->view('fields.text'), [
						'key' => 'zip',
						'id' => 'postcodeField',
                       'width' => 'third',
					])


					@include($campaign->view('fields.email'), [
						'key' => 'email_addr',
                        'width' => 'half',
					])



					@include($campaign->view('fields.gender'), [
						'key' => 'gender',
						'placeholder' => 'Please choose your gender...',
                        'width' => 'half',
                        'not_required' => $input->gender == 'Supplied'
					])

						@include($campaign->view('fields.dropdown'), [
						 'label' => false,
                        'key' => 'extra1',
                        'width' => 'full',
						'control_label' => 'Do you identify as Aboriginal or Torres Strait Islander?',
						'placeholder' => 'Please select',
                        'options' => [
                            'Aboriginal' => 'Aboriginal',
                            'Torres Strait Islander' => 'Torres Strait Islander',
                            'Both, Aboriginal & Torres Strait Islander' => 'Both, Aboriginal & Torres Strait Islander',
                            'Prefer not to say' => 'Prefer not to say',
                            'No' => 'No',
                           
                        ],
					])

					<div style="text-align: right;color:#000;margin-bottom: 30px;width: 100%;padding-right: 15px;">* Mandatory Field</div>


					@include($campaign->view('fields.checkbox'), [
						'key' => 'extra2',
                        'width' => 'full',
                        'control_label' => 'A $500 Harvey Norman gift voucher would be awesome, please include me in the draw and I’m happy to hear more from People’s Choice Home Loans and the Bulldogs',

					])
					

				</div>

				<div class="form__submit">

					@foreach($hidden_fields as $name=>$value)
					<input type="hidden" name="{{ $name }}" value="{{ $value }}">
					@endforeach

					<button class="form__submit__button" type="submit">{{ $campaign->creative->button_text }}</button>
					<!-- <div style="text-align: center; color:#000; margin-top: 20px;">* Mandatory Field</div> -->
				</div>



			</div>

		</form>

	</div>

    @include($campaign->view('common.footer'))

</div>


@endsection
