<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Confirm or update your details</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0 " />
<meta name="format-detection" content="telephone=no" />
<base href="<?php echo url('assets/western-bulldogs_confirm-update-2019/edm/test'); ?>">
<style type="text/css">
body {
	margin: 0 !important;
	padding: 0 !important;
	-webkit-text-size-adjust: 100% !important;
	-ms-text-size-adjust: 100% !important;
	-webkit-font-smoothing: antialiased !important;
}
.visible-mobile {
  max-height: 0;
  width: 0;
  display: none;
}
img {
	border: 0 !important;
	outline: none !important;
}
 
table {
	border-collapse: collapse;
	mso-table-lspace: 0px;
	mso-table-rspace: 0px;
}
td, a, span {
	border-collapse: collapse;
	mso-line-height-rule: exactly;
}
.ExternalClass * {
	line-height: 100%;
}
.em_defaultlink a {
	color: inherit !important;
	text-decoration: none !important;
}
span.MsoHyperlink {
	mso-style-priority: 99;
	color: inherit;
}
span.MsoHyperlinkFollowed {
	mso-style-priority: 99;
	color: inherit;
}
.em_text { font-family:Arial,sans-serif; font-size:14px; line-height:24px;  color:#ffffff;}
.em_text a { text-decoration:none; color:#bebebe;}
.em_text1, .em_text1 a { font-family: Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; color: #ffffff; text-decoration: none;   }
.em_text2, .em_text2 a { font-family: Helvetica, Arial, sans-serif; font-size: 22px; line-height: 26px; color:#ffffff; text-decoration: none; font-weight:bold; text-transform: uppercase;
	}
.em_text3, .em_text3 a { font-family: Helvetica, Arial, sans-serif; font-size: 18px; line-height: 20px; color: #ffffff; text-decoration: none; }
.em_text4, .em_text4 a { font-family: Helvetica, Arial, sans-serif; font-size: 12px; line-height: 16px; color: #464646; text-decoration: none; }
.preheader {
    display: none;
}
/*Stylesheet for the devices width between 481px to 660px*/
@media only screen and (min-width:481px) and (max-width:659px) {
.em_main_table { width:100% !important; }
.em_wrapper { width: 100% !important; }
.em_side{ padding: 0 50px !important; }
.em_aside { padding: 0 20px !important; /*Update the values as required*/ }
.em_aside2 { padding: 0 0px !important; /*Update the values as required*/ }
.em_aside3 { padding-left:20px!important;padding-right:20px!important; /*Update the values as required*/ }
.em_hide { display: none !important; }
.em_full_img {margin:auto; height: auto !important; max-width:none !important; }
.em_align_cent { text-align: center !important; }
.em_height { height:20px !important; font-size:0px !important; line-height:0px !important;}
.em_background { background-image:none  !important; height:auto !important; }
.em_pad { padding-top:20px !important;}
.em_side2 { width:20px !important;}
.em_side10 { width: 10px !important; /*Update the values as required*/ }
.em_pad_bottom {
	padding-bottom: 14px !important;
}
.em_text1, .em_text1 a {font-size: 14px;}
.em_center1{
	text-align: center!important;
	height: inherit!important;
	border-bottom: none!important;
	padding-top: 8px!important;
}
.em_center2{
	text-align: center!important;
	height: inherit!important;
	padding-bottom: 8px!important;
}
.visible-mobile {
		display: block !important;
		max-height: none !important;
		width:100%!important;
		max-width: 100% !important;
		height: auto !important;
	  }
}
/*Stylesheet for the devices width between 0px to 480px*/
@media only screen and (max-width:480px) {
.em_main_table { width: 100% !important; }
.em_wrapper { width: 100% !important; }
.em_side{ padding: 0 50px !important; }
.em_aside { padding: 0 20px !important; /*Update the values as required*/ }
.em_aside2 { padding: 0 0px !important; /*Update the values as required*/ }
.em_aside3 { padding-left:20px!important;padding-right:20px!important; /*Update the values as required*/ }
.em_hide { display: none !important; }
.em_full_img { margin:auto; height: auto !important; max-width:none !important; }
.em_align_cent { text-align: center !important; }
.em_height { height:20px !important; font-size:0px !important; line-height:0px !important;}
.em_background { background-image:none  !important; height:auto !important; }
.em_pad { padding-top:20px !important;}
.em_side2 { width:20px !important;}
.em_side10 { width: 10px !important; /*Update the values as required*/ }
.em_br {
	display: block;
}
.em_pad_bottom {
	padding-bottom: 14px !important;
}
.em_text1, .em_text1 a {font-size: 14px;}
.em_center1{
	text-align: center!important;
	height: inherit!important;
	border-bottom: none!important;
	padding-top: 8px!important;
}
.em_center2{
	text-align: center!important;
	height: inherit!important;
	padding-bottom: 8px!important;
}
.visible-mobile {
	display: block !important;
	max-height: none !important;
	width:100%!important;
	max-width: 100% !important;
	height: auto !important;
  }
}
</style>
<!--[if gte mso 9]>
<xml>
  <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
 </o:OfficeDocumentSettings>
</xml>
<![endif]-->
</head>
<body style="margin: 0px;padding: 0px;-webkit-text-size-adjust: 100% !important;-ms-text-size-adjust: 100% !important;-webkit-font-smoothing: antialiased !important;" bgcolor="#bebebe"><span class="preheader" style="display: none;">Confirm or update your details before FRIDAY 9 AUGUST for the chance to receive your 2020 membership for free! PLUS, thanks to People’s Choice Home Loans enter the draw to win a $500 Harvey Norman voucher!</span>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#bebebe" style="border-collapse: collapse;mso-table-lspace: 0px;mso-table-rspace: 0px;">
    	<tr>
        	<td>
            	<table width="700" cellpadding="0" cellspacing="0" border="0" align="center" class="em_main_table">
                	<tr>
                    	<td valign="top" align="center">
                    		<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                       			<tr>
										<td height="20" class="em_height">&nbsp;</td>
								</tr>
                        		<tr>
                        			<td align="center" class="em_text4">
                    					If you are having trouble viewing this email,<span class="em_br" style="border-collapse: collapse;mso-line-height-rule: exactly;"></span> please <webversion>click here</webversion> to view it as a webpage.
                    				</td>
								</tr>
                  				<tr>
										<td height="20" class="em_height">&nbsp;</td>
								</tr>
                   			</table>
                    	</td>
                    </tr>
                    <tr>
                    	<td valign="top" colspan="1">
                    		<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                        		<tr>
                        			<td align="center">
                    					<a href="http://www.westernbulldogs.com.au/"><img src="images/img_03.jpg" editable="true" width="700" border="0" style="display:block; max-width: 700px;" class="em_hide" alt="Confirm or update your details and WIN"></a>
                    				</td>
                    				<!--[if !mso]><!-->
									<td valign="top" colspan="1" class="visible-mobile">
											<a href="http://www.westernbulldogs.com.au/"><img src="images/mobile_01.jpg" width="320" height="250" border="0" style="-moz-hyphens:none;-ms-hyphens:none;-ms-interpolation-mode:bicubic;-webkit-hyphens:none;border:none;clear:both;display:none;hyphens:none;max-height:0;max-width:100%;mso-hide:all;outline:0;text-decoration:none;width:0;word-break:keep-all" alt="Confirm or update your details and WIN" class="visible-mobile"></a>
									</td>
									<!--<![endif]-->
								</tr>
                   			</table>
                    	</td>
                    </tr>
                    <tr>
                    	<td valign="top" colspan="1" bgcolor="#C60C30" class="em_aside" style="padding-left: 50px; padding-right: 50px;">
                    		<table width="600" style="width:600px;" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
								<tr>
									<td valign="top" colspan="1" bgcolor="#C60C30">
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="center" style="padding: 40px 0 50px 0; line-height: 24px;">
													 <span class="em_text1" style="color:#ffffff;"><strong>Hello <?php echo $record->name_first ? $record->name_first : '-'; ?>,</strong></span><br>
<br>
			<span class="em_text1">Confirm or update your details before FRIDAY 9 AUGUST for the chance to receive your 2020 membership for free! PLUS, thanks to People’s Choice Home Loans enter the draw to win a $500 Harvey Norman voucher!</span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td valign="top" colspan="1" bgcolor="#C60C30">
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="center" style="padding-bottom: 20px;">
													 <span class="em_text2">ARE YOUR <br class="visible-mobile">
 DETAILS CORRECT?</span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								 <tr>
									<td valign="top" colspan="1" bgcolor="#C60C30">
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="center" style="padding: 0px 0 20px; line-height: 24px;">
													 <span class="em_text">First Name</span><br>
													<span class="em_text3"><?php echo $record->name_first ? $record->name_first : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 20px; line-height: 24px;">
													 <span class="em_text">Surname</span><br>
													<span class="em_text3"><?php echo $record->name ? $record->name : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 20px; line-height: 24px;">
													 <span class="em_text">Mobile</span><br>
													<span class="em_text3"><?php echo $record->phone_mobile ? $record->phone_mobile : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 20px; line-height: 24px;">
													 <span class="em_text">Phone Number</span><br>
													<span class="em_text3"><?php echo $record->phone_home ? $record->phone_home : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 20px; line-height: 24px;">
													 <span class="em_text">Address</span><br>
													<span class="em_text3"><?php echo $record->street_addr_1 ? $record->street_addr_1 : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 20px; line-height: 24px;">
													 <span class="em_text">Suburb</span><br>
													<span class="em_text3"><?php echo $record->city ? $record->city : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 20px; line-height: 24px;">
													 <span class="em_text">State</span><br>
													<span class="em_text3"><?php echo $record->state ? $record->state : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 20px; line-height: 24px;">
													 <span class="em_text">Postcode</span><br>
													<span class="em_text3"><?php echo $record->zip ? $record->zip : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 20px; line-height: 24px;">
													 <span class="em_text">Email</span><br>
													<span class="em_text3"><?php echo $record->email_addr ? $record->email_addr : '-'; ?></a></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 20px; line-height: 24px;">
													 <span class="em_text">Gender</span><br>
													<span class="em_text3">To be provided</span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0px 0 40px; line-height: 24px;">
													 <span class="em_text">Date of birth</span><br>
													<span class="em_text3">To be provided</span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
                   			</table>
                    	</td>
                    </tr>
				</table>
     		</td>
        </tr>
         <tr>
        	<td>
            	<table width="700" cellpadding="0" cellspacing="0" border="0" align="center" class="em_main_table">
                    <tr>
                    	<td valign="top" colspan="1" bgcolor="#ffffff" class="em_aside" style="padding-left: 50px; padding-right: 50px;">
                    		<table width="600" style="width:600px;" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
								<tr>
									<td align="center" valign="top" colspan="1" bgcolor="#ffffff" style="padding-bottom:35px; padding-top:30px;">
										<table width="525" style="width:525px;" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td>
													
													<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="right">
														<tr>
															<td valign="top" align="left">
																  <a href="<?php echo $record->update_url; ?>"><img src="images/bulldog_05.jpg" width="500" height="60" border="0" style="display:block; max-width: 500px;margin: auto;" class="em_full_img em_hide"></a>
															</td>
															<td valign="top" class="visible-mobile">
																	 <a href="[_update_url]"><img src="images/mobile_btn.jpg" width="194" height="64" border="0" style="display:block; max-width: 194px;margin: auto;" class="em_full_img"></a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td valign="top" colspan="1" bgcolor="#ffffff">
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="center" style="padding: 0px 0 10px 0; line-height: 20px;">
													<span class="em_text1" style="color:#0039A6;">Click on the buttons to confirm or update your details.<br class="em_hide">
Do not reply to this email.</span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
                  		</td>
                  	</tr>
                     <tr>
                    	<td valign="top" colspan="1" class="em_aside" bgcolor="#ffffff">
                    		<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                    			<tr>
									<td valign="top" align="center" style="padding-top: 25px; padding-bottom: 20px;">
										<table width="181" style="width:181px;" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="left">
													<a href="http://www.westernbulldogs.com.au"><img src="images/bulldog_10.jpg" width="85" height="85" border="0" style="display:block; max-width: 85px;" class="em_full_img"></a>
												</td>
												<td align="left">
													<a href="https://www.missionfoods.com/"><img src="images/bulldog_11.jpg" width="96" height="85" border="0" style="display:block; max-width: 96px;" class="em_full_img"></a>
												</td>
											</tr>
										</table>
									</td>
                   				</tr>
							</table>
                    	</td>
                    </tr>
                     <tr>
                    	<td valign="top" colspan="1" bgcolor="#ffffff">
                    		<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                        		<tr>
                        			<td align="center" style="padding: 5px 50px 10px 50px;" class="em_aside3">
                    					 <span class="em_text4"><p>*Winners will be drawn at random using a random integer generator. First number drawn will indicate the first prize winner. Second number draw will indicate the second prize winner. </p>

 <p>First prize will be a free 2020 Western Bulldogs membership. Free 2020 membership will be based on membership held in 2019 and will be individual membership only. Family memberships not included. If no membership was purchased in 2019, a 2020 General Admission Melbourne Home Game membership will be offered. </p>
<p>Second prize will be a $500 Harvey Norman Voucher. $500 Harvey Norman Voucher winner will be contacted by phone and the voucher posted out.All personal information you provide will be used by the Western Bulldogs Football Club, AFL and on behalf of selected third parties in accordance to our <a href="http://www.westernbulldogs.com.au/privacy" style="text-decoration: underline">privacy policy</a>. By providing your personal information will be kept securely and confidentially. Only those who confirm or update details prior to 5:00pm Friday 9 August will be entered into the prize draw. The winner will be drawn by Western Bulldogs at 12:00pm on Wednesday 14 August 2019. The winner will be notified by phone. </p>

 
</span>
                    				</td>
								</tr>
                  				<tr>
                        			<td align="center" style="padding: 0px 50px 50px 50px;" class="em_aside3">
<span class="em_text4">If you have any questions please <br class="visible-mobile">
 contact us on <strong>1300 46 36 47</strong> <br class="em_hide">
or email <a href="mailto:membership@westernbulldogs.com.au"><strong>membership@westernbulldogs.com.au</strong></a><br>
<br>
<a href="http://www.westernbulldogs.com.au/privacy">PRIVACY POLICY</a>     <span class="em_hide">&nbsp;&nbsp;I&nbsp;&nbsp;</span><br class="visible-mobile">
     <unsubscribe>UNSUBSCRIBE</unsubscribe>     <span class="em_hide">&nbsp;&nbsp;I&nbsp;&nbsp;</span><br class="visible-mobile">
     <a href="https://membership.westernbulldogs.com.au/confirm-your-details-0">TERMS &amp; CONDITIONS</a>
</span>
                    				</td>
								</tr>
                   			</table>
                    	</td>
                    </tr>
                 </table>
            </td>
       	</tr>
    </table>
   
<div class="em_hide" style="white-space:nowrap;font:20px courier;color:#ffffff;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
</body>
</html>

