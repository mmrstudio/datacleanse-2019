<?php

require './vendor/autoload.php';

use mikehaertl\wkhtmlto\Image;

$placeholder = '&lt;NAME&gt;';
$output = isset($_GET['output']) ? $_GET['output'] : 'img';
$no_cache = isset($_GET['nc']);
$output = ($output === 'img' || $output === 'html') ? $output : 'img';
$name = isset($_GET['name']) ? trim(urldecode($_GET['name'])) : $placeholder;
$length = strlen($name);
if($length == 0) $name = $placeholder;

$cache_file = './cache/' . $name . '.jpg';

if($output == 'img' && !$no_cache && file_exists($cache_file) && is_file($cache_file)) {
    header('Content-Type: image/jpeg');
    echo file_get_contents($cache_file);
    exit;
}

$width = 1240;
$height = 328;

if($length <= 6) {
    $font_size = 34;
} elseif ($length <= 10) {
    $font_size = 30;
} elseif ($length <= 12) {
    $font_size = 28;
} elseif ($length <= 14) {
    $font_size = 24;
} elseif ($length <= 16) {
    $font_size = 20;
} elseif ($length <= 18) {
    $font_size = 18;
} elseif ($length <= 20) {
    $font_size = 16;
} else {
    $font_size = 14;
}

$font_size = $font_size / 2;
$width = $width / 2;
$height = $height / 2;

ob_start();

?><!DOCTYPE html>

<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Junior Merch</title>
        <style>

            html, body {
                margin: 0;
                padding: 0;
                font-size: 0;
            }

            html, body, #img, #img img {
                width: <?php echo $width; ?>px;
                height: <?php echo $height; ?>px;
            }

            #img {
                position: relative;
            }

            #img img {
                display: block;
                position: relative;
                z-index: 1;
            }

            .name {
                position: absolute;
                /* height: 50px;
                line-height: 50px;
                letter-spacing: -2px; */
                height: 25px;
                line-height: 25px;
                letter-spacing: -1px;
                font-family: Arial, sans-serif;
                font-weight: bold;
                font-style: italic;
                text-transform: uppercase;
                text-align: center;
                color: #fff;
                z-index: 2;
                overflow: hidden;
                text-overflow: ellipsis;
                white-space: nowrap;
            }

            .bootbag {
                /* width: 382px;
                top: 219px;
                left: 111px; */
                width: 191px;
                top: 109.5px;
                left: 55.5px;
                font-size: <?php echo $font_size + 8; ?>px;
            }

            .pencil-case {
                /* width: 236px;
                top: 146px;
                left: 715px; */
                width: 118px;
                top: 75px;
                left: 357.5px;
                font-size: <?php echo $font_size; ?>px;
            }

        </style>
    </head>

    <body>
        <div id="img">
            <?php include './junior-merch-img.php'; ?>
            <div class="name bootbag"><?php echo $name; ?></div>
            <div class="name pencil-case"><?php echo $name; ?></div>
        </div>
    </body>

</html><?php

    $img_html = ob_get_contents();

    ob_end_clean();

    if($output == 'html') {
        echo $img_html;
        exit;
    }

    $image = new Image($img_html);
    $image->setOptions([
        'binary' => '/usr/bin/xvfb-run /usr/local/bin/wkhtmltoimage',
        'width' => $width,
        'height' => $height,
        'quality' => 40,
    ]);

    $image->saveAs($cache_file);

    header('Content-Type: image/jpeg');

    if (!$image->send('merch.jpg', true)) {
        print_r($image->getError());
    } else {
        echo 'done';
    }
