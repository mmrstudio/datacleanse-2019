<?php

function get_data($field, $record) {

    $record->junior_names_split = strlen(trim($record->junior_names)) > 0 ? explode(',', $record->junior_names) : [];

    $junior_image_text = '';

    if(count($record->junior_names_split) > 0) {
        $junior_imgs = [];
        foreach($record->junior_names_split as $junior) {
            $junior_imgs[] = '<img src="https://campaign.datacleanse.com.au/assets/western-bulldogs_2019-junior-opt-in/junior-merch/?name=' . urlencode($junior).'" width="620" height="164" border="0" style="display:block;" alt="">';
        }
        $junior_image_text = implode('<br />', $junior_imgs);
    } else {
        $junior_image_text =  '<img src="https://campaign.datacleanse.com.au/assets/western-bulldogs_2019-junior-opt-in/junior-merch/?name=' . urlencode($record->name_first).'" width="620" height="164" border="0" style="display:block;" alt="">';
    }

    $junior_image_text_split = strlen($junior_image_text) > 0 ? str_split($junior_image_text, 200) : [];

    $record->junior_img_1 = isset($junior_image_text_split[0]) ? $junior_image_text_split[0] : '';
    $record->junior_img_2 = isset($junior_image_text_split[1]) ? $junior_image_text_split[1] : '';
    $record->junior_img_3 = isset($junior_image_text_split[2]) ? $junior_image_text_split[2] : '';
    $record->junior_img_4 = isset($junior_image_text_split[3]) ? $junior_image_text_split[3] : '';

    return $record;
}

$record = get_data(null, $record);
$email_record = new \stdClass();

//dd($record->toArray());

foreach($campaign->fields() as $field) {
    if($field->email == 1) {
        $email_record->{$field->name} = $record->{$field->name};
    }
}

//$record = $email_record;
//$record->update_url = '#';

?><!DOCTYPE html>
<!--
<?php print_r($email_record); ?>
<?php print_r($record->toArray()); ?>
<?php //print_r($record); ?>
-->
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>2019 Junior Opt-In</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0 " />
        <meta name="format-detection" content="telephone=no"/>
        <style>
        
    .font-arial { font-family: Helvetica, Arial, sans-serif; line-height: 1.2; }

	.font-bold { font-weight: bold; }
	.font-italic { font-style: italic; }

	.font-10 { font-size: 10px; }
	.font-12 { font-size: 12px; }
	.font-13 { font-size: 13px; }
	.font-14 { font-size: 14px; }
	.font-16 { font-size: 16px; }
	.font-18 { font-size: 18px; }
	.font-20 { font-size: 20px; }
	.font-22 { font-size: 22px; }
	.font-24 { font-size: 24px; }
	.font-26 { font-size: 26px; }
	.font-28 { font-size: 28px; }

	.font-uppercase { text-transform: uppercase; }

	.color-black { color: #000000; }
	.color-white { color: #ffffff; }
	.color-red { color: #c50b2f; }
	.color-blue { color: #0039a6; }
	.color-grey { color: #464646; }

	.bg-black { color: #000000; }
	.bg-white { color: #ffffff; }
	.bg-red { color: #c50b2f; }
	.bg-blue { color: #0039a6; }
	.bg-grey { color: #464646; }
	.bg-light-blue { background-color: #f2f5fa; }

	.text-center { text-align: center; }
	.text-left { text-align: left; }
	.text-right { text-align: right; }

	.gutters { padding: 40px; }

    .border-top-blue { border-top: 1px solid #0039a6; }

    .button {
        background-color:#001E46;
        color:#ffffff;
        display:inline-block;
        font-family:sans-serif;
        font-size:18px;
        font-weight:bold;
        line-height:60px;
        text-align:center;
        text-decoration:none;
        width:580px;
        -webkit-text-size-adjust:none;
    }
        
        </style>
        <base href="<?php echo url('assets/western-bulldogs_2019-member-renewal/edm/test'); ?>">
    </head>


<body bgcolor="#fff"><span class="preheader" style="display: none;"><?php echo $record->name_first; ?>, your loyalty to the Club through your membership is greatly appreciated. Thank you for your continued support.</span>

    <!--Full width table start-->
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="border-collapse: collapse;mso-table-lspace: 0px;mso-table-rspace: 0px;">
    	<tr>
        	<td>
            	<table width="700" cellpadding="0" cellspacing="0" border="0" align="center" class="em_main_table">

                	<tr>
                    	<td valign="top" align="center">
                    		<table width="100%"  class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                       			<tr>
									<td height="10"></td>
								</tr>
                        		<tr>
                        			<td align="center" class="font-arial font-12 color-grey">If you are having trouble viewing this email, <span class="em-br"></span> please <webversion class="color-grey">click here</webversion> to view it as a webpage.</td>
								</tr>
                  				<tr>
									<td height="10"></td>
								</tr>
                   			</table>
                    	</td>
                    </tr>

                    <tr>
                    	<td valign="top" colspan="1">
                    		<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                        		<tr>
                                    <td valign="top" align="center">
                    					<img src="images/header.png" border="0" style="display:block;" width="700" height="197" alt="THANK YOU FOR RENEWING WITH US FOR 2019!" >
                                    </td>
								</tr>
                   			</table>
                    	</td>
                    </tr>

                    <tr>
                    	<td valign="top" colspan="1" bgcolor="#ffffff" class="gutters">
							<span class="font-arial font-24 font-bold color-red font-uppercase"><?php echo $record->name_first; ?>, THANK YOU FOR RENEWING WITH US FOR 2019!</span>
							<br><br>
							<span class="font-arial font-14">
                                We’re excited to have your support and look forward to seeing you at the footy, cheering us on.
                                <br /><br />
                                We are in the process of preparing all of our junior packs to be mailed out and wanted to let you know of a special offer for 2019. All junior members on the Committed Bulldog Payment plan will be getting their pack personalised for FREE.
                                <br /><br />
                                Below is an impression of what your junior pack(s) could look like if you opt in for our convenient Committed Bulldog rollover for 2020. To learn more about this method for renewing your membership next year <a href="https://membership.westernbulldogs.com.au/payment-plans-3">click here</a>.
                                <br /><br />
                                Do you think your name looks good on these packs? Simply choose an opt-in option below and we will make sure your 2019 packs are personalised before they are sent out.
                                <singleline />							
                            </span>
                            
                            <br>
                            <br>
                            <br>

                            <div style="text-align: center;"><!--[if mso]>
                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="<?php echo $record->update_url; ?>?opt_in=upfront" style="height:60px;v-text-anchor:middle;width:480px;" stroke="f" fillcolor="#001E46">
                                <w:anchorlock/>
                                <center>
                            <![endif]-->
                                <a href="<?php echo $record->update_url; ?>?opt_in=upfront" class="button" target="_blank">OPT-IN FOR 2020 <span style="color:#c50b2f;">UPFRONT</span> COMMITTED BULLDOG RENEWAL</a>
                            <!--[if mso]>
                                </center>
                            </v:rect>
                            <![endif]-->

                            <br>
                            <br>

                            <div style="text-align: center;"><!--[if mso]>
                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="<?php echo $record->update_url; ?>?opt_in=monthly" style="height:60px;v-text-anchor:middle;width:480px;" stroke="f" fillcolor="#001E46">
                                <w:anchorlock/>
                                <center>
                            <![endif]-->
                                <a href="<?php echo $record->update_url; ?>?opt_in=monthly" class="button" target="_blank">OPT-IN FOR 2020 <span style="color:#c50b2f;">MONTHLY</span> COMMITTED BULLDOG RENEWAL</a>
                            <!--[if mso]>
                                </center>
                            </v:rect>
                            <![endif]-->

                            <!-- JUNIOR -->
                            <br>
                            <br>
                            <br>
                            <?php echo $record->junior_img_1; ?><?php echo $record->junior_img_2; ?><?php echo $record->junior_img_3; ?><?php echo $record->junior_img_4; ?>

                    	</td>
                    </tr>

                    <tr>
                    	<td valign="top" colspan="1" class="font-arial color-blue font-12 bg-light-blue border-top-blue gutters">
                            As a Committed Bulldog member, no action will be required when rolling over for 2020. If you would like to update your personal details, change your membership or opt out of the Committed Bulldog automatic roll over, please call our Membership team on 1300 46 36 47.
                            <br /><br />
                            By selecting an opt-in option, you agree to all the terms and conditions of the Committed Bulldog payment plan. As you have already renewed for 2019, your membership will be rolled over on the upfront option for 2020.
                    	</td>
                    </tr>

                    <tr>
                    	<td valign="top" colspan="1" bgcolor="#ffffff" align="center" class="border-top-blue">
							<a href="http://westernbulldogs.com.au"><img src="images/sponsors.png" width="700" height="170" border="0" style="display:block; max-width: 700px;" alt="" ></a>
                    	</td>
                    </tr>
                    <tr>
                    	<td valign="top" colspan="1" bgcolor="#011e46" align="center" style="padding-top: 20px; padding-bottom: 20px;">

							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td>

										<table width="150" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td><a href="http://www.facebook.com/Western.Bulldogs?"><img src="images/facebook.png" width="30" height="30" border="0" style="display:block;" alt="Facebook" ></a></td>
												<td width="10"></td>
												<td><a href="https://www.instagram.com/westernbulldogs"><img src="images/instagram.png" width="30" height="30" border="0" style="display:block;" alt="Instagram" ></a></td>
												<td width="10"></td>
												<td><a href="http://twitter.com/#!/westernbulldogs"><img src="images/twitter.png" width="30" height="30" border="0" style="display:block;" alt="Twitter" ></a></td>
												<td width="10"></td>
												<td><a href="http://www.youtube.com/user/BulldogsTVOfficial"><img src="images/youtube.png" width="30" height="30" border="0" style="display:block;" alt="YouTube" ></a></td>
											</tr>
										</table>

									</td>
								</tr>
								<tr>
									<td height="20"></td>
								</tr>
								<tr>
									<td align="center">
                                        <span class="font-arial color-white font-10">
										    Email sent by: Western Bulldogs, 417 Barkly St, Footscray West 3012<br>
                                            <a href="https://membership.westernbulldogs.com.au/terms-conditions-2" class="color-white">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a href="http://www.westernbulldogs.com.au/privacy" class="color-white">Privacy Policy</a> &nbsp;|&nbsp; To unsubscribe from future receipt please <unsubscribe class="color-white">click here</unsubscribe>
                                        </span>
									</td>
								</tr>
							</table>

                    	</td>
                    </tr>


				</table>
     		</td>
        </tr>
    </table>
<div class="hidden-mobile" style="white-space:nowrap;font:20px courier;color:#ffffff;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
</body>
</html>

