import React from 'react';
import Modal from './components/Modal';
import { fetchPOST } from './fetchUtils';

require('es6-promise').polyfill();
require('es6-object-assign').polyfill();

class App extends React.Component {

    constructor(props){
        super(props);

        //console.log('window.appData', window.appData);

        let initialState = {
            campaign: window.appData.campaign,
            record: window.appData.record,
            fields: {},
            hiddenFields: window.appData.fields,
            processUrl: window.appData.processUrl,
            loading: false,
            submitted: false,
            errors: []
        }

        // opt_in option
        initialState.fields.opt_in = window.appData.opt_in ? window.appData.opt_in : '';

        //console.log('initialState', initialState);

        this.state = initialState;

        this._modal = false;

        this._handleSubmit = this._handleSubmit.bind(this);
        this._onSubmitResponse = this._onSubmitResponse.bind(this);

    }

    componentDidMount() {
        this._handleSubmit();
    }

    _handleSubmit() {
        const { record, fields, hiddenFields, processUrl } = this.state;

        const postData = Object.assign(fields, hiddenFields);

        //console.log('postData', postData);

        this.setState({
            submitted: false,
            loading: true,
        });

        // send data
        fetchPOST(
            processUrl,
            postData,
            this._onSubmitResponse
        );

    }

    _onSubmitResponse(response) {

        if(response === true) {

            this.setState({
                submitted: true,
                loading: false,
            });

            scroll(0,0);

        } else {

            let errors = [];

            Object.keys(response).map((key) => {
                let value = response[key];
                value.map((error) => {
                    errors.push(error);
                });
            });

            this.setState({ errors: errors });

            //alert(errors.join("\r\n"));

            this.setState({
                loading: false
            });

        }

    }

    render() {

        const { record, fields } = this.state;

        let errors = [];
        if(this.state.errors.length > 0) {
            this.state.errors.map((error, i) => {
                errors.push(<p>{error}</p>);
            });
        }

        return (
            <div className="body-wrap">

                <header className="header">
                    <div className="header__bg"></div>
                </header>

                { this.state.submitted === true ? (
                    <div className="main">
                        <div className="thanks">
                            <div className="thanks__inner">
                                <div className="thanks__title">Thank You {record.name_first}!</div>
                                <p>By joining the Committed Bulldog payment plan for 2020 you have secured FREE personalisation on your junior membership pack(s) for the 2019 season.</p>
                                <p>You can expect to receive your personalised pack(s) before Christmas this year.</p>
                                <p>If you have any questions about the Committed Bulldog payment plan, please contact our team on 1300 46 36 47 or email <a href="mailto:membership@westernbulldogs.com.au">membership@westernbulldogs.com.au</a>.</p>
                                <p>Go Dogs!</p>
                                <p>Western Bulldogs Football Club</p>
                            </div>
                        </div>
                    </div>
                ) : (
                    <div className="main loading">
                            <img src="data:image/gif;base64,R0lGODlhIAAgAPMAAP///wEeRsbM1YSSpba+yZqltTZNbVZphNjc4uTn67zDzh44WwQhSAAAAAAAAAAAACH+GkNyZWF0ZWQgd2l0aCBhamF4bG9hZC5pbmZvACH5BAAKAAAAIf8LTkVUU0NBUEUyLjADAQAAACwAAAAAIAAgAAAE5xDISWlhperN52JLhSSdRgwVo1ICQZRUsiwHpTJT4iowNS8vyW2icCF6k8HMMBkCEDskxTBDAZwuAkkqIfxIQyhBQBFvAQSDITM5VDW6XNE4KagNh6Bgwe60smQUB3d4Rz1ZBApnFASDd0hihh12BkE9kjAJVlycXIg7CQIFA6SlnJ87paqbSKiKoqusnbMdmDC2tXQlkUhziYtyWTxIfy6BE8WJt5YJvpJivxNaGmLHT0VnOgSYf0dZXS7APdpB309RnHOG5gDqXGLDaC457D1zZ/V/nmOM82XiHRLYKhKP1oZmADdEAAAh+QQACgABACwAAAAAIAAgAAAE6hDISWlZpOrNp1lGNRSdRpDUolIGw5RUYhhHukqFu8DsrEyqnWThGvAmhVlteBvojpTDDBUEIFwMFBRAmBkSgOrBFZogCASwBDEY/CZSg7GSE0gSCjQBMVG023xWBhklAnoEdhQEfyNqMIcKjhRsjEdnezB+A4k8gTwJhFuiW4dokXiloUepBAp5qaKpp6+Ho7aWW54wl7obvEe0kRuoplCGepwSx2jJvqHEmGt6whJpGpfJCHmOoNHKaHx61WiSR92E4lbFoq+B6QDtuetcaBPnW6+O7wDHpIiK9SaVK5GgV543tzjgGcghAgAh+QQACgACACwAAAAAIAAgAAAE7hDISSkxpOrN5zFHNWRdhSiVoVLHspRUMoyUakyEe8PTPCATW9A14E0UvuAKMNAZKYUZCiBMuBakSQKG8G2FzUWox2AUtAQFcBKlVQoLgQReZhQlCIJesQXI5B0CBnUMOxMCenoCfTCEWBsJColTMANldx15BGs8B5wlCZ9Po6OJkwmRpnqkqnuSrayqfKmqpLajoiW5HJq7FL1Gr2mMMcKUMIiJgIemy7xZtJsTmsM4xHiKv5KMCXqfyUCJEonXPN2rAOIAmsfB3uPoAK++G+w48edZPK+M6hLJpQg484enXIdQFSS1u6UhksENEQAAIfkEAAoAAwAsAAAAACAAIAAABOcQyEmpGKLqzWcZRVUQnZYg1aBSh2GUVEIQ2aQOE+G+cD4ntpWkZQj1JIiZIogDFFyHI0UxQwFugMSOFIPJftfVAEoZLBbcLEFhlQiqGp1Vd140AUklUN3eCA51C1EWMzMCezCBBmkxVIVHBWd3HHl9JQOIJSdSnJ0TDKChCwUJjoWMPaGqDKannasMo6WnM562R5YluZRwur0wpgqZE7NKUm+FNRPIhjBJxKZteWuIBMN4zRMIVIhffcgojwCF117i4nlLnY5ztRLsnOk+aV+oJY7V7m76PdkS4trKcdg0Zc0tTcKkRAAAIfkEAAoABAAsAAAAACAAIAAABO4QyEkpKqjqzScpRaVkXZWQEximw1BSCUEIlDohrft6cpKCk5xid5MNJTaAIkekKGQkWyKHkvhKsR7ARmitkAYDYRIbUQRQjWBwJRzChi9CRlBcY1UN4g0/VNB0AlcvcAYHRyZPdEQFYV8ccwR5HWxEJ02YmRMLnJ1xCYp0Y5idpQuhopmmC2KgojKasUQDk5BNAwwMOh2RtRq5uQuPZKGIJQIGwAwGf6I0JXMpC8C7kXWDBINFMxS4DKMAWVWAGYsAdNqW5uaRxkSKJOZKaU3tPOBZ4DuK2LATgJhkPJMgTwKCdFjyPHEnKxFCDhEAACH5BAAKAAUALAAAAAAgACAAAATzEMhJaVKp6s2nIkolIJ2WkBShpkVRWqqQrhLSEu9MZJKK9y1ZrqYK9WiClmvoUaF8gIQSNeF1Er4MNFn4SRSDARWroAIETg1iVwuHjYB1kYc1mwruwXKC9gmsJXliGxc+XiUCby9ydh1sOSdMkpMTBpaXBzsfhoc5l58Gm5yToAaZhaOUqjkDgCWNHAULCwOLaTmzswadEqggQwgHuQsHIoZCHQMMQgQGubVEcxOPFAcMDAYUA85eWARmfSRQCdcMe0zeP1AAygwLlJtPNAAL19DARdPzBOWSm1brJBi45soRAWQAAkrQIykShQ9wVhHCwCQCACH5BAAKAAYALAAAAAAgACAAAATrEMhJaVKp6s2nIkqFZF2VIBWhUsJaTokqUCoBq+E71SRQeyqUToLA7VxF0JDyIQh/MVVPMt1ECZlfcjZJ9mIKoaTl1MRIl5o4CUKXOwmyrCInCKqcWtvadL2SYhyASyNDJ0uIiRMDjI0Fd30/iI2UA5GSS5UDj2l6NoqgOgN4gksEBgYFf0FDqKgHnyZ9OX8HrgYHdHpcHQULXAS2qKpENRg7eAMLC7kTBaixUYFkKAzWAAnLC7FLVxLWDBLKCwaKTULgEwbLA4hJtOkSBNqITT3xEgfLpBtzE/jiuL04RGEBgwWhShRgQExHBAAh+QQACgAHACwAAAAAIAAgAAAE7xDISWlSqerNpyJKhWRdlSAVoVLCWk6JKlAqAavhO9UkUHsqlE6CwO1cRdCQ8iEIfzFVTzLdRAmZX3I2SfZiCqGk5dTESJeaOAlClzsJsqwiJwiqnFrb2nS9kmIcgEsjQydLiIlHehhpejaIjzh9eomSjZR+ipslWIRLAgMDOR2DOqKogTB9pCUJBagDBXR6XB0EBkIIsaRsGGMMAxoDBgYHTKJiUYEGDAzHC9EACcUGkIgFzgwZ0QsSBcXHiQvOwgDdEwfFs0sDzt4S6BK4xYjkDOzn0unFeBzOBijIm1Dgmg5YFQwsCMjp1oJ8LyIAACH5BAAKAAgALAAAAAAgACAAAATwEMhJaVKp6s2nIkqFZF2VIBWhUsJaTokqUCoBq+E71SRQeyqUToLA7VxF0JDyIQh/MVVPMt1ECZlfcjZJ9mIKoaTl1MRIl5o4CUKXOwmyrCInCKqcWtvadL2SYhyASyNDJ0uIiUd6GGl6NoiPOH16iZKNlH6KmyWFOggHhEEvAwwMA0N9GBsEC6amhnVcEwavDAazGwIDaH1ipaYLBUTCGgQDA8NdHz0FpqgTBwsLqAbWAAnIA4FWKdMLGdYGEgraigbT0OITBcg5QwPT4xLrROZL6AuQAPUS7bxLpoWidY0JtxLHKhwwMJBTHgPKdEQAACH5BAAKAAkALAAAAAAgACAAAATrEMhJaVKp6s2nIkqFZF2VIBWhUsJaTokqUCoBq+E71SRQeyqUToLA7VxF0JDyIQh/MVVPMt1ECZlfcjZJ9mIKoaTl1MRIl5o4CUKXOwmyrCInCKqcWtvadL2SYhyASyNDJ0uIiUd6GAULDJCRiXo1CpGXDJOUjY+Yip9DhToJA4RBLwMLCwVDfRgbBAaqqoZ1XBMHswsHtxtFaH1iqaoGNgAIxRpbFAgfPQSqpbgGBqUD1wBXeCYp1AYZ19JJOYgH1KwA4UBvQwXUBxPqVD9L3sbp2BNk2xvvFPJd+MFCN6HAAIKgNggY0KtEBAAh+QQACgAKACwAAAAAIAAgAAAE6BDISWlSqerNpyJKhWRdlSAVoVLCWk6JKlAqAavhO9UkUHsqlE6CwO1cRdCQ8iEIfzFVTzLdRAmZX3I2SfYIDMaAFdTESJeaEDAIMxYFqrOUaNW4E4ObYcCXaiBVEgULe0NJaxxtYksjh2NLkZISgDgJhHthkpU4mW6blRiYmZOlh4JWkDqILwUGBnE6TYEbCgevr0N1gH4At7gHiRpFaLNrrq8HNgAJA70AWxQIH1+vsYMDAzZQPC9VCNkDWUhGkuE5PxJNwiUK4UfLzOlD4WvzAHaoG9nxPi5d+jYUqfAhhykOFwJWiAAAIfkEAAoACwAsAAAAACAAIAAABPAQyElpUqnqzaciSoVkXVUMFaFSwlpOCcMYlErAavhOMnNLNo8KsZsMZItJEIDIFSkLGQoQTNhIsFehRww2CQLKF0tYGKYSg+ygsZIuNqJksKgbfgIGepNo2cIUB3V1B3IvNiBYNQaDSTtfhhx0CwVPI0UJe0+bm4g5VgcGoqOcnjmjqDSdnhgEoamcsZuXO1aWQy8KAwOAuTYYGwi7w5h+Kr0SJ8MFihpNbx+4Erq7BYBuzsdiH1jCAzoSfl0rVirNbRXlBBlLX+BP0XJLAPGzTkAuAOqb0WT5AH7OcdCm5B8TgRwSRKIHQtaLCwg1RAAAOwAAAAAAAAAAAA==" alt="" />
                    </div>
                )}

                <footer className="footer">

                    <div className="footer__sponsors">
                        <div className="footer__inner">
                            <div className="footer__sponsors__lockup"></div>
                        </div>
                    </div>

                    <div className="footer__disclaimer">
                        <div className="footer__inner">
                            <p>All personal information you provide will be used by the Western Bulldogs Football Club, AFL and on behalf of selected third parties in accordance with our Privacy Policy, this may include for promotional and direct marketing purposes and other disclosures as specified in our Privacy Policy found at <a href="http://www.westernbulldogs.com.au/privacy" target="_blank">http://www.westernbulldogs.com.au/privacy</a>.</p>
                            <p>By providing your personal information, you agree to such use. Your personal information will be kept securely and confidentially. Although you may choose not to provide us with certain information, please be aware that this choice will affect the type of and quality of service we can provide to you.</p>
                        </div>
                    </div>

                    <div className="footer__links">
                        <div className="footer__inner">
                            <div className="links">
                                <a href="http://www.westernbulldogs.com.au" target="_blank">www.westernbulldogs.com.au</a> <span>/</span><br />
                                <a href="http://membership.westernbulldogs.com.au/" target="_blank">Members Home</a> <span>/</span><br />
                                <a href="http://www.westernbulldogs.com.au/privacy" target="_blank">Privacy Policy</a> <span>/</span><br />
                                <a href="https://membership.westernbulldogs.com.au/terms-conditions-2" target="_blank">Terms &amp; Conditions</a> <span>/</span><br />
                                <a href="https://membership.westernbulldogs.com.au/terms-conditions-2" target="_blank">Disclaimer</a> <span>/</span><br />
                                <a href="http://www.westernbulldogs.com.au/club/contact-us" target="_blank">Contact Us</a>
                            </div>
                            <br />
                            &copy; Western Bulldogs Football Club 2017
                        </div>
                    </div>

                </footer>

                <Modal visible={this.state.errors.length > 0} button={'Ok'} buttonAction={() => { this.setState({ errors: [] }) }} buttonDisabled={false} ref={(modal) => { this._modal = modal; }}>
                    {errors}
                </Modal>

            </div>
        );
    }

}

export default App;
