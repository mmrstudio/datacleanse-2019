
    <footer class="footer">

        @if($campaign->creative->brand_banner)
        <div class="footer__brand-banner">
            <a href="{{ $campaign->brand_banner_url }}?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank"><img src="{{ $campaign->asset('images/brand-banner.png') }}"></a>
        </div>
        @endif

        <div class="footer__text">{!! $campaign->footer_text !!}</div>

        <ul class="footer__links">
            @if($campaign->privacy_url)<li><a href="{{ $campaign->privacy_url }}?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank">Privacy Policy</a></li>@endif
            @if($campaign->terms_url)<li><a href="{{ $campaign->terms_url }}?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank">Terms &amp; Conditions</a></li>@endif
            @if($campaign->contact_url)<li><a href="{{ $campaign->contact_url }}?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank">Contact Us</a></li>@endif
        </ul>

        <div class="footer__datacleanse">

            <a class="footer__datacleanse__dc" href="http://datacleanse.com.au/?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                	 viewBox="0 0 162 20" style="enable-background:new 0 0 162 20;" xml:space="preserve">
                <g>
                	<path d="M8.6,19.7H0V0.3h8.6c6.7,0,8.6,2.1,9,5.5c0.2,1.6,0.3,2.9,0.3,4.2c0,1.3-0.1,2.6-0.3,4.2C17.3,17.5,15.3,19.7,8.6,19.7z
                		 M13.7,13.2c0.1-1.5,0.2-2.4,0.2-3.2c0-0.9,0-1.7-0.2-3.2C13.5,3.3,10.2,3,7.2,3H3.8V17h3.4C10.2,17,13.5,16.6,13.7,13.2z"/>
                	<path d="M18.9,10.2c0-3.5,2.7-4.9,7-4.9c2.8,0,6.5,0.8,6.5,3.7v9.8L31.5,19c-1.1,0.2-3.1,1-6,1c-4.2,0-6.8-1.3-6.8-4.4
                		c0-4.3,3.2-4,6.9-4.3c3.3-0.2,3.4-0.7,3.4-1.6c0-1.8-1-2.2-3-2.2c-2.8,0-3.5,0.8-3.5,2.6H18.9z M22.4,15.3c0,2,1.5,2.4,3.4,2.4
                		c1,0,3.2-0.1,3.2-1.5V13c-0.8,0.2-1.5,0.4-3.2,0.5C23.2,13.6,22.4,13.8,22.4,15.3z"/>
                	<path d="M42.5,17.6c0.7,0,1.3-0.1,2.1-0.2v2.3c-1,0.1-1.9,0.3-2.9,0.3c-2.2,0-4.6-0.6-5.3-2.7c-0.3-0.8-0.3-1.2-0.3-2V7.9h-2.7V5.6
                		h2.7V2.3h3.8v3.4h4.8v2.3h-4.8v7.3c0,0.5,0,0.8,0.2,1.3C40.6,17.4,41.6,17.6,42.5,17.6z"/>
                	<path d="M45.7,10.2c0-3.5,2.7-4.9,7-4.9c2.8,0,6.5,0.8,6.5,3.7v9.8L58.2,19c-1.1,0.2-3.1,1-6,1c-4.2,0-6.8-1.3-6.8-4.4
                		c0-4.3,3.2-4,6.9-4.3c3.3-0.2,3.4-0.7,3.4-1.6c0-1.8-1-2.2-3-2.2c-2.8,0-3.5,0.8-3.5,2.6H45.7z M49.1,15.3c0,2,1.5,2.4,3.4,2.4
                		c1,0,3.2-0.1,3.2-1.5V13c-0.8,0.2-1.5,0.4-3.2,0.5C50,13.6,49.1,13.8,49.1,15.3z"/>
                	<path d="M69.9,17.7c5.1,0,5.3-2.7,5.3-5.1h3.8c0,3.8-1.1,7.3-9.1,7.3c-6.7,0-8.8-2.6-9.1-5.8c-0.1-1.6-0.2-2.9-0.2-4.2
                		c0-1.3,0.1-2.6,0.2-4.2C61,2.6,63.2,0,69.9,0c7.4,0,9,2.5,9.1,6.8h-3.8c0-1.5-0.1-4.5-5.3-4.5c-4.7,0-5.1,2.5-5.2,3.9
                		c-0.1,1.5-0.2,2.7-0.2,3.9c0,1.2,0.1,2.3,0.2,3.9C64.8,15.3,65.7,17.7,69.9,17.7z"/>
                	<path d="M84,19.7h-3.8V0.3H84V19.7z"/>
                	<path d="M100.3,13.5H89.3c0,2.7,1.1,4.3,4,4.3c2.5,0,3.4-1,3.4-1.9v-0.4h3.7l0,0.4c-0.1,1.8-1.6,4.1-7.1,4.1
                		c-4.8,0-7.7-1.3-7.7-7.3c0-6,3-7.3,7.7-7.3c5.9,0,7.2,2.5,7.2,7.3V13.5z M96.7,10.9c0-1.8-0.7-3.3-3.6-3.3c-3.1,0-3.8,1.6-3.8,3.3
                		v0.3h7.4V10.9z"/>
                	<path d="M101.5,10.2c0-3.5,2.7-4.9,7-4.9c2.8,0,6.5,0.8,6.5,3.7v9.8L114,19c-1.1,0.2-3.1,1-6,1c-4.2,0-6.8-1.3-6.8-4.4
                		c0-4.3,3.2-4,6.9-4.3c3.3-0.2,3.4-0.7,3.4-1.6c0-1.8-1-2.2-3-2.2c-2.8,0-3.5,0.8-3.5,2.6H101.5z M104.9,15.3c0,2,1.5,2.4,3.4,2.4
                		c1,0,3.2-0.1,3.2-1.5V13c-0.8,0.2-1.5,0.4-3.2,0.5C105.8,13.6,104.9,13.8,104.9,15.3z"/>
                	<path d="M130.8,9v10.6H127V10c0-1-0.3-2.4-3.2-2.4c-2.8,0-3.2,1.4-3.2,2.4v9.7h-3.8V6.6l0.5,0c1.1-0.1,3.4-1.2,6.8-1.2
                		C128.2,5.3,130.8,6.5,130.8,9z"/>
                	<path d="M146.1,10.1h-3.6c0-1.2-0.4-2.7-3.2-2.7c-2.5,0-2.8,0.8-2.8,2c0,1.2,0.8,1.6,3.9,1.9c2.8,0.3,6.1,0.1,6.1,4
                		c0,4-4.3,4.8-6.9,4.8c-3.7,0-7.3-0.9-7.3-4.9h3.8c0,2.7,1.9,2.8,3.4,2.8c2.2,0,3.4-0.7,3.4-2.1c0-1.9-1.5-1.8-4.3-2
                		c-3.6-0.4-5.8-0.6-5.8-4c0-3.9,3.5-4.5,6.7-4.5C144.1,5.3,146.1,7,146.1,10.1z"/>
                	<path d="M162,13.5h-11.1c0,2.7,1.1,4.3,4,4.3c2.5,0,3.4-1,3.4-1.9v-0.4h3.7l0,0.4c-0.1,1.8-1.6,4.1-7.1,4.1c-4.8,0-7.7-1.3-7.7-7.3
                		c0-6,3-7.3,7.7-7.3c5.9,0,7.2,2.5,7.2,7.3V13.5z M158.3,10.9c0-1.8-0.7-3.3-3.6-3.3c-3.1,0-3.8,1.6-3.8,3.3v0.3h7.4V10.9z"/>
                </g>
                </svg>
            </a>

            <span></span>

            <a class="footer__datacleanse__mmr" href="http://mmr.com.au/?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                	 viewBox="0 0 64 25" style="enable-background:new 0 0 64 25;" xml:space="preserve">
                <g>
                	<g>
                		<path d="M2,0h0.6l1.7,3.9H3.6L3.2,3H1.4L1,3.9H0.3L2,0z M2.9,2.4L2.3,0.9L1.6,2.4H2.9z"/>
                		<path d="M6.6,0.1h1.5c0.9,0,1.5,0.5,1.5,1.3v0c0,0.9-0.7,1.3-1.5,1.3H7.3v1.2H6.6V0.1z M8,2.1c0.5,0,0.8-0.3,0.8-0.7v0
                			c0-0.5-0.3-0.7-0.8-0.7H7.3v1.4H8z"/>
                		<path d="M10.2,0.1H12c0.5,0,0.9,0.1,1.1,0.4c0.2,0.2,0.3,0.5,0.3,0.8v0c0,0.6-0.4,1-0.9,1.2l1,1.5h-0.8l-0.9-1.3h0h-0.8v1.3h-0.7
                			V0.1z M11.9,2c0.5,0,0.8-0.3,0.8-0.6v0c0-0.4-0.3-0.6-0.8-0.6h-1V2H11.9z"/>
                		<path d="M14,2L14,2c0-1.1,0.8-2,2-2c1.2,0,2,0.9,2,2v0c0,1.1-0.8,2-2,2C14.9,4,14,3.1,14,2z M17.3,2L17.3,2c0-0.8-0.5-1.4-1.3-1.4
                			c-0.8,0-1.3,0.6-1.3,1.4v0c0,0.8,0.5,1.4,1.3,1.4C16.8,3.4,17.3,2.8,17.3,2z"/>
                		<path d="M18.9,0.1h1.4c1.2,0,2,0.8,2,1.9v0c0,1.1-0.8,1.9-2,1.9h-1.4V0.1z M20.3,3.3c0.8,0,1.3-0.5,1.3-1.3v0
                			c0-0.8-0.5-1.3-1.3-1.3h-0.8v2.6H20.3z"/>
                		<path d="M23.1,2.3V0.1h0.7v2.2c0,0.7,0.4,1.1,1,1.1c0.6,0,1-0.4,1-1.1V0.1h0.7v2.2c0,1.2-0.6,1.7-1.6,1.7
                			C23.7,4,23.1,3.4,23.1,2.3z"/>
                		<path d="M27.1,2L27.1,2c0-1.1,0.8-2,2-2c0.7,0,1.1,0.2,1.5,0.6l-0.4,0.5c-0.3-0.3-0.6-0.5-1.1-0.5c-0.7,0-1.3,0.6-1.3,1.4v0
                			c0,0.8,0.5,1.4,1.3,1.4c0.5,0,0.8-0.2,1.1-0.5l0.4,0.4C30.2,3.7,29.8,4,29.1,4C28,4,27.1,3.1,27.1,2z"/>
                		<path d="M32.3,0.7h-1.2V0.1h3.1v0.6H33v3.2h-0.7V0.7z"/>
                		<path d="M36.4,2L36.4,2c0-1.1,0.8-2,2-2s2,0.9,2,2v0c0,1.1-0.8,2-2,2C37.2,4,36.4,3.1,36.4,2z M39.7,2L39.7,2
                			c0-0.8-0.5-1.4-1.3-1.4c-0.8,0-1.3,0.6-1.3,1.4v0c0,0.8,0.5,1.4,1.3,1.4C39.1,3.4,39.7,2.8,39.7,2z"/>
                		<path d="M41.2,0.1H44v0.6h-2.2v1.1h1.9v0.6h-1.9v1.6h-0.7V0.1z"/>
                	</g>
                	<g>
                		<g>
                			<g>
                				<g>
                					<path d="M46.4,5.9h8.4c1.7,0,3.1,0.2,4.2,0.6c1.1,0.4,2,0.9,2.6,1.6c0.6,0.6,1,1.2,1.3,2c0.3,0.7,0.4,1.6,0.4,2.6v0.1
                						c0,1.4-0.3,2.6-1,3.5c-0.7,1-1.6,1.7-2.7,2.3L64,25h-7.1l-3.6-5.5h-0.8V25h-6.2V5.9z M54.7,15c0.8,0,1.4-0.2,1.8-0.5
                						c0.4-0.3,0.6-0.8,0.6-1.4v-0.1c0-0.6-0.2-1.1-0.6-1.4c-0.4-0.3-1-0.5-1.8-0.5h-2.2V15H54.7z M0,7.5l6.2,9.6V25H0V7.5z
                						 M10.6,12.6L6.5,5.9H0l10.5,16.4h0.1l4.4-6.8V25h6.2V5.9h-6.5L10.6,12.6z M23.2,7.5l6.2,9.6V25h-6.2V7.5z M33.8,12.6l-4.1-6.7
                						h-6.5l10.5,16.4h0.1l4.4-6.8V25h6.2V5.9h-6.5L33.8,12.6z"/>
                				</g>
                			</g>
                		</g>
                	</g>
                </g>
                </svg>
            </a>

        </div>

    </footer>
