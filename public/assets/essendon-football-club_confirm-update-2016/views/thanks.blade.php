@extends($campaign->view('main'))

@section('content')

<header class="header">

    @include($campaign->view('common.header'))

    <div class="header__thanks"></div>

</header>

<footer class="footer">

    @include($campaign->view('common.footer'))

</footer>

@endsection
