

    <div class="footer__sponsors"></div>

    <div class="footer__disclaimer">
        <p>PRIVACY: All personal information you provide will be used by the Essendon Football Club and the AFL in accordance with the AFL &amp; AFL Clubs' Privacy Policy available at <a href="http://www.essendonfc.com.au/privacy" target="_blank">essendonfc.com.au/privacy</a>. By providing your personal information, you agree to such use by the Essendon Football Club and AFL.</p>
        <p>It is a condition of entry for this competition that you agree to the <a href="http://s.afl.com.au/staticfile/AFL%20Tenant/Essendon/Images/Terms%20and%20conditions%20-%20EFC%20Update-Confirm%20details%20campaign%202016.pdf" target="_blank">Terms and Conditions</a> and Essendon Football Club sending you news, information, promotions and updates and providing your personal information to selected partners for the purpose of receiving offers approved by the Club.  Entries open at 9am AEST on 23/6/16 and close at 5pm AEST 14/7/16.  Entry is open to residents of VIC, NT, QLD, SA, TAS, NSW, ACT and WA aged 18+. NSW Permit No. LTPS/16/03836. The winner will be drawn at 10am on 28/7/16 and advised by phone. One entrant will win a 2016 team signed guernsey valued at $500.  The Promoter is Essendon Football Club.</p>
        <p>You have received this communication because you subscribed to receive updates from Essendon Football Club Email sent by: Essendon Football Club, 275 Melrose Drive, Melbourne Airport VIC 3045</p>
        <p><a href="http://s.afl.com.au/staticfile/AFL%20Tenant/Essendon/Images/Terms%20and%20conditions%20-%20EFC%20Update-Confirm%20details%20campaign%202016.pdf" target="_blank">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a href="http://datacleanse.com.au" target="_blank">Product of Datacleanse</a></p>
    </div>
