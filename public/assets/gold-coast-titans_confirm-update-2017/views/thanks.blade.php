@extends($campaign->view('main'))

@section('content')
<div class="body-wrap__inner">

	<div class="main">

        <div class="form__inner">

            <div class="thanks">
                <h1 class="thanks__title">THANK YOU!</h1>
                <h2 class="thanks__sub-title" style="text-transform: uppercase">Thank for updating your details with the club. The information you provide enables us to provide better service and more direct communication to you as a member of the club.</h2>
                <p class="thanks__body">If you have a specific enquiry that you would like the club to contact you about (for example: 2018 membership options, renewal process, seat change requests etc), please feel free to submit your enquiry direct to the <a href="mailto:membersinfo@titans.com.au" style="color:inherit; text-decoration:underline;">Gold Coast Titans membership department</a>.</p>
                <h1 class="thanks__title" style="font-size: 3.5rem; margin: 40px 0 0;">Good luck!</h1>
            </div>

        </div>

	</div>

    @include($campaign->view('common.footer'))

</div>


@endsection
