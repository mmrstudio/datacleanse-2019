@extends($campaign->view('main'))

@section('content')
<div class="body-wrap__inner">

	<div class="main">

        <header class="header">
            <div class="header__logo">
                <a href="{{ $campaign->client_url }}/?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank"><img src="{{ $campaign->asset('images/header-logo.png') }}"></a>
            </div>
        </header>

        <div class="form__inner">

            <div class="thanks">
                <h1 class="thanks__title">THANK YOU!</h1>
                <h2 class="thanks__sub-title" style="text-transform: uppercase">This campaign is now closed</h2>
            </div>

        </div>

	</div>

    @include($campaign->view('common.footer'))

</div>


@endsection
