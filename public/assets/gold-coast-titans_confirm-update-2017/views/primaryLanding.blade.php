@extends($campaign->view('main'))

@section('content')
<div class="body-wrap__inner">

	<div class="main">

		<form action="{{ $process_url }}" method="post" class="form">

			@include($campaign->view('common.errors'))

			<div class="form__inner">

				<div class="form__top">
					<div class="form__top__intro intro" style="letter-spacing: 1px;">
                        We would like to ensure that the club has all of the correct details for you.<br><br>
                        Confirm or update your details by midnight Sunday 8th October and you could <span style="color:#0D9ED8">WIN</span> a <span style="color:#0D9ED8">FREE RENEWAL OF YOUR 2018 MEMBERSHIP PACKAGE(S).</span>
                    </div>
				</div>

				<div class="form__fields">

                    <h2 class="form__heading heading">Are your details correct?</h2>

					@include($campaign->view('fields.text'), [
						'key' => 'first_name',
                        'width' => 'half',
					])

                    @include($campaign->view('fields.text'), [
						'key' => 'last_name',
                        'width' => 'half',
					])


                    @include($campaign->view('fields.address'), [
						'keys' => ['street_addr_1', 'street_addr_2'],
					])

                    @include($campaign->view('fields.text'), [
						'key' => 'city',
						'id' => 'suburbField',
                        'width' => 'half',
					])

                    @include($campaign->view('fields.state'), [
						'key' => 'state',
                        'width' => 'half',
					])

                    @include($campaign->view('fields.text'), [
						'key' => 'postcode',
						'id' => 'postcodeField',
                        'width' => 'half',
					])

                    @include($campaign->view('fields.country'), [
						'key' => 'country',
                        'width' => 'half',
					])

                    @include($campaign->view('fields.dob'), [
						'key' => 'birth_date',
						'placeholder' => 'Please provide your DOB...',
                        'width' => 'half',
                        'not_required' => $input->birth_date == 'supplied'
					])

                    @include($campaign->view('fields.text'), [
						'key' => 'phone_mobile',
                        'width' => 'half',
					])

                    @include($campaign->view('fields.email'), [
						'key' => 'email_addr',
                        'width' => 'full',
					])

				</div>

				<div class="form__submit">

					@foreach($hidden_fields as $name=>$value)
					<input type="hidden" name="{{ $name }}" value="{{ $value }}">
					@endforeach

					<button class="form__submit__button" type="submit" style="text-transform: none;">{{ $campaign->creative->button_text }}</button>

				</div>

			</div>

		</form>

	</div>

    @include($campaign->view('common.footer'))

</div>


@endsection
