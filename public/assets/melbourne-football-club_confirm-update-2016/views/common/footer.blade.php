
    <div class="footer__sponsors">
        <img src="{{ $campaign->asset('images/sponsors.png') }}">
    </div>

    <p>Melbourne Football Club | 1300 DEMONS (1300 336 667 | PO Box 223 East Melbourne 8002</p>

    <ul class="footer__links">
        <li><a href="http://www.melbournefc.com.au/" target="_blank">Melbourne Football Club</a></li>
        <li><a href="http://www.melbournefc.com.au/privacy" target="_blank">Privacy Policy</a></li>
        <li><a href="http://www.melbournefc.com.au/projectconnect" target="_blank">Terms &amp; Conditions</a></li>
        <li><a href="http://datacleanse.com.au" target="_blank">Product of Datacleanse</a></li>
    </ul>
