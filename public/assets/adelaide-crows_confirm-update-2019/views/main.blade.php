<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">

    <head>

    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    	<title>{{ $campaign->campaignTitle() }}</title>

    	<link href="{{ $campaign->asset('css/campaign.css') }}" rel="stylesheet">
    	<link href="{{ $campaign->asset('fonts/IndustryInc-Base.css') }}" rel="stylesheet">
    	<link href="{{ $campaign->asset('fonts/ITCAvantGardeStd-Bk.css') }}" rel="stylesheet">
    	<link href="{{ $campaign->asset('fonts/ITCAvantGardeStd-Bold.css') }}" rel="stylesheet">

        <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
        <link rel="icon" href="{{ $campaign->asset('icon.ico') }}" />

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-33851616-15', 'auto');
          ga('send', 'pageview');

        </script>

        <script src="{{ $campaign->asset('js/modernizr.js') }}"></script>

        @if($campaign->creative)
        <style>

            body {
                font-family: {!! $campaign->creative->body_font !!}, Helvetica, Arial, sans-serif;
                background: url({{ $campaign->asset('images/hero-banner.jpg') }}) no-repeat center top/1800px auto {{ $campaign->creative->body_background_color }};
                color: {{ $campaign->creative->body_color }};
            }

            .title {
                font-family: {!! $campaign->creative->title_font !!}, Helvetica, Arial, sans-serif;
                font-size: {{ $campaign->creative->title_size['desktop'] }}rem;
                line-height: {{ $campaign->creative->title_line_height }};
                color: {{ $campaign->creative->title_color }};
                text-transform: uppercase;
                text-align: center;
            }

            .heading {
                font-family: {!! $campaign->creative->heading_font !!}, Helvetica, Arial, sans-serif;
                font-size: {{ $campaign->creative->heading_size['desktop'] }}rem;
                line-height: {{ $campaign->creative->heading_line_height }};
                color: {{ $campaign->creative->heading_color }};
                text-transform: uppercase;
            }

            .intro {
                font-family: {!! $campaign->creative->intro_font !!}, Helvetica, Arial, sans-serif;
                font-size: {{ $campaign->creative->intro_size['desktop'] }}rem;
                line-height: {{ $campaign->creative->intro_line_height }};
                color: {{ $campaign->creative->intro_color }};
                text-align: center;
            }

            .intro strong {
                font-family: 'ITCAvantGardeStd-Bold';
                color: #E31937;
            }

            .form__inner.afc_survey_form {
                padding: 66px 60px 0;
            }

            .afc_survey_form .form__submit {
                padding: 66px 0 77px;
            }

            .form__inner { background-color: {{ $campaign->creative->form_background_color }}; }
            .form__group { font-size: {{ $campaign->creative->field_size['desktop'] }}rem; }
            .form__group__border { background-color: {{ $campaign->creative->field_border_color }}; }
            .form__control--textarea { border-color: {{ $campaign->creative->field_border_color }}; }
            .form__group__label { color: {{ $campaign->creative->field_label_color }}; font-family: 'ITCAvantGardeStd-Bold'; }
            .form__control { color: {{ $campaign->creative->field_text_color }}; }
            .form__control:focus { color: {{ $campaign->creative->field_focus_color }}; }
            .form__control:focus + .form__group__border { background-color: {{ $campaign->creative->field_focus_border_color }}; }
            .form__control--textarea:focus { border-color: {{ $campaign->creative->field_focus_border_color }}; }
            .form__group--error .form__group__label { color: {{ $campaign->creative->field_error_color }}; }
            .form__group--error .form__group__border { background-color: {{ $campaign->creative->field_error_color }}; }
            .form__errors { background-color: {{ $campaign->creative->field_error_color }}; }
            .form__control--checkbox + span, .form__control--radio + span { border-color: #002a5b; }
            .form__control--checkbox + span:before, .form__control--radio + span:before { color: #002a5b; }

            ::-webkit-input-placeholder { color: {{ $campaign->creative->field_placeholder_color }}; }
            ::-moz-placeholder { color: {{ $campaign->creative->field_placeholder_color }}; }
            :-ms-input-placeholder { color: {{ $campaign->creative->field_placeholder_color }}; }
            :-moz-placeholder { color: {{ $campaign->creative->field_placeholder_color }}; }

            .form__submit__button {
                font-family: {{ $campaign->creative->button_font }}, Helvetica, Arial, sans-serif;
                font-size: {{ $campaign->creative->button_size['desktop'] }}rem;
                color: {{ $campaign->creative->button_foreground_color }};
                background-color: {{ $campaign->creative->button_background_color }};
                padding: {{ $campaign->creative->button_padding }};
                text-transform: lowercase;
            }

            .form__submit__button.inverse {
                background-color: {{ $campaign->creative->button_foreground_color }};
                color: {{ $campaign->creative->button_background_color }};
            }

            .form__submit__button:hover {
                color: {{ $campaign->creative->button_hover_foreground_color }};
                background-color: {{ $campaign->creative->button_hover_background_color }};
            }

            .footer {
                font-size: {{ $campaign->creative->footer_size['desktop'] }}rem;
                color: {{ $campaign->creative->footer_color }}
            }

            .footer svg path { fill: {{ $campaign->creative->footer_color }} }
            .footer__datacleanse { border-color: {{ $campaign->creative->footer_color }} }
            .footer__datacleanse span { background-color: {{ $campaign->creative->footer_color }}; }
            .footer__links > li + li { border-color: {{ $campaign->creative->footer_color }} }

            .thanks__title {
                font-family: {{ $campaign->creative->thanks_title_font }}, Helvetica, Arial, sans-serif;
                font-size: {{ $campaign->creative->thanks_title_size['desktop'] }}rem;
                color: {{ $campaign->creative->thanks_title_color }};
                line-height: 1.24;
            }

            .thanks__sub-title {
                font-family: {{ $campaign->creative->thanks_sub_title_font }}, Helvetica, Arial, sans-serif;
                font-size: {{ $campaign->creative->thanks_sub_title_size['desktop'] }}rem;
                color: {{ $campaign->creative->thanks_sub_title_color }};
                text-transform: uppercase;
                margin: -30px 0 30px;
            }

            .thanks__body {
                font-family: {{ $campaign->creative->thanks_body_font }}, Helvetica, Arial, sans-serif;
                font-size: {{ $campaign->creative->thanks_body_size['desktop'] }}rem;
                color: {{ $campaign->creative->thanks_body_color }};
            }

            .thanks__body strong {
                font-family: 'ITCAvantGardeStd-Bold';
                color: #E31937;
            }

            .afc_survey_thanks {
                padding: 0 20px;
            }

            .afc_survey .thanks__title {
                width: 100%;
                font-family: 'IndustryIncBase', Helvetica, Arial, sans-serif;
                font-size: 4.5625rem;
                color: #FFD200;
                margin: 0 0 20px;
                text-align: center;
            }

            .afc_survey .thanks__sub-title {
                width: 100%;
                font-family: {{ $campaign->creative->thanks_sub_title_font }}, Helvetica, Arial, sans-serif;
                font-size: 2.5rem;
                color: #fff;
                text-transform: uppercase;
                text-align: center;
                margin: 0 0 46px;
            }

            .afc_survey .thanks__body {
                width: 100%;
                font-family: {{ $campaign->creative->thanks_body_font }}, Helvetica, Arial, sans-serif;
                font-size: 1.625rem;
                line-height: 1.576;
                color: #fff;
                text-align: center;
                margin: 0 0 52px;
            }

            .afc_survey .thanks__body strong {
                font-family: 'ITCAvantGardeStd-Bold';
                color: #FFD200;
            }

            .footer {;
                font-family: 'ITCAvantGardeStd-Bk', Helvetica, Arial, sans-serif;
                font-size: 1.125rem;
            }

            @media screen and (max-width: 660px) {

                body {
                    background: url({{ $campaign->asset('images/hero-banner.jpg') }}) no-repeat center top/auto 220px {{ $campaign->creative->body_background_color }};
                }

                /*.header {
                    background-color: {{ $campaign->creative->body_background_color_mobile }};
                }*/

                .title {
                    font-size: {{ $campaign->creative->title_size['mobile'] }}rem;
                }

                .heading {
                    font-size: {{ $campaign->creative->heading_size['mobile'] }}rem;
                }

                .intro {
                    font-size: {{ $campaign->creative->intro_size['mobile'] }}rem;
                }

                .footer {
                    font-size: {{ $campaign->creative->footer_size['mobile'] }}rem;
                }

                .form__group {
                    font-size: {{ $campaign->creative->field_size['mobile'] }}rem;
                }

                .form__submit__button {
                    font-size: {{ $campaign->creative->button_size['mobile'] }}rem;
                }

                .thanks__title {
                    font-size: {{ $campaign->creative->thanks_title_size['mobile'] }}rem !important;
                }

                .thanks__sub-title {
                    font-size: {{ $campaign->creative->thanks_sub_title_size['mobile'] }}rem !important;
                }

                .thanks__body {
                    font-size: {{ $campaign->creative->thanks_body_size['mobile'] }}rem !important;
                }

                .thanks__body br {
                    display: none;
                }

            }

        </style>
        @endif

    </head>

    <body>

        <div class="body-wrap">

            @yield('content')

        </div>

        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    	<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
        <script src="{{ $campaign->asset('js/campaign.js') }}"></script>

    </body>

</html>
