<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--
<?php print_r($record->toArray()); ?>
-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Confirm or update your details and WIN</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0 " />
<meta name="format-detection" content="telephone=no" />
<base href="<?php echo url('assets/adelaide-crows_confirm-update-2019/edm'); ?>/">
<style type="text/css">
body {
	margin: 0 !important;
	padding: 0 !important;
	-webkit-text-size-adjust: 100% !important;
	-ms-text-size-adjust: 100% !important;
	-webkit-font-smoothing: antialiased !important;
}
.visible-mobile {
  max-height: 0;
  width: 0;
  display: none;
}
img {
	border: 0 !important;
	outline: none !important;
}
p {
	Margin: 0px !important;
	Padding: 0px !important;
}
table {
	border-collapse: collapse;
	mso-table-lspace: 0px;
	mso-table-rspace: 0px;
}
td, a, span {
	border-collapse: collapse;
	mso-line-height-rule: exactly;
}
.ExternalClass * {
	line-height: 100%;
}
.em_defaultlink a {
	color: inherit !important;
	text-decoration: none !important;
}
span.MsoHyperlink {
	mso-style-priority: 99;
	color: inherit;
}
span.MsoHyperlinkFollowed {
	mso-style-priority: 99;
	color: inherit;
}
.em_text { font-family:Arial,sans-serif; font-size:14px; line-height:24px;  color:#002A5B; text-transform: uppercase;}
.em_text a { text-decoration:none; color:#828282;}
.em_text1, .em_text1 a { font-family: Helvetica, Arial, sans-serif; font-size: 16px; line-height: 20px; color: #002A5B; text-decoration: none;   }
.em_text2, .em_text2 a { font-family: Helvetica, Arial, sans-serif; font-size: 22px; line-height: 26px; color:#002A5B; text-decoration: none; font-weight:bold; text-transform: uppercase;
	}
.em_text3, .em_text3 a { font-family: Helvetica, Arial, sans-serif; font-size: 18px; line-height: 20px; color: #002A5B; text-decoration: none; }
.em_text4, .em_text4 a { font-family: Helvetica, Arial, sans-serif; font-size: 14px; line-height: 17px; color: #002A5B; text-decoration: none; }
.em_text5, .em_text5 a { font-family: Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; color: #002A5B; text-decoration: none; }
.em_text6, .em_text6 a { font-family: Helvetica, Arial, sans-serif; font-size: 14px; line-height: 24px; color: #E21836; text-decoration: none; }
.em_text7, .em_text7 a { font-family: Helvetica, Arial, sans-serif; font-size: 11px; line-height: 17px; color: #002A5B; text-decoration: none; }
.em_text8, .em_text8 a { font-family: Helvetica, Arial, sans-serif; font-size: 14px; line-height: 17px; color: #fff; text-decoration: none; }
.preheader {
    display: none;
}
/*Stylesheet for the devices width between 481px to 660px*/
@media only screen and (min-width:481px) and (max-width:659px) {
.em_main_table { width:100% !important; }
.em_wrapper { width: 100% !important; }
.em_side{ padding: 0 50px !important; }
.em_aside { padding: 0 20px !important; /*Update the values as required*/ }
.em_aside2 { padding: 0 0px !important; /*Update the values as required*/ }
.em_aside3 { padding-left:10px!important;padding-right:10px!important; padding-bottom: 20px!important; /*Update the values as required*/ }
.em_hide { display: none !important; }
.em_text1, .em_text1 a{font-size:14px;line-height: 18px;}
.em_full_img { width: 100% !important; height: auto !important; max-width:none !important; }
.em_align_cent { text-align: center !important; }
.em_height { height:20px !important; font-size:0px !important; line-height:0px !important;}
.em_background { background-image:none  !important; height:auto !important; }
.em_pad { padding-top:20px !important;}
.em_side2 { width:20px !important;}
.em_side10 { width: 10px !important; /*Update the values as required*/ }
.em_pad_bottom {
	padding-bottom: 14px !important;
}
.em_text5{font-size:17px!important;line-height: 20px!important;}
.em_text8, .em_text8 a { font-size: 11px!important;}
.em_center1{
	text-align: center!important;
	height: inherit!important;
	border-bottom: none!important;
	padding-top: 8px!important;
}
.em_center2{
	text-align: center!important;
	height: inherit!important;
	padding-bottom: 8px!important;
}
.visible-mobile {
		display: block !important;
		max-height: none !important;
		width:100%!important;
		max-width: 100% !important;
		height: auto !important;
	  }
}
/*Stylesheet for the devices width between 0px to 480px*/
@media only screen and (max-width:480px) {
.em_main_table { width: 100% !important; }
.em_wrapper { width: 100% !important; }
.em_side{ padding: 0 50px !important; }
.em_aside { padding: 0 20px !important; /*Update the values as required*/ }
.em_aside2 { padding: 5px 0 0 0px !important; /*Update the values as required*/ }
.em_aside3 { padding-left:10px!important;padding-right:10px!important; padding-bottom: 20px!important; /*Update the values as required*/ }
.em_hide { display: none !important; }
.em_text1, .em_text1 a{font-size:14px;line-height: 18px;}
.em_text5{font-size:17px!important;line-height: 20px!important;}
.em_text8, .em_text8 a { font-size: 11px!important;}
.em_full_img { width: 100% !important; height: auto !important; max-width:none !important; }
.em_align_cent { text-align: center !important; }
.em_height { height:20px !important; font-size:0px !important; line-height:0px !important;}
.em_background { background-image:none  !important; height:auto !important; }
.em_pad { padding-top:20px !important;}
.em_side2 { width:20px !important;}
.em_side10 { width: 10px !important; /*Update the values as required*/ }
.em_br {
	display: block;
}
.em_pad_bottom {
	padding-bottom: 14px !important;
}
.em_center1{
	text-align: center!important;
	height: inherit!important;
	border-bottom: none!important;
	padding-top: 8px!important;
}
.em_center2{
	text-align: center!important;
	height: inherit!important;
	padding-bottom: 8px!important;
}
.visible-mobile {
	display: block !important;
	max-height: none !important;
	width:100%!important;
	max-width: 100% !important;
	height: auto !important;
  }
}
</style>
<!--[if gte mso 9]>
<xml>
  <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
 </o:OfficeDocumentSettings>
</xml>
<![endif]-->
</head>
<body style="margin: 0px;padding: 0px;-webkit-text-size-adjust: 100% !important;-ms-text-size-adjust: 100% !important;-webkit-font-smoothing: antialiased !important;" bgcolor="#f7f7f7"><span class="preheader" style="display: none;">Win a 2018 crows membership. Simply update or confirm your details by 4 July 2017</span>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f7f7f7" style="border-collapse: collapse;mso-table-lspace: 0px;mso-table-rspace: 0px;">
    	<tr>
        	<td>
            	<table width="700" cellpadding="0" cellspacing="0" border="0" align="center" class="em_main_table">
                	<tr>
                    	<td valign="top" align="center">
                    		<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                       			<tr>
										<td height="20" class="em_height">&nbsp;</td>
								</tr>
                        		<tr>
                        			<td align="center" class="em_text7">
                    					If you are having trouble viewing this email,<span class="em_br" style="border-collapse: collapse;mso-line-height-rule: exactly;"></span> please <webversion>click here</webversion> to view it as a webpage.
                    				</td>
								</tr>
                  				<tr>
										<td height="20" class="em_height">&nbsp;</td>
								</tr>
                   			</table>
                    	</td>
                    </tr>
                    <tr>
                    	<td valign="top" colspan="1">
                    		<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                        		<tr>
                        			<td align="center">
                    					<a href="http://www.afc.com.au/"><img src="images/img_03.jpg" editable="true" width="700" border="0" style="display:block; max-width: 700px;" class="em_hide" alt="Confirm or update your details and WIN"></a>
                    				</td>
                    				<!--[if !mso]><!-->
									<td valign="top" colspan="1">
											<a href="http://www.afc.com.au/"><img src="images/mobile_01.jpg" width="320" height="289" border="0" style="-moz-hyphens:none;-ms-hyphens:none;-ms-interpolation-mode:bicubic;-webkit-hyphens:none;border:none;clear:both;display:none;hyphens:none;max-height:0;max-width:100%;mso-hide:all;outline:0;text-decoration:none;width:0;word-break:keep-all" alt="Confirm or update your details and WIN" class="visible-mobile"></a>
									</td>
									<!--<![endif]-->
								</tr>
                   			</table>
                    	</td>
                    </tr>
                    <tr>
                    	<td valign="top" colspan="1" bgcolor="#fff" class="em_aside" style="padding-left: 50px; padding-right: 50px;">
                    		<table width="600" style="width:600px;" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
								<tr>
									<td valign="top" colspan="1" bgcolor="#fff" height="40"></td>
								</tr>
								<tr>
									<td valign="top" colspan="1" bgcolor="#fff">
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="center" style="padding-bottom: 32px;">
                                                     <span class="em_text5"><span style="font-weight:bold;">Hello <?php echo $record->name_first; ?>,</span><br>
<?php if($record->data_segement == 'WFAO Email' || $record->data_segement == 'WFAO SMS') : ?>
As a We Fly As One Member, we’d love to know more about you!<br>
<?php endif; ?>
It’s time to check your details! Simply confirm or update the below by 
10 July 2019 and you’ll go in the draw to <strong style="color: #E31937;">WIN a 2019 Team Signed Guernsey.</strong>
<br><br>
Plus if you provide additional details about yourself you will have the chance to<br> <strong style="color: #E31937;">WIN two tickets to the 2019 Club Champion Dinner.</strong></span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td valign="top" colspan="1" bgcolor="#fff">
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="center" style="padding-bottom: 20px;">
													 <span class="em_text2">CHECK YOUR DETAILS BELOW:</span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								 <tr>
									<td valign="top" colspan="1" bgcolor="#fff">
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="center" style="padding: 6px 0 6px 0; line-height: 24px;">
													 <span class="em_text">First Name*</span><br>
													<span class="em_text3"><?php echo $record->name_first ? $record->name_first : '<span style="color:#E21836;">Required</span>'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 6px 0 6px 0; line-height: 24px;">
													 <span class="em_text">Surname*</span><br>
													<span class="em_text3"><?php echo $record->name ? $record->name : '<span style="color:#E21836;">Required</span>'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 6px 0 6px 0; line-height: 24px;">
													 <span class="em_text">Address*</span><br>
													<span class="em_text3"><?php echo $record->street_addr_1 ? $record->street_addr_1 : '<span style="color:#E21836;">Required</span>'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 6px 0 6px 0; line-height: 24px;">
													 <span class="em_text">Suburb*</span><br>
													<span class="em_text3"><?php echo $record->city ? $record->city : '<span style="color:#E21836;">Required</span>'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 6px 0 6px 0; line-height: 24px;">
													 <span class="em_text">State*</span><br>
													<span class="em_text3"><?php echo $record->state ? $record->state : '<span style="color:#E21836;">Required</span>'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 6px 0 6px 0; line-height: 24px;">
													 <span class="em_text">Postcode*</span><br>
													<span class="em_text3"><?php echo $record->zip ? $record->zip : '<span style="color:#E21836;">Required</span>'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 6px 0 6px 0; line-height: 24px;">
													 <span class="em_text">Country*</span><br>
													<span class="em_text3"><?php echo $record->country ? $record->country : 'Australia'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 6px 0 6px 0; line-height: 24px;">
													 <span class="em_text">Email*</span><br>
													<span class="em_text3"><a href="mailto:<?php echo $record->email_addr; ?>" style="text-decoration:none;"><?php echo $record->email_addr; ?></a></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 6px 0 6px 0; line-height: 24px;">
													 <span class="em_text">Mobile Number*</span><br>
													<span class="em_text3"><?php echo $record->phone_mobile ? $record->phone_mobile : '<span style="color:#E21836;">Required</span>'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 6px 0 6px 0; line-height: 24px;">
													 <span class="em_text">Phone</span><br>
													<span class="em_text3"><?php echo $record->phone_home ? $record->phone_home : '-'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 6px 0 6px 0; line-height: 24px;">
													 <span class="em_text">Birthdate*</span><br>
													<span class="em_text3"><?php echo $record->birthdate ? $record->birthdate : '<span style="color:#E21836;">Required</span>'; ?></span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 6px 0 6px 0; line-height: 24px;">
													 <span class="em_text">Gender</span><br>
													<span class="em_text3"><?php echo $record->gender ? $record->gender : '-'; ?></span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
                   			</table>
                    	</td>
                    </tr>
				</table>
     		</td>
        </tr>
         <tr>
        	<td>
            	<table width="700" cellpadding="0" cellspacing="0" border="0" align="center" class="em_main_table">
                    <tr>
                    	<td valign="top" colspan="1" bgcolor="#fff" class="em_aside" style="padding-left: 50px; padding-right: 50px;">
                    		<table width="600" style="width:600px;" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                    			<tr>
									<td align="center" valign="top" colspan="1" bgcolor="#fff" style="padding-bottom:41px; padding-top:41px;">
										<table width="526" style="width:526px;" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td>
													<table width="251" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="left">
														<tr>
															<td valign="top" align="right" style="" class="em_pad_bottom">
																<a href="<?php echo $record->confirm_url; ?>"><img src="images/img_07.jpg" width="250" height="60" border="0" style="display:block; max-width: 251px;" class="em_full_img"></a>
															</td>
														</tr>
													</table>
													<table width="251" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="right">
														<tr>
															<td valign="top" align="left">
																  <a href="<?php echo $record->update_url; ?>"><img src="images/img_09.jpg" width="250" height="60" border="0" style="display:block; max-width: 251px;" class="em_full_img"></a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
                 				<tr>
									<td valign="top" colspan="1" bgcolor="#fff">
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="center" style="padding: 0px 0 6px 0; line-height: 20px;">
														<span class="em_text6">*Fields mandatory</span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
                  				<tr>
									<td valign="top" colspan="1" bgcolor="#fff">
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="center" style="padding: 0px 0 25px 0; line-height: 20px;">
													<span class="em_text1">Click on the buttons to confirm or update your details.<br class="em_hide">
 Do not reply to this email.</span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
                 				<tr>
									<td valign="top" colspan="1" bgcolor="#fff">
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="center" style="padding: 0px 0px 10px 0px;" class="em_aside3">
			<span class="em_text1">For any queries please contact Member Services at<br> <a href="mailto:membership@afc.com.au"><strong>membership@afc.com.au</strong></a>
or <strong>(08) 8440 6690</strong>.</span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td height="30" bgcolor="#FFF"></td>
					</tr>
					<tr>
						<td>
							<table width="700" style="width:700px;" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
								<tr>
									<td valign="top" colspan="1" bgcolor="#002B5C">
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="center" style="padding: 20px 0px 20px 0px;" class="em_aside3">
													<span class="em_text8"><a href="http://www.afc.com.au/privacy">PRIVACY POLICY</a>
														&nbsp;&nbsp;I&nbsp;&nbsp; <unsubscribe>UNSUBSCRIBE</unsubscribe> &nbsp;&nbsp;I&nbsp;&nbsp;
														<a href="https://crowsmembership.com.au/terms-conditions-7">TERMS &amp; CONDITIONS</a>
													</span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
                     <tr>
                    	<td valign="top" colspan="1" bgcolor="#fff" class="em_aside2">
                    		<a href="http://www.afc.com.au/"><img src="images/img_sponsors.jpg" editable="true" width="700" border="0" style="display:block; max-width: 700px;" class="em_hide" alt=""></a>
                    	</td>
                    </tr>
                     <tr>
                    	<td valign="top" align="center">
                    		<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                       			<tr>
										<td height="40" class="em_height">&nbsp;</td>
								</tr>
                   			</table>
                    	</td>
                    </tr>
                 </table>
            </td>
       	</tr>
    </table>
   
<div class="em_hide" style="white-space:nowrap;font:20px courier;color:#ffffff;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
</body>
</html>
