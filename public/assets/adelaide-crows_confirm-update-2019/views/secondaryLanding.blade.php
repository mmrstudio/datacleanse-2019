@extends($campaign->view('main'))

@section('content')
<div class="body-wrap__inner">

	<div class="main">

        <header class="header">
            <div class="header__logo">
                <a href="{{ $campaign->client_url }}/?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank"><img src="{{ $campaign->asset('images/header-logo.png') }}"></a>
            </div>
        </header>


        <form action="{{ $process_url }}" method="post" class="form">

			@include($campaign->view('common.errors'))

            <div class="thanks">

                <h1 class="thanks__title">
                    THANK YOU {{ $record->name_first }}<br>
                    For keeping your details up to date!
                </h1>
                <p class="thanks__body" style="margin: 0;">You are now in the running to <strong>WIN 1 of 5 2019 Team Signed Guernseys</strong></p>

            </div>

            <div class="form__inner afc_survey_form" style="background-color: #002a5b;">

				<div class="form__fields form__fields--secondary afc_survey">

                    <div class="afc_survey_thanks">

                        <h1 class="thanks__title">WANT MORE CHANCES TO WIN?</h1>
                        <h2 class="thanks__sub-title">Tell us a little more about you! </h2>
                        <p class="thanks__body">
                            Complete your profile with the short survey below and you’ll also go in the<br>
                            draw to <strong>WIN two tickets to the 2019 Crows Club Champion Dinner.</strong>
                        </p>

                    </div>

                    <h3 class="form__sub-heading afc_survey_heading">1. Who is your favourite current Adelaide Crows AFL player?</h3>

                    @include($campaign->view('fields.dropdown'), [
                        'label' => false,
						'key' => 'survey_favourite_afl_player',
                        'options' => [
                            'Rory Atkins' => 'Rory Atkins',
                            'Eddie Betts' => 'Eddie Betts',
                            'Luke Brown' => 'Luke Brown',
                            'Jordon Butts' => 'Jordon Butts',
                            'Brad Crouch' => 'Brad Crouch',
                            'Matt Crouch' => 'Matt Crouch',
                            'Ben Davis' => 'Ben Davis',
                            'Tom Doedee' => 'Tom Doedee',
                            'Richard Douglas' => 'Richard Douglas',
                            'Cameron Ellis-Yolmen' => 'Cameron Ellis-Yolmen',
                            'Darcy Fogarty' => 'Darcy Fogarty',
                            'Jordan Gallucci' => 'Jordan Gallucci',
                            'Bryce Gibbs' => 'Bryce Gibbs',
                            'Hugh Greenwood' => 'Hugh Greenwood',
                            'Will Hamill' => 'Will Hamill',
                            'Kyle Hartigan' => 'Kyle Hartigan',
                            'Elliot Himmelberg' => 'Elliot Himmelberg',
                            'Paul Hunter' => 'Paul Hunter',
                            'Sam Jacobs' => 'Sam Jacobs',
                            'Josh Jenkins' => 'Josh Jenkins',
                            'Chayce Jones' => 'Chayce Jones',
                            'Alex Keath' => 'Alex Keath',
                            'Jake Kelly' => 'Jake Kelly',
                            'Riley Knight' => 'Riley Knight',
                            'Rory Laird' => 'Rory Laird',
                            'Tom Lynch' => 'Tom Lynch',
                            'David Mackay' => 'David Mackay',
                            'Shane McAdam' => 'Shane McAdam',
                            'Edward McHenry' => 'Edward McHenry',
                            'Andrew Mcpherson' => 'Andrew Mcpherson',
                            'Wayne Milera' => 'Wayne Milera',
                            'Lachlan Murphy' => 'Lachlan Murphy',
                            "Reilly O'Brien" => "Reilly O'Brien",
                            'Andrew Otten' => 'Andrew Otten',
                            'Myles Poholke' => 'Myles Poholke',
                            'Paul Seedsman' => 'Paul Seedsman',
                            'Lachlan Sholl' => 'Lachlan Sholl',
                            'Rory Sloane' => 'Rory Sloane',
                            'Brodie Smith' => 'Brodie Smith',
                            'Tyson Stengle' => 'Tyson Stengle',
                            'Kieran Strachan' => 'Kieran Strachan',
                            'Daniel Talia' => 'Daniel Talia',
                            'Taylor Walker' => 'Taylor Walker',
                            'Patrick Wilson' => 'Patrick Wilson',
                        ],
					])

					@include($campaign->view('fields.dropdown'), [
                        'label' => false,
						'key' => 'survey_favourite_aflw_player',
                        'options' => [
                            'Sarah Allan' => 'Sarah Allan',
                            'Ailish Considine' => 'Ailish Considine',
                            'Dayna Cox' => 'Dayna Cox',
                            'Courtney Cramey' => 'Courtney Cramey',
                            'Angela Foley' => 'Angela Foley',
                            'Jessica Foley' => 'Jessica Foley',
                            'Renee Forth' => 'Renee Forth',
                            'Nikki Gore' => 'Nikki Gore',
                            'Anne Hatchard' => 'Anne Hatchard',
                            'Jasmyn Hewett' => 'Jasmyn Hewett',
                            'Eloise Jones' => 'Eloise Jones',
                            'Sophie Li' => 'Sophie Li',
                            'Rheanne Lugg' => 'Rheanne Lugg',
                            'Ebony Marinoff' => 'Ebony Marinoff',
                            'Hannah Martin' => 'Hannah Martin',
                            'Jenna McCormick' => 'Jenna McCormick',
                            'Rhiannon Metcalfe' => 'Rhiannon Metcalfe',
                            'Justine Mules' => 'Justine Mules',
                            'Maisie Nankivell' => 'Maisie Nankivell',
                            'sarah Perkins' => 'sarah Perkins',
                            'Erin Phillips' => 'Erin Phillips',
                            'Danielle Ponter' => 'Danielle Ponter',
                            'Marijana Rajcic' => 'Marijana Rajcic',
                            'Chelsea Randall' => 'Chelsea Randall',
                            'Sally Riley' => 'Sally Riley',
                            'Katelyn Rosenzweig' => 'Katelyn Rosenzweig',
                            'Chloe Scheer' => 'Chloe Scheer',
                            'Jessica Sedunary' => 'Jessica Sedunary',
                            'Stevie-Lee Thompson' => 'Stevie-Lee Thompson',
                            'Deni Varnhagen' => 'Deni Varnhagen',
                        ],
					])

                    <h3 class="form__sub-heading afc_survey_heading">2. When it comes to the Adelaide Football Club I consider myself a...</h3>

                    <div class="form__group" data-width="full">
                        <div class="form__group__controls">
                            <ul class="afc_range">
                                <li>Casual Observer</li>
                                <li><label><input name="survey_consider" type="radio" value="1" {{ old('survey_consider', $record->survey_consider) == '1' ? 'checked' : '' }}><span>1</span></label></li>
                                <li><label><input name="survey_consider" type="radio" value="2" {{ old('survey_consider', $record->survey_consider) == '2' ? 'checked' : '' }}><span>2</span></label></li>
                                <li><label><input name="survey_consider" type="radio" value="3" {{ old('survey_consider', $record->survey_consider) == '3' ? 'checked' : '' }}><span>3</span></label></li>
                                <li><label><input name="survey_consider" type="radio" value="4" {{ old('survey_consider', $record->survey_consider) == '4' ? 'checked' : '' }}><span>4</span></label></li>
                                <li><label><input name="survey_consider" type="radio" value="5" {{ old('survey_consider', $record->survey_consider) == '5' ? 'checked' : '' }}><span>5</span></label></li>
                                <li><label><input name="survey_consider" type="radio" value="6" {{ old('survey_consider', $record->survey_consider) == '6' ? 'checked' : '' }}><span>6</span></label></li>
                                <li><label><input name="survey_consider" type="radio" value="7" {{ old('survey_consider', $record->survey_consider) == '7' ? 'checked' : '' }}><span>7</span></label></li>
                                <li>Hardcore Fanatic</li>
                            </ul>
                        </div>
                    </div>

                    <h3 class="form__sub-heading afc_survey_heading">3. Which of the following best represents your life stage? (tick one)</h3>

                    @include($campaign->view('fields.radio-group'), [
						'key' => 'survey_life_stage',
                        'name' => 'survey_life_stage',
                        'options' => [
                            'Single living alone' => 'Single living alone',
                            'Group of adults living in shared accommodation' => 'Group of adults living in shared accommodation',
                            'Family without children' => 'Family without children',
                            'Family with children living at home (where youngest child is 5yrs or under)' => 'Family with children living at home (where youngest child is 5yrs or under)',
                            'Family with children living at home (where youngest child is 6-14yrs)' => 'Family with children living at home (where youngest child is 6-14yrs)',
                            'Family with children living at home (where youngest child is over 15yrs)' => 'Family with children living at home (where youngest child is over 15yrs)',
                            'Single/Couple family with children who have left home' => 'Single/Couple family with children who have left home',
                            'Other family' => 'Other family',
                        ],
                        'width' => 'half',
					])

                    <h3 class="form__sub-heading afc_survey_heading">4.	Considering the Adelaide Crows AFL Women’s Team, please choose the statement that best suits you:</h3>

                    @include($campaign->view('fields.radio-group'), [
						'key' => 'survey_aflw',
                        'name' => 'survey_aflw',
                        'options' => [
                            'I have attended an Adelaide Crows AFL Women’s match' => 'I have attended an Adelaide Crows AFL Women’s match',
                            'I have not attended an Adelaide Crows AFL Women’s match, but am open to attending next season' => 'I have not attended an Adelaide Crows AFL Women’s match, but am open to attending next season',
                            'I support the Adelaide Crows in AFL Women’s Competition, but prefer to watch it on TV' => 'I support the Adelaide Crows in AFL Women’s Competition, but prefer to watch it on TV',
                            'I am not interested in the Adelaide Crows AFL Women’s Team' => 'I am not interested in the Adelaide Crows AFL Women’s Team',
                        ],
                        'width' => 'full',
					])

                    <h3 class="form__sub-heading afc_survey_heading">5. Besides AFL Football, which of the following are areas of interest to you (tick all that apply):</h3>

                    @include($campaign->view('fields.checkbox-group'), [
						'key' => 'survey_interests',
                        'name' => 'survey_interests[]',
                        'options' => [
                            'Animals' => 'Animals',
                            'Baseball' => 'Baseball',
                            'Basketball' => 'Basketball',
                            'Beauty' => 'Beauty',
                            'Career/Personal Development' => 'Career/Personal Development',
                            'Cooking & Recipes' => 'Cooking & Recipes',
                            'Cricket' => 'Cricket',
                            'Dance' => 'Dance',
                            'Entertainment' => 'Entertainment',
                            'Fashion' => 'Fashion',
                            'Gaming (Video)' => 'Gaming (Video)',
                            'Golf' => 'Golf',
                            'Health/Fitness' => 'Health/Fitness',
                            'Motorsport' => 'Motorsport',
                            'Music' => 'Music',
                            'Netball' => 'Netball',
                            'Podcasts' => 'Podcasts',
                            'Soccer' => 'Soccer',
                            'Sports Business' => 'Sports Business',
                            'Swimming' => 'Swimming',
                            'Tennis' => 'Tennis',
                            'Theatre' => 'Theatre',
                            'Travel' => 'Travel',
                        ],
                        'width' => 'third',
                    ])
                    
                    <div class="form__group form__group--radio form__group--survey_interests" data-width="third"></div>

				</div>

				<div class="form__submit">

					@foreach($hidden_fields as $name=>$value)
					<input type="hidden" name="{{ $name }}" value="{{ $value }}">
					@endforeach

					<button class="form__submit__button inverse" type="submit">{{ $campaign->creative->button_text }}</button>

				</div>

			</div>

		</form>

	</div>

    @include($campaign->view('common.footer'))

</div>


@endsection
