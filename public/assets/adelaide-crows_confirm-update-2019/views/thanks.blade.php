@extends($campaign->view('main'))

@section('content')
<div class="body-wrap__inner">

	<div class="main">

        <header class="header">
            <div class="header__logo">
                <a href="{{ $campaign->client_url }}/?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank"><img src="{{ $campaign->asset('images/header-logo.png') }}"></a>
            </div>
        </header>

        <div class="thanks">
            <h1 class="thanks__title">THANK YOU {{ $record->name_first }}<br> FOR COMPLETING YOUR PROFILE.</h1>
            <p class="thanks__body">You are now in the running to <strong>WIN two tickets to the 2019<br>Crows Club Champion Dinner.</strong></p>
            <p class="thanks__body">You will be notified if you are one of the lucky winners shortly.</p>
            <p class="thanks__body" style="font-size: 1.125rem; margin: 40px 0 0;">
                <a href="https://crowsmembership.com.au/terms-conditions-7" style="color: inherit; text-decoration: underline;">FULL TERMS &amp; CONDITIONS</a>
            </p>
        </div>

	</div>

    @include($campaign->view('common.footer'))

</div>


@endsection
