@extends($campaign->view('main'))

@section('content')

<div class="body-wrap__inner">

	<div class="main">

        <header class="header">
            <div class="header__logo">
                <a href="{{ $campaign->client_url }}/?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank"><img src="{{ $campaign->asset('images/header-logo.png') }}"></a>
            </div>
        </header>

		<form action="{{ $process_url }}" method="post" class="form">

			@include($campaign->view('common.errors'))

			<div class="form__inner">

				<!--<div class="form__top">
					<h1 class="form__top__title title">{!! $campaign->title !!}</h1>
                    @if($campaign->creative && $campaign->creative->prize_image)
					<div class="form__top__prize"><img src="{{ $campaign->asset('images/prize-image.jpg') }}"></div>
                    @endif
				</div>
-->
				<div class="form__fields">
					<div class="form__heading heading">
						<h2> Are your details correct?</h2>
						<span class="form__logo"><img src="{{ $campaign->asset('images/swan-logo.png') }}"></span>
                    </div>

					@include($campaign->view('fields.text'), [
						'key' => 'name_first',
                        'width' => 'half',
					])

                    @include($campaign->view('fields.text'), [
						'key' => 'name',
                        'width' => 'half',
					])
                   
                   
                    
                    @if($record->birthdate !== 'Supplied')
						@include($campaign->view('fields.dob'), [
							'key' => 'birthdate',
							'placeholder' => 'Please provide your DOB...',
                            'width' => 'half',
                            'not_required' => false
						])
					@endif
                   
                   @include($campaign->view('fields.gender'), [
						'key' => 'gender',
						'placeholder' => 'Please choose your gender...',
                        'width' => 'half',
                        'not_required' => $input->gender == 'supplied'
					])
                    
                    @include($campaign->view('fields.text'), [
						'key' => 'phone_mobile',
                         'width' => 'half',
					])
                    
                    @include($campaign->view('fields.email'), [
						'key' => 'email_addr',
                       'width' => 'half',
                        
					])

                    @include($campaign->view('fields.address'), [
						'keys' => ['street_addr_1', 'street_addr_2'],
					])
                    
                    @include($campaign->view('fields.country'), [
					 	'key' => 'country',
						

					])
                  
                  
                   @include($campaign->view('fields.state'), [
						'key' => 'state',
                        'width' => 'third',
					])
                   

                    @include($campaign->view('fields.text'), [
						'key' => 'city',
						'id' => 'suburbField',
                        'width' => 'third',
                        'not_required' => false,
					])

                    

                    @include($campaign->view('fields.text'), [
						'key' => 'zip',
						'id' => 'postcodeField',
                        'width' => 'third',
                        'not_required' => false,
					])

                  
				
                   
                    @if($input->data_segement == 'members')
                   
					
                    @include($campaign->view('fields.checkbox'), [
						'key' => 'receive_updates',
                        'width' => 'full',
                        'control_label' => 'I am happy to receive important membership information via email, including my 2018 membership renewal.',
					])
                   
                    @else
                    
                     @include($campaign->view('fields.checkbox'), [
						'key' => 'receive_updates',
                        'width' => 'full',
                        'control_label' => 'I would like to receive information on membership and ticketing for the 2018 season',
					])
                    
                    
                  	@endif

                    

				</div>

				<div class="form__submit">

					@foreach($hidden_fields as $name=>$value)
					<input type="hidden" name="{{ $name }}" value="{{ $value }}">
					@endforeach

					<button class="form__submit__button" type="submit">{{ $campaign->creative->button_text }}</button>
					<div style="text-align: center; color:#464646; margin-top: 20px; margin-bottom: 20px; font-size:18px;">*Mandatory field</div>

				</div>

			</div>

		</form>

	</div>

    @include($campaign->view('common.footer'))

</div>


@endsection
