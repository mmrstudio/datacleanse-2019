<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">

    <head>

    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    	<title>{{ $campaign->campaignTitle() }}</title>

    	<link href="{{ $campaign->asset('css/campaign.css') }}" rel="stylesheet">
    	<link href="{{ $campaign->asset('fonts/font.css') }}" rel="stylesheet">

        <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
        <link rel="icon" href="{{ $campaign->asset('icon.ico') }}" />

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-33851616-15', 'auto');
          ga('send', 'pageview');

        </script>

        <script src="{{ $campaign->asset('js/modernizr.js') }}"></script>

        @if($campaign->creative)
        <style>

            body {
                font-family: {!! $campaign->creative->body_font !!}, Helvetica, Arial, sans-serif;
                background: url({{ $campaign->asset('images/hero-banner.jpg') }}) no-repeat center top/1800px auto {{ $campaign->creative->body_background_color }};
                color: {{ $campaign->creative->body_color }};
            }

            .title {
                font-family: {!! $campaign->creative->title_font !!}, Helvetica, Arial, sans-serif;
                font-size: {{ $campaign->creative->title_size['desktop'] }}rem;
                line-height: {{ $campaign->creative->title_line_height }};
                color: {{ $campaign->creative->title_color }};
            }

            .heading {
                font-family: {!! $campaign->creative->heading_font !!}, Helvetica, Arial, sans-serif;
                font-size: {{ $campaign->creative->heading_size['desktop'] }}rem;
                line-height: {{ $campaign->creative->heading_line_height }};
                color: {{ $campaign->creative->heading_color }};
                text-transform: lowercase;
            }

            .form__inner { background-color: {{ $campaign->creative->form_background_color }}; }
            .form__group { font-size: {{ $campaign->creative->field_size['desktop'] }}rem; }
            .form__group__border { background-color: {{ $campaign->creative->field_border_color }}; }
            .form__control--textarea { border-color: {{ $campaign->creative->field_border_color }}; }
            .form__group__label { color: {{ $campaign->creative->field_label_color }}; }
            .form__control { color: {{ $campaign->creative->field_text_color }}; }
            .form__control:focus { color: {{ $campaign->creative->field_focus_color }}; }
            .form__control:focus + .form__group__border { background-color: {{ $campaign->creative->field_focus_border_color }}; }
            .form__control--textarea:focus { border-color: {{ $campaign->creative->field_focus_border_color }}; }
            .form__group--error .form__group__label { color: {{ $campaign->creative->field_error_color }}; }
            .form__group--error .form__group__border { background-color: {{ $campaign->creative->field_error_color }}; }
            .form__errors { background-color: {{ $campaign->creative->field_error_color }}; }
            .form__control--checkbox + span { border-color: {{ $campaign->creative->button_background_color }}; }
            .form__control--checkbox + span:before { color: {{ $campaign->creative->button_background_color }}; }

            ::-webkit-input-placeholder { color: {{ $campaign->creative->field_placeholder_color }}; }
            ::-moz-placeholder { color: {{ $campaign->creative->field_placeholder_color }}; }
            :-ms-input-placeholder { color: {{ $campaign->creative->field_placeholder_color }}; }
            :-moz-placeholder { color: {{ $campaign->creative->field_placeholder_color }}; }

            .form__submit__button {
                font-family: {{ $campaign->creative->button_font }}, Helvetica, Arial, sans-serif;
                font-size: {{ $campaign->creative->button_size['desktop'] }}rem;
                color: {{ $campaign->creative->button_foreground_color }};
                background-color: {{ $campaign->creative->button_background_color }};
                padding: {{ $campaign->creative->button_padding }};
                text-transform: lowercase;
            }

            .form__submit__button:hover {
                color: {{ $campaign->creative->button_hover_foreground_color }};
                background-color: {{ $campaign->creative->button_hover_background_color }};
            }

            .footer {
                font-size: {{ $campaign->creative->footer_size['desktop'] }}rem;
                color: {{ $campaign->creative->footer_color }}
            }

            .footer svg path { fill: {{ $campaign->creative->footer_color }} }
            .footer__datacleanse { border-color: {{ $campaign->creative->footer_color }} }
            .footer__datacleanse span { background-color: {{ $campaign->creative->footer_color }}; }
            .footer__links > li + li { border-color: {{ $campaign->creative->footer_color }} }

            .thanks__title {
                font-family: {{ $campaign->creative->thanks_title_font }}, Helvetica, Arial, sans-serif;
                font-size: {{ $campaign->creative->thanks_title_size['desktop'] }}rem;
                color: {{ $campaign->creative->thanks_title_color }};
            }

            .thanks__sub-title {
                font-family: {{ $campaign->creative->thanks_sub_title_font }}, Helvetica, Arial, sans-serif;
                font-size: {{ $campaign->creative->thanks_sub_title_size['desktop'] }}rem;
                color: {{ $campaign->creative->thanks_sub_title_color }};
            }

            .thanks__body {
                font-family: {{ $campaign->creative->thanks_body_font }}, Helvetica, Arial, sans-serif;
                font-size: {{ $campaign->creative->thanks_body_size['desktop'] }}rem;
                color: {{ $campaign->creative->thanks_body_color }};
            }

            @media screen and (max-width: 660px) {

                body {
                    background: url({{ $campaign->asset('images/hero-banner.jpg') }}) no-repeat center top/auto 220px {{ $campaign->creative->body_background_color }};
                }

                /*.header {
                    background-color: {{ $campaign->creative->body_background_color_mobile }};
                }*/

                .title {
                    font-size: {{ $campaign->creative->title_size['mobile'] }}rem;
                }

                .heading {
                    font-size: {{ $campaign->creative->heading_size['mobile'] }}rem;
                }

                .footer {
                    font-size: {{ $campaign->creative->footer_size['mobile'] }}rem;
                }

                .form__group {
                    font-size: {{ $campaign->creative->field_size['mobile'] }}rem;
                }

                .form__submit__button {
                    font-size: {{ $campaign->creative->button_size['mobile'] }}rem;
                }

                .thanks__title {
                    font-size: {{ $campaign->creative->thanks_title_size['mobile'] }}rem;
                }

                .thanks__sub-title {
                    font-size: {{ $campaign->creative->thanks_sub_title_size['mobile'] }}rem;
                }

                .thanks__body {
                    font-size: {{ $campaign->creative->thanks_body_size['mobile'] }}rem;
                }

            }

        </style>
        @endif

    </head>

    <body>

        <div class="body-wrap">


                @yield('content')


        </div>

        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    	<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
        <script src="{{ $campaign->asset('js/campaign.js') }}"></script>

    </body>

</html>
