@extends($campaign->view('main'))

@section('content')
<div class="body-wrap__inner">

	<div class="main">

        <header class="header">
            <div class="header__logo">
                <a href="{{ $campaign->client_url }}/?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank"><img src="{{ $campaign->asset('images/header-logo.png') }}"></a>
            </div>
        </header>


        <form action="{{ $process_url }}" method="post" class="form">

			@include($campaign->view('common.errors'))

			<div class="form__inner">

                <div class="thanks">

                    @if($campaign->thanks_title)
                    <h1 class="thanks__title">{{ $campaign->thanks_title }}</h1>
                    @endif

                    @if($campaign->thanks_sub_title)
                    <h2 class="thanks__sub-title">{{ $campaign->thanks_sub_title }}</h2>
                    @endif

                    @if($campaign->thanks_body)
                    <p class="thanks__body">{{ $campaign->thanks_body }}</p>
                    @endif

                </div>

				<div class="form__fields">

                    <h2 class="form__heading heading" style="text-align: center;">Tell us in 25 words or less what you love most about the parramatta eels</h2>

					@include($campaign->view('fields.textarea'), [
						'key' => 'love_most',
					])

				</div>

				<div class="form__submit">

					@foreach($hidden_fields as $name=>$value)
					<input type="hidden" name="{{ $name }}" value="{{ $value }}">
					@endforeach

					<button class="form__submit__button" type="submit">{{ $campaign->creative->button_text }}</button>

				</div>

			</div>

		</form>

	</div>

    @include($campaign->view('common.footer'))

</div>


@endsection
