
    <div class="footer__sponsors">
        <img src="{{ $campaign->asset('images/sponsors.png') }}">
    </div>

    <p>Brisbane Lions, 812 Stanley Street, Woolloongabba QLD 4102.</p>

    <ul class="footer__links">
        <li><a href="http://www.lions.com.au/" target="_blank">Brisbane Lions</a></li>
        <li><a href="http://www.lions.com.au/privacy" target="_blank">Privacy Policy</a></li>
        <li><a href="http://membership.lions.com.au/terms-and-conditions" target="_blank">Terms &amp; Conditions</a></li>
        <li><a href="http://datacleanse.com.au" target="_blank">Product of Datacleanse</a></li>
    </ul>
