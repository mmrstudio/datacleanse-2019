@extends($campaign->view('main'))



<header class="header">

    @include($campaign->view('common.header'))

</header>

<section class="main">
	<div class="main__inner container">

		<div class="main__sidebar">

			<div class="main__sidebar__player">
				<div class="main__sidebar__player__inner"></div>

				<div class="main__sidebar__text">
					<div class="main__sidebar__text__inner"></div>
				</div>
				<div class="main__sidebar__logo">
					<img src="{{  $campaign->asset('images/logo.svg') }}" alt="logo">
				</div>

			</div>

			<div class="main__sidebar__text main__sidebar__text--secondary">
				<div class="main__sidebar__text__inner"></div>
			</div>

		</div>

		<div class="main__content">

			<div class="main__content__inner">

				@include($campaign->view('common.errors'))

				<div class="main__content__column main__content__column--left campaign">

					<div class="campaign__intro tk-rift">YOU’RE JUST ONE CLICK AWAY FROM RENEWING YOUR MEMBERSHIP!</div>

					<div class="campaign__member-details">
						<div class="campaign__member-details__intro">Make sure we have your correct details and then click the renew button.</div>
						<div class="campaign__member-details__heading tk-rift">Your Details</div>
						<div class="campaign__member-details__contact">
							<span class="name">{{ $record->name_first }} {{ $record->name }}</span><br>

							@if($record->phone_cell)
							{{ $record->phone_cell }}<br>
							@endif
							{{ $record->street_addr_1 }}<br>
							{{ $record->street_addr_2 ? $record->street_addr_2 . '<br>' : '' }}
							{{ $record->city }}, {{ $record->state }} {{ $record->zip }}
						</div>
						<div class="campaign__member-details__incorrect">If these details are incorrect please call us on<br> 1300 MAGPIE (1300 62 47 43) during business hours to update.</div>
					</div>

				</div>

				<div class="main__content__column main__content__column--right membership">

					<div class="membership__inner">

						<div class="membership__name"><div class="membership__name__inner tk-rift">{{ $record->pc_info1_renewal }}</div></div>
						@if($record->price_code != '$0.00')
						<div class="membership__price tk-rift">${{ $record->price_code }}</div>
						@endif
						<div class="membership__term tk-rift">One year membership</div>

						@if($record->cc_num)
						<div class="membership__card-info">We have your credit card ending in {{ str_replace('x', '', $record->cc_num) }} on file. To renew your membership with this credit card, please click "Renew Now".</div>
						@else
						<div class="membership__card-info">To renew your membership, please click "Renew Now".</div>
						@endif

						<div class="membership__buttons">

							
							<form action="{{ $process_url }}" method="post" class="form">
							

						
							<input type="hidden" name="renew" value="Yes">
							<input type="hidden" name="declined_reason" value="">
							<input type="hidden" name="declined_reason_other" value="">
							<input type="hidden" name="invalid_cc" value="No">
							<input type="hidden" name="invalid_cc_reason" value="">
							<input type="hidden" name="invalid_cc_reason_other" value="">
							
							 
							@foreach($hidden_fields as $name=>$value)
							<input type="hidden" name="{{ $name }}" value="{{ $value }}">
							@endforeach

							<button type="submit" class="membership__buttons__renew tk-rift">Renew Now!</button>
							</form>

							<button class="membership__buttons__decline tk-rift" id="noThanks">No Thanks</button>

						</div>

						@if($record->cc_num)
						<div class="membership__incorrect-card">If this card is incorrect please <a href="#" id="incorrectCard">click here</a> and we will contact you to update your details.</div>
						@endif

					</div>

				</div>

				

			</div>

		</div>

	</div>
</section>

<footer class="footer">

    @include($campaign->view('common.footer'))

</footer>


