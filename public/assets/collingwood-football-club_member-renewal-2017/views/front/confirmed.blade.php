@extends('../front')

@section('content')

@include('../includes.header')

<div class="container-fluid intro thanks">
    <div class="container">
        <h2>Thank you</h2>
        <p>You have already confirmed your details.</p>
    </div>
</div>

@endsection
