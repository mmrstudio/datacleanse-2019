
    <div class="hero">

        <h1 class="hero__title hero__title--two-line">Are You a<br> Lost Bulldog?</h1>

        <div class="hero__video hero__video--home">
            <div class="hero__video__inner hero__video__home-wrap">
                <div class="hero__video__poster"></div>
                <div class="hero__video__player hero__video__player--hidden" id="dynamicVideoPlayer"></div>
                <div class="hero__video__home" id="dynamicVideoWrap">
                    <div class="hero__video__home__inner">
                        <div class="hero__video__home__title">ENTER YOUR NAME &amp; YEAR<br> THEN PRESS PLAY</div>
                        <form action="#" method="POST" class="hero__video__home__form form" id="dynamicVideoForm">

                            <div class="hero__video__home__form__field form__field">
                                <input type="text" name="name" placeholder="Your name" class="form__input" id="dynamicVideoName">
                            </div>

                            <div class="hero__video__home__form__field form__field">
                                <input type="text" name="last_year" placeholder="What year did you last attend a game?" class="form__input" id="dynamicVideoLastYear">
                            </div>

                            <div class="hero__video__home__form__field form__field">
                                <input type="text" name="member_number" placeholder="Member number (optional)" class="form__input" id="dynamicVideoMemberID">
                            </div>

                            <div class="hero__video__home__form__buttons form__submit">
                                <button class="button button--red" type="submit" id="dynamicVideoSubmit">Play Now</button>
                            </div>

                        </form>
                    </div>
                    <div class='uil-rolling-css' style='transform:scale(0.45);'><div><div></div><div></div></div></div>
                </div>
            </div>
        </div>

        <div class="hero__home">
            <h2 class="hero__home__tagline">#BEMOREBULLDOG IN 2016</h2>
        </div>

    </div>
