@extends('../front')

@section('content')

    <div class="the-kennel">

        <div class="container">
            <h1>The Kennel</h1>
            <h2>HAVE A QUESTION?<br> CALL US ON<br class="resp"> 1300 46 36 47</h2>

            @if($posted)

            <p class="the-kennel__sent">Thanks! Your enquiry has been sent. A member of the Lost Dog Squad will be in touch with you shortly.</p>
            <script> fbq('track', 'Lead'); </script>

            @else

            <p>If you have any queries please send us a message via the below form and our Membership team will get back to you as soon as possible.</p>

            @if ($errors->has())
            <div class="the-kennel__errors">
                <div class="the-kennel__errors__title">Please check the following errors:</div>
                @foreach ($errors->all() as $message)
                {{ $message }}<br>
                @endforeach
            </div>
            @endif

            {!! Form::open(array('action' => 'FrontController@postTheKennel', 'class' => 'the-kennel__form form')) !!}

                <div class="the-kennel__form__field form__field">
                    <input type="text" name="name" placeholder="Name *" class="form__input" value="{{ old('name', '') }}">
                </div>

                <div class="the-kennel__form__field form__field">
                    <input type="text" name="email" placeholder="Email *" class="form__input" value="{{ old('email', '') }}">
                </div>

                <div class="the-kennel__form__field form__field">
                    <input type="text" name="phone" placeholder="Phone *" class="form__input" value="{{ old('phone', '') }}">
                </div>

                <div class="the-kennel__form__field form__field">
                    <textarea name="message" placeholder="Message *" class="form__input">{{ old('message', '') }}</textarea>
                </div>

                <div class="the-kennel__form__submit form__submit">
                    <button class="button button--red" type="submit">Submit</button>
                </div>

            {!! Form::close() !!}

            @endif

            <div class="the-kennel__connect">

                <p>Visit <a href="http://www.westernbulldogs.com.au/" target="_blank">westernbulldogs.com.au</a></p>

                <ul class="social-buttons">
                    <li><a href="http://www.facebook.com/Western.Bulldogs?" class="social-buttons__button social-buttons__button__facebook" target="_blank">Facebook</a></li>
                    <li><a href="http://twitter.com/#!/westernbulldogs" class="social-buttons__button social-buttons__button__twitter" target="_blank">Twitter</a></li>
                    <li><a href="https://www.instagram.com/westernbulldogs" class="social-buttons__button social-buttons__button__instagram" target="_blank">Instagram</a></li>
                </ul>

            </div>

        </div>

    </div>

    @include('front.callouts')

@endsection
