@extends('../primaryLanding')

@section('content')

<div class="main__content__column main__content__column--left campaign">

    <div class="campaign__intro">Great job, you’re just one click away from renewing your membership!</div>

    <div class="campaign__member-details">
        <div class="campaign__member-details__intro">Make sure we have your correct details and then click the renew button.</div>
        <div class="campaign__member-details__heading">Your Details</div>
        <div class="campaign__member-details__contact">
            {{ $source->name_first }} {{ $source->name }}<br>
            @if($source->email_addr)
            {{ $source->email_addr }}<br>
            @endif
            @if($source->phone_cell)
            {{ $source->phone_cell }}<br>
            @endif
            {{ $source->street_addr_1 }}<br>
            {{ $source->street_addr_2 ? $source->street_addr_2 . '<br>' : '' }}
            {{ $source->city }}, {{ $source->state }} {{ $source->zip }}
        </div>
        <div class="campaign__member-details__incorrect">If these details are incorrect please call us on 1300 MAGPIE (1300 62 47 43) during business hours to update.</div>
    </div>

</div>

<div class="main__content__column main__content__column--right membership">

    <div class="membership__inner">

        <div class="membership__name"><div class="membership__name__inner">{{ $source->_2015_pc_info1_renewal }}</div></div>
        @if($source->_2015_purchase_price != '$0.00')
        <div class="membership__price">{{ $source->_2015_purchase_price }}</div>
        @endif
        <div class="membership__term">2016 membership</div>

        @if($source->data_mask)
        <div class="membership__card-info">We have your credit card ending in {{ str_replace('x', '', $source->data_mask) }} on file.<br>To renew your membership with this credit card, please click "Renew Now".</div>
        @else
        <div class="membership__card-info">To renew your membership, please click "Renew Now".</div>
        @endif

        <div class="membership__buttons">

            {!! Form::open(array('url' => url('member/update/' . $source->hash))) !!}

            <button type="submit" class="membership__buttons__renew">Renew Now!</button>

            <input type="hidden" name="id" value="{{ $source->id }}">
            <input type="hidden" name="source" value="{{ $source->hash }}">
            <input type="hidden" name="entry_type" value="update">
            <input type="hidden" name="renew" value="Yes">
            <input type="hidden" name="declined_reason" value="">
            <input type="hidden" name="declined_reason_other" value="">
            <input type="hidden" name="invalid_cc" value="No">
            <input type="hidden" name="invalid_cc_reason" value="">
            <input type="hidden" name="invalid_cc_reason_other" value="">

            @if ($source->source_id)
            <input type="hidden" name="update" value="{{ $source->source_id }}">
            @endif

            {!! Form::close() !!}

            <button class="membership__buttons__decline" id="noThanks">No Thanks</button>

        </div>

        @if($source->data_mask)
        <div class="membership__incorrect-card">If this card is incorrect please <a href="#" id="incorrectCard">click here</a> and we will contact you to update your details.</div>
        @endif

    </div>

</div>

@endsection
