@extends('../front')

@section('content')

    <div class="hero">

        <h1 class="hero__title hero__title--two-line">HELP US FIND OUR<br> LOST BULLDOGS!</h1>

        <div class="hero__image">
            <img src="{{ asset('/images/create-poster-hero.jpg') }}" alt="Create Poster">
        </div>

        <div class="hero__create-poster">
            <h2 class="hero__create-poster__title">DO YOU KNOW A LOST BULLDOG?</h2>
            <div class="hero__create-poster__text">Know some lost Bulldogs? Help us bring them home to the Kennel. Create a poster with their name and share on Facebook and Twitter or email your friends directly.</div>
        </div>

    </div>

    <div class="create-poster create-poster--friend">

        <form class="create-poster__form form" id="createFriendPoster">

            <div class="create-poster__form__field form__field">
                <input type="text" name="first_name" placeholder="Name" class="form__input" id="createFriendPosterName">
            </div>

            <div class="create-poster__form__submit form__submit">
                <button class="button button--red" type="submit" id="createFriendPosterSubmit">Create Poster</button>
            </div>

        </form>

        <div class="create-poster__created" id="createFriendPosterCreated">

            <div class="create-poster__created__inner" id="createFriendPosterCreatedInner">

                <div class="create-poster__created__thumbnail">
                    <img src="" id="createFriendPosterCreatedThumb">
                </div>

                <div class="create-poster__created__text">
                    <p>All done! Your Lost Bulldog Poster for <span id="createFriendPosterCreatedName"></span> has been created. Select from the following options to send.</p>

                    <div class="create-poster__created__buttons">
                        <ul class="social-buttons">
                            <li><a href="#" class="social-buttons__button social-buttons__button__facebook track-link" target="_blank" id="createFriendPosterCreatedFBShare" data-track-category="Share" data-track-action="Facebook" data-track-label="Friend Poster">Facebook</a></li>
                            <li><a href="#" class="social-buttons__button social-buttons__button__twitter track-link" target="_blank" id="createFriendPosterCreatedTwitterShare" data-track-category="Share" data-track-action="Twitter" data-track-label="Friend Poster">Twitter</a></li>
                            <li><a href="#" class="social-buttons__button social-buttons__button__email send-friend-poster track-link" data-track-category="Share" data-track-action="Email" data-track-label="Friend Poster">Email</a></li>
                        </ul>
                        <a href="#" class="button button--red button--large track-link" id="createFriendPosterCreatedPDFPrint" data-track-category="Friend Poster" data-track-action="Download" data-track-label="JPG">Download JPG</a><br>
                        <a href="#" class="button button--red button--large track-link" id="createFriendPosterCreatedPDFDownload" data-track-category="Friend Poster" data-track-action="Download" data-track-label="PDF">Download PDF</a>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="modal modal--hidden" id="sendFriendPosterModal">
        <div class="modal__inner">
            <h3>Send to Friend</h3>

            <div id="sendFriendPosterModalBody">

            <p>Enter your name and your friend's email to send them your poster:</p>

            <div class="modal__errors modal__errors--hidden" id="sendFriendPosterModalErrors">
                <div class="modal__errors__title">Please check the following errors:</div>
                <div class="errors"></div>
            </div>

            {!! Form::open(array('action' => 'FrontController@postSendFriendPoster', 'class' => 'form', 'id' => 'sendFriendPosterForm')) !!}

                <div class="create-poster__form__field form__field">
                    <input type="text" name="sender_name" placeholder="Your Name" class="form__input" id="sendFriendPosterSender">
                </div>

                <div class="create-poster__form__field form__field">
                    <input type="text" name="sender_email" placeholder="Your Email" class="form__input" id="sendFriendPosterSenderEmail">
                </div>

                <div class="create-poster__form__field form__field">
                    <input type="text" name="email" placeholder="Friend's Email" class="form__input" id="sendFriendPosterEmail">
                </div>

                <!-- <div class="create-poster__form__field form__field">
                    <textarea name="message" placeholder="Message" class="form__input" id="sendFriendPosterMessage"></textarea>
                </div> -->

                <div class="create-poster__form__submit form__submit">
                    <button class="button button--red" type="submit" id="sendFriendPosterSubmit">Send Poster</button>
                </div>

                <input type="hidden" name="name" value="" id="createFriendPosterCreatedEmail">

            {!! Form::close() !!}

            </div>

            <button class="modal__close">Close</button>

        </div>
    </div>

    @include('front.callouts')

@endsection
