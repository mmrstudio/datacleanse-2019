@extends('../front')

@section('content')

    @include('front.home-hero')

    @include('front.home-content')

    @include('front.callouts')


@endsection
