
    @if(isset($posted) && ! $posted)

    <div class="product-offerings product-offerings--home">

        <div class="product-offerings__current">It's easy and affordable to become a Western Bulldogs Member in 2016</div>

        <div class="product-offerings__offering">
            <div class="product-offerings__offering__title"><strong>All Home Games</strong></div>
            <div class="product-offerings__offering__offer__action">You can see all our home games with an 11 game package for just $205</div>
            <div class="product-offerings__offering__button"><a href="https://oss.ticketmaster.com/aps/bulldogs/EN/buy/quickbuy/450" target="_blank" class="button button--red track-link fb-track" data-track-category="Sign Me Up" data-track-action="Click" data-track-label="All Home Games" data-track-value="450" data-fb-track="CompleteRegistration">Sign Me Up</a></div>
        </div>

        <div class="product-offerings__offering">
            <div class="product-offerings__offering__title"><strong>3 Game Membership</strong></div>
            <div class="product-offerings__offering__offer__action">A 3 Game membership is also a great option if you can only attend a few matches and is just $95</div>
            <div class="product-offerings__offering__button"><a href="https://oss.ticketmaster.com/aps/bulldogs/EN/buy/quickbuy/452" target="_blank" class="button button--red track-link fb-track" data-track-category="Sign Me Up" data-track-action="Click" data-track-label="3 Game Membership" data-track-value="452" data-fb-track="CompleteRegistration">Sign Me Up</a></div>
        </div>

        <div class="product-offerings__offering">
            <div class="product-offerings__offering__title"><strong>Sideline Membership</strong></div>
            <div class="product-offerings__offering__offer__action">Can't get to the game? Support us as a Sideline member for just $55 which includes a full membership pack.</div>
            <div class="product-offerings__offering__button"><a href="https://oss.ticketmaster.com/aps/bulldogs/EN/buy/quickbuy/463" target="_blank" class="button button--red track-link fb-track" data-track-category="Sign Me Up" data-track-action="Click" data-track-label="Sideline Membership" data-track-value="463" data-fb-track="CompleteRegistration">Sign Me Up</a></div>
        </div>

        <div class="product-offerings__bottom">Not sure? Visit our <a href="http://membership.westernbulldogs.com.au/" target="_blank" class="track-link" data-track-category="Outbound" data-track-action="Click" data-track-label="Membership Website">website</a> to see our wide range of membership options for this season that suit every fan on any budget.</div>

    </div>

    @endif

    <div class="request-call">

        <div class="request-call__text">Want to discuss the options with an expert? <a href="#" class="request-call-modal track-link" data-track-category="Contact" data-track-action="Request a Call" data-track-label="Open">Click here</a> to request a call from our Lost Bulldogs Squad.</div>

    </div>
