@extends('../front')

@section('content')

@include('../includes.header')

<header class="container-fluid header">

    <div class="container header-top">
        <div class="top-header"></div>
        <div class="form-header"></div>
    </div>

</header>

<div class="container">

	<div class="row">

		<div class="main col-xs-12">

            <div class="form-wrap">

                <div class="form-wrap-top"><div class="freo-logo"></div></div>

                <div class="terms-conditions">
                    <h2>Terms and Conditions</h2>
                    <p>This promotion runs from Monday 7th September 2015, 9.00am (EST) and concludes on Friday 25th September 2015, 5.00pm (EST). Entries are open to all members and non-members who confirm or update their details. 5 x winners will be drawn and notified on Wednesday 4 November 2015 via phone and email. Each prize winner will receive 1 x Raiders ISC Marvel Super Hero Hulk Jerseys valued at $159. Size of Jersey is to be selected by winner and is subject to availability. </p>
                </div>

                <div class="form-wrap-bottom"></div>

            </div>

		</div>

	</div>

</div>

@endsection
