@extends('../front')

@section('content')

    <div class="hero">

        <h1 class="hero__title hero__title--two-line">HAVE YOU SEEN A<br>LOST BULLDOG?</h1>

        <div class="hero__image">
            <img src="{{ asset('/images/poster-friend-hero.jpg') }}" alt="Have you seen a lost bulldog?">
        </div>

        <div class="hero__friend-share">
            <h2 class="hero__friend-share__name">{{ $name }}</h2>
            <div class="hero__friend-share__text">
                <span>MISSING FOR TOO LONG</span>
            </div>
        </div>

        <div class="hero__home">
            <h2 class="hero__home__tagline">#BEMOREBULLDOG IN 2016</h2>
        </div>

    </div>

    @include('front.home-content')

    @include('front.callouts')

@endsection
