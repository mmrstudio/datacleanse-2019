
<div class="callouts">

    <div class="callouts__callout">
        <div class="callouts__callout__image">
            <img src="{{ asset('/images/create-poster.jpg') }}" alt="Create a Poster">
        </div>
        <div class="callouts__callout__text">
            <p>Know some Lost Bulldogs? Help us bring them home to The Kennel. Create a poster with their name and share on Facebook and Twitter.</p>
            <a href="{{ url('create-poster') }}" class="button button--blue button--medium">Create Poster</a>
        </div>
    </div>

    <div class="callouts__callout">
        <div class="callouts__callout__image">
            <img src="{{ asset('/images/send-to-friend.jpg') }}" alt="Send to Friend">
        </div>
        <div class="callouts__callout__text">
            <p>Share this to a Lost Bulldog you know - let them know how much we miss them!</p>
            <ul class="social-buttons">
                <li><a href="https://www.facebook.com/dialog/feed?link=http://bit.ly/1SsKMAa" class="social-buttons__button social-buttons__button__facebook track-link" data-track-category="Share" data-track-action="Facebook" data-track-label="Homepage" target="_blank">Facebook</a></li>
                <li><a href="https://twitter.com/intent/tweet?status={{ urlencode('Have you seen a lost bulldog? #bemorebulldog in 2016 #lostbulldogs @westernbulldogs http://bit.ly/1p0NzEG') }}" class="social-buttons__button social-buttons__button__twitter track-link" data-track-category="Share" data-track-action="Twitter" data-track-label="Homepage" target="_blank">Twitter</a></li>
                <li><a href="mailto:?subject=Are you a Lost Bulldog?&amp;body=Hey, are you a lost bulldog? #bemorebulldog in 2016. Check out http://bit.ly/1ptgh1D" class="social-buttons__button social-buttons__button__email track-link" data-track-category="Share" data-track-action="Email" data-track-label="Homepage">Email</a></li>
            </ul>
        </div>
    </div>

</div>
