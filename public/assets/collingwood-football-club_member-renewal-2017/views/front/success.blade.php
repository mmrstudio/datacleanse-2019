@extends('../front')

@section('content')

<div class="main__content__column main__content__column--left campaign membership__thanks__left">

    <div class="campaign__intro">Great job, you’re just one click away from renewing your membership!</div>

    <div class="campaign__member-details">
        <div class="campaign__member-details__intro">Make sure we have your correct details and then click the renew button.</div>
        <div class="campaign__member-details__heading">Your Details</div>
        <div class="campaign__member-details__contact">
            {{ $source->name_first }} {{ $source->name }}<br>
            {{ $source->phone_cell }}<br>
            {{ $source->street_addr_1 }}<br>
            {{ $source->street_addr_2 ? $source->street_addr_2 . '<br>' : '' }}
            {{ $source->city }}, {{ $source->state }} {{ $source->zip }}
        </div>
        <div class="campaign__member-details__incorrect">If these details are incorrect please call us on 1300 MAGPIE (1300 62 47 43) to update.</div>
    </div>

</div>

<div class="main__content__column main__content__column--right membership">

    <div class="membership__thanks">

        <div class="membership__thanks__inner">

            @if($output->invalid_cc === 'Yes')

            THANKS! WE'LL BE IN TOUCH WITH YOU SHORTLY TO CHANGE YOUR CREDIT CARD DETAILS

            @elseif($output->renew === 'No')

            THANKS FOR LETTING US KNOW, WE HOPE YOU'LL STAND SIDE BY SIDE WITH US AGAIN IN THE FUTURE

            @else

            THANKS FOR RENEWING YOUR MEMBERSHIP AND STANDING SIDE BY SIDE WITH US IN 2016!

            @endif

        </div>

    </div>

</div>

@endsection
