@extends($campaign->view('main'))



<header class="header">

    @include($campaign->view('common.header'))

</header>

<section class="main">
	<div class="main__inner container">

		<div class="main__sidebar">

			<div class="main__sidebar__player">
				<div class="main__sidebar__player__inner"></div>

				<div class="main__sidebar__text">
					<div class="main__sidebar__text__inner"></div>
				</div>
				<div class="main__sidebar__logo">
					<img src="{{  $campaign->asset('images/logo.svg') }}" alt="logo">
				</div>

			</div>

			<div class="main__sidebar__text main__sidebar__text--secondary">
				<div class="main__sidebar__text__inner"></div>
			</div>

		</div>

		<div class="main__content">

			<div class="main__content__inner">


				<div class="main__content__column main__content__column--left campaign membership__thanks__left">

					<div class="campaign__intro tk-rift">YOU’RE JUST ONE CLICK AWAY FROM RENEWING YOUR MEMBERSHIP!</div>

					<div class="campaign__member-details">
						<div class="campaign__member-details__intro">Make sure we have your correct details and then click the renew button.</div>
						<div class="campaign__member-details__heading tk-rift">Your Details</div>
						<div class="campaign__member-details__contact">
							<span class="name">{{ $record->name_first }} {{ $record->name }}</span><br>

							@if($record->phone_cell)
							{{ $record->phone_cell }}<br>
							@endif
							{{ $record->street_addr_1 }}<br>
							{{ $record->street_addr_2 ? $record->street_addr_2 . '<br>' : '' }}
							{{ $record->city }}, {{ $record->state }} {{ $record->zip }}
						</div>
						<div class="campaign__member-details__incorrect">If these details are incorrect please call us on<br> 1300 MAGPIE (1300 62 47 43) during business hours to update.</div>
					</div>

				</div>

				<div class="main__content__column main__content__column--right membership">

					<div class="membership__thanks">

						<div class="membership__thanks__inner tk-rift">

							@if($record->invalid_cc === 'Yes')

							THANKS! WE'LL BE IN TOUCH WITH YOU SHORTLY TO CHANGE YOUR CREDIT CARD DETAILS

							@elseif($record->renew === 'No')

							THANKS FOR LETTING US KNOW, WE HOPE YOU'LL STAND SIDE BY SIDE WITH US AGAIN IN THE FUTURE

							@else

							THANKS FOR RENEWING YOUR MEMBERSHIP AND STANDING SIDE BY SIDE WITH US IN 2017!

							@endif

						</div>

					</div>

				</div>

				

			</div>

		</div>

	</div>
</section>



<footer class="footer footer--thanks">

    <div class="footer__tagline">
        <div class="footer__tagline__inner"></div>
    </div>

    @include($campaign->view('common.thanksfooter'))

</footer>

