
    <footer class="footer">

        <div class="footer__inner">
           <!-- <div class="footer__tagline">The 2016 Season is here and we want you to stand Side by Side with us!</div>-->
            <div class="footer__bottom">
               
                <a href="https://membership.collingwoodfc.com.au/terms-and-conditions" target="_blank">Terms &amp; Conditions</a> &nbsp;|&nbsp;
                <a href="https://membership.collingwoodfc.com.au/privacy" target="_blank">Privacy Policy</a>
            </div>
        </div>

    </footer>

    