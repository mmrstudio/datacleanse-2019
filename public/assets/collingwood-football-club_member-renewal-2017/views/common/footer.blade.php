
    <footer class="footer">

        <div class="footer__inner">
           <!-- <div class="footer__tagline">The 2016 Season is here and we want you to stand Side by Side with us!</div>-->
            <div class="footer__bottom">
               
                <a href="https://membership.collingwoodfc.com.au/terms-and-conditions" target="_blank">Terms &amp; Conditions</a> &nbsp;|&nbsp;
                <a href="http://datacleanse.com.au" target="_blank">Powered by Datacleanse</a>
            </div>
        </div>

    </footer>

    <div class="modal modal--hidden" id="declineModal">
        <div class="modal__inner">
            <h3 class="modal__title tk-rift">Please let us know your reason for not wanting to renew your membership this year</h3>
            <div class="modal__body">

                <!--{!! Form::open(array('url' => url('member/update/' . $record->hash), 'id' => 'declineModalForm')) !!}-->
				<form action="{{ $process_url }}" method="post" class="form">
                <ul class="modal__options">

                    <li class="modal__options__option">
                        <label>
                            <span>1.</span> Change of work and/or family circumstance
                            <input type="radio" name="declined_reason" value="Change of work and/or family circumstance">
                            <div class="checkbox"></div>
                        </label>
                    </li>

                    <li class="modal__options__option">
                        <label>
                            <span>2.</span> Last season's on field performance
                            <input type="radio" name="declined_reason" value="Last season's on field performance">
                            <div class="checkbox"></div>
                        </label>
                    </li>

                    <li class="modal__options__option">
                        <label>
                            <span>3.</span> There isn't a membership package that meets my needs
                            <input type="radio" name="declined_reason" value="There isn't a membership package that meets my needs">
                            <div class="checkbox"></div>
                        </label>
                    </li>

                    <li class="modal__options__option">
                        <label>
                            <span>4.</span> Value for money
                            <input type="radio" name="declined_reason" value="Value for money">
                            <div class="checkbox"></div>
                        </label>
                    </li>

                    <li class="modal__options__option">
                        <label class="modal__options__option__other" for="declineOptionOtherCheckbox" id="declineOptionOther">
                            <span>5.</span> Other
                            <input type="text" name="declined_reason_other" value="" id="declineOptionOtherField">
                            <input type="radio" name="declined_reason" value="Other" id="declineOptionOtherCheckbox">
                            <div class="checkbox"></div>
                        </label>
                    </li>

                </ul>

                <div class="modal__submit">
                    <button type="submit" class="modal__submit__button tk-rift">Submit</button>
                </div>

				@foreach($hidden_fields as $name=>$value)
				<input type="hidden" name="{{ $name }}" value="{{ $value }}">
				@endforeach
                <input type="hidden" name="renew" value="No">
                <input type="hidden" name="invalid_cc" value="No">
                <input type="hidden" name="invalid_cc_reason" value="">
                <input type="hidden" name="invalid_cc_reason_other" value="">

                @if ($record->source_id)
                <input type="hidden" name="update" value="{{ $record->source_id }}">
                @endif

                </form>

            </div>
            <button class="modal__close">Close</button>
        </div>
    </div>

    <div class="modal modal--hidden" id="incorrectCCModal">
        <div class="modal__inner">
            <h3 class="modal__title tk-rift">Please let us know the reason for the incorrect card details</h3>
            <div class="modal__body">

                <form action="{{ $process_url }}" method="post" class="form">

                <ul class="modal__options">

                    <li class="modal__options__option">
                        <label>
                            <span>1.</span> Expired card
                            <input type="radio" name="invalid_cc_reason" value="Expired card">
                            <div class="checkbox"></div>
                        </label>
                    </li>

                    <li class="modal__options__option">
                        <label>
                            <span>2.</span> I would like to use a different card
                            <input type="radio" name="invalid_cc_reason" value="I would like to use a different card">
                            <div class="checkbox"></div>
                        </label>
                    </li>

                    <li class="modal__options__option">
                        <label>
                            <span>3.</span> I received my membership as a gift
                            <input type="radio" name="invalid_cc_reason" value="I received my membership as a gift">
                            <div class="checkbox"></div>
                        </label>
                    </li>

                    <li class="modal__options__option">
                        <label class="modal__options__option__other" for="incorrectCCOptionOtherCheckbox" id="incorrectCCOptionOther">
                            <span>5.</span> Other
                            <input type="text" name="invalid_cc_reason_other" value="" id="incorrectCCOptionOtherField">
                            <input type="radio" name="invalid_cc_reason" value="Other" id="incorrectCCOptionOtherCheckbox">
                            <div class="checkbox"></div>
                        </label>
                    </li>

                </ul>

                <div class="modal__submit">
                    <button type="submit" class="modal__submit__button tk-rift">Submit</button>
                </div>

                @foreach($hidden_fields as $name=>$value)
				<input type="hidden" name="{{ $name }}" value="{{ $value }}">
				@endforeach
                <input type="hidden" name="renew" value="No">
                <input type="hidden" name="declined_reason" value="">
                <input type="hidden" name="declined_reason_other" value="">
                <input type="hidden" name="invalid_cc" value="Yes">

                @if ($record->acct_id)
                <input type="hidden" name="update" value="{{ $record->acct_id }}">
                @endif

				</form>

            </div>
            <button class="modal__close">Close</button>
        </div>
    </div>

