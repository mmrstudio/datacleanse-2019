
    @@include('plugins/datepicker.js')
    @@include('plugins/datepicker.en.js')
    //@@include('plugins/moment.js')
	
	function capitalize(s) {
        return s[0].toUpperCase() + s.slice(1);
    }

    function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

	
	
	function trackEvent(trackData) {
        //alert(JSON.stringify(trackData, null, 4));
        //console.log(trackData);
        ga('send', 'event', trackData.category, trackData.action, trackData.label, trackData.value);
    }

    function trackLink() {

        var link = $(this);
        var trackData = {
            category: link.data('track-category'),
            action: link.data('track-action'),
            label: link.data('track-label'),
            value: link.data('track-value') ? link.data('track-value') : 0
        }

        trackEvent(trackData);

    }

    function fbTrackLink() {
        var link = $(this);
        fbq('track', link.data('fb-track'));
    }

    function initTrackLinks($root) {
        $root.find('.track-link').on('click', trackLink);
        $root.find('.fb-track').on('click', fbTrackLink);
    }

    $(document).ready(function() {
		
		
		
		var $b = $('body');

        initTrackLinks($('body'));

        var declineModal = $('#declineModal');
        if(declineModal.length > 0) {

            $('#noThanks').on('click', function(e) {
                e.preventDefault();
                declineModal.removeClass('modal--hidden');
            });

            var declineModalForm = $('#declineModalForm');

            var declineOptionOther = $('#declineOptionOther');
            var declineOptionOtherField = $('#declineOptionOtherField');
            var declineOptionOtherCheckbox = $('#declineOptionOtherCheckbox');

            declineOptionOther.on('click', function() {
                declineOptionOtherField.focus();
            });

            declineOptionOtherField.on('focus', function() {
                declineOptionOtherCheckbox.prop('checked', true);
            });

            declineModalForm.on('submit', function(e) {

                var selectedOption = declineModalForm.find("input[name='declined_reason']:checked");

                if(selectedOption.length === 0) {
                    e.preventDefault();
                    alert('Please select an option');
                }

            });

        }

        var incorrectCCModal = $('#incorrectCCModal');
        if(incorrectCCModal.length > 0) {

            $('#incorrectCard').on('click', function(e) {
                e.preventDefault();
                incorrectCCModal.removeClass('modal--hidden');
            });

            var incorrectCCModalForm = $('#incorrectCCModalForm');

            var incorrectCCOptionOther = $('#incorrectCCOptionOther');
            var incorrectCCOptionOtherField = $('#incorrectCCOptionOtherField');
            var incorrectCCOptionOtherCheckbox = $('#incorrectCCOptionOtherCheckbox');

            incorrectCCOptionOther.on('click', function() {
                incorrectCCOptionOtherField.focus();
            });

            incorrectCCOptionOtherField.on('focus', function() {
                incorrectCCOptionOtherCheckbox.prop('checked', true);
            });

            incorrectCCModalForm.on('submit', function(e) {

                var selectedOption = incorrectCCModalForm.find("input[name='invalid_cc_reason']:checked");

                if(selectedOption.length === 0) {
                    e.preventDefault();
                    alert('Please select an option');
                }

            });

        }

        $(document).on('click', '.modal__close', function(e) {
            e.preventDefault();
            $(this).parent().parent().addClass('modal--hidden');
        });

		
		
		
		
		
		
	
		

        
        //$('.date-picker').datepicker();

    });
