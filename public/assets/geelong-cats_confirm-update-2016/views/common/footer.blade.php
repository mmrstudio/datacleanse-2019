

    <div class="footer__bottom">

        <div class="footer__bottom__links">
            <a href="mailto:membership@geelongcats.com.au">membership@geelongcats.com.au</a><br>
            <a href="tel:1300462287">1300 46 22 87</a><br>
            <strong><a href="http://geelongcats.com.au" target="_blank">geelongcats.com.au</a></strong>
        </div>

        <ul class="footer__bottom__sponsors">
            <li><a href="#" target="_blank"><img src="{{ $campaign->asset('images/gfc-logo.png') }}"></a></li>
            <li><a href="#" target="_blank"><img src="{{ $campaign->asset('images/ford-logo.png') }}"></a></li>
            <li><a href="#" target="_blank"><img src="{{ $campaign->asset('images/ga-logo.png') }}"></a></li>
        </ul>

    </div>
