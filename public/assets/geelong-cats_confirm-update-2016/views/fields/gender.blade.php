<?php
    if(! isset($fields[$key])) return '';
    $field = $fields[$key];
    $id = isset($id) ? $id : camel_case($field->name . '_field');
    $label = isset($label) ? $label : $field->front_label;
    $control_label = isset($control_label) ? $control_label : false;
    $placeholder = isset($placeholder) ? $placeholder : $field->front_placeholder;
    $options = isset($options) ? $options : $field->getOptions();
    $props = isset($props) ? implode(' ', $props) : false;
    $form_group_classes = ['form__group', 'form__group--select', 'form__group--' . $field->name];
    if($errors->has($field->name)) $form_group_classes[] = 'form__group--error';
    if(isset($width)) $form_group_classes[] = 'form__group--width-' . $width;
    $field_classes = ['form__control', 'form__control--select', 'form__control--' . $field->name];
?>

<div class="{{ implode(' ', $form_group_classes) }}" @if($id)id="{{ $id }}"@endif>
    <label class="form__group__label" for="{{ $field->name }}">
        {{ $label }}
        @if($field->required)<span class="required">*</span>@endif
    </label>
    <div class="form__group__controls">
        @if($control_label)
        <label class="form__group__controls__label" for="{{ $field->name }}">
            {{ $control_label }}
        </label>
        @endif
        {!! Form::select($field->name, select($placeholder, $options), old($field->name, $record->{$field->name}), ['id'=>$field->name, 'class'=>implode(' ', $field_classes)]) !!}
        @if($show_other)
        <input id="{{ $field->name }}_other" name="{{ $field->name }}_other" type="text" placeholder="Enter other..." class="form__control form__control--text form__control--select--other form__control--{{ $field->name }}" value="{{ old($field->name . '_other', $record->{$field->name}) }}" {{ $props }}>
        @endif
    </div>
</div>
