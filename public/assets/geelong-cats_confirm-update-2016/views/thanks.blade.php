@extends($campaign->view('main'))

@section('content')

<header class="header">

    @include($campaign->view('common.header'))

    <div class="header__bottom header__bottom--thanks">
        <div class="header__bottom__player header__bottom__player--thanks"></div>
        <div class="header__bottom__thanks"></div>
    </div>

</header>

<footer class="footer">

    @include($campaign->view('common.footer'))

</footer>

@endsection
