@extends($campaign->view('main'))

@section('content')

<header class="header">

    @include($campaign->view('common.header'))

    <div class="header__bottom">
        <div class="header__bottom__player"></div>
        <div class="header__bottom__text"></div>
    </div>

</header>

<div class="main">

    <form action="{{ $process_url }}" method="post" class="form">

        @include($campaign->view('common.errors'))

        <div class="form__inner">

            @include($campaign->view('fields.text'), [
                'key' => 'name_first',
                'props' => $record->name_first ? ['readonly'] : [],
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'name',
                'props' => $record->name ? ['readonly'] : []
            ])

            @include($campaign->view('fields.address'), [
                'keys' => ['street_addr_1', 'street_addr_2'],
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'city',
                'id' => 'suburbField',
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'zip',
                'id' => 'postcodeField',
            ])

            @include($campaign->view('fields.country'), [
                'key' => 'country',
            ])

            @include($campaign->view('fields.state'), [
                'key' => 'state',
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'phone_home',
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'phone_mobile',
            ])

            @include($campaign->view('fields.email'), [
                'key' => 'email_addr',
            ])

            @include($campaign->view('fields.dropdown'), [
                'key' => 'gender',
            ])

            @if($record->data_segement == 'MEMBER')

            @include($campaign->view('fields.dropdown'), [
                'key' => 'f_2016_renewal_notification',
            ])

            @endif

            <div class="form__submit">

                @foreach($hidden_fields as $name=>$value)
                <input type="hidden" name="{{ $name }}" value="{{ $value }}">
                @endforeach

                <button class="form__submit__button" type="submit">Update</button>

            </div>

        </div>

    </form>

</div>

<footer class="footer">

    <div class="footer__disclaimer">
        <p>By confirming or updating your details you understand that the AFL &amp; AFL Club Privacy Policy will be applied to your information. Entries open 11am AEST 21/06/2016, close 11am AEST 05/07/2016. Winner will be drawn 11am AEST 06/07/2016 at Simonds Stadium, Kardinia Park and notified via email. <a href="http://www.geelongcats.com.au/privacy" target="_blank">Click here</a> to view our AFL &amp; AFL Club Privacy Policy.</p>
    </div>

    @include($campaign->view('common.footer'))

</footer>

@endsection
