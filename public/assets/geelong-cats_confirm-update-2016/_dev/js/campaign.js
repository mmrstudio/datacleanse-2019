
    @@include('plugins/bootstrap.js')
    @@include('plugins/moment.js')
    @@include('plugins/bootstrap-datepicker.js')
    @@include('plugins/bootstrap-datepicker-mobile.js')

    $(document).ready(function() {

        // Country/State Fields
        var countryField = $("#countryField select");
        var stateField = $("#stateField");
        var postcodeField = $("#postcodeField");
        var suburbField = $("#suburbField");

        var auStateFieldWrap = stateField.find('.state-field-au');
        var nzStateFieldWrap = stateField.find('.state-field-nz');
        var auStateField = auStateFieldWrap.find('select');
        var nzStateField = nzStateFieldWrap.find('select');
        var stateOtherWrap = stateField.find('.state-field-other');
        var stateOtherField = stateOtherWrap.find('input');
        var stateRequired = stateField.find('.required');
        var stateError = stateField.hasClass('has-error');
        var postcodeRequired = postcodeField.find('.required');
        var stateFieldName = stateField.data('field-name');
        var suburbRequired = suburbField.find('.required');

        // save original state value
        var auOriginalState = auStateField.val();
        var nzOriginalState = nzStateField.val();

        function checkCountryField() {

            var countryVal = countryField.val();

            if(countryVal == 'AUSTRALIA') {

                auStateFieldWrap.show();
                auStateField.attr('name', stateFieldName);

                nzStateFieldWrap.hide();
                nzStateField.attr('name', '');

                stateOtherWrap.hide();
                stateOtherField.attr('name', '');

                stateRequired.show();
                postcodeRequired.show();
                suburbRequired.show();

                if(stateError) {
                    stateField.removeClass('has-error');
                }

            } else if(countryVal == 'NEW ZEALAND') {

                auStateFieldWrap.hide();
                auStateField.attr('name', '');

                nzStateFieldWrap.show();
                nzStateField.attr('name', stateFieldName);

                stateOtherWrap.hide();
                stateOtherField.attr('name', '');

                stateRequired.show();
                postcodeRequired.show();
                suburbRequired.show();

                if(stateError) {
                    stateField.removeClass('has-error');
                }

            } else {

                auStateFieldWrap.hide();
                nzStateFieldWrap.hide();

                stateOtherWrap.hide();

                stateOtherWrap.show();
                stateOtherField.attr('name', stateFieldName);

                stateRequired.hide();
                postcodeRequired.hide();
                suburbRequired.hide();

                if(stateError) {
                    stateField.addClass('has-error');
                }

            }

        }

        setTimeout(function() {
            checkCountryField();
        }, 500);

        countryField.on('change', checkCountryField);

        $(".form__control--select").select2({ minimumResultsForSearch: Infinity });

        var formGroupSelectOther = $('.form__group--select--other');
        formGroupSelectOther.each(function() {
            var group = $(this);
            var selectField = group.find('select');
            var selectOther = group.find('.form__control--select--other');

            selectOther.hide();

            selectField.on('change', function() {
                var selectVal = $(this).val();

                if(selectVal === 'other') {
                    selectOther.show();
                } else {
                    selectOther.hide();
                }

            });

        });

        $('.date-picker').datepicker();

    });
