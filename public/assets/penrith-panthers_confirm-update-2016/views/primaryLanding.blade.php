@extends($campaign->view('main'))

@section('content')

<header class="header">

    @include($campaign->view('common.header'))

</header>

<div class="main">

    <form action="{{ $process_url }}" method="post" class="form">

        @include($campaign->view('common.errors'))

        <div class="form__inner">

            <div class="form__intro form__intro--thanks">
                @if($record->first_name != '')
                <div class="form__intro__greeting">Dear {{ $record->first_name }},</div>
                @endif
                <div class="form__intro__text">
                    Are your details correct?<br>
                    <span>Simply confirm or update your details and you will go into the running to WIN two corporate box tickets to the Panthers v Roosters game at Pepper Stadium in Round 22.</span>
                </div>
            </div>

            <div class="form__title">
                <h1>Your Details</h1>
            </div>

            @include($campaign->view('fields.text'), [
                'key' => 'first_name',
                'props' => $entry_type == 'update' ? ['readonly'] : [],
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'last_name',
                'props' => $entry_type == 'update' ? ['readonly'] : [],
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'address',
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'billing_suburb_city',
                'id' => 'suburbField',
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'billing_zip_postal_code',
                'id' => 'postcodeField',
            ])

            @include($campaign->view('fields.country'), [
                'key' => 'billing_country',
            ])

            @include($campaign->view('fields.state'), [
                'key' => 'billing_state_province',
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'mobile_phone',
            ])

            @include($campaign->view('fields.email'), [
                'key' => 'email',
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'member_twitter_handle',
            ])

            @include($campaign->view('fields.gender'), [
                'key' => 'gender',
                'placeholder' => 'Please choose your gender...',
            ])

            @include($campaign->view('fields.dob'), [
                'key' => 'date_of_birth',
                'placeholder' => 'Please provide your DOB...',
                'not_required' => false,
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'member_since',
            ])

            <div class="form__submit">

                @foreach($hidden_fields as $name=>$value)
                <input type="hidden" name="{{ $name }}" value="{{ $value }}">
                @endforeach

                <button class="form__submit__button" type="submit">Update</button>

            </div>

        </div>

    </form>

</div>

<footer class="footer">

    @include($campaign->view('common.footer'))

</footer>

@endsection
