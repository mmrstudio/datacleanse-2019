@extends($campaign->view('main'))

@section('content')

<header class="header header--thanks">

</header>

<div class="main">

    <div class="form">
        <div class="form__inner">
            <div class="form__intro form__intro--thanks">
                <div class="form__intro__text">
                    <span>You are now in the running to WIN two corporate box tickets to the Panthers v Roosters game at Pepper Stadium in Round 22</span>
                </div>
            </div>
        </div>
    </div>

</div>

<footer class="footer">

    @include($campaign->view('common.footer'))

</footer>

@endsection
