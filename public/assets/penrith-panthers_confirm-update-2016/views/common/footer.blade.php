
    <div class="footer__sponsors">
        <img src="{{ $campaign->asset('images/sponsors.png') }}">
    </div>

    <p>Penrith Panthers Rugby League Football Club | 1-300 PANTHERS | <a href="mailto:footymembership@panthers.com.au">footymembership@panthers.com.au</a></p>

    <ul class="footer__links">
        <li><a href="http://www.penrithpanthers.com.au/" target="_blank">Penrith Panthers Rugby League Football Club</a></li>
        <li><a href="http://membership.penrithpanthers.com.au/privacy-policy/" target="_blank">Privacy Policy</a></li>
        <li><a href="http://membership.penrithpanthers.com.au/terms-conditions/" target="_blank">Terms &amp; Conditions</a></li>
        <li><a href="http://datacleanse.com.au" target="_blank">Product of Datacleanse</a></li>
    </ul>
