<?php

    // get secondary account holders
    $secondary_accounts = [];
    $get_secondary_accounts = $campaign->input()->where('acct_id', $record->acct_id)->where('primary_code', '<>', 'P')->get();

    if(count($get_secondary_accounts) > 0)
    {
        foreach($get_secondary_accounts as $input)
        {
            // get output record (if it exists)
            $output = $campaign->output()->where('input_id', $input->id)->first();

            // set record
            $secondary_accounts[] = $output ? $output : $input;
        }      
    }

    //dd($secondary_accounts);

    // get product stock
    $stock = DB::table('_campaign_0078_stock')->get();
    //dd($stock->toArray());

    foreach($stock as &$item) {
        if($item->item == 'hoodie' || $item->item == 'polo') {
            $get_count = DB::select("SELECT `item`, `option`, count(id) as 'count' FROM `_campaign_0078_upsells` WHERE `item` = ? AND `option` = ? GROUP BY `item`, `option`", [$item->item, $item->option]);
            if (count($get_count) > 0) {
                $count = $get_count[0]->count;
                $item->total = $item->stock;
                $item->allocated = $count;
                $item->stock = $item->stock - $count;
            }
        }
    }

    //print_r($stock->toArray());

    $get_stock = function($item, $option = null) use ($stock) {
        foreach($stock as $stock_item) {

            if ($stock_item->item == $item && $stock_item->option == $option) {
                return $stock_item->stock;
            }

        }
        return null;
    };

    $get_options = function($item) use ($stock) {
        $options = [];
        foreach($stock as $stock_item) {
            if ($stock_item->item == $item) {
                $options[] = [
                    'item' => $stock_item->item,
                    'option' => $stock_item->option,
                    'stock' => $stock_item->stock,
                ];
            }
        }
        return $options;
    };

    // merch items
    $merch = [
        [
            'key' => 'cap',
            'name' => 'Cap',
            'note' => '(Navy Only)',
            'price' => 20,
            'personalisable' => false,
            'stock' => $get_stock('cap', null),
            "memberChoice" => true,
        ],
        [
            'key' => 'scarf',
            'name' => 'Scarf',
            'note' => '(Navy Only)',
            'price' => 20,
            'personalisable' => false,
            'stock' => $get_stock('scarf', null),
            "memberChoice" => true,
        ],
        [
            'key' => 'thermos',
            'name' => 'Thermos',
            'note' => '(With Personalised Name)',
            'price' => 20,
            'personalisable' => true,
            'stock' => $get_stock('thermos', null),
            "memberChoice" => true,
        ],
        [
            'key' => 'bottle',
            'name' => 'Drink Bottle',
            'note' => '(With Personalised Name)',
            'price' => 20,
            'personalisable' => true,
            'stock' => $get_stock('bottle', null),
            "memberChoice" => true,
        ],
        [
            'key' => 'backpack',
            'name' => 'Backpack',
            'note' => '(Black Only & Personalised Name)',
            'price' => 40,
            'personalisable' => true,
            'stock' => $get_stock('backpack', null),
            "memberChoice" => false,
        ],
    ];

    //dd($merch);

    // addon items

    $addons = [
        [
            'key' => 'aflw',
            'name' => "Crows Women's Team Membership",
            'options' => [],
            'optionsLabel' => '',
            'stock' => $get_stock('aflw'),
            'price' => 45,
            'priceMonth' => '(or $4.50 p/month)',
            'description' => '<p>Get behind the Crows Women’s Team in 2020! Member pack includes: member kit, sports socks, and exclusive match day experiences*</p>'
        ],
        [
            'key' => 'junior',
            'name' => 'Junior',
            'options' => $get_options('junior'),
            'optionsLabel' => 'Choose your junior pack',
            'price' => 35,
            'priceMonth' => '(or $3.50 p/month)',
            'description' => '
                <p>For all the little footy fans (up to 14 years)</p>
                <p><strong>AFL Junior Bronze (3-Game GA)</strong> – Up to 3 games General Admission entry*, Junior pack including footy, hand flag, shoe laces and personalised lunch cooler PLUS invite to Junior clinic*</p>
                <p><strong>AFLW Junior Membership</strong> – Crows Women’s Team Junior pack including footy, shoe laces, sticker and personalised lunch cooler PLUS invite to Junior clinic*</p>
            ',
        ],
        [
            'key' => 'hoodie',
            'name' => '2020 ISC Members Hoodie',
            'options' => $get_options('hoodie'),
            'optionsLabel' => 'Choose your sizes',
            'price' => 70,
            'priceMonth' => '(or $7 p/month)',
            'description' => '<p>Grab your 2020 member exclusive ISC hoodie at our special Members’ only price!*</p>'
        ],
        [
            'key' => 'polo',
            'name' => '2020 ISC Members Polo',
            'options' => $get_options('polo'),
            'optionsLabel' => 'Choose your sizes',
            'price' => 50,
            'priceMonth' => '(or $5 p/month)',
            'description' => '<p>Get your 2020 member exclusive ISC polo for only $50!*</p>'
        ],
    ];

    //dd($addons);
    
    function selections_intro($name_first, $selections) {
        $selections_text = '<strong>%s</strong> you are eligible to choose <strong>%d</strong> %s, one choice for each of your member packs.';
        return sprintf($selections_text, $name_first, $selections, ($selections == 1 ? 'item' : 'items'));
    }

    $content = [
        'introTitle' => 'More choice than ever before!',
        'introText' => '<p>After fantastic feedback, Member’s Choice is back with even more options for 2020.</p><p>For 2020 you can select a <strong>cap, scarf, thermos</strong> or <strong>drink bottle</strong> to be included with your member pack for each of your eligible memberships.</p>',
        'introSubTitle' => 'Choose your preferred item(s) now.',
        'upsellsTitle' => 'WANT TO ADD MORE VALUE TO YOUR MEMBERSHIP? SELECT YOUR MERCHANDISE AND ADD-ONS BELOW:',
        'upsellsText' => '<p>Simply make your selections below and we will update your 2020 Membership invoice & payment(s) prior to you renewing.<br />You will receive further confirmation of payment amount prior to your monthly payments commencing.</p>',
        'thanksTitle' => 'Thank you',
        'thanksText' => '<p>For making your Member’s Choice selection.</p>',
        'thanksSubText' => '<p>You will receive further communication in October regarding the details and pricing of your <strong>2020 Membership</strong>.</p><br /><p>If you have any queries please contact Member Services on (08) 8440 6690, email <a href="mailto:membership@afc.com.au">membership@afc.com.au</a><br />or visit our <a href="https://www.crowsmembership.com.au/2020-members-choice-0">Frequently Asked Questions</p>',
    ];

    if($record->account_type == 'monthly') {
        $content['introSubText'] = '<p>By October you will receive a detailed summary of your 2020 Membership and your monthly Easy Pay payment plan rollover.</p><p><strong><span>Stock is strictly limited so members are encouraged to select early to avoid missing out on preferred items.</span></strong></p>';
    } elseif($record->account_type == 'upfront') {
        $content['introSubText'] = '<p>By October you will receive a detailed summary of your 2020 Membership and your upfront Easy Pay payment plan rollover.</p><p><strong><span>Stock is strictly limited so members are encouraged to select early to avoid missing out on preferred items.</span></strong></p>';
        $content['upsellsText'] = '<p>Simply make your selections below and we will update your 2020 Membership invoice & payment(s) prior to you renewing.<br />You will receive further confirmation of payment amount prior to your upfront payment.</p>';
    } elseif($record->account_type == 'nopp') {
        $content['introSubText'] = '<p>By October you will receive a detailed summary of your 2020 Membership and your renewal payment details.</p><p><strong><span>Stock is strictly limited so members are encouraged to select early to avoid missing out on preferred items.</span></strong></p>';
        $content['upsellsText'] = '<p>Simply make your selections below and we will update your 2020 Membership invoice & payment(s) prior to you renewing.<br />You will receive further confirmation of payment amount prior to your payment.</p>';
    }


    $record->selections = [
        'mc_cap' => [],
        'mc_scarf' => [],
        'mc_thermos' => [],
        'mc_bottle' => [],
        'extra_cap' => [],
        'extra_scarf' => [],
        'extra_thermos' => [],
        'extra_bottle' => [],
        'extra_backpack' => [],
        'upsell_aflw' => [],
        'upsell_junior' => [],
        'upsell_hoodie' => [],
        'upsell_polo' => [],
    ];

    foreach($secondary_accounts as &$secondary) {
        $secondary->selections = [
            'mc_cap' => [],
            'mc_scarf' => [],
            'mc_thermos' => [],
            'mc_bottle' => [],
        ];
    }

    $checkboxes = [
        'accept' => [
            'field' => 'accept',
            'validation' => 'Please accept Terms & Conditions',
            'text' => '<p>I understand that the Adelaide Crows will update my 2020 membership package and payments to reflect my above selections and will adjust the payments of my automatic renewal monthly payment plan, to be deducted from the credit card on file. If I have any queries I can contact the Club on (08) 8440 6690 or via membership@afc.com.au</p>',
            'required' => true,
        ],
        'payment' => [],
    ];

    if($record->account_type == 'upfront') {
        $checkboxes['accept']['text'] = '<p>I understand that the Adelaide Crows will update my 2020 membership package and payments to reflect my above selections and will adjust the payments of my automatic renewal upfront payment plan, to be deducted from the credit card on file. If I have any queries I can contact the Club on (08) 8440 6690 or via membership@afc.com.au</p>';
    }

    if($record->account_type == 'nopp') {
        $checkboxes['accept']['text'] = '<p>I understand that the Adelaide Crows will update my 2020 membership package and payments to reflect my above selections and will adjust my invoice for my membership renewal. I understand my payment will need to be made before I receive my pack and other selections. If I have any queries I can contact the Club on (08) 8440 6690 or via membership@afc.com.au</p>';
        $checkboxes['payment'][] = [
            'field' => 'optin_monthly',
            'validation' => '',
            'text' => '<p>(Optional) <strong>Monthly Payment Plan</strong>; I would like to opt in to the Monthly Easy-Pay Payment Plan commencing in October, to pay my membership in 10 instalments. Please contact me for my credit card details.</p>',
            'required' => false,
        ];
        $checkboxes['payment'][] = [
            'field' => 'optin_upfront',
            'validation' => '',
            'text' => '<p>(Optional) <strong>Upfront Payment Plan</strong>; I would like to opt in to the Upfront Easy-Pay Payment Plan commencing in October. Please contact me for my credit card details.</p>',
            'required' => false,
        ];
    }

    $app_data = [
        'campaign' => $campaign,
        'record' => $record,
        'secondaryAccounts' => $secondary_accounts,
        'merch' => $merch,
        'addons' => $addons,
        'content' => $content,
        'checkboxes' => $checkboxes,
        'hiddenFields' => $hidden_fields,
        'processUrl' => $process_url,
    ];

    //dd($app_data);
    //print_r($app_data); exit;

?><!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">

    <head>

    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    	<title>{{ $campaign->campaignTitle() }}</title>

        <link href="{{ $campaign->asset('static/css/main.css') }}" rel="stylesheet">
        <link rel="icon" href="{{ $campaign->asset('icon.ico') }}" />

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-33851616-15', 'auto');
          ga('send', 'pageview');

        </script>

        <link rel="stylesheet" href="https://use.typekit.net/lbw0iqp.css">

        <script>
            window._token = '{{ csrf_token() }}';
            window.appData = {!! json_encode($app_data) !!};
        </script>

    </head>

    <body>
        <div id="root"></div>
        <script src="{{ $campaign->asset('static/js/main.js') }}"></script>
    </body>

</html>
