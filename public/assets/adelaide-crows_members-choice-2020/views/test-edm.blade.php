<!DOCTYPE html>
<!--
<?php print_r($record->toArray()); ?>
<?php //print_r($record); ?>
--><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
  <head>
    <!--[if gte mso 9]>
    <xml>
      <o:OfficeDocumentSettings>
        <o:AllowPNG/>
        <o:PixelsPerInch>96</o:PixelsPerInch>
      </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <title>ADELAIDE FOOTBALL CLUB</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0 " />
    <meta name="format-detection" content="telephone=no" />
    <style type="text/css">
      body { margin: 0; padding: 0; -webkit-text-size-adjust: 100% !important; -ms-text-size-adjust: 100% !important; -webkit-font-smoothing: antialiased !important; }
      img { border: 0 !important; outline: none !important; }
      p { Margin: 0px !important; Padding: 0px !important; }
      table { border-collapse: collapse; mso-table-lspace: 0px; mso-table-rspace: 0px; }
      td, a, span { border-collapse: collapse; mso-line-height-rule: exactly; }
      .ExternalClass * { line-height: 100%; }
      .em_defaultlink a { color: inherit !important; text-decoration: none !important; }
			.em_blue a { color:#002a5b; text-decoration:none; }
			.em_blue2 a { color:#22456e; text-decoration:none; }
			.em_blue_u a { color:#002a5b; text-decoration:underline; }
			.em_wht a { color:#ffffff; text-decoration:none; }
			.em_g_img + div { display: none; }
      @media only screen and (min-width:481px) and (max-width:599px) {
      .em_main_table { width: 100% !important; }
      .em_wrapper { width: 100% !important; }
      .em_hide { display: none !important; }
      .em_full_img { width: 100% !important; height: auto !important; }
      .em_side10 { width: 20px !important; }
      .em_aside10 { padding: 20px 10px !important; }
			.em_auto { height:auto !important; }
			.em_aside { padding:0px 40px !important; }
      }
			@media only screen and (min-width:375px) and (max-width:480px) {
      .em_main_table { width: 100% !important; }
      .em_wrapper { width: 100% !important; }
      .em_hide { display: none !important; }
      .em_full_img { width: 100% !important; height: auto !important; }
      .em_side10 { width: 20px !important; }
      .em_aside10 { padding: 20px 10px !important; }
			.em_cta { font-size:16px !important; height:45px !important; }
			.em_cta a { line-height:45px !important; }
			.em_auto { height:auto !important; }
			.em_aside { padding:0px 40px !important; }
			.em_font { font-size:14px !important; line-height:18px !important; }
			.em_font2 { font-size:16px !important; line-height:18px !important; }
      }
      @media screen and (max-width: 374px) {
      .em_main_table { width: 100% !important; }
      .em_wrapper { width: 100% !important; }
      .em_hide { display: none !important; }
      .em_full_img { width: 100% !important; height: auto !important; }
      .em_side10 { width: 20px !important; }
      .em_aside10 { padding: 20px 10px !important; }
      u + .em_body .em_full_wrap { width: 100% !important; width: 100vw !important; }
			.em_cta { font-size:16px !important; height:45px !important; }
			.em_cta a { line-height:45px !important; }
			.em_auto { height:auto !important; }
			.em_aside { padding:0px 40px !important; }
			.em_font { font-size:13px !important; line-height:18px !important; }
			.em_font2 { font-size:15px !important; line-height:18px !important; }
      }
      span.preheader { display: none !important; }
    </style>
  </head>
  <body class="em_body" style="margin:0px; padding:0px;" bgcolor="#ffffff">
    <span class="preheader">We’re offering you the option to CHOOSE a cap, scarf or beanie in your 2019 Member Pack.</span>
    <singleline></singleline>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="em_full_wrap" bgcolor="#ffffff">
      <tr>
        <td align="center" valign="top">
          <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_main_table" style="width:600px;">
            <tr>
            	<td align="center" valign="top" class="em_auto">
                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                	<tr>
                  	<td align="center" valign="top">
                      <a href="<?php echo $record->update_url; ?>" target="_blank" style="text-decoration:none;">
                        <img class="em_full_img" src="<?php echo $campaign->asset('edm/images/banner.jpg'); ?>" width="600" height="300" alt="" border="0" style="display:block;" />
                      </a>
                    </td>
                  </tr>
                	<tr>
                  	<td align="center" valign="top">
                      <img class="em_full_img" src="<?php echo $campaign->asset('edm/images/content.png'); ?>" width="450" height="254" alt="" border="0" style="display:block; max-width: 450px;" />
                    </td>
                  </tr>
                </table>
                <!--[if gte mso 9]>
                </v:textbox>
                </v:rect>
                <![endif]-->
              </td> 
            </tr>
            <tr>
            	<td align="center" valign="top" style="padding:0px 30px 0px 30px;">
              	<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                  <tr>
                    <td align="center" valign="top">
                      <a href="<?php echo $record->update_url; ?>">
                        <img src="<?php echo $campaign->asset('edm/images/button.png'); ?>" width="402" height="60" alt="" border="0" style="display:block; max-width:400px;" />
                      </a>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
            	<td align="center" valign="top">
              	<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                  <?php if($record->segment == 'legcc') : ?>
                  <tr>
                    <td align="center" valign="top">
                      <img src="<?php echo $campaign->asset('edm/images/legends.png'); ?>" width="400" height="139" alt="" border="0" style="display:block; max-width: 400px;" />
                    </td>
                  </tr>
                  <?php else : ?>
                  <tr>
                    <td align="center" valign="top" height="36"></td>
                  </tr>
                <?php endif; ?>
                </table>
              </td>
            </tr>
            <tr>
            	<td height="8" style="font-size:1px; line-height:1px;" bgcolor="#e11635">&nbsp;</td>
            </tr>
            <tr>
            	<td align="center" valign="top" bgcolor="#ffd200" style="padding:20px 60px 15px 60px;" class="em_aside10">
              	<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                	<tr>
                  	<td align="center" valign="top">
                    	<table align="center" cellpadding="0" cellspacing="0" border="0">
                      	<tr>
                        	<td align="center" valign="top"><a href="http://www.facebook.com/adelaidecrows" target="_blank" style="text-decoration:none;"><img class="em_g_img" src="<?php echo $campaign->asset('edm/images/fb.jpg'); ?>" width="32" alt="FB" border="0" style="display:block; max-width:32px; font-family:Arial, sans-serif; font-size:11px; line-height:16px; color:#002a5b;" /></a></td>
                          <td width="10" style="width:10px;">&nbsp;</td>
                          <td align="center" valign="top"><a href="https://twitter.com/adelaide_fc" target="_blank" style="text-decoration:none;"><img class="em_g_img" src="<?php echo $campaign->asset('edm/images/tw.jpg'); ?>" width="32" alt="TW" border="0" style="display:block; max-width:32px; font-family:Arial, sans-serif; font-size:11px; line-height:16px; color:#002a5b;" /></a></td>
                          <td width="10" style="width:10px;">&nbsp;</td>
                          <td align="center" valign="top"><a href="https://www.instagram.com/adelaide_fc" target="_blank" style="text-decoration:none;"><img class="em_g_img" src="<?php echo $campaign->asset('edm/images/insta.jpg'); ?>" width="32" alt="INSTA" border="0" style="display:block; max-width:32px; font-family:Arial, sans-serif; font-size:11px; line-height:16px; color:#002a5b;" /></a></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                  	<td height="15" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                  	<td align="center" class="em_blue_u" style="font-family:Arial, sans-serif; font-size:9px; line-height:12px; color:#002a5b;"><span style="font-family:Arial, sans-serif; font-size:9px; line-height:12px; color:#002a5b;">For any queries please contact Member Services at <a href="mailto:membership@afc.com.au" style="text-decoration:none; color:#002a5b;">membership@afc.com.au</a> or <a href="tel:0884406690" style="text-decoration:none; color:#002a5b;">(08) 8440 6690</a>. <br class="em_hide" />
                      <a href="https://crowsmembership.com.au/terms-conditions-7" target="_blank" style="text-decoration:underline; color:#002a5b; white-space:nowrap;">Terms
                        &amp; Conditions</a> &nbsp; | &nbsp; <a href="http://www.afc.com.au/privacy" target="_blank" style="text-decoration:underline; color:#002a5b; white-space:nowrap;">Privacy
                        Policy</a> &nbsp; | &nbsp; To unsubscribe from future receipt please </span><unsubscribe style="text-decoration:underline; color:#002a5b; white-space:nowrap;">click here</unsubscribe>
                      </td>
                  </tr>
                  <tr>
                  	<td height="13" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                  	<td align="center" valign="top">
                    	<table align="center" cellpadding="0" cellspacing="0" border="0">
                      	<tr>
                        	<td align="center" valign="top"><a href="http://www.afc.com.au/" target="_blank" style="text-decoration:none;"><img class="em_full_img em_g_img" src="<?php echo $campaign->asset('edm/images/footer_img1.jpg'); ?>" width="77" height="54" alt="We Fly As One" border="0" style="display:block; max-width:77px; font-family:Arial, sans-serif; font-size:11px; line-height:16px; color:#002a5b;" /></a></td>
                          <td width="50" style="width:50px;" class="em_side10">&nbsp;</td>
                          <td align="center" valign="top"><a href="http://www.afc.com.au/" target="_blank" style="text-decoration:none;"><img class="em_full_img em_g_img" src="<?php echo $campaign->asset('edm/images/footer_img2.jpg'); ?>" width="51" height="54" alt="TOYOTA" border="0" style="display:block; max-width:51px; font-family:Arial, sans-serif; font-size:11px; line-height:16px; color:#002a5b;" /></a></td>
                          <td width="50" style="width:50px;" class="em_side10">&nbsp;</td>
                          <td align="center" valign="top"><a href="http://www.afc.com.au/" target="_blank" style="text-decoration:none;"><img class="em_full_img em_g_img" src="<?php echo $campaign->asset('edm/images/footer_img3.jpg'); ?>" width="112" height="54" alt="AFC.COM.AU" border="0" style="display:block; max-width:112px; font-family:Arial, sans-serif; font-size:11px; line-height:16px; color:#002a5b;" /></a></td>
                          <td width="42" style="width:42px;" class="em_side10">&nbsp;</td>
                          <td align="center" valign="top"><a href="http://www.afc.com.au/" target="_blank" style="text-decoration:none;"><img class="em_full_img em_g_img" src="<?php echo $campaign->asset('edm/images/footer_img4.jpg'); ?>" width="65" height="54" alt="RAA" border="0" style="display:block; max-width:65px; font-family:Arial, sans-serif; font-size:11px; line-height:16px; color:#002a5b;" /></a></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td class="em_hide" style="line-height:1px;min-width:600px;background-color:#ffffff;"><img alt="" src="<?php echo $campaign->asset('edm/images/spacer.gif'); ?>" height="1" width="600" style="max-height:1px; min-height:1px; display:block; width:600px; min-width:600px;" border="0" /></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>
</html>
