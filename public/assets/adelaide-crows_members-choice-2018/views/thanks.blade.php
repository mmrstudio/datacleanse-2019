@extends($campaign->view('main'))

@section('content')

<div class="main">

    <div class="thanks">

        <div class="thanks__inner">

            <div class="thanks__title">Thank You {{ $record->name_first }}!</div>

            <p>Thank you for boosting your membership package for Season 2018. Our membership team will process your request and make the neccessary changes to your membership.</p>

            <p>If you have any queries, please do not hesitate to contact us on 1300 46 36 47 or via email at <a href="mailto:membership@westernbulldogs.com.au">membership@westernbulldogs.com.au</a>.</p>

            <p>Go Dogs!</p>

            <p>Western Bulldogs Football Club</p>

        </div>

    </div>

</div>

@endsection
