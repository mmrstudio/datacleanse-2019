<?php

    // convert arrays

    if(isset($record->addon_hoodie) && strlen($record->addon_hoodie) > 0) {
        $record->addon_hoodie = explode(',', $record->addon_hoodie);
    } else {
        $record->addon_hoodie = false;
    }

    if(isset($record->addon_polo) && strlen($record->addon_polo) > 0) {
        $record->addon_polo = explode(',', $record->addon_polo);
    } else {
        $record->addon_polo = false;
    }

    //print_r($record->toArray()); exit;

    $addons = [

        [
            'key' => 'aflw_team',
            'field' => 'addon_aflw_team',
            'class' => 'aflw-team',
            'name' => '2018 Crows Women\'s Team Membership',
            'description' => 'Support the Crows Women\'s Team in 2018! Member pack includes: member kit plus stubby holder, choice of collectable bobble head, opportunity to join Guard of Honour.*',
            'price' => 60,
            'monthly' => '(or $6 p/month)',
            'options' => false,
            'default' => 0,
        ],

        [
            'key' => 'aflw_junior',
            'field' => 'addon_aflw_junior',
            'class' => 'aflw-junior',
            'name' => '2018 Crows Women\'s Team Junior Membership',
            'description' => 'For little footy fans (Up to 14 years) – Get our new Crows Women\'s Team Junior pack! Member pack includes: member kit, plus sunglasses, foam finger, sticker sheet, choice of collectable bobble head, opportunity to join Guard of Honour.*',
            'price' => 30,
            'monthly' => '(or $3 p/month)',
            'options' => false,
            'default' => 0,
        ],

        [
            'key' => 'hoodie',
            'field' => 'addon_hoodie',
            'class' => 'hoodie',
            'name' => '2018 ISC Members Hoodie',
            'description' => 'Gear up in our 2018 member exclusive ISC hoodie!*',
            'price' => 70,
            'monthly' => '(or $7 p/month)',
            'options' => ['Mens S','Mens M','Mens L','Mens XL','Mens 2XL','Mens 3XL','Mens 4XL','Mens 5XL','Ladies 8','Ladies 10','Ladies 12','Ladies 14','Ladies 16','Ladies 18'],
            'optionsLabel' => 'Size',
            'optionsDropdownLabel' => 'Select size...',
            'default' => [],
        ],

        [
            'key' => 'polo',
            'field' => 'addon_polo',
            'class' => 'polo',
            'name' => '2018 ISC Members Polo',
            'description' => 'Get you 2018 ISC member polo at your exclusive price!*',
            'price' => 50,
            'monthly' => '(or $5 p/month)',
            'options' => ['Mens S','Mens M','Mens L','Mens XL','Mens 2XL','Mens 3XL','Mens 4XL','Mens 5XL','Ladies 8','Ladies 10','Ladies 12','Ladies 14','Ladies 16','Ladies 18'],
            'optionsLabel' => 'Size',
            'optionsDropdownLabel' => 'Select size...',
            'default' => [],
        ],

    ];

    $merch_items = [
        [
            'key' => 'cap',
            'field' => 'merch_cap',
            'name' => 'Member Cap',
            'label' => [
                'one' => 'I want a cap',
                'multiple' => ['singular' => 'Cap', 'plural' => 'Caps']
            ],
            'note' => '(In your bay colour)',
            'price' => 0,
        ],
        [
            'key' => 'scarf',
            'field' => 'merch_scarf',
            'name' => 'Member Scarf',
            'label' => [
                'one' => 'I want a scarf',
                'multiple' => ['singular' => 'Scarf', 'plural' => 'Scarves']
            ],
            'note' => '',
            'price' => 0,
        ],
        [
            'key' => 'both',
            'field' => 'merch_both',
            'name' => 'Member Cap and Scarf',
            'label' => [
                'one' => 'I want both',
                'multiple' => ['singular' => 'Both', 'plural' => 'Both']
            ],
            'note' => '(Cap in your bay colour)',
            'price' => 20,
        ],
    ];

    $app_data = [
        'campaign' => $campaign,
        'record' => $record,
        'fields' => $hidden_fields,
        'allowedSelections' => $record->member_choice,
        'processUrl' => $process_url,
        'addOns' => $addons,
        'merchItems' => $merch_items,
    ];

    //dd($app_data);
    //print_r($app_data); exit;

?><!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">

    <head>

    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    	<title>{{ $campaign->campaignTitle() }}</title>

    	<link href="{{ $campaign->asset('dist/bundle.css') }}" rel="stylesheet">
        <link rel="icon" href="{{ $campaign->asset('icon.ico') }}" />

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-33851616-15', 'auto');
          ga('send', 'pageview');

        </script>

        <link href="https://fonts.googleapis.com/css?family=Poppins:400,700" rel="stylesheet">

        <script>
            window._token = '{{ csrf_token() }}';
            window.appData = {!! json_encode($app_data) !!};
        </script>

    </head>

    <body>
        <div id="app"></div>
        <div class="incompatible" id="incompatible">
            <div class="incompatible__inner">
                <div class="incompatible__text">
                    <p class="title">Please upgrade your browser</p>
                    <p class="description">Unfortunately your browser doesn't support this site.<br>Please use one of the following broswers:<br>Google Chrome, Firefox, Safari, Internet Explorer 11 or Microsoft Edge</p>
                </div>
            </div>
        </div>
        <script src="{{ $campaign->asset('dist/bundle.js') }}"></script>
    </body>

</html>
