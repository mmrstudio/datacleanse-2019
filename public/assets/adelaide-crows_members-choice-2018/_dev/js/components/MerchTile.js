import React from 'react';
import { plural } from '../utils';
import Checkbox from './Checkbox';

class MerchTile extends React.Component {

    constructor(props){
        super(props);
    }

    _toggleMerchQty() {
        const { merchItem, merchQty, onUpdate } = this.props;
        onUpdate(merchItem.field, merchQty === 0 ? 1 : 0);
    }

    _incrementMerchQty() {
        const { merchItem, merchQty, onUpdate, totalSelections, allowedSelections } = this.props;
        if(totalSelections < allowedSelections) {
            onUpdate(merchItem.field, merchQty + 1);
        }
    }

    _decrementMerchQty() {
        const { merchItem, merchQty, onUpdate } = this.props;
        if(merchQty > 0) {
            onUpdate(merchItem.field, merchQty - 1);
        }
    }

    render() {

        const renderMerchLabel = () => {
            const { merchItem, merchQty, allowedSelections } = this.props;
            let merchLabel = '';
            if(merchItem.field !== false) {
                merchLabel = allowedSelections === 1 ? merchItem.label.one : plural(merchItem.label.multiple.singular, merchItem.label.multiple.plural, merchQty, true);
            } else {
                merchLabel = merchItem.label.one;
            }

            return (
                <div className="member-choice__tile__label">
                    <span className="member-choice__tile__label__main">{merchLabel} </span>
                    { merchItem.price > 0 ? <strong className="member-choice__tile__label__price">Add ${merchItem.price}</strong> : undefined }
                    <br />
                    { merchItem.note !== false ? <span className="member-choice__tile__label__note">{merchItem.note}</span> : undefined }
                </div>
            );
        }

        const renderOptions = () => {
            const { merchItem, merchQty, allowedSelections, totalSelections } = this.props;

            if(merchItem.field !== false) {

                if(allowedSelections === 1) {
                    return (
                        <div className="member-choice__tile__checkbox">
                            <Checkbox class="" checked={merchQty > 0} onClick={this._toggleMerchQty.bind(this)} />
                        </div>
                    )
                } else {
                    return (
                       <div className="member-choice__tile__qty">
                           <button className="member-choice__tile__qty__button member-choice__tile__qty__button--decrement" disabled={this.props.merchQty <= 0} onClick={this._decrementMerchQty.bind(this)}>
                               <svg viewBox="0 0 16 16"><path d="M15.7,6.1c-0.2-0.2-0.5-0.3-0.8-0.3H1.1c-0.3,0-0.6,0.1-0.8,0.3C0.1,6.3,0,6.6,0,6.9v2.2c0,0.3,0.1,0.6,0.3,0.8 c0.2,0.2,0.5,0.3,0.8,0.3h13.8c0.3,0,0.6-0.1,0.8-0.3C15.9,9.7,16,9.4,16,9.1V6.9C16,6.6,15.9,6.3,15.7,6.1z"/></svg>
                           </button>
                           <div className="member-choice__tile__qty__number">
                               <span>{merchQty}</span>
                           </div>
                           <button className="member-choice__tile__qty__button member-choice__tile__qty__button--increment" disabled={totalSelections >= allowedSelections} onClick={this._incrementMerchQty.bind(this)}>
                               <svg viewBox="0 0 16 16"><path d="M15.7,6.1c-0.2-0.2-0.5-0.3-0.8-0.3h-4.7V1.1c0-0.3-0.1-0.6-0.3-0.8C9.7,0.1,9.4,0,9.1,0H6.9C6.6,0,6.3,0.1,6.1,0.3 C5.9,0.5,5.8,0.8,5.8,1.1v4.7H1.1c-0.3,0-0.6,0.1-0.8,0.3C0.1,6.3,0,6.6,0,6.9v2.2c0,0.3,0.1,0.6,0.3,0.8c0.2,0.2,0.5,0.3,0.8,0.3 h4.7v4.7c0,0.3,0.1,0.6,0.3,0.8C6.3,15.9,6.6,16,6.9,16h2.2c0.3,0,0.6-0.1,0.8-0.3c0.2-0.2,0.3-0.5,0.3-0.8v-4.7h4.7 c0.3,0,0.6-0.1,0.8-0.3C15.9,9.7,16,9.4,16,9.1V6.9C16,6.6,15.9,6.3,15.7,6.1z"/></svg>
                           </button>
                       </div>
                   )
                }
            } else {
                return null;
            }

        }

        const { merchItem, merchQty, allowedSelections } = this.props;

        return (
            <div className="member-choice__tile" data-selected={merchQty > 0}>
                <div className={`member-choice__tile__image ${merchItem.key}`}></div>
                <div className="member-choice__tile__bottom">
                    {renderOptions()}
                    {renderMerchLabel()}
                </div>
            </div>
        );
    }

}

export default MerchTile;
