import React from 'react';
import { plural, pushArray, popArray } from '../utils';

class AddonTile extends React.Component {

    constructor(props){
        super(props);
    }

    _incrementAddonQty() {
        const { addOn, addonQty, addonSelections, onUpdate } = this.props;

        if(addOn.options === false) {
            return onUpdate(addOn.field, addonQty + 1);
        } else {
            return onUpdate(addOn.field, pushArray(addonSelections, ''));
        }
    }

    _decrementAddonQty() {
        const { addOn, addonQty, addonSelections, onUpdate } = this.props;

        if(addOn.options === false) {
            if(addonQty >= 0) return onUpdate(addOn.field, addonQty - 1);
        } else {
            return onUpdate(addOn.field, popArray(addonSelections));
        }
    }

    render() {
        const { addOn, addonQty, addonSelections } = this.props;

        return (
            <div className="add-ons__tile" data-selected={addonQty > 0}>
                <div className="add-ons__tile__inner">
                    <div className="add-ons__tile__image">
                        <div className={`add-ons__tile__image__inner ${addOn.class}`}></div>
                    </div>
                    <div className="add-ons__tile__desc" dangerouslySetInnerHTML={{__html: addOn.description}}></div>
                    { addOn.price !== false ? (
                    <div className="add-ons__tile__options">

                        <div className="add-ons__tile__price">
                            <span className="add-ons__tile__price__total">${addOn.price} </span>
                            { addOn.monthly !== false ? <span className="add-ons__tile__price__monthly">{addOn.monthly}</span> : false }
                        </div>

                        <div className="add-ons__tile__qty">
                            <button className="add-ons__tile__qty__button add-ons__tile__qty__button--decrement" disabled={addonQty <= 0} onClick={this._decrementAddonQty.bind(this)}>
                                <svg viewBox="0 0 16 16"><path d="M15.7,6.1c-0.2-0.2-0.5-0.3-0.8-0.3H1.1c-0.3,0-0.6,0.1-0.8,0.3C0.1,6.3,0,6.6,0,6.9v2.2c0,0.3,0.1,0.6,0.3,0.8 c0.2,0.2,0.5,0.3,0.8,0.3h13.8c0.3,0,0.6-0.1,0.8-0.3C15.9,9.7,16,9.4,16,9.1V6.9C16,6.6,15.9,6.3,15.7,6.1z"/></svg>
                            </button>
                            <div className="add-ons__tile__qty__number">
                                <span>{addonQty}</span>
                            </div>
                            <button className="add-ons__tile__qty__button add-ons__tile__qty__button--increment" onClick={this._incrementAddonQty.bind(this)}>
                                <svg viewBox="0 0 16 16"><path d="M15.7,6.1c-0.2-0.2-0.5-0.3-0.8-0.3h-4.7V1.1c0-0.3-0.1-0.6-0.3-0.8C9.7,0.1,9.4,0,9.1,0H6.9C6.6,0,6.3,0.1,6.1,0.3 C5.9,0.5,5.8,0.8,5.8,1.1v4.7H1.1c-0.3,0-0.6,0.1-0.8,0.3C0.1,6.3,0,6.6,0,6.9v2.2c0,0.3,0.1,0.6,0.3,0.8c0.2,0.2,0.5,0.3,0.8,0.3 h4.7v4.7c0,0.3,0.1,0.6,0.3,0.8C6.3,15.9,6.6,16,6.9,16h2.2c0.3,0,0.6-0.1,0.8-0.3c0.2-0.2,0.3-0.5,0.3-0.8v-4.7h4.7 c0.3,0,0.6-0.1,0.8-0.3C15.9,9.7,16,9.4,16,9.1V6.9C16,6.6,15.9,6.3,15.7,6.1z"/></svg>
                            </button>
                        </div>

                    </div>
                    ) : false }
                </div>
            </div>
        );
    }

}

export default AddonTile;
