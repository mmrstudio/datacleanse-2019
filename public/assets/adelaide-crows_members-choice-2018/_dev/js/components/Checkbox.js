import React from 'react';

class Checkbox extends React.Component {

    constructor(props){
        super(props);
    }

    render() {
        return (
            <div className={`checkbox ${this.props.class}`} data-checked={this.props.checked} onClick={this.props.onClick}>
                <div className="checkbox__inner">
                    <svg viewBox="0 0 40 40"><polygon points="14.1,31.7 6,23.3 8.6,20.9 14.2,26.7 31.7,9.1 34.2,11.6"/></svg>
                </div>
            </div>
        )
    }

}

export default Checkbox;
