import React from 'react';
import MerchTile from './components/MerchTile';
import AddonTile from './components/AddonTile';
import Modal from './components/Modal';
import Checkbox from './components/Checkbox';
import { plural } from './utils';
import { fetchPOST } from './fetchUtils';

require('es6-promise').polyfill();
require('es6-object-assign').polyfill();

class App extends React.Component {

    constructor(props){
        super(props);

        let initialState = {
            campaign: window.appData.campaign,
            record: window.appData.record,
            fields: {},
            hiddenFields: window.appData.fields,
            addOns: window.appData.addOns,
            merchItems: window.appData.merchItems,
            memberPackage: window.appData.memberPackage,
            allowedSelections: parseInt(window.appData.allowedSelections),
            processUrl: window.appData.processUrl,
            totalSelections: 0,
            loading: false,
            submitted: false,
            errors: []
        }

        //console.log('initialState', initialState);

        // initial merch items
        initialState.merchItems.map((merchItem, i) => {
            if(merchItem.field !== false) {
                initialState.fields[merchItem.field] = initialState.record[merchItem.field] ? initialState.record[merchItem.field] : 0;
                initialState.totalSelections += initialState.fields[merchItem.field];
            }
        });

        // initial add-on selections
        initialState.addOns.map((addOn, i) => {
            const defValue = addOn.options === false ? 0 : [];
            initialState.fields[addOn.field] = initialState.record[addOn.field] ? initialState.record[addOn.field] : defValue;
        });

        // donation options
        initialState.fields.donation = initialState.record.donation ? initialState.record.donation : 'no';
        initialState.fields.donation_amount = initialState.record.donation_amount ? initialState.record.donation_amount : 0;

        // accept option
        initialState.fields.accept = initialState.record.accept ? initialState.record.accept : 'no';

        //console.log('initialState', initialState);

        this.state = initialState;

        this._modal = false;

        this._handleAddonSelection = this._handleAddonSelection.bind(this);
        this._handleAcceptChange = this._handleAcceptChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        this._onSubmitResponse = this._onSubmitResponse.bind(this);
        this._renderMemberChoice = this._renderMemberChoice.bind(this);
        this._renderAddOns = this._renderAddOns.bind(this);

    }

    _handleMerchSelection(merchField, merchQty) {

        let fields = this.state.fields;

        if(this.state.allowedSelections === 1) {

            // toggle all selections
            this.state.merchItems.map((merchItem, i) => {
                if(merchItem.field !== false) fields[merchItem.field] = 0;
            });

        }

        // set current selection
        fields[merchField] = merchQty;

        // re-calculate total selections
        let totalSelections = 0;
        this.state.merchItems.map(merchItem => {
            if(merchItem.field !== false) totalSelections += fields[merchItem.field];
        });

        this.setState({
            fields: fields,
            totalSelections: totalSelections
        });
    }

    _handleAddonSelection(addonField, addonSelection) {
        let fields = this.state.fields;
        fields[addonField] = addonSelection;
        this.setState({ fields: fields });
    }

    _handleAcceptChange() {
        let fields = this.state.fields;
        fields.accept = fields.accept === 'no' ? 'yes' : 'no';
        this.setState({ fields: fields });
    }

    _handleSubmit() {
        const { record, fields, hiddenFields, allowedSelections, totalSelections, processUrl, addOns } = this.state;
        let submitErrors = [];

        // check if all selections made
        if(totalSelections < allowedSelections) {
            submitErrors.push(`Please make all your merchandise selections - you have ${allowedSelections - totalSelections} remaining.`);
        }

        // check if all add options selected
        addOns.map(addOn => {
            const addonField = fields[addOn.field];
            if(addOn.options !== false) {
                for(let i=0; i < addonField.length; i++) {
                    if(addonField[i] === '') {
                        submitErrors.push(`Please choose ${addOn.optionsLabel.toLowerCase()} for ${addOn.name}`);
                    }
                }
            }
        });

        // check if donation amount entered
        if(fields.donation === 'yes' && fields.donation_amount === 0) {
            submitErrors.push(`Please enter your donation amount.`);
        }

        // check if agree
        if(fields.accept === 'no') {
            submitErrors.push(`Please agree to the membership terms & conditions.`);
        }

        if(submitErrors.length > 0) return this.setState({ errors: submitErrors });

        // update loading/submit state
        //this.setState({ submitted: false, loading: true });

        // send data
        fetchPOST(
            processUrl,
            Object.assign(fields, hiddenFields),
            this._onSubmitResponse
        );

    }

    _onSubmitResponse(response) {

        if(response === true) {

            this.setState({
                submitted: true,
                loading: false,
            });

            scroll(0,0);

        } else {

            let errors = [];

            Object.keys(response).map((key) => {
                let value = response[key];
                value.map((error) => {
                    errors.push(error);
                });
            });

            this.setState({ errors: errors });

            //alert(errors.join("\r\n"));

            this.setState({
                loading: false
            });

        }

    }

    _renderMemberChoice() {

        const { record, allowedSelections, totalSelections, fields, memberPackage } = this.state;
        const selectionsRemaining = allowedSelections - totalSelections;

        return (
            <div className="member-choice">

                <h2>We're offering you the option to select a cap or scarf in your 2018 member pack.</h2>
                <p>Choose your preferred {plural('item', 'items', allowedSelections)} now. You will receive more information in October regarding your 2018 Membership and your Easy Pay payment plan rollover.</p>
                <p><strong>{record.name_first}</strong>, your account <strong>{record.acct_id}</strong> is eligible to choose <strong className="member-choice__allowed-selections">{allowedSelections}</strong> {plural('item', 'items', allowedSelections)}{allowedSelections > 1 ? ', one choice for each of your member packs' : ''}.</p>
                <p><strong>Choose between the following options:</strong></p>

                <div className="member-choice__inner member-choice__tiles">
                    {this.state.merchItems.map(merchItem => <MerchTile key={merchItem.field} merchItem={merchItem} merchQty={fields[merchItem.field]} allowedSelections={allowedSelections} totalSelections={totalSelections} onUpdate={this._handleMerchSelection.bind(this)} />)}
                </div>

                <div className="member-choice__inner">

                    { allowedSelections > 1 ?
                        <div className="member-choice__selections-remaining">You have <span>{selectionsRemaining}</span> {plural('choice', 'choices', selectionsRemaining)} remaining</div>
                    : false }

                    <div className="member-choice__notes">
                        <strong>Please note:</strong> If you hold a Legends, Gold or Captain Club membership you will automatically receive both a Cap and Scarf and will not need to make a selection for that membership. You can still select your choice for any other memberships in your account.<br /><strong>Stock is strictly limited so members are encouraged to select early to avoid missing out on preferred items.</strong>
                    </div>

                </div>

            </div>
        );

    }

    _renderAddOns() {

        if(this.state.addOns.length > 0) {

            const { fields } = this.state;

            return (
                <div className="add-ons">

                    <h2>WANT TO ADD MORE VALUE TO YOUR MEMBERSHIP?</h2>
                    <p>Simply select merchandise or add-ons below and we will update your 2018 Membership payments.</p>

                    <div className="add-ons__inner add-ons__tiles">
                        {this.state.addOns.map(addOn => <AddonTile key={addOn.key} addOn={addOn} addonSelections={fields[addOn.field]} addonQty={addOn.options === false ? fields[addOn.field] : fields[addOn.field].length} onUpdate={this._handleAddonSelection.bind(this)} />)}
                    </div>

                </div>
            );

        }

    }

    _handleAddonOptionChange(addonField, addonIndex, value) {
        let fields = Object.assign({}, this.state.fields);
        fields[addonField][addonIndex] = value;
        this.setState(fields: fields);
    }

    _handleDeleteAddonOption(addOn, addonIndex) {
        let fields = Object.assign({}, this.state.fields);

        if(addOn.options === false) {
            this._handleAddonSelection(addOn.field, fields[addOn.field] - 1);
        } else {
            fields[addOn.field].splice(addonIndex, 1);
            this.setState(fields: fields);
        }

    }

    _renderAddonTotal() {
        const { merchItems, addOns, fields } = this.state;
        let addonTotal = 0;

        // merch items
        merchItems.map(merchItem => {
            const merchItemField = fields[merchItem.field];
            addonTotal = addonTotal + (merchItemField * merchItem.price);
        });

        // addons
        addOns.map(addOn => {
            const addonField = fields[addOn.field];
            const addonQty = addOn.options === false ? addonField : addonField.length;
            addonTotal = addonTotal + (addonQty * addOn.price);
        });

        // donation
        addonTotal = addonTotal + parseInt(fields.donation_amount);

        return addonTotal;
    }

    _renderLineItems() {
        const { merchItems, addOns, fields } = this.state;

        let lineItems = [];

        merchItems.map(merchItem => {
            const merchItemField = fields[merchItem.field];

            for(let i=0; i < merchItemField; i++) {
                if(merchItem.price === 0) continue;
                lineItems.push(
                    <div className="line-items__item line-items__item--row" key={`${merchItem.key}.${i}`}>
                        <div className="line-items__item__name">{merchItem.name}</div>
                        <div className="line-items__item__options line-items__item__options--no-option"></div>
                        <div className="line-items__item__delete"></div>
                        <div className="line-items__item__price">{`$${merchItem.price}`}</div>
                    </div>
                );
            }

        });

        addOns.map(addOn => {
            const addonField = fields[addOn.field];
            const crossSVG = (<svg viewBox="0 0 16 16"><path d="M8,10.7L2.8,16L0,13.2L5.3,8L0.1,2.8l2.7-2.7L8,5.3L13.2,0L16,2.8L10.7,8l5.1,5.1l-2.7,2.7L8,10.7z"/></svg>);
            const arrowSVG = (<svg viewBox="0 0 16 16"><polygon points="8,12.8 0,4.8 1.6,3.2 8,9.6 14.4,3.2 16,4.8 "/></svg>);

            if(addOn.options === false) {
                if(addonField > 0) {
                    for(let i=0; i < addonField; i++) {
                        lineItems.push(
                            <div className="line-items__item line-items__item--row" key={`${addOn.key}.${i}`}>
                                <div className="line-items__item__name">{addOn.name}</div>
                                <div className="line-items__item__options line-items__item__options--no-option"></div>
                                <div className="line-items__item__delete"><button onClick={event => this._handleDeleteAddonOption(addOn, i)}>{crossSVG}</button></div>
                                <div className="line-items__item__price">{`$${addOn.price}`}</div>
                            </div>
                        );
                    }
                }
            } else {
                if(addonField.length > 0) {
                    for(let i=0; i < addonField.length; i++) {
                        lineItems.push(
                            <div className="line-items__item line-items__item--row line-items__item--has-option" key={`${addOn.key}.${i}`}>
                                <div className="line-items__item__name">{addOn.name}</div>
                                <div className="line-items__item__options line-items__item__options--has-option">
                                    <div className="line-items__item__options__select">
                                        <select name={`${addOn.key}.${i}`} value={addonField[i]} onChange={event => this._handleAddonOptionChange(addOn.field, i, event.target.value)}>
                                            <option value="">{addOn.optionsDropdownLabel}</option>
                                            {addOn.options.map(option => <option key={option} value={option}>{option}</option>)}
                                        </select>
                                        {arrowSVG}
                                    </div>
                                </div>
                                <div className="line-items__item__delete"><button onClick={event => this._handleDeleteAddonOption(addOn, i)}>{crossSVG}</button></div>
                                <div className="line-items__item__price">{`$${addOn.price}`}</div>
                            </div>
                        );
                    }
                }
            }

        });

        if(lineItems.length > 0) {

            lineItems.unshift(
                <div className="line-items__item line-items__item--header" key="lineItemsHeader">
                    <div className="line-items__item__name">Item</div>
                    <div className="line-items__item__options line-items__item__options--no-option"></div>
                    <div className="line-items__item__delete"></div>
                    <div className="line-items__item__price">Amount</div>
                </div>
            );

        }

        return lineItems;
    }

    _handleDonationToggle(value) {
        this._handleAddonSelection('donation', value);
        if(value === 'no') {
            this._handleAddonSelection('donation_amount', 0);
        }
    }

    _handleDonationAmount(event) {
        const donationAmount = event.target.value.length > 0 && !isNaN(event.target.value) ? parseInt(event.target.value) : 0;
        const hasDonation = donationAmount > 0 ? 'yes' : 'no';
        this._handleAddonSelection('donation', hasDonation);
        this._handleAddonSelection('donation_amount', donationAmount);
    }

    render() {
        const { record, fields } = this.state;

        return (
            <div className="body-wrap">

                <header className="header">
                    <div className="header__bg"></div>
                </header>

                { this.state.submitted === true ? (
                    <div className="main">
                        <div className="thanks">
                            <div className="thanks__inner">
                                <div className="thanks__title">Thank You {record.name_first}!</div>
                                <p>For making your member's choice selection.</p>
                                <p>You will receive further communication in October regarding the details and pricing of your 2018 membership.<br />If you have any queries please contact the Club on (08) 8440 6690 or via <a href="mailto:membership@afc.com.au">membership@afc.com.au</a></p>
                            </div>
                        </div>
                    </div>
                ) : (
                    <form className="main" onSubmit={e => e.preventDefault()}>

                        {this._renderMemberChoice()}
                        {this._renderAddOns()}

                        <div className="line-items">
                            <div className="line-items__inner">
                                {this._renderLineItems()}
                                <div className="line-items__donation">
                                    <Checkbox class="line-items__donation__checkbox" checked={fields.donation === 'yes'} onClick={() => this._handleDonationToggle(fields.donation === 'yes' ? 'no' : 'yes')} />
                                    <div className="line-items__donation__label">I would like to make a tax deductible donation to the Crows Children's Foundation. Your generous donation will help the Foundation distribute funds for children in need across Australia, through health, education and welfare programs.</div>
                                    <div className="line-items__donation__amount">
                                        <span>$</span>
                                        <input type="text" value={fields.donation_amount} onChange={this._handleDonationAmount.bind(this)} onBlur={this._handleDonationAmount.bind(this)} />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="total-addons">
                            <div className="total-addons__title">Your total add-on cost for 2018 will be:</div>
                            <div className="total-addons__amount">${this._renderAddonTotal()}</div>
                        </div>

                        <div className="form__submit">
                            <div className="form__submit__inner">
                                <div className="form__submit__accept">
                                    <Checkbox class="form__submit__accept__checkbox" checked={fields.accept === 'yes'} onClick={this._handleAcceptChange} />
                                    I understand that the Adelaide Crows will update my 2018 membership package and payments to reflect the choices I have made with my above selections and will adjust the payments to be deducted from credit card on file. If I have any queries I can contact the Club on (08) 8440 6690 or via <a href="mailto:membership@afc.com.au">membership@afc.com.au</a>
                                </div>
                                <button className="form__submit__button" onClick={this._handleSubmit} disabled={this.state.loading}>{ this.state.loading ? 'Loading...' : 'Submit' }</button>
                            </div>
                        </div>

                    </form>
                )}

                <footer className="footer">

                    <div className="footer__disclaimer">
                        <div className="footer__inner">
                            <p>*Full entitlements and terms and conditions available at <a href="http://www.afc.com.au/" target="_blank">19thman.com.au</a>.<br />Membership packs will be sent in December, following Membership payments commencing in October. Merchandise and membership add-ons may be sent separately.</p>
                        </div>
                    </div>

                    <div className="footer__links">
                        <div className="footer__inner">
                            <div className="disclaimer">For any queries please contact Member Services at membership@afc.com.au or (08) 8440 6690.</div>
                            <div className="links">
                                <a href="http://www.afc.com.au/privacy" target="_blank">Privacy Policy</a><span>|</span><br />
                                <a href="https://www.19thman.com.au/terms-conditions-7" target="_blank">Terms &amp; Conditions</a>
                            </div>
                            <br />
                            &copy; Adelaide Football Club 2017
                        </div>
                    </div>

                    <div className="footer__sponsors">
                        <div className="footer__sponsors__lockup">
                            <a href="http://www.afc.com.au/" target="_blank">19thman.com.au</a>
                        </div>
                    </div>

                </footer>

                <Modal visible={this.state.errors.length > 0} button={'Ok'} buttonAction={() => { this.setState({ errors: [] }) }} buttonDisabled={false} ref={(modal) => { this._modal = modal; }}>
                    {this.state.errors.map(error => <p>{error}</p>)}
                </Modal>

            </div>
        );
    }

}

export default App;
