@extends($campaign->view('main'))

@section('content')

<header class="container-fluid header">

    <div class="container header-top">
        <img src="{{ $campaign->asset('images/thank-you.jpg') }}" />
    </div>

</header>

<div class="container-fluid main-wrap">

    <div class="container">

    	<div class="row">

    		<div class="main col-xs-12">

                <div class="main-inner">

                    <div class="welcome thanks">
                        <div class="text">THANK YOU FOR CONFIRMING YOUR DETAILS. <strong>YOU ARE NOW IN THE DRAW</strong> TO WIN 1 OF 10 DOUBLE PASSES TO THE GREAT NORTHERN DECK OR 1 OF 10 SIGNED 2015 RAIDERS AUCKLAND NINES JERSEYS.</div>
                    </div>

                </div>

                <div class="thanks-callout">
                    <div class="thanks-callout__top">
                        <div class="image"><img src="{{ $campaign->asset('images/jersey-special.png') }}"></div>
                        <div class="text">
                            <p><strong>AVAILABLE NOW!</strong></p>
                            <p>RICKY STUART FOUNDATION TO FEATURE ON CANBERRA RAIDERS JERSEY</p>
                            <p>
                                <a href="http://raidersshop.com.au/products/2016-raiders-ricky-stuart-foundation-jersey-adult?variant=20385135683" class="visit-the-shop" target="_blank">
                                    <img src="{{ $campaign->asset('images/visit-the-shop.png') }}">
                                </a>
                            </p>
                        </div>
                    </div>
                    <div class="thanks-callout__bottom">Raiders Head Coach and Chairman of the Ricky Stuart Foundation, Ricky Stuart, said the opportunity to promote the Ricky Stuart Foundation on the Raiders NRL jersey was made possible through the generosity of the Club’s major sponsor Huawei.</div>
                </div>

    		</div>

    	</div>

    </div>

</div>

@endsection
