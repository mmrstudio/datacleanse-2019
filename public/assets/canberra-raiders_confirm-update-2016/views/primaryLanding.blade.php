@extends($campaign->view('main'))

@section('content')

<header class="container-fluid header">

    <div class="container header-top">
        <img src="{{ $campaign->asset('images/landing-header.jpg') }}" />
    </div>

</header>

<div class="container-fluid main-wrap">

    <div class="container">

    	<div class="row">

    		<div class="main col-xs-12">

                <div class="main-inner">

                    <div class="welcome">
                        <div class="image hidden-xs"><img src="{{ $campaign->asset('images/jersey.png') }}"></div>
                        <div class="text"><strong>WIN 1 OF 10</strong> DOUBLE PASSES TO THE GREAT NORTHERN DECK OR 1 OF 10 SIGNED 2015 RAIDERS AUCKLAND NINES JERSEYS.</div>
                    </div>

                    <form action="{{ $process_url }}" method="post" class="entry-form">

                        <div class="form-wrap">

                            @if (count($errors))
                            <div class="error-messages">
                                <p><strong>Please check the following errors:</strong></p>
                                <ul>
                                    @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif

                            <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                                <label class="control-label" for="first_name">First Name <span class="required">*</span></label>
                                <div class="controls">
                                    <input id="first_name" name="first_name" type="text" placeholder="Enter first name..." class="form-control" value="{{ old('first_name', $record->first_name) }}" {{ $entry_type == 'update' ? 'disabled' : '' }}>
                                    @if($entry_type == 'update')
                                    <input type="hidden" name="first_name" value="{{ $record->first_name }}">
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                                <label class="control-label" for="last_name">Last Name <span class="required">*</span></label>
                                <div class="controls">
                                    <input id="last_name" name="last_name" type="text" placeholder="Enter last name..." class="form-control" value="{{ old('last_name', $record->last_name) }}" {{ $entry_type == 'update' ? 'disabled' : '' }}>
                                    @if($entry_type == 'update')
                                    <input type="hidden" name="last_name" value="{{ $record->last_name }}">
                                    @endif
                                </div>
                            </div>

                            <div class="form-group width-half left {{ $errors->has('gender') ? 'has-error' : '' }}">
                                <label class="control-label" for="gender">Gender <span class="required">*</span></label>
                                <div class="controls">
                                    {!! Form::select('gender', select('Select gender...', genders()), old('gender', $record->gender), ['id'=>'gender', 'class'=>'form-control select']) !!}
                                </div>
                            </div>

                            <div class="form-group width-half right {{ $errors->has('date_of_birth') ? 'has-error' : '' }}">
                                <label class="control-label" for="date_of_birth">DOB</label>
                                <div class="controls">
                                    <?php if(! $record->date_of_birth) : ?>
                                    <input id="date_of_birth" name="date_of_birth" type="text" placeholder="Please provide your DOB" class="form-control date-picker" value="{{ old('date_of_birth', $record->date_of_birth) }}" data-date-start-view="decade" data-date-format="dd/mm/yyyy" data-date="{{ old('date_of_birth', $record->date_of_birth) }}">
                                    <?php else : ?>
                                    <input type="text" class="form-control" value="Not required" disabled>
                                    <input type="hidden" name="date_of_birth" value="{{ $record->date_of_birth }}">
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                <label class="control-label" for="email">Email <span class="required">*</span></label>
                                <div class="controls">
                                    <input id="email" name="email" type="email" placeholder="Enter email address.." class="form-control" value="{{ old('email', $record->email) }}">
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('billing_address') ? 'has-error' : '' }}">
                                <label class="control-label" for="billing_address">Address<span class="required">*</span></label>
                                <div class="controls">
                                    <input id="billing_address" name="billing_address" type="text" placeholder="Enter address..." class="form-control" value="{{ old('billing_address', $record->billing_address) }}">
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('billing_suburb_city') ? 'has-error' : '' }}" id="suburbField">
                                <label class="control-label" for="billing_suburb_city">Suburb <span class="required">*</span></label>
                                <div class="controls">
                                    <input id="billing_suburb_city" name="billing_suburb_city" type="text" placeholder="Enter suburb..." class="form-control" value="{{ old('billing_suburb_city', $record->billing_suburb_city) }}">
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('billing_country') ? 'has-error' : '' }}" id="countryField">
                                <label class="control-label" for="billing_country">Country <span class="required">*</span></label>
                                <div class="controls">
                                    {!! Form::select('billing_country', select('Select country...', countries()), old('billing_country', ($record->billing_country ? $record->billing_country : 'AUSTRALIA')), ['id'=>'billing_country', 'class'=>'form-control select']) !!}
                                </div>
                            </div>

                            <div class="form-group width-two-thirds left state-field {{ $errors->has('billing_state_province') ? 'has-error' : '' }}" id="stateField" data-field-name="billing_state_province">
                                <label class="control-label">State <span class="required">*</span></label>
                                <div class="controls">
                                    <div class="state-field-au">
                                        {!! Form::select('state', select('Select state...', australian_states()), old('billing_state_province', $record->billing_state_province), ['class'=>'form-control select']) !!}
                                    </div>
                                    <div class="state-field-nz">
                                        {!! Form::select('state', select('Select...', nz_states()), old('billing_state_province', $record->billing_state_province), ['class'=>'form-control select']) !!}
                                    </div>
                                    <div class="state-field-other">
                                        <input name="state" type="text" placeholder="Enter state/province..." class="form-control" value="{{ old('billing_state_province', $record->billing_state_province) }}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group width-third {{ $errors->has('billing_zip_postal_code') ? 'has-error' : '' }}" id="postcodeField">
                                <label class="control-label" for="billing_zip_postal_code">Postcode <span class="required">*</span></label>
                                <div class="controls">
                                    <input id="billing_zip_postal_code" name="billing_zip_postal_code" type="text" placeholder="Enter postcode..." class="form-control" value="{{ old('billing_zip_postal_code', $record->billing_zip_postal_code) }}" maxlength="4">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label" for="home_phone">Home Phone</label>
                                <div class="controls">
                                    <input id="home_phone" name="home_phone" type="tel" placeholder="Enter home phone..." class="form-control" value="{{ old('home_phone', $record->home_phone) }}">
                                </div>
                            </div>

                            <div class="form-group">
                               <label class="control-label" for="work_phone">Work Phone</label>
                               <div class="controls">
                                   <input id="work_phone" name="work_phone" type="tel" placeholder="Enter work phone..." class="form-control" value="{{ old('work_phone', $record->work_phone) }}">
                               </div>
                           </div>

                           <div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
                               <label class="control-label" for="mobile">Mobile <span class="required">*</span></label>
                               <div class="controls">
                                   <input id="mobile" name="mobile" type="tel" placeholder="Enter mobile number..." class="form-control" value="{{ old('mobile', $record->mobile) }}">
                               </div>
                           </div>

                            <div class="submit">

                                @foreach($hidden_fields as $name=>$value)
                                <input type="hidden" name="{{ $name }}" value="{{ $value }}">
                                @endforeach

                                <button class="button submit-button" type="submit">Update</button>

                            </div>

                        </div>

                    </form>

                </div>

    		</div>

    	</div>

    </div>

</div>

@endsection
