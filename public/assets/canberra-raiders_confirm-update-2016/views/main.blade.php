<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">

    <head>

    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    	<title>{{ $campaign->campaignTitle() }}</title>

    	<link href="{{ $campaign->asset('css/campaign.css') }}" rel="stylesheet">

        <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
        <link rel="icon" href="http://www.raiders.com.au/favicon.ico" />

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-33851616-15', 'auto');
          ga('send', 'pageview');

        </script>

        <script src="{{ $campaign->asset('js/modernizr.js') }}"></script>

    </head>

    <body class="">

        @yield('content')

        <footer class="container-fluid footer">

            <div class="container footer-lockup">

                <div class="footer-wrap">
                    <div class="social-icons">
                        <a href="http://www.raiders.com.au/social/facebook.html" target="_blank"><img src="{{ $campaign->asset('images/facebook.png') }}"></a>
                        <a href="http://www.raiders.com.au/social/twitter.html" target="_blank"><img src="{{ $campaign->asset('images/twitter.png') }}"></a>
                        <a href="http://www.raiders.com.au/social/instagram.html" target="_blank"><img src="{{ $campaign->asset('images/instagram.png') }}"></a>
                    </div>
                    <a href="http://weareraiders.com.au/" class="we-are-raiders" target="_blank"><img src="{{ $campaign->asset('images/we-are-raiders.png') }}"></a>
                </div>

            </div>

            <div class="container footer-text">
                <div class="row">
                    <div class="col-xs-12">

                        <div class="footer-wrap">
                            <p><strong>YOUR PRIVACY IS IMPORTANT TO US</strong></p>
                            <p>All personal information you provide will be used by the Club for promotional and direct marketing purposes which may include our sponsors or third parties. By providing your personal information, you agree to such use by the Club. </p>
                            <p class="links"><a href="http://weareraiders.com.au/terms-conditions/" target="_blank">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a href="http://datacleanse.com.au" target="_blank">Product of Datacleanse</a></p>
                        </div>

                    </div>
                </div>

            </div>

        </footer>

        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    	<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
        <script src="{{ $campaign->asset('js/campaign.js') }}"></script>

    </body>

</html>
