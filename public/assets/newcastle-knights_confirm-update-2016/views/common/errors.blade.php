
@if (count($errors))
<div class="form__errors">
    <p><strong>Please check the following errors:</strong></p>
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
