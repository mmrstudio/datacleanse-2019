
    <div class="footer__logo">
        <img src="{{ $campaign->asset('images/logo.png') }}">
    </div>

    <p>Newcastle Knights | 1300 465 644 | <a href="mailto:membership@newcastleknights.com.au">membership@newcastleknights.com.au</a></p>

    <ul class="footer__links">
        <li><a href="http://www.newcastleknights.com.au/" target="_blank">Newcastle Knights</a></li>
        <li><a href="https://knightshub.com.au/privacy-policy" target="_blank">Privacy Policy</a></li>
        <li><a href="https://knightshub.com.au/terms-and-conditions" target="_blank">Terms &amp; Conditions</a></li>
        <li><a href="http://datacleanse.com.au" target="_blank">Product of Datacleanse</a></li>
    </ul>
