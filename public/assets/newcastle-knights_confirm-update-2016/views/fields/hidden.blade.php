<?php
    if(! isset($fields[$key])) return '';
    $field = $fields[$key];
    $value = isset($value) ? $value : old($field->name, $record->{$field->name});
?>

<input id="{{ $field->name }}" name="{{ $field->name }}" type="hidden" value="{{ $value }}">
