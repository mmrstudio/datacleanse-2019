@extends($campaign->view('main'))

@section('content')

<header class="header">

    @include($campaign->view('common.header'))

    <div class="header__landing-title">
        <div class="header__landing-title__inner"></div>
    </div>

    <div class="header__players">
        <div class="header__players__inner"></div>
    </div>

</header>

<div class="main">

    <form action="{{ $process_url }}" method="post" class="form">

        <div class="form__inner">

            @include($campaign->view('common.errors'))

            <div class="form__intro">
                <div class="form__intro__logo"></div>
                @if($record->first_name != '')
                <div class="form__intro__greeting">Dear {{ $record->first_name }},</div>
                @endif
                <div class="form__intro__text">
                    Are your details correct?<br>
                    Confirm or update your personal details below and you could <strong>WIN</strong> a <strong>DOUBLE PASS</strong> to the <strong>2016 NRL GRAND FINAL WORTH $450</strong>.
                </div>
            </div>

            <div class="form__title">
                <h1>Your Details</h1>
            </div>

            @include($campaign->view('fields.text'), [
                'key' => 'first_name',
                'props' => ['readonly'],
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'last_name',
                'props' => ['readonly'],
            ])

            @if($record->gender !== 'Supplied')
                @include($campaign->view('fields.dropdown'), [
                    'key' => 'gender',
                ])
            @endif

            @include($campaign->view('fields.address'), [
                'keys' => ['street_addr_1', 'street_addr_2'],
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'city',
                'id' => 'suburbField',
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'postcode',
                'id' => 'postcodeField',
            ])

            @include($campaign->view('fields.country'), [
                'key' => 'country',
            ])

            @include($campaign->view('fields.state'), [
                'key' => 'state',
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'phone_mobile',
            ])

            @include($campaign->view('fields.email'), [
                'key' => 'email_addr',
            ])

            @include($campaign->view('fields.dob'), [
                'key' => 'birth_date',
                'not_required' => false,
            ])

            @if($record->data_segment == 'Active')
                @include($campaign->view('fields.dropdown'), [
                    'key' => 'years_membership',
                ])
            @endif

            <!-- {{ $record->data_segment }} -->

            <div class="form__submit">

                @foreach($hidden_fields as $name=>$value)
                <input type="hidden" name="{{ $name }}" value="{{ $value }}">
                @endforeach

                <button class="form__submit__button" type="submit">Update</button>

            </div>

        </div>

    </form>

</div>

<footer class="footer">

    @include($campaign->view('common.footer'))

</footer>

@endsection
