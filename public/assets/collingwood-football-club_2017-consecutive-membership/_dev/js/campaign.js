
    @@include('plugins/datepicker.js')
    @@include('plugins/datepicker.en.js')
    //@@include('plugins/moment.js')

    $(document).ready(function() {

        // digits
        var digitsField = $('#digitsField');
        var leftDigit = $('#leftDigit');
        var rightDigit = $('#rightDigit');

        function digitChange(e) {
            var digitValue = digitsField.val();

            if(e.which === 8 || e.which === 46) {
                digitsField.val('');
                digitValue = 0;
            }

            if(digitValue > 99) { return e.preventDefault(); }

            var digitStr = "" + digitValue
            var digitPad = "00"
            var digitPadded = digitPad.substring(0, digitPad.length - digitStr.length) + digitStr;
            var digitSplit = digitPadded.split('');

            var leftDigitVal = parseInt(digitSplit[0]) > 0 ? digitSplit[0] : '';
            var rightDigitVal = digitSplit[1];

            if(digitValue == 0 || digitValue == '0') {
                rightDigitVal = '';
            }

            leftDigit.attr('data-digit', leftDigitVal);
            rightDigit.attr('data-digit', rightDigitVal);
        }

        digitsField.on('keyup', digitChange);
        digitChange(false);

        document.getElementById('digitsField').oninput = function () {

            if (this.value.length > 2) {
                this.value = this.value.slice(-1);
            }

            if(this.value == '0' || this.value == 0) {
                this.value = '';
            }

        }

        // Country/State Fields
        var countryField = $("#countryField select");
        var stateField = $("#stateField");
        var postcodeField = $("#postcodeField");
        var suburbField = $("#suburbField");

        var auStateFieldWrap = stateField.find('.state-field-au');
        var nzStateFieldWrap = stateField.find('.state-field-nz');
        var auStateField = auStateFieldWrap.find('select');
        var nzStateField = nzStateFieldWrap.find('select');
        var stateOtherWrap = stateField.find('.state-field-other');
        var stateOtherField = stateOtherWrap.find('input');
        var stateRequired = stateField.find('.required');
        var stateError = stateField.hasClass('has-error');
        var postcodeRequired = postcodeField.find('.required');
        var stateFieldName = stateField.data('field-name');
        var suburbRequired = suburbField.find('.required');

        // save original state value
        var auOriginalState = auStateField.val();
        var nzOriginalState = nzStateField.val();

        function checkCountryField() {

            var countryVal = countryField.val();

            if(countryVal == 'AUSTRALIA') {

                auStateFieldWrap.show();
                auStateField.attr('name', stateFieldName);

                nzStateFieldWrap.hide();
                nzStateField.attr('name', '');

                stateOtherWrap.hide();
                stateOtherField.attr('name', '');

                stateRequired.show();
                postcodeRequired.show();
                suburbRequired.show();

                if(stateError) {
                    stateField.removeClass('has-error');
                }

            } else if(countryVal == 'NEW ZEALAND') {

                auStateFieldWrap.hide();
                auStateField.attr('name', '');

                nzStateFieldWrap.show();
                nzStateField.attr('name', stateFieldName);

                stateOtherWrap.hide();
                stateOtherField.attr('name', '');

                stateRequired.show();
                postcodeRequired.show();
                suburbRequired.show();

                if(stateError) {
                    stateField.removeClass('has-error');
                }

            } else {

                auStateFieldWrap.hide();
                nzStateFieldWrap.hide();

                stateOtherWrap.hide();

                stateOtherWrap.show();
                stateOtherField.attr('name', stateFieldName);

                stateRequired.hide();
                postcodeRequired.hide();
                suburbRequired.hide();

                if(stateError) {
                    stateField.addClass('has-error');
                }

            }

        }

        setTimeout(function() {
            checkCountryField();
        }, 500);

        countryField.on('change', checkCountryField);

        //$(".form__control--select").select2({ minimumResultsForSearch: Infinity });

        function setSelectLabel(field, labelEl) {
            var value = field.val();
            var label = field.find('option:selected').text();
            if(value.length > 0) {
                labelEl.html(label).removeClass('form__select-label--placeholder');
            } else {
                labelEl.html(field.data('placeholder')).addClass('form__select-label--placeholder');
            }
        }

        $(".form__control--select").each(function() {
            var selectField = $(this);
            var selectFieldLabel = $('<div class="form__select-label"></div>');

            selectFieldLabel.insertBefore(selectField);
            setSelectLabel(selectField, selectFieldLabel);

            selectField.on('change', function() {
                setSelectLabel(selectField, selectFieldLabel);
            });

        });

        var formGroupSelectOther = $('.form__group--select--other');
        formGroupSelectOther.each(function() {
            var group = $(this);
            var selectField = group.find('select');
            var selectOther = group.find('.form__control--select--other');

            selectOther.hide();

            selectField.on('change', function() {
                var selectVal = $(this).val();

                if(selectVal === 'other') {
                    selectOther.show();
                } else {
                    selectOther.hide();
                }

            });

        });

        //$('.date-picker').datepicker();

    });
