@extends($campaign->view('main'))

@section('content')

<div class="thanks-header">
    <div class="thanks-header__banner">
        <div class="thanks-header__banner__inner"></div>
    </div>
</div>

<div class="main">

    <div class="main__inner">

        <div class="thanks">
            <div class="thanks__title">For your loyal years of membership with the Collingwood Football Club.</div>
            <div class="thanks__byline">We look forward to standing side by side with you again in 2017.</div>
        </div>

    </div>

</div>

<footer class="footer footer--thanks">

    <div class="footer__tagline">
        <div class="footer__tagline__inner"></div>
    </div>

    @include($campaign->view('common.footer'))

</footer>

@endsection
