@extends($campaign->view('main'))

@section('content')

<header class="header">

    @include($campaign->view('common.header'))

</header>

<div class="main">

    <div class="main__inner">

        @include($campaign->view('common.errors'))

        <div class="sidebar">

            <div class="sidebar__intro">Hi {{ $record->name_first }}, our records indicate that in 2017 you will have been a member for {{ $record->years_consecutive_2017 }} consecutive years.</div>
            <div class="sidebar__info">If this number is inaccurate, please enter your correct number of consecutive years as a Collingwood member in the number field on the right, and click the update button to submit.*</div>
            <div class="sidebar__details">
                <div class="sidebar__details__title">Your Details</div>
                {{ $record->name_first }} {{ $record->name }}<br>
                @if($record->email_addr)
                {{ $record->email_addr }}<br>
                @endif
                @if($record->phone_cell)
                {{ $record->phone_cell }}<br>
                @endif
                {{ $record->street_addr_1 }}<br>
                {{ $record->street_addr_2 ? $record->street_addr_2 . '<br>' : '' }}
                {{ $record->city }}, {{ $record->state }} {{ $record->zip }}
            </div>
            <div class="sidebar__disclaimer">*If your years of consecutive membership is more than 5 years from what we have on record, Collingwood Membership Services will contact you to request a statutory declaration before updating our records. </div>

        </div>

        <form action="{{ $process_url }}" method="post" class="form">

            <div class="form__inner">

                <div class="consecutive-years">
                    <div class="consecutive-years__title">Consecutive<br><span>Years</span></div>
                    <div class="consecutive-years__field">
                        <input id="digitsField" name="years_consecutive_confirmed" type="number" class="consecutive-years__field__input" value="{{ old('years_consecutive_confirmed', (isset($record->years_consecutive_confirmed) ? $record->years_consecutive_confirmed : $record->years_consecutive_2017)) }}" min="0" max="99" pattern="\d*">
                        <div class="consecutive-years__field__display">
                            <div class="consecutive-years__field__display__digit consecutive-years__field__display__digit--left">
                                <div class="consecutive-years__field__display__digit__inner" data-digit="" id="leftDigit"></div>
                            </div>
                            <div class="consecutive-years__field__display__digit consecutive-years__field__display__digit--right">
                                <div class="consecutive-years__field__display__digit__inner" data-digit="" id="rightDigit"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form__submit">

                    @foreach($hidden_fields as $name=>$value)
                    <input type="hidden" name="{{ $name }}" value="{{ $value }}">
                    @endforeach

                    <button class="form__submit__button" type="submit">Update</button>

                </div>

            </div>

        </form>

    </div>

</div>

<footer class="footer">

    @include($campaign->view('common.footer'))

</footer>

@endsection
