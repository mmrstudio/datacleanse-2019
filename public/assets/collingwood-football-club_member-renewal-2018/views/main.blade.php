<?php

    function hasPackage($package, $package_name) {
        return strpos($package_name, $package) !== false;
    }

    //print_r($record->toArray()); exit;

    $packages = [
        'Captains' => ['description' => 'Enjoy a reserved seat for all of Collingwood’s Home games at the MCG and Etihad Stadium, as well as Social Club access.'],
        'Club5' => ['description' => 'Join our most prestigious group of members by committing to the club through to 2022. Five years of exclusive functions & first-rate service await, so become a Club 5 member now!'],
        'Legends' => ['description' => 'Our premium package, a Legends membership includes a reserved seat for all Home & Away games at the MCG & Etihad Stadium + access to the MCG’s Legend’s Lounge & Social Club.'],
        'MCGLegends' => ['description' => 'Join us at the mighty ‘G with your own reserved seat for every game at the home of football. This package also includes Legend Lounge & Social Club access.'],
        'LegendsBay' => ['description' => 'A flexible take on our premium membership package. Choose to sit in one of our reserved Legends Bays for the ability to bring along guests and alternate views for MCG Home Games. For all other games you’ll be watching from the comfort of your own reserved seat. Includes Legends Bay & Social Club access.'],
        'RSHA' => ['description' => 'Don’t miss a moment of the action. Lock in your guaranteed reserved seat for all Home & Away games played at the MCG & Etihad Stadium in 2018. '],
        'RSH' => ['description' => 'Enjoy peace of mind on Game-Day by securing your own guaranteed reserved seat for all Home games played at the MCG & Etihad Stadium in 2018. '],
        'GAHA' => ['description' => 'Be there for every Jamie special & Howe screamer with General Admission access to all Home & Away games at the MCG & Etihad Stadium in 2018.'],
        'GAH' => ['description' => 'Stand side-by-side with the Magpie Army for all Home games* played at the MCG & Etihad Stadium with this membership’s General Admission access. *Excludes ANZAC Day.'],
        'Country' => ['description' => 'Do you live 120km outside of Melbourne CBD? Don’t let that stop you from getting in on the action! A Country membership gives you General Admission access to your choice of five games from our total of eleven Home games at the MCG & Etihad Stadium in 2018.'],
        'Interstate' => ['description' => 'Secure a reserved seat amongst your fellow Magpie members when we play in your state this year, and get down to Melbourne to watch the Pies with access to five Home games at the MCG or Etihad Stadium. '],
        'InterstateMagpieStars' => ['description' => 'For our next generation of interstate stars! This package includes access to exclusive footy clinics & awesome Magpie-merch! Available for kids aged 5 to 14.'],
        '4GameGA' => ['description' => 'Get your footy fix with a 4-Game membership. Pick any four Home games* at the MCG or Etihad Stadium to attend with your General Admission access. *Excludes ANZAC Day'],
        'AnzacGA' => ['description' => 'Secure your spot at this year’s ANZAC Day game. Includes a guaranteed reserved seat on ANZAC Day + General Admission access to your choice of two Home games at the MCG or Etihad Stadium in 2018.'],
        'AnzacRS' => ['description' => 'Be there for the biggest game on the calendar with an ANZAC Day membership. Includes a guaranteed reserved seat on ANZAC Day + a reserved seat at two predetermined Home games at the MCG or Etihad Stadium in 2018. '],
        'MagpieInsider' => ['description' => 'Can’t get to games this year? Stay connected & continue your Years of Service with our Magpie Insider package. With exclusive digital content & your 2018 membership pack you’ll feel right in the thick of the footy action!'],
        'MagpieInsiderDigital' => ['description' => 'Catch all the Magpies action in real time from anywhere in Australia or New Zealand with an AFL Live Pass + exclusive member content and your 2018 membership pack.'],
        'International' => ['description' => 'Living overseas? Stay connected to the club with our International membership! Enjoy digital member content as well as General Admission access to three Home games. With your 2018 membership pack items you can fly the Magpie colours high no matter which country you’re in!'],
        'InternationalGlobalInsider' => ['description' => 'Whether you’re living overseas or travelling abroad, you can watch all the Magpies action in real time with an AFL Live Pass as part of our Global Insider package. With your 2018 membership pack & digital member content included you’ll feel right at home wherever you are in the world!'],
        'MagpieNestBaby' => ['description' => 'You don’t want a Carlton supporter in your family, do you? Make the newest addition to your family the newest addition to ours!'],
        'MagpieNestToddler' => ['description' => 'Loyalty begins at a young age, so why not give your little Magpie their very own membership? Packed with items that’ll keep those pesky non-Collingwood family members at bay.'],
        'MagpiePet' => ['description' => 'Some members of the family won’t have much use for a human-sized cap, so make sure everyone is in black & white this year with a Magpie Pet Membership!'],
        'Social' => ['description' => 'Experience the footy in style with a Social Club upgrade. Enjoy access to the Dean Jones Bar & Lindsay Hassett Room at the MCG, as well as the Victory Room at Etihad Stadium – not to mention your own Social Club Medallion. Guest passes available on game-day for $10'],
        'MagpieStars' => ['description' => 'For our next generation of stars! Upgrade any junior Season Ticket membership with the Magpie Stars Add-On to gain access to exclusive footy clinics & awesome Magpie-merch! Available for kids aged 5 to 14.'],
        'AFLW' => ['description' => 'Footy action starts in summer. Get your gear on & be there to support the team for what’s sure to be an exciting season.'],
    ];

    $packageKey = trim($record->package);

    if(strlen($packageKey) > 0 && $packageKey != 'NA') {
        $memberPackage = $packages[$record->package];
        $memberPackage['upfront'] = '$' . number_format($record->total_purchase, 2);
        $memberPackage['monthly'] = '$' . number_format(($record->total_purchase / 12), 2);
    } else {
        $memberPackage = false;
    }

    $addons = [

        'Copeland' => [
            'key' => 'Copeland',
            'class' => 'copeland',
            'name' => 'Copeland',
            'description' => 'Secure your spot at Collingwood’s night-of-nights by including the cost of the ticket in your monthly membership payments.',
            'price' => false,
            'monthly' => 20.83,
            'upfront' => 250,
            'options' => false,
            'special' => false,
        ],
        'FD' => [
            'key' => 'FD',
            'class' => 'fd',
            'name' => 'Foundation Donation',
            'description' => 'Support the club’s mission to unite & inspire people through the power of sport. With your help we can continue our proud history of being a force for change within the community.',
            'price' => false,
            'monthly' => false,
            'upfront' => false,
            'options' => ['$10', '$20', '$50', '$100'],
            'optionLabel' => 'Select amount...',
        ],
        'SC' => [
            'key' => 'SC',
            'class' => 'sc',
            'name' => 'Social Club',
            'description' => 'Experience the footy in style with a Social Club upgrade. Enjoy access to the Dean Jones Bar & Lindsay Hassett Room at the MCG, as well as the Victory Room at Etihad Stadium – not to mention your own Social Club Medallion. Guest passes available on game-day for $10',
            'price' => false,
            'monthly' => ['adult' => 8.33, 'concession' => 8.33, 'junior' => 3.75, 'family' => 23.75],
            'upfront' => ['adult' => 100, 'concession' => 100, 'junior' => 42, 'family' => 285],
            'options' => false,
            'optionLabel' => false,
        ],
        'CS' => [
            'key' => 'CS',
            'class' => 'cs',
            'name' => 'Cheer Squad',
            'description' => 'Do you have what it takes to be a crowd leader? Join the most passionate group in the Magpie Army (behind the goals) in 2018 as part of our Official Collingwood Cheer Squad. ',
            'price' => false,
            'monthly' => ['adult' => 6.67, 'concession' => 5.83, 'junior' => 5, 'family' => 19.17],
            'upfront' => ['adult' => 80, 'concession' => 70, 'junior' => 60, 'family' => 230],
            'options' => false,
            'optionLabel' => false,
        ],
        'MS' => [
            'key' => 'MS',
            'class' => 'ms',
            'name' => 'Magpie Stars',
            'description' => 'For our next generation of stars! Upgrade any junior Season Ticket membership with the Magpie Stars Add-On to gain access to exclusive footy clinics & awesome Magpie-merch! Available for kids aged 5 to 14.',
            'price' => false,
            'monthly' => ['adult' => 6.67, 'concession' => 5.83, 'junior' => 5, 'family' => 19.17],
            'upfront' => ['adult' => 80, 'concession' => 70, 'junior' => 60, 'family' => 230],
            'options' => false,
            'optionLabel' => false,
        ],
        'AFLW' => [
            'key' => 'AFLW',
            'class' => 'aflw',
            'name' => 'AFLW Membership',
            'description' => 'Footy action starts in summer. Get your gear on & be there to support the team for what’s sure to be an exciting season.',
            'price' => false,
            'monthly' => 5,
            'upfront' => 60,
            'options' => false,
            'special' => false,
        ],
        'Netball' => [
            'key' => 'Netball',
            'class' => 'netball',
            'name' => 'Netball Membership',
            'description' => 'For a different breed of Collingwood experience check out the thrill of Magpies netball in 2018. You’ll receive General Admission access to three Home games + awesome Magpies gear.',
            'price' => false,
            'monthly' => 3.33,
            'upfront' => 40,
            'options' => false,
            'special' => false,
        ],

    ];

    $member_addons = [];

    $recommendation1Key = trim($record->recommendation_1);
    $recommendation2Key = trim($record->recommendation_2);

    if(strlen($recommendation1Key) > 0 && isset($addons[$recommendation1Key])) {
        $member_addons[] = $addons[$recommendation1Key];
    }

    if(strlen($recommendation2Key) > 0 && isset($addons[$recommendation2Key])) {
        $member_addons[] = $addons[$recommendation2Key];
    }

    //$member_addons = [$addons['Copeland'], $addons['FD']];

    if(isset($member_addons[0])) $member_addons[0]['field'] = 'addon_1';
    if(isset($member_addons[1])) $member_addons[1]['field'] = 'addon_2';

    //print_r($member_addons); exit;

    $merch_items = [
        // [
        //     'key' => 'scarf',
        //     'field' => 'merch_scarf',
        //     'name' => 'Check Scarf',
        //     'price' => 60,
        //     'sizes' => false,
        //     'sizeLabel' => false,
        //     'sizeField' => false,
        // ],
        [
            'key' => 'polo',
            'field' => 'merch_polo',
            'name' => 'Member Polo',
            'description' => false,
            'price' => 34.95,
            'sizes' => ['2XS', 'XS', 'S', 'M', 'L', 'XL', '2XL', '3XL', '4XL'],
            'sizeLabel' => 'Select size...',
            'sizeField' => 'merch_polo_size',
        ],
        [
            'key' => 'cap',
            'field' => 'merch_cap',
            'name' => 'Member Cap',
            'description' => false,
            'price' => 15,
            'sizes' => false,
            'sizeLabel' => false,
            'sizeField' => false,
        ],
        [
            'key' => 'supporter',
            'field' => 'merch_supporter',
            'name' => 'Supporter Pack',
            'description' => 'Includes Member Polo, Cap, Lapel Pin & Tote Bag',
            'price' => 49.95,
            'sizes' => ['2XS', 'XS', 'S', 'M', 'L', 'XL', '2XL', '3XL', '4XL'],
            'sizeLabel' => 'Select polo size...',
            'sizeField' => 'merch_supporter_size',
        ],
    ];

    $app_data = [
        'campaign' => $campaign,
        'record' => $record,
        'fields' => $hidden_fields,
        'allowedSelections' => 1,
        'processUrl' => $process_url,
        'addOns' => count($member_addons) > 0 ? $member_addons : false,
        'merchItems' => $merch_items,
        'memberPackage' => $memberPackage,
    ];

    //dd($app_data);
    //print_r($app_data); exit;

?><!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">

    <head>

    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    	<title>{{ $campaign->campaignTitle() }}</title>

    	<link href="{{ $campaign->asset('dist/bundle.css') }}" rel="stylesheet">
        <link rel="icon" href="{{ $campaign->asset('icon.ico') }}" />

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-33851616-15', 'auto');
          ga('send', 'pageview');

        </script>

        <script src="https://use.typekit.net/xdu2jcf.js"></script>
        <script>try{Typekit.load({ async: true });}catch(e){}</script>

        <script>
            window._token = '{{ csrf_token() }}';
            window.appData = {!! json_encode($app_data) !!};
        </script>

    </head>

    <body>
        <div id="app"></div>
        <div class="incompatible" id="incompatible">
            <div class="incompatible__inner">
                <div class="incompatible__text">
                    <p class="title">Please upgrade your browser</p>
                    <p class="description">Unfortunately your browser doesn't support this site.<br>Please use one of the following broswers:<br>Google Chrome, Firefox, Safari, Internet Explorer 11 or Microsoft Edge</p>
                </div>
            </div>
        </div>
        <script src="{{ $campaign->asset('dist/bundle.js') }}"></script>
    </body>

</html>
