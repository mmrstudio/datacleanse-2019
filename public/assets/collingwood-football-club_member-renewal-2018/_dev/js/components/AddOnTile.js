import React from 'react';
import { plural, pushArray, popArray } from '../utils';
import Checkbox from './Checkbox';

class AddonTile extends React.Component {

    constructor(props){
        super(props);
    }

    _toggleAddOn() {
        const { addOn, addOnValue, onUpdate } = this.props;
        if(addOn.options !== false) return;
        onUpdate(addOn.field, addOnValue === addOn.key ? '' : addOn.key);
    }

    _onDropdownChange(event) {
        const { addOn, addOnValue, onUpdate } = this.props;

        if(event.target.value !== '') {
            onUpdate(addOn.field, addOn.key);
        } else {
            onUpdate(addOn.field, '');
        }

        onUpdate('addon_option', event.target.value);
    }

    _renderDropdown() {

        const { addOn, addOnValue, addOnOption } = this.props;

        if(addOn.options !== false) {

            let dropdownOptions = [];
            addOn.options.map((option, i) => {
                dropdownOptions.push(<option key={`${addOn.field}_${option}`} value={option}>{option}</option>);
            });

            return (
                <div className="package__dropdown">
                    <select value={addOnOption} onChange={this._onDropdownChange.bind(this)}>
                        <option value="">{addOn.optionLabel}</option>
                        {dropdownOptions}
                    </select>
                    <svg viewBox="0 0 18 10"><path d="M9,10c-0.4,0-0.8-0.1-1.1-0.4L0.4,2.4c-0.6-0.6-0.6-1.5,0-2C1-0.1,2-0.1,2.6,0.4L9,6.6l6.4-6.1c0.6-0.6,1.5-0.6,2.1,0 c0.6,0.6,0.6,1.5,0,2l-7.5,7.1C9.8,9.9,9.4,10,9,10z"/></svg> 
                </div>
            );

        }

    }

    render() {
        const { addOn, addOnValue, record } = this.props;

        let monthlyPrice = false;
        let upfrontPrice = false;

        if(typeof addOn.monthly == 'object') {

            if(record.pricing) {
                monthlyPrice = addOn.monthly[record.pricing];
                upfrontPrice = addOn.upfront[record.pricing];
            } else {
                monthlyPrice = addOn.monthly.adult;
                upfrontPrice = addOn.upfront.adult;
            }

        } else {
            monthlyPrice = addOn.monthly;
            upfrontPrice = addOn.upfront;
        }

        return (
            <div className="package" data-selected={addOnValue === addOn.key}>
                <div className="package__inner">
                    <div className="package__name">{addOn.name}</div>
                    <div className="package__description">{addOn.description}</div>
                    <div className="package__prices">
                        {addOn.price !== false ? (<div className="package__price package__price--single">${addOn.price}</div>) : false}
                        {addOn.monthly !== false ? (<div className="package__price package__price--monthly"><strong>${monthlyPrice}</strong>/MTH</div>) : false}
                        {addOn.upfront !== false ? (<div className="package__price package__price--upfront">or <strong>${upfrontPrice}</strong> upfront</div>) : false}
                    </div>
                    {this._renderDropdown()}
                    <div className="package__overlay" onClick={this._toggleAddOn.bind(this)}></div>
                </div>
            </div>
        );
    }

}

export default AddonTile;
