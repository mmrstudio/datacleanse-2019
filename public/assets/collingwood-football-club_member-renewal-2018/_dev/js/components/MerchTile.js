import React from 'react';
import { plural } from '../utils';
import Checkbox from './Checkbox';

class MerchTile extends React.Component {

    constructor(props){
        super(props);
    }

    _toggleMerchQty() {
        const { merchItem, merchQty, sizeField, onUpdate } = this.props;

        const newQty = merchQty === 0 ? 1 : 0;

        if(newQty === 0 && sizeField !== false) {
            onUpdate(sizeField, '');
        }

        onUpdate(merchItem.field, newQty);
    }

    _onDropdownChange(event) {
        const { sizeField, onUpdate } = this.props;
        onUpdate(sizeField, event.target.value);
    }

    _renderDropdown() {

        const { merchItem, merchQty, sizeField, selectedSize } = this.props;

        if(merchItem.sizes !== false && merchQty > 0) {

            let dropdownOptions = [];
            merchItem.sizes.map((option, i) => {
                dropdownOptions.push(<option key={`${sizeField}_${option}`} value={option}>{option}</option>);
            });

            return (
                <div className="member-choice__tile__dropdown">
                    <select value={selectedSize} onChange={this._onDropdownChange.bind(this)}>
                        <option value="">{merchItem.sizeLabel}</option>
                        {dropdownOptions}
                    </select>
                    <svg viewBox="0 0 18 10"><path d="M9,10c-0.4,0-0.8-0.1-1.1-0.4L0.4,2.4c-0.6-0.6-0.6-1.5,0-2C1-0.1,2-0.1,2.6,0.4L9,6.6l6.4-6.1c0.6-0.6,1.5-0.6,2.1,0 c0.6,0.6,0.6,1.5,0,2l-7.5,7.1C9.8,9.9,9.4,10,9,10z"/></svg> 
                </div>
            );

        }

    }

    render() {
        const { merchItem, merchQty, allowedSelections } = this.props;

        return (
            <div className="member-choice__tile" data-selected={merchQty > 0}>
                <div className="member-choice__tile__inner">
                    <Checkbox class="member-choice__tile__checkbox black" checked={merchQty > 0} onClick={this._toggleMerchQty.bind(this)} />
                    <div className={`member-choice__tile__name`}>{merchItem.name}</div>
                    <div className={`member-choice__tile__price`}>
                        <span>$</span>
                        {merchItem.price}
                    </div>
                    {merchItem.description !== false ? <div className={`member-choice__tile__desc`}>{merchItem.description}</div> : false}
                    {this._renderDropdown()}
                </div>
            </div>
        );
    }

}

export default MerchTile;
