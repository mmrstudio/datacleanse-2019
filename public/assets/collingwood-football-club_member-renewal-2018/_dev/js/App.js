import React from 'react';
import MerchTile from './components/MerchTile';
import AddonTile from './components/AddonTile';
import Modal from './components/Modal';
import Checkbox from './components/Checkbox';
import { plural } from './utils';
import { fetchPOST } from './fetchUtils';

require('es6-promise').polyfill();
require('es6-object-assign').polyfill();

class App extends React.Component {

    constructor(props){
        super(props);

        let initialState = {
            campaign: window.appData.campaign,
            record: window.appData.record,
            fields: {},
            hiddenFields: window.appData.fields,
            addOns: window.appData.addOns,
            merchItems: window.appData.merchItems,
            memberPackage: window.appData.memberPackage,
            allowedSelections: parseInt(window.appData.allowedSelections),
            processUrl: window.appData.processUrl,
            memberPackage: window.appData.memberPackage,
            totalSelections: 0,
            loading: false,
            submitted: false,
            termsVisible: false,
            noThanksVisible: false,
            thanksMessage: 'For renewing your membership and standing side by side with us in 2018!',
            errors: []
        }

        //console.log('initialState', initialState);

        // initial merch items
        initialState.merchItems.map((merchItem, i) => {
            if(merchItem.field !== false) {
                initialState.fields[merchItem.field] = initialState.record[merchItem.field] ? initialState.record[merchItem.field] : 0;
                initialState.totalSelections += initialState.fields[merchItem.field];

                if(merchItem.sizeField !== false) {
                    initialState.fields[merchItem.sizeField] = initialState.record[merchItem.sizeField] ? initialState.record[merchItem.sizeField] : '';
                }
            }
        });

        // initial add-on selections
        if(initialState.addOns !== false) {
            initialState.addOns.map((addOn, i) => {
                const defValue = addOn.options === false ? 0 : [];
                initialState.fields[addOn.field] = initialState.record[addOn.field] ? initialState.record[addOn.field] : defValue;
            });
        }

        // addon option
        initialState.fields.addon_option = initialState.record.addon_option ? initialState.record.addon_option : '';

        // accept option
        initialState.fields.accept = initialState.record.accept ? initialState.record.accept : 'no';

        // change details option
        initialState.fields.change_details = initialState.record.change_details ? initialState.record.change_details : 'no';

        // no thanks
        initialState.fields.no_reason = initialState.record.no_reason ? initialState.record.no_reason : '';
        initialState.fields.no_reason_other = initialState.record.no_reason_other ? initialState.record.no_reason_other : '';

        //console.log('initialState', initialState);

        this.state = initialState;

        this._modal = false;

        this._handleAddonSelection = this._handleAddonSelection.bind(this);
        this._handleAcceptChange = this._handleAcceptChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        this._onSubmitResponse = this._onSubmitResponse.bind(this);
        this._renderMemberChoice = this._renderMemberChoice.bind(this);
        this._renderAddOns = this._renderAddOns.bind(this);

    }

    _showTerms(e) {
        e.preventDefault();
        this.setState({ termsVisible: true });
    }

    _showNoThanks() {
        this.setState({ noThanksVisible: true });
    }

    _handleMerchSelection(merchField, merchQty) {

        let fields = this.state.fields;

        // set current selection
        fields[merchField] = merchQty;

        // re-calculate total selections
        let totalSelections = 0;
        this.state.merchItems.map(merchItem => {
            if(merchItem.field !== false) totalSelections += fields[merchItem.field];
        });

        this.setState({
            fields: fields,
            totalSelections: totalSelections
        });
    }

    _handleAddonSelection(addonField, addonSelection) {
        let fields = this.state.fields;
        fields[addonField] = addonSelection;
        this.setState({ fields: fields });
    }

    _handleAcceptChange() {
        let fields = this.state.fields;
        fields.accept = fields.accept === 'no' ? 'yes' : 'no';
        this.setState({ fields: fields });
    }

    _handleSubmit(renewing, thanksMessage) {
        const { record, fields, hiddenFields, allowedSelections, totalSelections, processUrl, merchItems } = this.state;
        let submitErrors = [];

        if(renewing && this.state.fields.change_details === 'yes') {
            thanksMessage = thanksMessage + ' We will be in touch with you soon to update your details and/or membership.';
        }

        this.setState({ thanksMessage: thanksMessage });

        // check if all add options selected
        merchItems.map(merchItem => {
            if(merchItem.sizes !== false && merchItem.sizeField !== false) {
                if(fields[merchItem.field] === 1 && fields[merchItem.sizeField] === '') {
                    submitErrors.push(`Please choose ${merchItem.sizeLabel.toLowerCase()} for ${merchItem.name.toLowerCase()}`);
                }
            }
        });

        // check if agree
        // if(renewing && fields.accept === 'no') {
        //     submitErrors.push(`Please agree to the membership terms & conditions.`);
        // }

        if(submitErrors.length > 0) return this.setState({ errors: submitErrors });

        // update loading/submit state
        //this.setState({ submitted: false, loading: true });

        const postData = Object.assign(fields, hiddenFields);

        if(renewing) {
            postData.no_reason = '';
            postData.no_reason_other = '';
        }

        //return console.log('postData', postData);

        // send data
        fetchPOST(
            processUrl,
            postData,
            this._onSubmitResponse
        );

    }

    _onSubmitResponse(response) {

        if(response === true) {

            this.setState({
                submitted: true,
                loading: false,
            });

            scroll(0,0);

        } else {

            let errors = [];

            Object.keys(response).map((key) => {
                let value = response[key];
                value.map((error) => {
                    errors.push(error);
                });
            });

            this.setState({ errors: errors });

            //alert(errors.join("\r\n"));

            this.setState({
                loading: false
            });

        }

    }

    _renderMemberChoice() {

        const { record, allowedSelections, totalSelections, fields, memberPackage } = this.state;
        const selectionsRemaining = allowedSelections - totalSelections;

        return (
            <div className="member-choice">

                <div className="member-choice__inner">

                    <h2>EXCLUSIVE MEMBER MERCHANDISE</h2>
                    <p>Click on an option below to add it to your 2018 membership pack</p>

                    <div className="member-choice__tiles">
                        {this.state.merchItems.map(merchItem => <MerchTile key={merchItem.field} merchItem={merchItem} merchQty={fields[merchItem.field]} allowedSelections={allowedSelections} totalSelections={totalSelections} sizeField={merchItem.sizeField} selectedSize={merchItem.sizeField ? fields[merchItem.sizeField] : false} onUpdate={this._handleMerchSelection.bind(this)} />)}
                    </div>

                </div>

            </div>
        );

    }

    _renderAddOns() {

        if(this.state.addOns.length > 0) {

            const { fields, record } = this.state;

            return (
                <div className="add-ons">
                    <div className="packages__title">Recommended upgrade options (click or select an option)</div>
                    <div className="add-ons__inner">
                        <div className="add-ons__tiles">
                            {this.state.addOns.map(addOn => <AddonTile key={addOn.key} addOn={addOn} addOnValue={fields[addOn.field]} addOnOption={fields.addon_option} record={record} onUpdate={this._handleAddonSelection.bind(this)} />)}
                        </div>
                    </div>
                </div>
            );

        }

    }

    _toggleChangeDetails() {
        let fields = Object.assign({}, this.state.fields);
        fields.change_details = fields.change_details === 'yes' ? 'no' : 'yes';
        this.setState({ fields: fields });
    }

    _handleNoThanksOption(option) {
        let fields = Object.assign({}, this.state.fields);
        fields.no_reason = fields.no_reason !== option ? option : '';

        if(this.state.fields.no_reason === 'Other' && fields.no_reason !== 'Other') {
            fields.no_reason_other = '';
        }

        this.setState({ fields: fields });
    }

    _handleNoThanksOther(other) {
        let fields = Object.assign({}, this.state.fields);
        fields.no_reason_other = other;

        if(fields.no_reason_other.length > 0) {
            fields.no_reason = 'Other';
        } else {
            fields.no_reason = '';
        }

        this.setState({ fields: fields });
    }

    _handleNoThanksClose() {
        let fields = Object.assign({}, this.state.fields);
        fields.no_reason = '';
        fields.no_reason_other = '';
        this.setState({ noThanksVisible: false, fields: fields });
    }

    _handleNoThanksSubmit() {
        this.setState({ noThanksVisible: false });
        this._handleSubmit(false, 'For letting us know why you won’t be renewing your membership.');
    }

    _renderNoThanksOptions() {

        const options = [
            'Change of work and/or family circumstance',
            'Last season’s on field performance',
            'There isn’t a membership package that meets my needs',
            'Value for money',
        ]

        let count = 1;
        let noThanksOptions = [];

        options.map(option => {
            noThanksOptions.push(
                <li key={`noThanksOption${count}`}>
                    <span>{count}.</span> {option}
                    <Checkbox class="no-thanks-modal__checkbox black" checked={this.state.fields.no_reason === option} onClick={e => this._handleNoThanksOption(option)} />
                </li>
            );
            count++;
        });

        noThanksOptions.push(
            <li key={`noThanksOption${count}`}>
                <span>{count}.</span> Other
                <input type="text" value={this.state.fields.no_reason_other} onChange={e => this._handleNoThanksOther(e.target.value)} />
                <Checkbox class="no-thanks-modal__checkbox black" checked={this.state.fields.no_reason === 'Other'} onClick={e => this._handleNoThanksOption('Other')} />
            </li>
        );

        return noThanksOptions;
    }

    render() {
        const { record, fields, memberPackage } = this.state;

        return (
            <div className="body-wrap">

                { this.state.submitted === true ? (
                    <div className="wrap">
                        <div className="thanks">
                            <div className="thanks__inner">
                                <div className="thanks__title">Thank You!</div>
                                <p>{this.state.thanksMessage}</p>
                                <div className="thanks__banner">
                                    <div className="thanks__banner__inner"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                ) : (
                    <div className="wrap">

                        <div className="sidebar">
                            <div className="sidebar__scarf">
                                <div className="sidebar__scarf__years">{record.years_membership}</div>
                            </div>
                            <div className="sidebar__title"></div>
                            <div className="sidebar__details">
                                <div className="sidebar__details__inner">
                                    <div className="sidebar__details__title">Your Details</div>
                                    <span><strong>{record.name_first} {record.name}</strong><br /></span>
                                    <span>{record.phone_mobile}<br /></span>
                                    {record.street_addr_1.length > 0 ? <span>{record.street_addr_1}<br /></span> : false}
                                    {record.street_addr_2.length > 0 ? <span>{record.street_addr_2}<br /></span> : false}
                                    <span>{record.city}, {record.state} {record.zip}</span><br />
                                    {record.country.length > 0 ? <span>{record.country}</span> : false}
                                </div>
                            </div>
                        </div>

                        <div className="main">


                            {memberPackage !== false ? (
                                <div>
                                    <h2><span className="inline2018"></span> Membership Packages</h2>
                                    <div className="packages">
                                        <div className="current-package">
                                            <div className="packages__title">Your current membership</div>
                                            <div className="package" data-selected="true">
                                                <div className="package__inner">
                                                    <div className="package__name">{record.pc_info_1}</div>
                                                    <div className="package__description">{memberPackage.description}</div>
                                                    <div className="package__prices">
                                                        <div className="package__price package__price--monthly"><strong>{memberPackage.monthly}</strong>/MTH</div>
                                                        <div className="package__price package__price--upfront">or <strong>{memberPackage.upfront}</strong> upfront</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {this._renderAddOns()}
                                    </div>
                                </div>
                            ) : false}

                            {this._renderMemberChoice()}

                            <div className="form__submit">
                                <div className="form__submit__inner">
                                    {record.cc_num ? <div className="form__submit__note">We have your credit card ending in <strong>{record.cc_num}</strong> on file. To renew your membership with this credit card, please click "Renew Now". If this card or other details are incorrect please click here and we will contact you.</div> : false}
                                    <div className="form__submit__change-details">
                                        <Checkbox class="form__submit__change-details__checkbox white" checked={fields.change_details === 'yes'} onClick={this._toggleChangeDetails.bind(this)} />
                                        <strong>Please contact me to update my details</strong>
                                    </div>
                                    <button className="form__submit__button" onClick={e => this._handleSubmit(true, this.state.thanksMessage)} disabled={this.state.loading}>{ this.state.loading ? 'Loading...' : 'Renew Now' }</button>
                                </div>
                            </div>

                            <div className="no-thanks">
                                <button className="no-thanks__button" onClick={this._showNoThanks.bind(this)}>No Thanks</button>
                            </div>

                            <div className="footer">
                                <a href="https://membership.collingwoodfc.com.au/terms-and-conditions" target="_blank">Terms and Conditions</a>
                                <span>|</span>
                                <a href="http://www.collingwoodfc.com.au/privacy" target="_blank">Privacy Policy</a>
                            </div>

                        </div>

                    </div>
                )}

                <Modal classes="errors-modal" visible={this.state.errors.length > 0} title="Please check the following errors:" button={'Ok'} buttonAction={() => { this.setState({ errors: [] }) }} onClose={() => { this.setState({ errors: [] }) }} ref={(modal) => { this._modal = modal; }}>
                    <div className="errors-modal__list">
                        {this.state.errors.map((error, i) => <p key={`error_${i}`}>{error}</p>)}
                    </div>
                </Modal>

                <Modal classes="no-thanks-modal" visible={this.state.noThanksVisible} title="Please let us know your reason for not wanting to renew your membership this year" button={'Submit'} buttonAction={this._handleNoThanksSubmit.bind(this)} buttonDisabled={this.state.fields.no_reason === ''} onClose={this._handleNoThanksClose.bind(this)} ref={(modal) => { this._modal = modal; }}>
                    <ul className="no-thanks-modal__options">
                        {this._renderNoThanksOptions()}
                    </ul>
                </Modal>

            </div>
        );
    }

}

export default App;
