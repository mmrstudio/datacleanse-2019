import React from 'react';
import { plural } from '../utils';

class AddonTile extends React.Component {

    constructor(props){
        super(props);

        this._toggleAddon = this._toggleAddon.bind(this);
        this._onDropdownChange = this._onDropdownChange.bind(this);
        this._renderDropdown = this._renderDropdown.bind(this);

    }

    _toggleAddon() {
        if(this.props.showCheckbox === false) return;
        this.props.onUpdate(this.props.addOn.field);
    }

    _onDropdownChange(event) {
        this.props.onDropdownChange(this.props.addOn.dropdown.field, event.target.value);
    }

    _renderDropdown() {

        const { addOn, isSelected, dropdownValue } = this.props;

        if(addOn.dropdown !== false && isSelected) {

            let dropdownOptions = [];
            addOn.dropdown.options.map((option, i) => {
                dropdownOptions.push(<option key={i} selected={dropdownValue === option}>{option}</option>);
            });

            return (
                <div className="add-ons__tile__options__dropdown">
                    <select name="addon_polo_size" onChange={this._onDropdownChange}>
                        <option value="">{addOn.dropdown.label}</option>
                        {dropdownOptions}
                    </select>
                    <svg viewBox="0 0 18 10"><path d="M9,10c-0.4,0-0.8-0.1-1.1-0.4L0.4,2.4c-0.6-0.6-0.6-1.5,0-2C1-0.1,2-0.1,2.6,0.4L9,6.6l6.4-6.1c0.6-0.6,1.5-0.6,2.1,0 c0.6,0.6,0.6,1.5,0,2l-7.5,7.1C9.8,9.9,9.4,10,9,10z"/></svg> 
                </div>
            );

        }

    }

    render() {

        const { addOn, isSelected, showCheckbox } = this.props;

        //<div className="add-ons__tile__highlight"></div>

        return (
            <div className="add-ons__tile" data-selected={isSelected}>
                <div className="add-ons__tile__inner">
                    <div className={`add-ons__tile__image ${addOn.class}`}></div>
                    <div className="add-ons__tile__desc" dangerouslySetInnerHTML={{__html: addOn.description}}></div>
                    {addOn.monthly !== false && !isSelected ? <div className="add-ons__tile__note">Monthly payment option available.</div> : false }
                    { addOn.price !== false ? (
                    <div className="add-ons__tile__options">
                        {this._renderDropdown()}
                        <div className="add-ons__tile__options__select" onClick={this._toggleAddon}>
                            <span className="add-ons__tile__options__label">
                                {addOn.from === true ? (
                                    <span className="add-ons__tile__options__label__from">FROM</span>
                                ) : null}
                                <span dangerouslySetInnerHTML={{ __html: addOn.price }}></span>
                            </span>
                            {showCheckbox ? (
                                <div className="add-ons__tile__options__checkbox">
                                    <div className="add-ons__tile__options__checkbox__inner">
                                        <svg viewBox="0 0 40 40"><polygon points="14.1,31.7 6,23.3 8.6,20.9 14.2,26.7 31.7,9.1 34.2,11.6"/></svg>
                                    </div>
                                </div>
                            ) : false}
                        </div>
                    </div>
                    ) : false }
                </div>
            </div>
        );
    }

}

AddonTile.contextTypes = {
    //router: React.PropTypes.object.isRequired
};

export default AddonTile;
