import React from 'react';
import MerchTile from './components/MerchTile';
import AddonTile from './components/AddonTile';
import Modal from './components/Modal';
import { plural } from './utils';
import { fetchPOST } from './fetchUtils';

require('es6-promise').polyfill();
require('es6-object-assign').polyfill();

class App extends React.Component {

    constructor(props){
        super(props);

        //console.log('window.appData', window.appData);

        let initialState = {
            campaign: window.appData.campaign,
            record: window.appData.record,
            fields: {},
            hiddenFields: window.appData.fields,
            addOns: window.appData.addOns,
            merchItems: window.appData.merchItems,
            memberPackage: window.appData.memberPackage,
            allowedSelections: Number(window.appData.allowedSelections),
            processUrl: window.appData.processUrl,
            totalSelections: 0,
            loading: false,
            submitted: false,
            errors: []
        }

        // initial merch items
        initialState.merchItems.map((merchItem, i) => {
            if(merchItem.field !== false) {
                initialState.fields[merchItem.field] = initialState.record[merchItem.field] ? initialState.record[merchItem.field] : 0;
                initialState.totalSelections = initialState.totalSelections + Number(initialState.fields[merchItem.field]);
            }
        });

        // initial add-on selections
        initialState.addOns.map((addOn, i) => {
            initialState.fields[addOn.field] = initialState.record[addOn.field] ? initialState.record[addOn.field] : 'no';

            if(addOn.dropdown !== false) {
                initialState.fields[addOn.dropdown.field] = initialState.record[addOn.dropdown.field] ? initialState.record[addOn.dropdown.field] : '';
            }

        });

        // accept option
        initialState.fields.accept = initialState.record.accept ? initialState.record.accept : 'no';

        console.log('initialState', initialState);

        this.state = initialState;

        this._modal = false;

        this._handleMerchSelection = this._handleMerchSelection.bind(this);
        this._handleAddonSelection = this._handleAddonSelection.bind(this);
        this._handleAddonDropdown = this._handleAddonDropdown.bind(this);
        this._handleAcceptChange = this._handleAcceptChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        this._onSubmitResponse = this._onSubmitResponse.bind(this);
        this._renderMemberChoice = this._renderMemberChoice.bind(this);
        this._renderAddOns = this._renderAddOns.bind(this);

    }

    _handleMerchSelection(merchKey, merchQty) {

        let fields = this.state.fields;

        if (this.state.allowedSelections === 1 && merchKey !== 'donate_pack') {

            // toggle all selections
            this.state.merchItems.map((merchItem, i) => {
                if (merchItem.field !== false && !merchItem.toggleOnly) fields[merchItem.field] = 0;
            });

        }

        // set current selection
        fields[`merch_${merchKey}`] = merchQty;

        // update total selections
        let totalSelections = 0;
        this.state.merchItems.map((merchItem, i) => {
            if (merchItem.field !== false && !merchItem.toggleOnly) totalSelections = totalSelections + Number(fields[merchItem.field]);
        });

        this.setState({
            fields: fields,
            totalSelections: totalSelections
        });
    }

    _handleAddonSelection(addonField) {
        let fields = this.state.fields;
        fields[addonField] = fields[addonField] === 'no' ? 'yes' : 'no';
        this.setState({ fields: fields });
    }

    _handleAddonDropdown(dropdownField, value) {
        let fields = this.state.fields;
        fields[dropdownField] = value;
        this.setState({ fields: fields });
    }

    _handleAcceptChange() {
        let fields = this.state.fields;
        fields.accept = fields.accept === 'no' ? 'yes' : 'no';
        this.setState({ fields: fields });
    }

    _handleSubmit() {
        const { record, fields, hiddenFields, allowedSelections, totalSelections, processUrl } = this.state;

        if(totalSelections < allowedSelections) {
            this.setState({ errors: [`Please make all your merchandise selections - you have ${allowedSelections - totalSelections} remaining.`] })
            return;
            //return alert(`Please make all your merchandise selections - you have ${allowedSelections - totalSelections} remaining.`);
        }

        if(fields.accept === 'no') {
            this.setState({ errors: [`Please agree to the membership terms & conditions.`] })
            return;
            //return alert('Please agree to the membership terms & conditions');
        }

        this.setState({
            submitted: false,
            loading: true,
        });


        // send data
        fetchPOST(
            processUrl,
            Object.assign(fields, hiddenFields),
            this._onSubmitResponse
        );

    }

    _onSubmitResponse(response) {

        if(response === true) {

            this.setState({
                submitted: true,
                loading: false,
            });

            scroll(0,0);

        } else {

            let errors = [];

            Object.keys(response).map((key) => {
                let value = response[key];
                value.map((error) => {
                    errors.push(error);
                });
            });

            this.setState({ errors: errors });

            //alert(errors.join("\r\n"));

            this.setState({
                loading: false
            });

        }

    }

    _renderMemberChoice() {

        const { allowedSelections, totalSelections, fields, record, memberPackage } = this.state;

        if (memberPackage.mc === 'Yes' && Number(allowedSelections) > 0) {

            const selectionsRemaining = allowedSelections - totalSelections;

            let merchItemTiles = [];
            this.state.merchItems.map((merchItem, i) => {

                let merchLabel = '';
                let merchQty = 0;

                if(merchItem.field !== false) {
                    merchLabel = allowedSelections === 1 ? merchItem.label.one : plural(merchItem.label.multiple.singular, merchItem.label.multiple.plural, fields[merchItem.field], true);
                    merchQty = fields[merchItem.field];
                } else {
                    merchLabel = merchItem.label.one;
                }

                const merchAllowedSelections = merchItem.toggleOnly ? 1 : allowedSelections;
                const merchTotalSelections = merchItem.toggleOnly ? 1 : totalSelections;

                merchItemTiles.push(<MerchTile key={i} merchField={merchItem.field} merchQty={merchQty} allowedSelections={merchAllowedSelections} totalSelections={merchTotalSelections} merchKey={merchItem.key} label={merchLabel} onUpdate={this._handleMerchSelection} />);
            });

            return (
                <div className="member-choice">

                    <div className="member-choice__intro">
                        <h2>CHOOSE WHAT YOU WANT IN YOUR 2020 MEMBERSHIP PACK!</h2>
                        <h3>Hurry, stock is limited and you don’t want to miss out on your preferred item.</h3>
                        { allowedSelections > 1 ? <p>You are entitled to make <span>{allowedSelections}</span> member choices for 2020.<br />Use the +/- buttons below to make your selections.</p> : false }
                    </div>

                    <div className="member-choice__inner">
                        {merchItemTiles}
                    </div>

                    { allowedSelections > 1 ? <div className="member-choice__selections-remaining">You have <span>{selectionsRemaining}</span> {plural('selection', 'selections', selectionsRemaining)} remaining</div> : false }

                </div>
            );
        }

    }

    _renderAddOns() {

        if(this.state.addOns.length > 0) {

            const { fields, record } = this.state;

            let addOnTiles = [];
            this.state.addOns.map((addOn, i) => {
                addOnTiles.push(<AddonTile key={i} showCheckbox={record.renewal === 'no'} addOn={addOn} isSelected={fields[addOn.field] === 'yes'} dropdownValue={addOn.dropdown !== false ? fields[addOn.dropdown.field] : ''} onUpdate={this._handleAddonSelection} onDropdownChange={this._handleAddonDropdown} />);
            });

            return (
                <div className="add-ons">

                    {record.type === 'Renewal' ? (
                        <div>
                            <h2>WANT TO ADD EXCLUSIVE MEMBER MERCHANDISE OR<br /> ADD-ONS TO YOUR 2020 MEMBERSHIP?</h2>
                        </div>
                    ) : (
                        <div>
                            <h2>WANT TO ADD EXCLUSIVE MEMBER MERCHANDISE OR<br /> ADD-ONS TO YOUR 2020 MEMBERSHIP?</h2>
                            <h3>Simply make your selection below and we will update your 2020 membership package.</h3>
                        </div>
                    )}

                    <div className="add-ons__inner">
                        {addOnTiles}
                    </div>

                </div>
            );

        }

    }

    render() {

        //console.log(this.state);

        const { record, fields } = this.state;

        let errors = [];
        if(this.state.errors.length > 0) {
            this.state.errors.map((error, i) => {
                errors.push(<p>{error}</p>);
            });
        }

        return (
            <div className="body-wrap">

                <header className="header">
                    <div className="header__top">
                        <div className="header__inner">
                            <div className="header__top__logo"><a href="http://www.westernbulldogs.com.au/" target="_blank">Western Bulldogs</a></div>
                            <nav className="header__top__nav">
                                <ul>
                                    <li className="hidden-mobile"><a href="http://www.westernbulldogs.com.au/" target="_blank">Bulldogs Home</a></li>
                                    <li><a href="https://membership.westernbulldogs.com.au/contact" target="_blank">Contact Us</a></li>
                                </ul>
                            </nav>
                            <ul className="header__top__social">
                                <li><a href="https://instagram.com/westernbulldogs/?hl=en" target="_blank" className="instagram">Instagram</a></li>
                                <li><a href="http://twitter.com/#!/westernbulldogs" target="_blank" className="twitter">Twitter</a></li>
                                <li><a href="http://www.facebook.com/Western.Bulldogs?" target="_blank" className="facebook">Facebook</a></li>
                                <li><a href="http://www.youtube.com" target="_blank" className="youtube">YouTube</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className="header__bg"></div>
                </header>

                { this.state.submitted === true ? (
                    <div className="main">
                        <div className="thanks">
                            <div className="thanks__inner">
                                <div className="thanks__title">Thank You {record.name_first}!</div>
                                <p>By renewing as a member, you are part of a club which truly values your support.</p>
                                <p>A Western Bulldogs membership enables you to experience a greater connection with our great club, and we look forward to 2020 with great anticipation.</p>
                                <p>While we process your membership, if you have any questions, please contact our team on 1300 46 36 47 or email <a href="mailto:membership@westernbulldogs.com.au">membership@westernbulldogs.com.au</a>.</p>
                                <p>Go Dogs!</p>
                                <p>Western Bulldogs Football Club</p>
                            </div>
                        </div>
                    </div>
                ) : (
                    <div className="main">

                        {this._renderMemberChoice()}
                        {this._renderAddOns()}

                        <div className="form__submit">
                            {record.renewal === 'no' ? (
                                <div className="form__submit__inner">
                                    <div className="form__submit__accept">
                                        <label className="wb-checkbox">
                                            <input type="checkbox" name="accept" value="yes" checked={fields.accept === 'yes'} onChange={this._handleAcceptChange} />
                                            <span>
                                                <div className="inner">
                                                    <svg viewBox="0 0 40 40"><polygon points="14.1,31.7 6,23.3 8.6,20.9 14.2,26.7 31.7,9.1 34.2,11.6" /></svg>
                                                </div>
                                            </span>
                                        </label>
                                        I understand that the Western Bulldogs will update my 2020 membership package to reflect the choices I have made with my above selections and will adjust the payments to be deducted from my credit card on file. If I have any queries I can contact the club on 1300 46 36 47.
                                    </div>
                                    <button className="form__submit__button" onClick={this._handleSubmit} disabled={this.state.loading}>{ this.state.loading ? 'Loading...' : 'Submit' }</button>
                                </div>
                            ) : (
                                <div className="form__submit__inner">
                                    {/* <a href="https://www.surveymonkey.com/r/ML2WTPB" className="form__submit__button" style={{ fontSize: '2rem', textDecoration: 'none' }}>Request a Call</a> */}
                                    <a href="https://am.ticketmaster.com/bulldogs/" className="form__submit__button" style={{ fontSize: '2rem', textDecoration: 'none' }}>Renew Now</a>
                                </div>
                            )}
                        </div>

                    </div>
                )}

                <footer className="footer">

                    <div className="footer__sponsors">
                        <div className="footer__inner">
                            <div className="footer__sponsors__lockup"></div>
                        </div>
                    </div>

                    <div className="footer__disclaimer">
                        <div className="footer__inner">
                            <p>All personal information you provide will be used by the Western Bulldogs Football Club, AFL and on behalf of selected third parties in accordance with our Privacy Policy, this may include for promotional and direct marketing purposes and other disclosures as specified in our Privacy Policy found at <a href="http://www.westernbulldogs.com.au/privacy" target="_blank">http://www.westernbulldogs.com.au/privacy</a>.</p>
                            <p>By providing your personal information, you agree to such use. Your personal information will be kept securely and confidentially. Although you may choose not to provide us with certain information, please be aware that this choice will affect the type of and quality of service we can provide to you.</p>
                        </div>
                    </div>

                    <div className="footer__links">
                        <div className="footer__inner">
                            <div className="links">
                                <a href="http://www.westernbulldogs.com.au" target="_blank">www.westernbulldogs.com.au</a> <span>/</span><br />
                                <a href="http://membership.westernbulldogs.com.au/" target="_blank">Members Home</a> <span>/</span><br />
                                <a href="http://www.westernbulldogs.com.au/privacy" target="_blank">Privacy Policy</a> <span>/</span><br />
                                <a href="https://membership.westernbulldogs.com.au/terms-conditions-2" target="_blank">Terms &amp; Conditions</a> <span>/</span><br />
                                <a href="https://membership.westernbulldogs.com.au/terms-conditions-2" target="_blank">Disclaimer</a> <span>/</span><br />
                                <a href="http://www.westernbulldogs.com.au/club/contact-us" target="_blank">Contact Us</a>
                            </div>
                            <br />
                            &copy; Western Bulldogs Football Club 2019
                        </div>
                    </div>

                </footer>

                <Modal visible={this.state.errors.length > 0} button={'Ok'} buttonAction={() => { this.setState({ errors: [] }) }} buttonDisabled={false} ref={(modal) => { this._modal = modal; }}>
                    {errors}
                </Modal>

            </div>
        );
    }

}

App.contextTypes = {
    //router: React.PropTypes.object.isRequired
};

export default App;
