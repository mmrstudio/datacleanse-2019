<?php

function get_data($field, $record) {

    if(!function_exists('package_lookup'))
    {
        function package_lookup($event_name, $price_code)
        {
            $package = false;

            $get_package_sql = "
                select
                    _campaign_0079_lookup.var_text1,
                    _campaign_0079_lookup.var_text2,
                    _campaign_0079_lookup.var_text3,
                    _campaign_0079_benefits.id as 'package_id',
                    _campaign_0079_lookup.price_code_desc as 'price_code_desc',
                    _campaign_0079_lookup.price_code_info as 'price_code_info',
                    _campaign_0079_lookup.member_benefits as 'package_benefits_code',
                    _campaign_0079_benefits.benefits as 'package_benefits',
                    vt1.text as 'var_text1_text',
                    vt2.text as 'var_text2_text',
                    vt3.text as 'var_text3_text',
                    _campaign_0079_lookup.choice_3 as 'choice_3',
                    _campaign_0079_lookup.mc as 'mc',
                    _campaign_0079_lookup.coterie as 'coterie'
                    from _campaign_0079_lookup
                    left join _campaign_0079_benefits on _campaign_0079_benefits.code = _campaign_0079_lookup.member_benefits
                    left join _campaign_0079_vartext vt1 on vt1.code = _campaign_0079_lookup.var_text1
                    left join _campaign_0079_vartext vt2 on vt2.code = _campaign_0079_lookup.var_text2
                    left join _campaign_0079_vartext vt3 on vt3.code = _campaign_0079_lookup.var_text3
                where
                    _campaign_0079_lookup.member_key = '" . $event_name . "/" . $price_code . "'
            ";

            $get_package = DB::select($get_package_sql, []);

            if($get_package)
            {
                $package = $get_package[0];

                $package->package_name = $package->price_code_info ? $package->price_code_info : $package->price_code_desc;
            }

            return $package;
        }
    }

    $membership_text = [];
    $other_family_members_text = '';
    $family_total_upfront = 0;
    $family_total_monthly = 0;
    $group_codes_text = '';
    $var_text = '';
    $var_text_arr = [];
    $has_scarf = 'No';
    $has_mc = 'No';
    $has_benefit_text = 'No';
    $is_junior = 'No';
    $is_coterie = 'No';
    $family_juniors = [];

    $member_package_info = package_lookup($record->event_name_1, $record->pc_1);

    if($member_package_info)
    {
        // coterie?
        $is_coterie = $member_package_info->coterie;

        $main_package_text = '<strong>' . $record->pc_desc_1 . '</strong>';
        $main_package_text .= $record->block_price_1 > 0 && $record->event_name_1 != '20LA' ? ' - $' . number_format($record->block_price_1, 0, '.', ',') : '';

        if($is_coterie === 'No' && $record->section_1 != '' && $record->row_1 != '' && $record->seats_1 != '')
        {
            $main_package_text .= '<br />';
            $main_package_text .= $record->section_1 != '' ? '<strong>Section:</strong> '.$record->section_1.' ' : '';
            $main_package_text .= $record->row_1 != '' ? '<strong>Row:</strong> '.$record->row_1.' ' : '';
            $main_package_text .= $record->seats_1 != '' ? '<strong>Seat:</strong> '.$record->seats_1.' ' : '';
        }

        $membership_text[] = $main_package_text;

        if($member_package_info->package_benefits && $member_package_info->package_benefits_code !== 'C1')
        {
            $membership_text[] = nl2br($member_package_info->package_benefits);
            $has_benefit_text = 'Yes';
        }

        // has scarf?
        $has_scarf = $member_package_info->choice_3 === 'B4' ? 'Yes' : 'No';

        // has member choice?
        $has_mc = $member_package_info->mc;

        // junior?
        //$is_junior = $member_package_info->junior;
        if(in_array(substr($record->pc_1, -1), ['J', 'X', 'W', 'L', 'M'])) {
            $is_junior = 'yes';
        }

        // add family members
        if(in_array(substr($record->pc_1, -1), ['U','V','W','R','X','Y','Z']) && $record->family_id)
        {
            $get_family_members =  DB::select(
                "select `acct_id`, `name_first`, `name`, `event_name_1`, `pc_1`, `pc_desc_1`, `pc_info_1`, `block_price_1`, `pp_install_1`, `section_1`, `row_1`, `seats_1` from `_campaign_0079_input` where `family_id` = ? and `acct_id` <> ? and RIGHT(`pc_1`, 1) IN ('U','V','W','R','X','Y','Z')",
                [ $record->family_id, $record->acct_id ]
            );

            if($get_family_members)
            {
                foreach($get_family_members as $family_member)
                {
                    // is family member a junior?
                    if(in_array(substr($family_member->pc_1, -1), ['J', 'X', 'W', 'L', 'M'])) {
                        $family_juniors[] = $family_member->name_first;
                    }

                    $other_family_members_text = '';
                    if($family_member->name_first != '' || $family_member->name != '')
                    {
                        $other_family_members_text .= $family_member->name_first . ' ' . $family_member->name . '<br />';
                    }
                    $other_family_members_text .= '<strong>' . $family_member->pc_desc_1 . '</strong>';
                    $other_family_members_text .= $family_member->block_price_1 > 0 && $family_member->event_name_1 != '20LA' ? ' - $' . number_format($family_member->block_price_1, 0, '.', ',') : '';

                    $family_member_lookup = package_lookup($family_member->event_name_1, $family_member->pc_1);
                    $family_member_coterie = $family_member_lookup ? $family_member_lookup->coterie : 'Yes';

                    if($family_member_coterie === 'No' && $family_member->section_1 != '' && $family_member->row_1 != '' && $family_member->seats_1 != '')
                    {
                        $other_family_members_text .= '<br />';
                        $other_family_members_text .= $family_member->section_1 != '' ? '<strong>Section:</strong> '.$family_member->section_1.' ' : '';
                        $other_family_members_text .= $family_member->row_1 != '' ? '<strong>Row:</strong> '.$family_member->row_1.' ' : '';
                        $other_family_members_text .= $family_member->seats_1 != '' ? '<strong>Seat:</strong> '.$family_member->seats_1.' ' : '';
                    }

                    $membership_text[] = $other_family_members_text;

                }
            }
        }

        // var text            
        if($member_package_info->var_text1) $var_text_arr[$member_package_info->var_text1] = $member_package_info->var_text1_text;
        if($member_package_info->var_text2) $var_text_arr[$member_package_info->var_text2] = $member_package_info->var_text2_text;
        if($member_package_info->var_text3) $var_text_arr[$member_package_info->var_text3] = $member_package_info->var_text3_text;

        if($record->extra1) {
            $var_text_arr[] = 'Unfortunately our last attempts to process your remaining payments for your 2019 membership have been unsuccessful. A decline message appeared which may be due to the expiration of your credit card, insufficient funds in the nominated account or the credit card may have been reported lost or stolen. If you would like to become a member again in 2019, this balance will need to be paid in full prior to renewing. To settle your outstanding amount and renew for 2020, please contact us on 1300 46 36 47.';
        }

        $var_text = implode('<br /><br />', $var_text_arr);

    }

    // add group codes
    $search_group_codes = ['RENCONTOAD', 'RENYOUTHFAM', 'RENJNRYOU', 'RENJUNTOYOUHOME', 'RENFAMJNRYOU', 'RENJNRTOADULT', 'RENTOOCLOSECTY', 'RENYOUTOAD', 'RENNODOB', 'RENNOTFAM', 'RENBABYTOADULT', 'RENBABYTOJUN', 'RENBABYTOYOUTH', 'RENEXCLUDE', 'RENFAMYOUAD', 'RENFAMJNRAD', 'RENKIDSTOSIDE', 'RENNOTAFL', 'RENCHANGE3OR5YTH', 'RENBABYTOTODD', 'RENCONCTOYOUTH', 'RENSOCUPG', 'RENTODDTOJUN', 'RENTODDTOAD', 'RENEXCLUDECOMMS', 'RENSEATMOVE', 'RENBALLAINEL', 'RENRELOCATE', 'RENPASTPLAYER', 'RENFAMSTANDARD', 'RENSOCIALLIST', 'RENSOCIALADD', 'RENBABYTOROOK', 'RENCOUNTRYISTOLD', 'RENFAMAFLWJNRYOU', 'RENJUNTOCON', 'RENJNRTORS', 'RENNOTFAMAFLW'];
    $member_group_codes = $record->group_codes ? explode(',', $record->group_codes) : [];
    if(count($member_group_codes) > 0)
    {
        $group_codes_text_arr = [];
        foreach($member_group_codes as $code)
        {
            if(in_array($code, $search_group_codes))
            {
                $get_group_code =  DB::select(
                    "select * from `_campaign_0079_groupcodes` where `code` = ?;",
                    [ $code ]
                );

                if($get_group_code)
                {
                    $group_code = $get_group_code[0];
                    $group_codes_text_arr[] = $group_code->text;
                }
            }
        }
        $group_codes_text = implode('<br /><br />', $group_codes_text_arr);
    }

    // Other packages
    $other_packages = '';
    foreach(range(2,10) as $i)
    {
        if($record->{"event_name_$i"} !== '')
        {
            $other_package_lookup = package_lookup($record->{"event_name_$i"}, $record->{"pc_$i"});

            if($other_package_lookup) {
                $other_package_name = $other_package_lookup->package_name;
            } else {
                //$other_package_name = strlen($record->{"pc_info_$i"}) > 0 ? $record->{"pc_info_$i"} : $record->{"pc_desc_$i"};
                $other_package_name = $record->{"pc_desc_$i"};
            }

            $other_package_text = '<strong>' . $other_package_name . '</strong>';
            $other_package_text .= $record->{"block_price_$i"} > 0 && $record->{"event_name_$i"} != '20LA' ? ' - $' . $record->{"block_price_$i"} : '';

            $other_package_coterie = $other_package_lookup ? $other_package_lookup->coterie : 'Yes';

            if($other_package_coterie === 'No' && $record->{"section_$i"} != '' && $record->{"row_$i"} != '' && $record->{"seats_$i"} != '')
            {
                $other_package_text .= '<br />';
                $other_package_text .= $record->{"section_$i"} != '' ? '<strong>Section:</strong> '.$record->{"section_$i"}.' ' : '';
                $other_package_text .= $record->{"row_$i"} != '' ? '<strong>Row:</strong> '.$record->{"row_$i"}.' ' : '';
                $other_package_text .= $record->{"seats_$i"} != '' ? '<strong>Seat:</strong> '.$record->{"seats_$i"}.' ' : '';
            }

            $membership_text[] = $other_package_text;
        }
    }

    //dd($membership_text);

    $membership = implode('<br /><br />', $membership_text);
    $membership_split = str_split($membership, 220);

    $record->membership_1 = isset($membership_split[0]) ? $membership_split[0] : '';
    $record->membership_2 = isset($membership_split[1]) ? $membership_split[1] : '';
    $record->membership_3 = isset($membership_split[2]) ? $membership_split[2] : '';
    $record->membership_4 = isset($membership_split[3]) ? $membership_split[3] : '';
    $record->membership_5 = isset($membership_split[4]) ? $membership_split[4] : '';
    $record->membership_6 = isset($membership_split[5]) ? $membership_split[5] : '';

    // replace last space with non-breaking space
    if(ctype_space(substr($record->membership_1, -1))) $record->membership_1 = $record->membership_1 . '';
    if(ctype_space(substr($record->membership_2, -1))) $record->membership_2 = $record->membership_2 . '';
    if(ctype_space(substr($record->membership_3, -1))) $record->membership_3 = $record->membership_3 . '';
    if(ctype_space(substr($record->membership_4, -1))) $record->membership_4 = $record->membership_4 . '';
    if(ctype_space(substr($record->membership_5, -1))) $record->membership_5 = $record->membership_5 . '';
    if(ctype_space(substr($record->membership_6, -1))) $record->membership_6 = $record->membership_6 . '';

    // has membership text
    $record->has_membership_text = strlen($membership) > 0 ? 'Yes' : 'No';

    // Benefits
    if(strlen($var_text) > 0) {
        $var_text= '<span class="font-14 color-blue font-italic">' . $var_text . '</span><br><br>';
    }
    $vartext_split = str_split($var_text, 240);

    $record->vartext_1 = isset($vartext_split[0]) ? $vartext_split[0] : '';
    $record->vartext_2 = isset($vartext_split[1]) ? $vartext_split[1] : '';
    $record->vartext_3 = isset($vartext_split[2]) ? $vartext_split[2] : '';
    $record->vartext_4 = isset($vartext_split[3]) ? $vartext_split[3] : '';

    // replace last space with non-breaking space
    if(ctype_space(substr($record->vartext_1, -1))) $record->vartext_1 = $record->vartext_1 . '';
    if(ctype_space(substr($record->vartext_2, -1))) $record->vartext_2 = $record->vartext_2 . '';
    if(ctype_space(substr($record->vartext_3, -1))) $record->vartext_3 = $record->vartext_3 . '';
    if(ctype_space(substr($record->vartext_4, -1))) $record->vartext_4 = $record->vartext_4 . '';

    // Group codes
    if(strlen($group_codes_text) > 0) {
        $group_codes_text= '<span class="font-14 color-blue font-italic">' . $group_codes_text . '</span><br><br>';
    }
    $group_codes_split = str_split($group_codes_text, 240);

    $record->group_code_1 = isset($group_codes_split[0]) ? $group_codes_split[0] : '';
    $record->group_code_2 = isset($group_codes_split[1]) ? $group_codes_split[1] : '';
    $record->group_code_3 = isset($group_codes_split[2]) ? $group_codes_split[2] : '';
    $record->group_code_4 = isset($group_codes_split[3]) ? $group_codes_split[3] : '';

    // replace last space with non-breaking space
    if(ctype_space(substr($record->group_code_1, -1))) $record->group_code_1 = $record->group_code_1 . '';
    if(ctype_space(substr($record->group_code_2, -1))) $record->group_code_2 = $record->group_code_2 . '';
    if(ctype_space(substr($record->group_code_3, -1))) $record->group_code_3 = $record->group_code_3 . '';
    if(ctype_space(substr($record->group_code_4, -1))) $record->group_code_4 = $record->group_code_4 . '';

    // has scarf
    $record->has_scarf = $has_scarf;

    // has member choice
    if($record->type == 'Renewal') {
        $has_mc = 'No';
    }
    $record->mc = $has_mc;

    // is junior
    $record->junior = $is_junior;

    // has benefit text
    $record->has_benefit_text = $has_benefit_text;

    // is monthy?
    $record->is_monthly = $record->total_purchase == $record->total_payment_plan ? 'No' : 'Yes';

    // has benefit text
    $record->is_coterie = $is_coterie;

    // total costs
    $record->cost_per_month = '$' . number_format($record->total_payment_plan, 2);
    $record->first_installment = $record->extra1 ? '$' . number_format($record->total_payment_plan + $record->extra1, 2) : '$' . number_format($record->total_payment_plan, 2); // extra1 === balfwd
    $record->total_cost = $record->total_purchase > 0 ? '$' . number_format($record->total_purchase, 2) : '';

    // format address
    $address = [];
    if($record->street_addr_1) $address[] = $record->street_addr_1;
    if($record->street_addr_2) $address[] = $record->street_addr_2;
    $address_2 = [];
    if($record->city) $address_2[] = $record->city;
    if($record->state) $address_2[] = $record->state;
    if($record->zip) $address_2[] = $record->zip;
    if(count($address_2) > 0) $address[] = implode(', ', $address_2);
    if($record->country) $address[] = $record->country;
    if(count($address) > 0) {
        $record->street_addr_1 = implode('<br />', $address);
    } else {
        $record->street_addr_1 = 'Address not provided';
    }

    return $record;
}

$record = get_data(null, $record);
$email_record = new \stdClass();

foreach($campaign->fields() as $field) {
    if($field->email == 1) {
        $email_record->{$field->name} = $record->{$field->name};
    }
}

//$record = $email_record;
//$record->update_url = '#';

?><!DOCTYPE html>
<!--
<?php print_r($email_record); ?>
<?php print_r($record->toArray()); ?>
<?php //print_r($record); ?>
-->
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Renew with us in 2020!</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0 " />
        <meta name="format-detection" content="telephone=no"/>
        <style>
        
    .font-arial { font-family: Helvetica, Arial, sans-serif; line-height: 1.2; }

	.font-bold { font-weight: bold; }
	.font-italic { font-style: italic; }

	.font-10 { font-size: 10px; }
	.font-12 { font-size: 12px; }
	.font-13 { font-size: 13px; }
	.font-14 { font-size: 14px; }
	.font-16 { font-size: 16px; }
	.font-18 { font-size: 18px; }
	.font-20 { font-size: 20px; }
	.font-22 { font-size: 22px; }
	.font-24 { font-size: 24px; }
	.font-26 { font-size: 26px; }
	.font-28 { font-size: 28px; }

	.font-uppercase { text-transform: uppercase; }

	.color-black { color: #000000; }
	.color-white { color: #ffffff; }
	.color-red { color: #c50b2f; }
	.color-blue { color: #0039a6; }
	.color-grey { color: #464646; }

	.bg-black { color: #000000; }
	.bg-white { color: #ffffff; }
	.bg-red { color: #c50b2f; }
	.bg-blue { color: #0039a6; }
	.bg-grey { color: #464646; }
	.bg-light-blue { background-color: #f2f5fa; }

	.text-center { text-align: center; }
	.text-left { text-align: left; }
	.text-right { text-align: right; }

	.gutters { padding: 40px; }

    .border-top-blue { border-top: 1px solid #0039a6; }

    .button {
        background-color:#c50b2f;
        color:#ffffff;
        display:inline-block;
        font-family:sans-serif;
        font-size:21px;
        font-weight:bold;
        line-height:60px;
        text-align:center;
        text-decoration:none;
        width:480px;
        -webkit-text-size-adjust:none;
    }
        
        </style>
        <base href="<?php echo url('assets/western-bulldogs_2020-member-renewal/edm/test'); ?>">
    </head>


<body bgcolor="#fff"><span class="preheader" style="display: none;"><?php echo $record->name_first; ?>, your loyalty to the Club through your membership is greatly appreciated. Thank you for your continued support.</span>

    <!--Full width table start-->
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="border-collapse: collapse;mso-table-lspace: 0px;mso-table-rspace: 0px;">
    	<tr>
        	<td>
            	<table width="700" cellpadding="0" cellspacing="0" border="0" align="center" class="em_main_table">

                	<tr>
                    	<td valign="top" align="center">
                    		<table width="100%"  class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                       			<tr>
									<td height="10"></td>
								</tr>
                        		<tr>
                        			<td align="center" class="font-arial font-12 color-grey">If you are having trouble viewing this email, <span class="em-br"></span> please <webversion class="color-grey">click here</webversion> to view it as a webpage.</td>
								</tr>
                  				<tr>
									<td height="10"></td>
								</tr>
                   			</table>
                    	</td>
                    </tr>

                    <tr>
                    	<td valign="top" colspan="1">
                    		<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                        		<tr>

                                    <?php if($record->aflw == 'yes') : ?>
                                    <td valign="top" align="center">
                    					<a href="<?php echo $record->update_url; ?>"><img src="images/header-aflw.jpg" border="0" style="display:block;" width="700" height="330" alt="Renew with us in 2020" ></a>
                                    </td>
                                    <?php elseif($record->junior == 'yes') : ?>
                                    <td valign="top" align="center">
                    					<a href="<?php echo $record->update_url; ?>"><img src="images/header-junior.jpg" border="0" style="display:block;" width="700" height="330" alt="Renew with us in 2020" ></a>
                                    </td>
                                    <?php else : ?>
                                    <td valign="top" align="center">
                    					<a href="<?php echo $record->update_url; ?>"><img src="images/header.jpg" border="0" style="display:block;" width="700" height="330" alt="Renew with us in 2020" ></a>
                                    </td>
                                    <?php endif; ?>

								</tr>
                   			</table>
                    	</td>
                    </tr>

                    <tr>
                    	<td valign="top" colspan="1" bgcolor="#ffffff" class="gutters">
							<span class="font-arial font-24 font-bold color-red font-uppercase"><?php echo $record->name_first; ?>, belong to the west in 2020</span>
							<br><br>
							<span class="font-arial font-14">
                                The Western Bulldogs Football Club exists to create lasting memories for its people, as much as it strives to win premierships.  It’s a truly remarkable place characterised by ambition, strength, determination, and a real sense of belonging.  It represents its community with pride, the Mighty West. And it welcomes you to the Bulldog family for the 2020 season.
								<singleline />
							</span>
                    	</td>
                    </tr>

                    <tr>
                    	<td valign="top" colspan="1" class="font-arial bg-light-blue border-top-blue gutters">

							<span class="font-22 font-bold color-blue font-uppercase">Your 2020 membership DETAILS - MEMBER ID <?php echo $record->acct_id; ?></span>
							<br><br>

		                   	<table width="100%" cellpadding="0" cellspacing="0" border="0">
			                   	<tr>
									<td class="width-50 font-arial font-14" valign="top">
										<span class="font-bold font-uppercase"><?php echo $record->name_first; ?> <?php echo $record->name; ?></span><br>
										<?php echo $record->street_addr_1; ?><br><br>
									</td>
									<td class="width-50 font-arial font-14" valign="top">
										<?php if($record->phone_mobile) : echo $record->phone_mobile; ?><br><?php endif; ?>
										<?php if($record->phone_home) : echo $record->phone_home; ?><br><?php endif; ?>
										<?php if($record->email_addr) : echo $record->email_addr; ?><?php endif; ?>
									</td>
								</tr>
							</table>

							<br>

                            <!-- MEMBERSHIP PACKAGE -->
                            <?php if($record->has_membership_text == 'Yes') : ?>
							<span class="font-16 font-bold font-uppercase color-red">Membership Package</span>
							<br><br>
							<span class="font-14">
								<?php echo $record->membership_1; ?><?php echo $record->membership_2; ?><?php echo $record->membership_3; ?><?php echo $record->membership_4; ?><?php echo $record->membership_5; ?><?php echo $record->membership_6; ?>
                            </span>
                            <br><br>
                            <?php endif; ?>
                            
                            <?php if($record->type == 'Renewal') : ?>
                            <br>
                            <div style="text-align: center;"><!--[if mso]>
                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="https://am.ticketmaster.com/bulldogs/" style="height:60px;v-text-anchor:middle;width:480px;" stroke="f" fillcolor="#c50b2f">
                                <w:anchorlock/>
                                <center>
                            <![endif]-->
                                <a href="https://am.ticketmaster.com/bulldogs/" class="button" target="_blank">RENEW NOW</a>
                            <!--[if mso]>
                                </center>
                            </v:rect>
                            <![endif]--></div>
                            <br>
                            <?php endif; ?>
                              
                            <?php if($record->mc === 'Yes') : ?>
                            <!-- MEMBER CHOICE -->
                            <span class="font-arial font-24 font-bold color-blue">CHOOSE YOUR OWN MEMBERSHIP ITEM</span>
                            <br><br><br>

                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <?php if($record->has_scarf === 'Yes') : ?>
                                <tr>
                                    <td class="width-40 font-arial font-16 font-bold" style="padding-right: 40px;" valign="top">
                                        <span class="font-uppercase color-red">DO YOU WANT A CAP OR BEANIE?</span>
                                        <br><br>
                                        Click the button below to choose your own membership item for 2020.
                                        <br><br>
                                    </td>
                                    <td class="width-60" align="right">
                                        <a href="<?php echo $record->update_url; ?>"><img src="images/merch-no-scarf.jpg" width="280" height="131" border="0" style="display:block; max-width: 320px;" alt="2020 Merchandise Selection" ></a>
                                    </td>
                                </tr>
                                <?php else : ?>
                                <tr>
                                    <td class="width-40 font-arial font-16 font-bold" style="padding-right: 40px;" valign="top">
                                        <span class="font-uppercase color-red">DO YOU WANT A SCARF, CAP OR BEANIE?</span>
                                        <br><br>
                                        Click the button below to choose your own membership item for 2020.
                                        <br><br>
                                    </td>
                                    <td class="width-60" align="right">
                                        <a href="<?php echo $record->update_url; ?>"><img src="images/merch.jpg" width="280" height="131" border="0" style="display:block; max-width: 320px;" alt="2020 Merchandise Selection" ></a>
                                    </td>
                                </tr>
                                <?php endif; ?>
                                <tr>
                                    <td colspan="2" style="padding-top: 36px; padding-bottom: 30px;">

                                        <div style="text-align: center;"><!--[if mso]>
                                        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="<?php echo $record->update_url; ?>" style="height:60px;v-text-anchor:middle;width:480px;" stroke="f" fillcolor="#c50b2f">
                                            <w:anchorlock/>
                                            <center>
                                        <![endif]-->
                                            <a href="<?php echo $record->update_url; ?>" class="button" target="_blank">CHOOSE YOUR ITEM</a>
                                        <!--[if mso]>
                                            </center>
                                        </v:rect>
                                        <![endif]-->
                                        </div>

                                    </td>
                                </tr>
                            </table>
                            <br>

                            <?php else : ?>
                            <!-- NO MEMBER CHOICE -->
                            <div style="text-align: center;"><!--[if mso]>
                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="<?php echo $record->update_url; ?>" style="height:60px;v-text-anchor:middle;width:480px;" stroke="f" fillcolor="#c50b2f">
                                <w:anchorlock/>
                                <center>
                            <![endif]-->
                                <a href="<?php echo $record->update_url; ?>" class="button" target="_blank">CHECK OUT OUR 2020 MEMBER ADD-ONS</a>
                            <!--[if mso]>
                                </center>
                            </v:rect>
                            <![endif]-->
                            </div>
                            <br>
                            <?php endif; ?>

                            <br>

                            <?php if($record->is_monthly == 'Yes') : ?>
                            <span class="font-16 font-bold font-uppercase color-red">Cost Per Month</span> - <?php echo $record->cost_per_month; ?>
                            <br><br>
                            <?php endif; ?>

                            <?php if($record->total_cost) : ?>
                            <span class="font-16 font-bold font-uppercase color-red">Total Cost For Your Membership</span> - <?php echo $record->total_cost; ?>
                            <br><br>
                            <?php endif; ?>
                            
                            <!-- VARTEXT -->
                            <?php echo $record->vartext_1; ?><?php echo $record->vartext_2; ?><?php echo $record->vartext_3; ?><?php echo $record->vartext_4; ?>

                            <?php if($record->type == 'CommittedBulldog') : ?>
                            <span class="font-14 color-blue font-italic">As a member on the Committed Bulldog payment plan, your membership will automatically roll over for Season 2020 on 28 October 2019. So, there is no need to contact us – we will take care of everything for you! If you do need to supply us with new credit card details or make any changes to your 2020 membership please reach out to us on 1300 46 36 47.</span>
                            <?php endif; ?>

                            <!-- GROUP CODE -->
							<?php echo $record->group_code_1; ?><?php echo $record->group_code_2; ?><?php echo $record->group_code_3; ?><?php echo $record->group_code_4; ?>

                    	</td>
                    </tr>

                    <tr>
                    	<td valign="top" colspan="1" bgcolor="#ffffff" align="center" class="border-top-blue">
							<a href="http://westernbulldogs.com.au"><img src="images/sponsors.png" width="700" height="143" border="0" style="display:block; max-width: 700px;" alt="" ></a>
                    	</td>
                    </tr>
                    <tr>
                    	<td valign="top" colspan="1" bgcolor="#011e46" align="center" style="padding-top: 20px; padding-bottom: 20px;">

							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td>

										<table width="150" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td><a href="http://www.facebook.com/Western.Bulldogs?"><img src="images/facebook.png" width="30" height="30" border="0" style="display:block;" alt="Facebook" ></a></td>
												<td width="10"></td>
												<td><a href="https://www.instagram.com/westernbulldogs"><img src="images/instagram.png" width="30" height="30" border="0" style="display:block;" alt="Instagram" ></a></td>
												<td width="10"></td>
												<td><a href="http://twitter.com/#!/westernbulldogs"><img src="images/twitter.png" width="30" height="30" border="0" style="display:block;" alt="Twitter" ></a></td>
												<td width="10"></td>
												<td><a href="http://www.youtube.com/user/BulldogsTVOfficial"><img src="images/youtube.png" width="30" height="30" border="0" style="display:block;" alt="YouTube" ></a></td>
											</tr>
										</table>

									</td>
								</tr>
								<tr>
									<td height="20"></td>
								</tr>
								<tr>
									<td align="center">
                                        <span class="font-arial color-white font-10">
										    Email sent by: Western Bulldogs, 417 Barkly St, Footscray West 3012<br>
                                            <a href="https://membership.westernbulldogs.com.au/terms-conditions-2" class="color-white">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a href="http://www.westernbulldogs.com.au/privacy" class="color-white">Privacy Policy</a> &nbsp;|&nbsp; To unsubscribe from future receipt please <unsubscribe class="color-white">click here</unsubscribe>
                                        </span>
									</td>
								</tr>
							</table>

                    	</td>
                    </tr>


				</table>
     		</td>
        </tr>
    </table>
<div class="hidden-mobile" style="white-space:nowrap;font:20px courier;color:#ffffff;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
</body>
</html>

