<?php

    $get_member_package_info = DB::select('select _campaign_0079_lookup.* from _campaign_0079_lookup where _campaign_0079_lookup.event_name = ? and _campaign_0079_lookup.price_code = ?', [
        $record->event_name_1,
        $record->pc_1
    ]);

    $member_package_info = false;
    if($get_member_package_info)
    {
        $member_package_info = $get_member_package_info[0];
    }

    //dd($member_package_info);

    //print_r($member_package_info); exit;

    $addons = [

        'A1' => [
            'field' => 'addon_aflw_membership',
            'class' => 'aflw-membership',
            'name' => 'AFLW Membership',
            'description' => '<strong>AFLW Membership</strong><br />Women’s football is now firmly entrenched in the fabric of the Club, support our AFLW team in 2020 <br /><br /><strong>Adult $50, Junior $25</strong>',
            'price' => '$25',
            'monthly' => true,
            'dropdown' => [
                'label' => 'Select',
                'field' => 'addon_aflw_membership_option',
                'options' => ['Adult', 'Junior', 'Family']
            ],
            'from' => true,
        ],

        'A2' => [
            'field' => 'addon_aflw_ambassador',
            'class' => 'aflw-ambassador',
            'name' => 'AFLW Ambassador',
            'description' => '<strong>AFLW Ambassador</strong><br />Show your pride in AFLW be joining as an AFLW Ambassador. Get access to an exclusive AFLW event as well as other benefits.',
            'price' => '$200',
            'monthly' => true,
            'dropdown' => false,
            'from' => false,
        ],

        'A3' => [
            'field' => 'addon_spray_jacket',
            'class' => 'spray-jacket',
            'name' => 'Member Spray Jacket',
            'description' => '<strong>Member Spray Jacket</strong><br />New to 2020 the Member Spray Jacket. The exclusive member only jacket is the perfect wet weather protector. Buy early to secure your size before they sell out.',
            'price' => '$95',
            'monthly' => true,
            'dropdown' => [
                'label' => 'Size',
                'field' => 'addon_spray_jacket_size',
                'options' => ['XXS', 'XS', 'S', 'M', 'L', 'XL', '2XL', '3XL', '4XL', '5XL', '7XL']
            ],
            'from' => false,
        ],

        'A4' => [
            'field' => 'addon_rugby_top',
            'class' => 'rugby-top',
            'name' => 'Member Rugby Top',
            'description' => '<strong>Member Rugby Top</strong><br />New in 2020 the members only Rugby Top is available in adult and junior sizes. Buy early to secure your size before they sell out.',
            'price' => '$65',
            'monthly' => true,
            'dropdown' => [
                'label' => 'Size',
                'field' => 'addon_rugby_top_size',
                'options' => ['6', '8', '10', '12', '14', 'XXS', 'XS', 'S', 'M', 'L', 'XL', '2XL', '3XL', '4XL', '5XL', '7XL']
            ],
            'from' => false,
        ],

        'A5' => [
            'field' => 'addon_polo',
            'class' => 'polo',
            'name' => 'Member Polo',
            'description' => '<strong>Member Polo</strong><br />The 2020 member Polo is exclusive to members. Buy early to secure your size before they sell out.',
            'price' => '$45',
            'monthly' => true,
            'dropdown' => [
                'label' => 'Size',
                'field' => 'addon_polo_size',
                'options' => ['XS', 'S', 'M', 'L', 'XL', '2XL', '3XL', '4XL', '5XL', '7XL']
            ],
            'from' => false,
        ],

        'A6' => [
            'field' => 'addon_ballarat',
            'class' => 'ballarat',
            'name' => 'Ballarat 2 Game Add On',
            'description' => '<strong>Ballarat 2 Game Add On</strong><br />Guarantee a Premium Reserved Seat for our 2020 home games at Mars Stadium before tickets go on sale to the general public, and get an exclusive Ballarat member scarf. <br /><br /><strong>Adult $75, Concession $65, Junior $45</strong>',
            'price' => '$45',
            'monthly' => true,
            'dropdown' => [
                'label' => 'Select',
                'field' => 'addon_ballarat_option',
                'options' => ['Adult', 'Concession', 'Junior']
            ],
            'from' => true,
        ],

        'A7' => [
            'field' => 'addon_fightback',
            'class' => 'fightback',
            'name' => 'Fightback Pack',
            'description' => '<strong>Fightback Pack</strong><br />Celebrate the Fightback 30 years on with this exclusive Fightback Pack. Includes Fightback Scarf, Up Yours Oakley badge and Fightback badge.',
            'price' => '$40',
            'monthly' => true,
            'dropdown' => false,
            'from' => false,
        ],        

        'A8' => [
            'field' => 'addon_pet',
            'class' => 'pet',
            'name' => 'Pet Membership',
            'description' => '<strong>Pet Membership</strong><br />Even your pet can Belong to the West with our Pet membership. Includes pet collar and lead.',
            'price' => '$45',
            'monthly' => true,
            'dropdown' => false,
            'from' => false,
        ],        

    ];

    // append relevant addons
    $member_addons = [];
    $can_select_addons = true;
    $addon_keys = array_only((array)$member_package_info, ['addon_1', 'addon_2', 'addon_3', 'addon_4', 'addon_5', 'addon_6']);

    if($record->type == 'Renewal') {

        foreach ($addons as $key => $addon) {
            $member_addons[] = $addon;
        }

    } else {

        foreach ($addon_keys as $key => $addon_key) {
            if ($addon_key && isset($addons[$addon_key])) {
                $member_addons[] = $addons[$addon_key];
            }
        }

    }

    // if ($member_package_info->addon_1 && isset($addons[$member_package_info->addon_1])) $member_addons[] = $addons[$member_package_info->addon_1];
    // if ($member_package_info->addon_2 && isset($addons[$member_package_info->addon_2])) $member_addons[] = $addons[$member_package_info->addon_2];
    // if ($member_package_info->addon_3 && isset($addons[$member_package_info->addon_3])) $member_addons[] = $addons[$member_package_info->addon_3];
    // if ($member_package_info->addon_4 && isset($addons[$member_package_info->addon_4])) $member_addons[] = $addons[$member_package_info->addon_4];
    // if ($member_package_info->addon_5 && isset($addons[$member_package_info->addon_5])) $member_addons[] = $addons[$member_package_info->addon_5];
    // if ($member_package_info->addon_6 && isset($addons[$member_package_info->addon_6])) $member_addons[] = $addons[$member_package_info->addon_6];

    // foreach($addons as $addon)
    // {
    //     $member_addons[] = $addon;
    // }

    //print_r($member_addons); exit;
    //$member_addons[] = $addons['A7'];

    $merch_items = [
        'B1' => [
            'key' => 'cap',
            'field' => 'merch_cap',
            'label' => [
                'one' => 'I want a cap',
                'multiple' => ['singular' => 'Cap', 'plural' => 'Caps']
            ],
            'toggleOnly' => false,
        ],
        'B2' => [
            'key' => 'beanie',
            'field' => 'merch_beanie',
            'label' => [
                'one' => 'I want a beanie',
                'multiple' => ['singular' => 'Beanie', 'plural' => 'Beanies']
            ],
            'toggleOnly' => false,
        ],
        'B3' => [
            'key' => 'scarf',
            'field' => 'merch_scarf',
            'label' => [
                'one' => 'I want a scarf',
                'multiple' => ['singular' => 'Scarf', 'plural' => 'Scarves']
            ],
            'toggleOnly' => false,
        ],
        'B4' => [
            'key' => 'no_scarf',
            'field' => false,
            'label' => [
                'one' => 'Choose a cap or beanie',
                'multiple' => ['singular' => '', 'plural' => '']
            ],
            'toggleOnly' => false,
        ],
        'B5' => [
            'key' => 'donate_item',
            'field' => 'merch_donate_item',
            'label' => [
                'one' => 'Donate item',
                'multiple' => ['singular' => 'Item', 'plural' => 'Items']
            ],
            'toggleOnly' => false,
        ],
        'B6' => [
            'key' => 'donate_pack',
            'field' => 'merch_donate_pack',
            'label' => [
                'one' => 'Donate pack',
                'multiple' => ['singular' => 'Pack', 'plural' => 'Packs']
            ],
            'toggleOnly' => true,
        ],
        'B7' => [
            'key' => 'donate_item_pack',
            'field' => 'merch_donate_item_pack',
            'label' => [
                'one' => 'Donate item & pack',
                'multiple' => ['singular' => 'Item & Pack', 'plural' => 'Items & Packs']
            ],
            'toggleOnly' => false,
        ],
    ];

    // append merch items
    $member_merch_items = [];
    if($member_package_info)
    {
        if($member_package_info->choice_1) $member_merch_items[] = $merch_items[$member_package_info->choice_1];
        if($member_package_info->choice_2) $member_merch_items[] = $merch_items[$member_package_info->choice_2];
        if($member_package_info->choice_3) $member_merch_items[] = $merch_items[$member_package_info->choice_3];
        if($member_package_info->choice_4) $member_merch_items[] = $merch_items[$member_package_info->choice_4];
        if($member_package_info->choice_6) $member_merch_items[] = $merch_items[$member_package_info->choice_6];
        if($member_package_info->choice_5) $member_merch_items[] = $merch_items[$member_package_info->choice_5];
    }

    //$member_merch_items[] = $merch_items['B4'];
    //print_r($member_merch_items); exit;

    if($record->type == 'Renewal') {
        $record->mc = 'No';
        if($member_package_info) $member_package_info->mc = 'No';
    }

    $app_data = [
        'campaign' => $campaign,
        'record' => $record,
        'fields' => $hidden_fields,
        'allowedSelections' => $member_package_info && $member_package_info->mc == 'Yes' ? $member_package_info->member_choice : 0,
        'memberPackage' => $member_package_info,
        'processUrl' => $process_url,
        'addOns' => $member_addons,
        'merchItems' => $member_merch_items,
        'canSelectAddons' => $can_select_addons,
    ];

    //dd($app_data);

?><!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">

    <head>

    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    	<title>{{ $campaign->campaignTitle() }}</title>

    	<link href="{{ $campaign->asset('dist/bundle.css') }}" rel="stylesheet">
        <link rel="icon" href="{{ $campaign->asset('icon.ico') }}" />

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-33851616-15', 'auto');
          ga('send', 'pageview');

        </script>

        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700|Arvo:400italic,400,700,700italic' rel='stylesheet' type='text/css'>

        <script>
            window._token = '{{ csrf_token() }}';
            window.appData = {!! json_encode($app_data) !!};
        </script>

    </head>

    <body>
        <div id="app"></div>
        <div class="incompatible" id="incompatible">
            <div class="incompatible__inner">
                <div class="incompatible__text">
                    <p class="title">Please upgrade your browser</p>
                    <p class="description">Unfortunately your browser doesn't support this site.<br>Please use one of the following broswers:<br>Google Chrome, Firefox, Safari, Internet Explorer 11 or Microsoft Edge</p>
                </div>
            </div>
        </div>
        <script src="{{ $campaign->asset('dist/bundle.js') }}"></script>
    </body>

</html>
