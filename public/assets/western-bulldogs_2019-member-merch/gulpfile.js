
    var gulp = require('gulp');
    var autoprefixer = require('gulp-autoprefixer');
    var cssmin = require('gulp-cssmin');

    var webpack = require('webpack-stream');
    var webpackConfig;

    // WATCH

    gulp.task('watch', [], function () {

        gulp.watch('_dev/scss/**/*.scss', ['webpack']);
        gulp.watch('_dev/js/**/*.js', ['webpack']);

    });

    gulp.task('webpack', function() {

        webpackConfig = require('./webpack.config.js').getConfig('development');

        // webpack
        return gulp.src(webpackConfig.entry)
            .pipe(webpack(webpackConfig))
            .on('error', function handleError() {
                this.emit('end');
            })
            .pipe(gulp.dest('./dist/'));

    });

    gulp.task('publish', function() {

        webpackConfig = require('./webpack.config.js').getConfig('production');

        // webpack
        gulp.src(webpackConfig.entry)
            .pipe(webpack(webpackConfig))
            .on('error', function handleError() {
                this.emit('end');
            })
            .pipe(gulp.dest('./dist/'));

        // autoprefix & minify
        gulp.src('dist/bundle.css')
            .pipe(autoprefixer({ browsers: ['last 3 versions'], cascade: false }))
            .pipe(cssmin())
            .pipe(gulp.dest('dist'));

    });
