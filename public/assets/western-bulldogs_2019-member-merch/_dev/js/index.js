import React from 'react';
import ReactDOM from 'react-dom';
//import 'react-fastclick';
import '../scss/styles.scss';
import App from './App';

let appEl = document.getElementById('app');

function isIE () {
  var myNav = navigator.userAgent.toLowerCase();
  return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
}

if(appEl !== null) {

    var inCompat = isIE() && isIE() < 11;

    if (inCompat) {
        document.getElementById('incompatible').style.display = 'block';
        //document.getElementById('loading').style.display = 'none';
    } else {
        ReactDOM.render(<App />, appEl);
    }

}
