<?php

    //dd($member_package_info);

    //print_r($member_package_info); exit;

    $addons = [

        'A3' => [
            'field' => 'addon_hoodie',
            'class' => 'hoodie',
            'name' => 'Member Hoodie',
            'description' => 'New to 2019 the Member hoodie. The exclusive member only hoodie is the perfect winter warmer. Buy early to secure your size before they sell out.',
            'price' => '$80',
            'monthly' => true,
            'dropdown' => [
                'label' => 'Size',
                'field' => 'addon_hoodie_size',
                'options' => ['L', 'XL', '2XL', '3XL', '4XL', '5XL', '7XL']
            ],
            'from' => false,
        ],

        'A5' => [
            'field' => 'addon_polo',
            'class' => 'polo',
            'name' => 'Member Polo',
            'description' => 'The 2019 member Polo is exclusive to members. Buy early to secure your size before they sell out.',
            'price' => '$40',
            'monthly' => true,
            'dropdown' => [
                'label' => 'Size',
                'field' => 'addon_polo_size',
                'options' => ['XXS', 'XS', 'S', 'M', 'L', 'XL', '2XL', '3XL', '4XL', '5XL', '7XL']
            ],
            'from' => false,
        ],

        'A7' => [
            'field' => 'addon_beanie',
            'class' => 'beanie',
            'name' => 'Member Beanie',
            'description' => 'Our 2019 Member only beanie is sure to keep you toastie in the depths of footy winter',
            'price' => '$20',
            'monthly' => true,
            'dropdown' => false,
            'from' => false,
        ],   

        'A4' => [
            'field' => 'addon_turbo',
            'class' => 'turbo',
            'name' => 'Turbo Pack',
            'description' => 'The complete member kit with all the essentials. Includes member-only polo, cap and mug. Buy early to secure your size before they sell out.',
            'price' => '$60',
            'monthly' => true,
            'dropdown' => [
                'label' => 'Size',
                'field' => 'addon_turbo_size',
                'options' => ['XXS', 'XS', 'S', 'M', 'L', 'XL', '2XL', '3XL', '4XL', '5XL', '7XL']
            ],
            'from' => false,
        ],     

    ];

    // append relevant addons
    $member_addons = [];
    $can_select_addons = true;
    foreach($addons as $addon)
    {
        $member_addons[] = $addon;
    }

    $app_data = [
        'campaign' => $campaign,
        'record' => $record,
        'fields' => $hidden_fields,
        'allowedSelections' => 0,
        'memberPackage' => null,
        'processUrl' => $process_url,
        'addOns' => $member_addons,
        'merchItems' => [],
        'canSelectAddons' => true,
    ];

    //dd($app_data);

?><!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">

    <head>

    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    	<title>{{ $campaign->campaignTitle() }}</title>

    	<link href="{{ $campaign->asset('dist/bundle.css') }}" rel="stylesheet">
        <link rel="icon" href="{{ $campaign->asset('icon.ico') }}" />

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-33851616-15', 'auto');
          ga('send', 'pageview');

        </script>

        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700|Arvo:400italic,400,700,700italic' rel='stylesheet' type='text/css'>

        <script>
            window._token = '{{ csrf_token() }}';
            window.appData = {!! json_encode($app_data) !!};
        </script>

    </head>

    <body>
        <div id="app"></div>
        <div class="incompatible" id="incompatible">
            <div class="incompatible__inner">
                <div class="incompatible__text">
                    <p class="title">Please upgrade your browser</p>
                    <p class="description">Unfortunately your browser doesn't support this site.<br>Please use one of the following broswers:<br>Google Chrome, Firefox, Safari, Internet Explorer 11 or Microsoft Edge</p>
                </div>
            </div>
        </div>
        <script src="{{ $campaign->asset('dist/bundle.js') }}"></script>
    </body>

</html>
