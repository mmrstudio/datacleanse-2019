
    <footer class="footer__inner">

        <p><strong>Sydney Swans Football Club | 02 9339 9123 | Light Tower #4, Driver Avenue, Moore Park, NSW, 2021</strong></p>

        <p>All personal information you provide will be used and stored in accordance with the Sydney Swans Privacy Policy. Only those who confirm or update their details prior to 5pm AEST, Friday September 9 will be entered into the draw to win one of five $100 gift vouchers for 2017 Sydney Swans membership. The winners will be drawn at Sydney Swans Football Club, SCG Light Tower 4, Driver Avenue, Moore Park NSW 2021 on Monday, September 12, 2016. Winners will be notified on the same day.</p>

        <ul class="footer__links">
            <li><a href="http://www.sydneyswans.com.au" target="_blank">Sydney Swans Football Club</a></li>
            <li><a href="http://www.sydneyswans.com.au/privacy" target="_blank">Privacy Policy</a></li>
            <li><a href="https://membership.sydneyswans.com.au/terms-conditions/" target="_blank">Terms &amp; Conditions</a></li>
            <li><a href="http://datacleanse.com.au" target="_blank">Product of Datacleanse</a></li>
        </ul>

    </footer>
