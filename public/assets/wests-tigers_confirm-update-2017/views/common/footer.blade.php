

    
    
    
<div class="footer__logo">
  
   @if( empty($thanks))
   
	  <img src="{{ $campaign->asset('images/tiger_logo.png') }}">
   @endif
    
   <div class="footer__logo__title"><span>#</span>RisingUp</div></div> 
    
    <ul class="footer__social">
        <li><a href="http://www.weststigers.com.au/social/facebook.html" target="_blank"><img src="{{ $campaign->asset('images/facebook.png') }}"></a></li>
        <li><a href="http://www.weststigers.com.au/social/instagram.html" target="_blank"><img src="{{ $campaign->asset('images/instagram.png') }}"></a></li>
        <li><a href="http://www.weststigers.com.au/social/twitter.html" target="_blank"><img src="{{ $campaign->asset('images/twitter.png') }}"></a></li>
         
    </ul>
<p>Email sent by: Wests Tigers Football Club, Concord Oval, Loftus Street, Concord NSW 2137</p>
    <ul class="footer__links">
        <li><a href="http://www.weststigers.com.au/corporate/UltimateWestsTigersExperienceTermsandConditions.html" target="_blank">Terms &amp; Conditions</a></li>
         
        
        <li><a href="http://mmr.com.au" target="_blank">Product of MMR Studio</a></li>
    </ul>