@extends($campaign->view('main'))

@section('content')

<header class="header header--thanks">

    @include($campaign->view('common.header'))
    
    
	 @if($entry_type == 'update')
    <div class="header__thanks-title">
       
      
			<div class="header__thanks-title__inner"></div>			
			
	
				  
        
    </div>
    @else
	<div class="header__thanks-title register">
       
      
			<div class="header__thanks-title__inner enter"></div>			
			
	
				  
        
    </div>
	@endif

</header>

<footer class="footer  thanks">

    @include($campaign->view('common.footer'))

</footer>

@endsection
