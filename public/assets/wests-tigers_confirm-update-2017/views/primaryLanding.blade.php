@extends($campaign->view('main'))

@section('content')

<header class="header">

    @include($campaign->view('common.header'))
	
  
   @if($entry_type == 'update' )  
    <div class="header__landing-title  ">
        <div class="header__landing-title__inner"></div>
    </div>
    @else 
    <div class="header__landing-title">
        <div class="header__landing-title__inner  register"></div>
    </div>
    @endif

</header>

<div class="content">

	<div class="intro">
		<div class="intro__logo"></div>
		@if($record->first_name != '')
		<div class="intro__greeting">Dear {{ $record->first_name }},</div>
		@endif
		<div class="intro__text">
			@if($entry_type == 'update')
			Are your details correct?<br>
		   Confirm or update your personal details below and you could <strong>WIN*:</strong>
		   
		   @else
		   	  Enter your personal details below and you could <strong>WIN*:</strong>
		   @endif
		</div>
	</div>

	<div class="prize">
		<div class="prize__first">
			<h3>FIRST PRIZE</h3> 
			<h1>Ultimate Wests Tigers Experience</h1>
			<ul>
				<li>Flights to Sydney for you and a friend</li>
				<li>One night’s accommodation</li>
				<li>Attend Captain’s Run and meet the team</li>
				<li>Hospitality and premium seating</li>
				<li>Post-game dressing room access</li>

			</ul>
		</div>
		<div class="prize__right">

			<div class="prize__second">
				<h3>Second PRIZE</h3> 
				<h1>Signed Wests Tigers<br>jersey </h1>

			</div>

			<div class="prize__third">
				<h3>Third PRIZE</h3> 
				<h1>Wests Tigers <br>merchandise Pack</h1>

			</div>
		</div>	
	</div>


	<div class="main_heading">
		<h2>Your Details</h2>
	</div>

	<div class="main">

		<form action="{{ $process_url }}" method="post" class="form">

			@include($campaign->view('common.errors'))

			<div class="form__inner">





				@include($campaign->view('fields.text'), [
					'key' => 'first_name',
					
				])

				@include($campaign->view('fields.text'), [
					'key' => 'last_name',
					
				])


				@include($campaign->view('fields.address'), [
					'keys' => ['street_addr_1', 'street_addr_2'],
				])

				@include($campaign->view('fields.text'), [
					'key' => 'city',
					'id' => 'suburbField',
				])

				@include($campaign->view('fields.text'), [
					'key' => 'postcode',
				])

				@include($campaign->view('fields.country'), [
					'key' => 'country',
				])

				@include($campaign->view('fields.state'), [
					'key' => 'state',
				])



				@include($campaign->view('fields.email'), [
					'key' => 'email_addr',
				])

				@include($campaign->view('fields.dob'), [
					'key' => 'birth_date',
					'not_required' => false,
				])

				@include($campaign->view('fields.text'), [
					'key' => 'phone_mobile',
				])
				
				
				
				
				@include($campaign->view('fields.member'), [
					'key' => 'member',
				])
				
				

				<div class="form__submit">

					@foreach($hidden_fields as $name=>$value)
					<input type="hidden" name="{{ $name }}" value="{{ $value }}">
					@endforeach
					
					
					

					
					@if($entry_type == 'update')
						
						<button class="form__submit__button" type="submit">Update </button> 
				    @else
						 <button class="form__submit__button enter" type="submit">Enter </button> 
				    @endif
				  

				</div>

				<div class="form__border"></div>

			</div>

		</form>

	</div>
	
	
	
	
    <div class="sponsors">

		

			
		<div class="sponsors__primary">

			<div class="sponsors__sponsor">
				<a href="http://www.brydens.com.au/" target="_blank" title="Brydens Lawyers">
					<img src="http://weststigersmembership.com.au/wp-content/uploads/brydens-lawyers.jpg" title="Brydens Lawyers" alt="Brydens Lawyers Logo">                                </a>
			</div>

		</div>

		<div class="sponsors__grid">

			<div class="sponsors__sponsor">
				<a href="http://www.alliedexpress.com.au/" target="_blank" title="Allied Express">
				<img src="http://weststigersmembership.com.au/wp-content/uploads/allied-express.jpg" title="Allied Express" alt="Allied Express Logo">                                </a>
			</div>
			<div class="sponsors__sponsor">
				<a href="http://www.handyfinance.com.au/" target="_blank" title="Handy Finance">
					<img src="{{ $campaign->asset('images/handy_finance.png') }}" title="Best Buy Autos" alt="Best Buy Autos Logo">                                </a>
			</div>
			<div class="sponsors__sponsor">
			<a href="http://www.iscsport.com/" target="_blank" title="ISC Sport">
				<img src="http://weststigersmembership.com.au/wp-content/uploads/isc_logo.png" title="ISC Sport" alt="ISC Sport Logo">                                </a>
			</div>


			<div class="sponsors__sponsor">
				<a href="https://www.bestbuyautos.com.au/" target="_blank" title="Best Buy Autos">
					<img src="http://weststigersmembership.com.au/wp-content/uploads/best_buy_logo.png" title="Best Buy Autos" alt="Best Buy Autos Logo">                                </a>
			</div>
			<div class="sponsors__sponsor">
				<a href="http://www.releagues.com.au/" target="_blank" title="Ryde-Eastwood Leagues Club">
					<img src="http://weststigersmembership.com.au/wp-content/uploads/ryde_eastwood_logo.png" title="Ryde-Eastwood Leagues Club" alt="Ryde-Eastwood Leagues Club Logo">                                </a>
			</div>
			<div class="sponsors__sponsor small">
				<a href="https://www.victoriabitter.com.au/" target="_blank" title="Victoria Bitter">
					<img src="{{ $campaign->asset('images/vb_logo.png') }}" title="Victoria Bitter" alt="Victoria Bitter Logo">                                </a>
			</div>
			<div class="sponsors__sponsor">
				<a href="http://westsashfield.com.au/" target="_blank" title="Wests Ashfield Leagues">
					<img src="http://weststigersmembership.com.au/wp-content/uploads/wests_logo.png" title="Wests Ashfield Leagues" alt="Wests Ashfield Leagues Logo">                                </a>
			</div>
			<div class="sponsors__sponsor small">
				<a href="http://www.westslc.com.au/" target="_blank" title="Wests Let's Go">
					<img src="{{ $campaign->asset('images/west_letsgo_logo.png') }}" title="Wests Let's Go" alt="Wests Let's Go Logo">                                </a>
			</div>

			<div class="sponsors__sponsor small">
				<a href="http://www.isri.com.au/en/menue/home.html" target="_blank" title="ISRI">
					<img src="{{ $campaign->asset('images/isri_logo.png') }}" title="ISRI" alt="ISRI Logo">                                </a>
			</div>

		</div>

		

	</div>


</div>	

<footer class="footer">

    @include($campaign->view('common.footer'))

</footer>

@endsection
