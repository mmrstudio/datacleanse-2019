<?php
    if(! isset($fields[$key])) return '';
    $field = $fields[$key];
	$segment =  $record->data_segment;
    $id = isset($id) ? $id : camel_case($field->name . '_field');
    $label = isset($label) ? $label : $field->front_label;
    $control_label =  $label;
    $placeholder = isset($placeholder) ? $placeholder : $field->front_placeholder;
    $props = isset($props) ? implode(' ', $props) : false;
    $form_group_classes = ['form__group', 'form__group--' . $field->name];
    if($errors->has($field->name)) $form_group_classes[] = 'form__group--error';
    if(isset($width)) $form_group_classes[] = 'form__group--width-' . $width;
    $field_classes = ['form__control', 'form__control--text', 'form__control--' . $field->name];

	//echo $segment;
?>



@if($segment == 'Active Member')
<input type="hidden" class="{{ implode(' ', $field_classes) }}" name="{{ $field->name }}" id="yes" value="yes">

@else

<div class="{{ implode(' ', $form_group_classes) }}" @if($id)id="{{ $id }}"@endif>
    <label class="form__group__label__holder" for="{{ $field->name }}">
        
        
    </label>
    <div class="form__group__controls member">
       
        @if($control_label)
        <label class="form__group__controls__label title form__group__label" for="{{ $field->name }}">
            {{ $control_label }}
            
        </label>
        @endif
       
        
        
		<input type="radio" class="{{ implode(' ', $field_classes) }}" name="{{ $field->name }}" id="yes" value="yes"> <label for="yes" class="radiobox">YES</label>
 		 <input type="radio" class="{{ implode(' ', $field_classes) }}" name="{{ $field->name }}" id="no" value="no"> <label for="no" class="radiobox">NO</label>
    </div>
</div>

	
@endif
