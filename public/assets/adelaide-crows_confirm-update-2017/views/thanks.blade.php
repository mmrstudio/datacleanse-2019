@extends($campaign->view('main'))

@section('content')
<div class="body-wrap__inner">

	<div class="main">

        <header class="header">
            <div class="header__logo">
                <a href="{{ $campaign->client_url }}/?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank"><img src="{{ $campaign->asset('images/header-logo.png') }}"></a>
            </div>
        </header>

        <div class="form__inner">

            <div class="thanks">
                <h1 class="thanks__title">THANK YOU!</h1>
                <h2 class="thanks__sub-title" style="text-transform: uppercase">for completing our fan survey.</h2>
                <p class="thanks__body">You are now in the running to win two tickets to the Adelaide Crows 2017 Club Champion dinner. You will be notified if you are one of the lucky winners by Monday 10 July 2017.</p>
                <p class="thanks__body" style="font-size: 1.125rem; margin: 40px 0 0;"><a href="https://www.19thman.com.au/terms-conditions-7">FULL TERMS &amp; CONDITIONS</a></p>
            </div>

        </div>

	</div>

    @include($campaign->view('common.footer'))

</div>


@endsection
