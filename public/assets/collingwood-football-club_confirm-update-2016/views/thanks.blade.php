@extends($campaign->view('main'))

@section('content')

<header class="header">

    @include($campaign->view('common.header'))

    <div class="header__title">
        <div class="header__title__inner header__title__inner--thanks"></div>
    </div>

    <div class="header__player"></div>

</header>

@if($output->data_segement === 'Prospects' || $output->data_segement === 'Unrenewed')

<div class="main">

    <div class="callouts">

        <a href="https://membership.collingwoodfc.com.au/packages/view/590/Magpie-Insider" class="callouts__callout" target="_blank">
            <div class="callouts__callout__inner callouts__callout__inner--magpie-insider"></div>
        </a>

        @if($output->data_segement === 'Unrenewed')

        <div class="callouts__or">OR</div>

        <a href="https://membership.collingwoodfc.com.au/packages/view/726/Absentee-Membership-for-2016" class="callouts__callout" target="_blank">
            <div class="callouts__callout__inner callouts__callout__inner--years-service"></div>
        </a>

        @endif

    </div>

</div>

@endif

<footer class="footer">

    @include($campaign->view('common.footer'))

</footer>

@endsection
