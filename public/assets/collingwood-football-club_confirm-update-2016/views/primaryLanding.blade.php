@extends($campaign->view('main'))

@section('content')

<header class="header">

    @include($campaign->view('common.header'))

    <div class="header__title">
        <div class="header__title__inner header__title__inner--primary"></div>
    </div>

    <div class="header__player"></div>

</header>

<div class="main">

    <form action="{{ $process_url }}" method="post" class="form">

        @include($campaign->view('common.errors'))

        <div class="form__inner">

            <div class="form__title">
                <h1>Your Details</h1>
            </div>

            @if($record->data_segement == 'Members')
                @include($campaign->view('fields.text'), [
                    'key' => 'years_consecutive',
                ])
            @endif

            @include($campaign->view('fields.text'), [
                'key' => 'name_first',
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'name',
            ])

            @include($campaign->view('fields.address'), [
                'keys' => ['street_addr_1', 'street_addr_2'],
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'city',
                'id' => 'suburbField',
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'zip',
                'id' => 'postcodeField',
            ])

            @include($campaign->view('fields.country'), [
                'key' => 'country',
            ])

            @include($campaign->view('fields.state'), [
                'key' => 'state',
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'phone_mobile',
            ])

            @include($campaign->view('fields.email'), [
                'key' => 'email_addr',
            ])

            @include($campaign->view('fields.dropdown'), [
                'key' => 'gender',
            ])

            @include($campaign->view('fields.dob'), [
                'key' => 'birthdate',
                'not_required' => false,
            ])

            <div class="form__submit">

                @foreach($hidden_fields as $name=>$value)
                <input type="hidden" name="{{ $name }}" value="{{ $value }}">
                @endforeach

                <button class="form__submit__button" type="submit">Update</button>

            </div>

        </div>

    </form>

</div>

<footer class="footer">

    @include($campaign->view('common.footer'))

</footer>

@endsection
