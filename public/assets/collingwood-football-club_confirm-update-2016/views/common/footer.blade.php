
    <div class="footer__tagline">
        <div class="footer__tagline__inner"></div>
    </div>

    <div class="footer__bottom">
        <ul class="footer__bottom__links">
            <li><a href="http://www.collingwoodfc.com.au/" target="_blank">Collingwood Football Club</a></li>
            <li><a href="http://www.collingwoodfc.com.au/privacy" target="_blank">Privacy Policy</a></li>
            <li><a href="http://membership.collingwoodfc.com.au/terms-and-conditions" target="_blank">Terms &amp; Conditions</a></li>
            <li><a href="http://datacleanse.com.au" target="_blank">Product of Datacleanse</a></li>
        </ul>
    </div>
