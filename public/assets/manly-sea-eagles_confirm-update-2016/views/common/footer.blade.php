
    <div class="footer__logo">
        <img src="{{ $campaign->asset('images/logo.png') }}">
    </div>

    <p>Manly Warringah Sea Eagles | (02) 9970 3000 | <a href="mailto:membership@seaeagles.com.au">membership@seaeagles.com.au</a></p>

    <ul class="footer__links">
        <li><a href="http://www.seaeagles.com.au" target="_blank">Manly Warringah Sea Eagles</a></li>
        <li><a href="{{ $campaign->asset('Privacy_Policy.pdf') }}" target="_blank">Privacy Policy</a></li>
        <li><a href="{{ $campaign->asset('Terms_and_Conditions.pdf') }}" target="_blank">Terms &amp; Conditions</a></li>
        <li><a href="http://datacleanse.com.au" target="_blank">Product of Datacleanse</a></li>
    </ul>
