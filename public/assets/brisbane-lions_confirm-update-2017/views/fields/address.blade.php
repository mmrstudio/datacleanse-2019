<?php
    if(! isset($fields[$keys[0]])) return '';
    $field_1 = $fields[$keys[0]];
    $field_2 = isset($keys[1]) && isset($fields[$keys[1]]) ? $fields[$keys[1]] : false;
    $id = isset($id) ? $id : 'addressField';
    $props = isset($props) ? implode(' ', $props) : false;
    $label = isset($label) ? $label : $field_1->front_label;
    $placeholder_1 = isset($placeholder_1) ? $placeholder_1 : $field_1->front_placeholder;
    $placeholder_2 = isset($placeholder_2) ? $placeholder_2 : $field_2->front_placeholder;
    $form_group_classes = ['form__group', 'form__group--' . $field_1->name];
    if($errors->has($field_1->name)) $form_group_classes[] = 'form__group--error';
    $width = isset($width) ? $width : 'full';
?>

<div class="{{ implode(' ', $form_group_classes) }}" data-width="{{ $width }}" @if($id)id="{{ $id }}"@endif>
    <label class="form__group__label" for="{{ $field_1->name }}">
        {{ $label }}
        @if($field_1->required)<span class="required">*</span>@endif
    </label>
    <div class="form__group__controls">
        <input id="{{ $field_1->name }}" name="{{ $field_1->name }}" type="text" placeholder="{{ $placeholder_1 }}" class="form__control form__control--text form__control--{{ $field_1->name }}" value="{{ old($field_1->name, $record->{$field_1->name}) }}" {{ $props }}>
        <span class="form__group__border"></span>
    </div>
</div>

@if($field_2)
<div class="{{ implode(' ', $form_group_classes) }}" data-width="{{ $width }}">
    <div class="form__group__controls">
        <input id="{{ $field_2->name }}" name="{{ $field_2->name }}" type="text" placeholder="{{ $placeholder_2 }}" class="form__control form__control--text form__control--{{ $field_2->name }}" value="{{ old($field_2->name, $record->{$field_2->name}) }}" {{ $props }}>
        <span class="form__group__border"></span>
    </div>
</div>
@endif
