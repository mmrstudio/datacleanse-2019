@extends($campaign->view('main'))

@section('content')
<div class="body-wrap__inner">

	<div class="main">

        <header class="header">
            <div class="header__logo">
                <a href="{{ $campaign->client_url }}/?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank"><img src="{{ $campaign->asset('images/header-logo.png') }}"></a>
            </div>
        </header>

		<form action="{{ $process_url }}" method="post" class="form">

			@include($campaign->view('common.errors'))

			<div class="form__inner">

				<div class="form__top">
					<h1 class="form__top__title title">{{ $campaign->title }}</h1>
                    @if($campaign->creative && $campaign->creative->prize_image)
					<div class="form__top__prize"><img src="{{ $campaign->asset('images/prize-image.jpg') }}"></div>
                    @endif
                    @if($campaign->intro)
					<div class="form__top__intro intro">{{ $campaign->intro }}</div>
                    @endif
				</div>

				<div class="form__fields">

                    <h2 class="form__heading heading">Are your details correct?</h2>
                    
                    
                    @include($campaign->view('fields.text'), [
						'key' => 'name_prefix1',
                        'width' => 'half',
											 
					])

					<div class="form__group hidemobile" data-width="half" >

					</div>
                    

					@include($campaign->view('fields.text'), [
						'key' => 'name_first',
                        'width' => 'half',
											 
					])

                    @include($campaign->view('fields.text'), [
						'key' => 'name',
                        'width' => 'half',
							 
					])
					
					
					
					


                    @include($campaign->view('fields.address'), [
						'keys' => ['street_addr_1', 'street_addr_2'],
					])


								

                   @include($campaign->view('fields.text'), [
						'key' => 'city',
						'id' => 'suburbField',
                        'width' => 'third',
					])

                    @include($campaign->view('fields.state'), [
						'key' => 'state',
                        'width' => 'third',
					])

                    @include($campaign->view('fields.text'), [
						'key' => 'zip',
						'id' => 'postcodeField',
                       'width' => 'third',
					])
                   
                   
                   	@include($campaign->view('fields.country'), [
					 	'key' => 'country',
						'width' => 'half',

					])

                   
					@include($campaign->view('fields.email'), [
						'key' => 'email_addr',
                        'width' => 'half',
					])
					
					@include($campaign->view('fields.dob'), [
						'key' => 'birthdate',
						'placeholder' => 'Please provide your DOB...',
                        'width' => 'half',
                        'not_required' => $input->birthdate == 'Supplied'
					])

					
					@include($campaign->view('fields.text'), [
						'key' => 'phone_mobile',
                        'width' => 'half',
					])

					
					
					@include($campaign->view('fields.text'), [
						'key' => 'phone_home',
                        'width' => 'half',
					])
					
					
					<div class="form__group hidemobile" data-width="half" >

					</div>
					
					
					 

                    <div class="form__group question" data-width="full">
                        <label class="form__group__label" for="aboriginal">
							Do you identify as an Aboriginal or Torres Strait Islander?
						</label>
                        @include($campaign->view('fields.radio-group'), [
						'key' => 'aboriginal',
                        'name' => 'aboriginal',
                        'options' => [
                            'YES',
                            'NO',
                             
                        ],
                        'width' => 'half',
					])
                      <span class="form__group__border"></span>   
                    </div>
                    
                    <div class="form__group question" data-width="full">
                        <label class="form__group__label" for="oversea">
							Were you or your parent/s born overseas?
						</label>
                        @include($campaign->view('fields.radio-group'), [
						'key' => 'oversea',
                        'name' => 'oversea',
                        'options' => [
                            'YES',
                            'NO',
                             
                        ],
                        'width' => 'half',
					])
                        <span class="form__group__border"></span>
                         
                    </div>

				</div>

				<div class="form__submit">

					@foreach($hidden_fields as $name=>$value)
					<input type="hidden" name="{{ $name }}" value="{{ $value }}">
					@endforeach

					<button class="form__submit__button" type="submit">{{ $campaign->creative->button_text }}</button>
					<div style=" font-family: Arial, Helvetica, sans-serif;text-align: center; color:#46466; margin-top: 20px;">* Mandatory Field</div>
				</div>
				
				

			</div>

		</form>

	</div>

    @include($campaign->view('common.footer'))

</div>


@endsection
