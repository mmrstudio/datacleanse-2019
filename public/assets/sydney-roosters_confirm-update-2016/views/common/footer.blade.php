
    <div class="footer__logo">
        <img src="{{ $campaign->asset('images/logo.png') }}">
    </div>

    <div class="footer__sponsors">
        <img src="{{ $campaign->asset('images/sponsors.png') }}">
    </div>

    <p>Sydney Roosters Rugby League Football Club | 1300 327 871 | <a href="mailto:membership@sydneyroosters.com.au">membership@sydneyroosters.com.au</a></p>

    <ul class="footer__links">
        <li><a href="http://www.roosters.com.au/" target="_blank">Sydney Roosters</a></li>
        <li><a href="http://www.roosters.com.au/about/privacy-policy.html" target="_blank">Privacy Policy</a></li>
        <li><a href="http://membership.roosters.com.au/terms#T8EFAYOSAGwFVKsf.97" target="_blank">Terms &amp; Conditions</a></li>
        <li><a href="http://datacleanse.com.au" target="_blank">Product of Datacleanse</a></li>
    </ul>
