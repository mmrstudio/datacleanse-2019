
    <div class="footer__sponsors">
        <img src="{{ $campaign->asset('images/sponsors.png') }}">
    </div>

    <p>North Melbourne Football Club | 1300 KANGAS (1300 526 427) | 204-206 Arden Street, North Melbourne</p>

    <ul class="footer__links">
        <li><a href="http://www.nmfc.com.au/" target="_blank">North Melbourne Football Club</a></li>
        <li><a href="http://www.nmfc.com.au/privacy" target="_blank">Privacy Policy</a></li>
        <li><a href="https://membership.nmfc.com.au/terms--conditions" target="_blank">Terms &amp; Conditions</a></li>
        <li><a href="http://datacleanse.com.au" target="_blank">Product of Datacleanse</a></li>
    </ul>
