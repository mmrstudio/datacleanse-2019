
    export function plural(singularString, pluralString, checkNumber, exZero) {

        if(checkNumber === 0 && exZero) return singularString;

        if(checkNumber === 1) {
            return singularString;
        } else {
            return pluralString;
        }
    }

    export function getCookie(name) {
        const value = "; " + document.cookie;
        const parts = value.split("; " + name + "=");
        if (parts.length == 2) return parts.pop().split(";").shift();
    }

    export function pushArray(array, value) {
        let newArray = array.splice(0);
        newArray.push(value);
        return newArray;
    }

    export function popArray(array) {
        let newArray = array.splice(0);
        newArray.pop();
        return newArray;
    }
