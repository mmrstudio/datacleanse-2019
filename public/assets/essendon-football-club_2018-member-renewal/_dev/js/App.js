import React from 'react';
import MerchTile from './components/MerchTile';
import AddonTile from './components/AddonTile';
import Modal from './components/Modal';
import Checkbox from './components/Checkbox';
import { plural } from './utils';
import { fetchPOST } from './fetchUtils';

require('es6-promise').polyfill();
require('es6-object-assign').polyfill();

class App extends React.Component {

    constructor(props){
        super(props);

        let initialState = {
            campaign: window.appData.campaign,
            record: window.appData.record,
            fields: {},
            hiddenFields: window.appData.fields,
            addOns: window.appData.addOns,
            merchItems: window.appData.merchItems,
            memberPackage: window.appData.memberPackage,
            allowedSelections: parseInt(window.appData.allowedSelections),
            processUrl: window.appData.processUrl,
            totalSelections: 0,
            loading: false,
            submitted: false,
            termsVisible: false,
            noThanksVisible: false,
            thanksMessage: '',
            errors: []
        }

        //console.log('initialState', initialState);

        // initial merch items
        initialState.merchItems.map((merchItem, i) => {
            if(merchItem.field !== false) {
                initialState.fields[merchItem.field] = initialState.record[merchItem.field] ? initialState.record[merchItem.field] : 0;
                initialState.totalSelections += initialState.fields[merchItem.field];

                if(merchItem.sizeField !== false) {
                    initialState.fields[merchItem.sizeField] = initialState.record[merchItem.sizeField] ? initialState.record[merchItem.sizeField] : '';
                }
            }
        });

        // initial add-on selections
        initialState.addOns.map((addOn, i) => {
            const defValue = addOn.options === false ? 0 : [];
            initialState.fields[addOn.field] = initialState.record[addOn.field] ? initialState.record[addOn.field] : defValue;
        });

        // accept option
        initialState.fields.accept = initialState.record.accept ? initialState.record.accept : 'no';

        // change details option
        initialState.fields.change_details = initialState.record.change_details ? initialState.record.change_details : 'no';

        // no thanks
        initialState.fields.no_reason = initialState.record.no_reason ? initialState.record.no_reason : '';
        initialState.fields.no_reason_other = initialState.record.no_reason_other ? initialState.record.no_reason_other : '';

        //console.log('initialState', initialState);

        this.state = initialState;

        this._modal = false;

        this._handleAddonSelection = this._handleAddonSelection.bind(this);
        this._handleAcceptChange = this._handleAcceptChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        this._onSubmitResponse = this._onSubmitResponse.bind(this);
        this._renderMemberChoice = this._renderMemberChoice.bind(this);
        this._renderAddOns = this._renderAddOns.bind(this);

    }

    _showTerms(e) {
        e.preventDefault();
        this.setState({ termsVisible: true });
    }

    _showNoThanks() {
        this.setState({ noThanksVisible: true });
    }

    _handleMerchSelection(merchField, merchQty) {

        let fields = this.state.fields;

        // set current selection
        fields[merchField] = merchQty;

        // re-calculate total selections
        let totalSelections = 0;
        this.state.merchItems.map(merchItem => {
            if(merchItem.field !== false) totalSelections += fields[merchItem.field];
        });

        this.setState({
            fields: fields,
            totalSelections: totalSelections
        });
    }

    _handleAddonSelection(addonField, addonSelection) {
        let fields = this.state.fields;
        fields[addonField] = addonSelection;
        this.setState({ fields: fields });
    }

    _handleAcceptChange() {
        let fields = this.state.fields;
        fields.accept = fields.accept === 'no' ? 'yes' : 'no';
        this.setState({ fields: fields });
    }

    _handleSubmit(renewing, thanksMessage) {
        const { record, fields, hiddenFields, allowedSelections, totalSelections, processUrl, merchItems } = this.state;
        let submitErrors = [];

        if(renewing && this.state.fields.change_details === 'yes') {
            thanksMessage = thanksMessage + ' We will be in touch with you soon to update your details and/or membership.';
        }

        this.setState({ thanksMessage: thanksMessage });

        // check if all add options selected
        merchItems.map(merchItem => {
            if(merchItem.sizes !== false && merchItem.sizeField !== false) {
                if(fields[merchItem.field] === 1 && fields[merchItem.sizeField] === '') {
                    submitErrors.push(`Please choose ${merchItem.sizeLabel.toLowerCase()} for ${merchItem.name.toLowerCase()}`);
                }
            }
        });

        // check if agree
        if(renewing && fields.accept === 'no') {
            submitErrors.push(`Please agree to the membership terms & conditions.`);
        }

        if(submitErrors.length > 0) return this.setState({ errors: submitErrors });

        // update loading/submit state
        //this.setState({ submitted: false, loading: true });

        const postData = Object.assign(fields, hiddenFields);

        if(renewing) {
            postData.no_reason = '';
            postData.no_reason_other = '';
        }

        // send data
        fetchPOST(
            processUrl,
            postData,
            this._onSubmitResponse
        );

    }

    _onSubmitResponse(response) {

        if(response === true) {

            this.setState({
                submitted: true,
                loading: false,
            });

            scroll(0,0);

        } else {

            let errors = [];

            Object.keys(response).map((key) => {
                let value = response[key];
                value.map((error) => {
                    errors.push(error);
                });
            });

            this.setState({ errors: errors });

            //alert(errors.join("\r\n"));

            this.setState({
                loading: false
            });

        }

    }

    _renderMemberChoice() {

        const { record, allowedSelections, totalSelections, fields, memberPackage } = this.state;
        const selectionsRemaining = allowedSelections - totalSelections;

        return (
            <div className="member-choice">

                <div className="member-choice__inner">

                    <h2>ADD EXCLUSIVE 2018 MEMBERSHIP MERCHANDISE</h2>
                    <p>Click on an option below to add it to your purchase.</p>

                    <div className="member-choice__tiles">
                        {this.state.merchItems.map(merchItem => <MerchTile key={merchItem.field} merchItem={merchItem} merchQty={fields[merchItem.field]} allowedSelections={allowedSelections} totalSelections={totalSelections} sizeField={merchItem.sizeField} selectedSize={merchItem.sizeField ? fields[merchItem.sizeField] : false} onUpdate={this._handleMerchSelection.bind(this)} />)}
                    </div>

                </div>

            </div>
        );

    }

    _renderAddOns() {

        if(this.state.addOns.length > 0) {

            const { fields } = this.state;

            return (
                <div className="add-ons">
                    <div className="add-ons__inner">
                        <h2>Additional Upgrades</h2>
                        <div className="add-ons__tiles">
                            {this.state.addOns.map(addOn => <AddonTile key={addOn.key} addOn={addOn} addonSelections={fields[addOn.field]} addonQty={addOn.options === false ? fields[addOn.field] : fields[addOn.field].length} onUpdate={this._handleAddonSelection.bind(this)} />)}
                        </div>
                    </div>
                </div>
            );

        }

    }

    _toggleChangeDetails() {
        let fields = Object.assign({}, this.state.fields);
        fields.change_details = fields.change_details === 'yes' ? 'no' : 'yes';
        this.setState({ fields: fields });
    }

    _renderAddonTotal() {
        const { merchItems, addOns, fields, record } = this.state;
        let addonTotal = 0;
        let firstInstalment = 0;
        let merchTotal = 0;
        const initBaseCost = parseInt(record.total_purchase);
        let baseCost = parseInt(record.total_purchase);
        const hasDiscount = record.years_consecutive < 5 && fields.addon_rollover === 1;
        const hasMonthly = fields.addon_rollover === 1 && record.advantage === 0 && record.years_consecutive >= 5;

        if(hasDiscount) {
            baseCost = baseCost * 0.9;
        }

        // addons
        addOns.map(addOn => {
            const addonField = fields[addOn.field];
            const addonQty = addOn.options === false ? addonField : addonField.length;

            if(addOn.key === 'flexi' && addonQty > 0) {
                baseCost = addOn.price;
            } else {
                addonTotal = addonTotal + (addonQty * addOn.price);
            }

        });

        if(hasMonthly) {

            // merch items
            merchItems.map(merchItem => {
                const merchItemField = fields[merchItem.field];
                firstInstalment = firstInstalment + (merchItemField * merchItem.price);
            });

        } else {

            // merch items
            merchItems.map(merchItem => {
                const merchItemField = fields[merchItem.field];
                addonTotal = addonTotal + (merchItemField * merchItem.price);
                merchTotal = merchTotal + (merchItemField * merchItem.price);
            });

        }

        addonTotal = baseCost + addonTotal;

        if(hasMonthly) {
            addonTotal = addonTotal / 10;

            if(Math.round(addonTotal) !== addonTotal) {
                addonTotal = addonTotal.toFixed(2);
            }

            const firstInstalmentValue = parseFloat(addonTotal) + parseFloat(firstInstalment);

            return (
                <div className="total-addons__amount">
                    <div className="total-addons__amount__main">${addonTotal}</div>
                    {hasDiscount ? <div className="total-addons__amount__discount">Includes 10% roll over discount (${((initBaseCost * 0.1) / 10).toFixed(2)})</div> : false }
                    {firstInstalment > 0 ? <div className="total-addons__amount__first-instalment">Your first instalment will be <strong>${firstInstalmentValue.toFixed(2)}</strong> including merchandise</div> : false }
                </div>
            );

        } else {

            if(Math.round(addonTotal) !== addonTotal) {
                addonTotal = addonTotal.toFixed(2);
            }

            return (
                <div className="total-addons__amount">
                    <div className="total-addons__amount__main">${addonTotal}</div>
                    {merchTotal > 0 ? <div className="total-addons__amount__first-instalment">Includes <strong>${merchTotal.toFixed(2)}</strong> for merchandise selections</div> : false }
                    {hasDiscount ? <div className="total-addons__amount__discount">Includes 10% roll over discount (${(initBaseCost * 0.1).toFixed(2)})</div> : false }
                </div>
            );

        }

    }

    _handleNoThanksOption(option) {
        let fields = Object.assign({}, this.state.fields);
        fields.no_reason = fields.no_reason !== option ? option : '';

        if(this.state.fields.no_reason === 'Other' && fields.no_reason !== 'Other') {
            fields.no_reason_other = '';
        }

        this.setState({ fields: fields });
    }

    _handleNoThanksOther(other) {
        let fields = Object.assign({}, this.state.fields);
        fields.no_reason_other = other;

        if(fields.no_reason_other.length > 0) {
            fields.no_reason = 'Other';
        } else {
            fields.no_reason = '';
        }

        this.setState({ fields: fields });
    }

    _handleNoThanksClose() {
        let fields = Object.assign({}, this.state.fields);
        fields.no_reason = '';
        fields.no_reason_other = '';
        this.setState({ noThanksVisible: false, fields: fields });
    }

    _handleNoThanksSubmit() {
        this.setState({ noThanksVisible: false });
        this._handleSubmit(false, 'For letting us know why you won’t be renewing your membership.');
    }

    _renderNoThanksOptions() {

        const options = [
            'I can’t afford it',
            'Seating options don’t suit',
            'I prefer to watch on TV',
            'Membership doesn’t offer good value for money',
            'The membership was a gift',
            'The renewal process is too hard',
            'I can no longer attend games',
            'I didn’t feel appreciated as a member',
        ]

        let count = 1;
        let noThanksOptions = [];

        options.map(option => {
            noThanksOptions.push(
                <li key={`noThanksOption${count}`}>
                    <span>{count}.</span> {option}
                    <Checkbox class="no-thanks-modal__checkbox red" checked={this.state.fields.no_reason === option} onClick={e => this._handleNoThanksOption(option)} />
                </li>
            );
            count++;
        });

        noThanksOptions.push(
            <li key={`noThanksOption${count}`}>
                <span>{count}.</span> Other
                <input type="text" value={this.state.fields.no_reason_other} onChange={e => this._handleNoThanksOther(e.target.value)} />
                <Checkbox class="no-thanks-modal__checkbox red" checked={this.state.fields.no_reason === 'Other'} onClick={e => this._handleNoThanksOption('Other')} />
            </li>
        );

        return noThanksOptions;
    }

    render() {
        const { record, fields } = this.state;

        const pageTitle = () => {
            if(record.advantage === 0) return (<h1>Renew Your<br />Membership</h1>);
            if(record.advantage === 1) return (<h1>Membership Add-Ons</h1>);
        }

        const buttonLabel = () => {
            if(record.advantage === 0) return 'Renew Now >';
            if(record.advantage === 1) return 'Buy Now >';
        }

        const noteText = () => {
            if(record.advantage === 0) return (<div className="form__submit__note">We have your credit card ending in <strong>{record.cc_num}</strong> on file. To renew your membership with this credit card, please click "Renew Now". If this card or other details are incorrect please indicate below.</div>);
            if(record.advantage === 1) return (<div className="form__submit__note">We have your credit card ending in <strong>{record.cc_num}</strong> on file. To purchase your addons with this credit card, please click "Buy Now". If this card or other details are incorrect please indicate below.</div>);
        }

        const updateText = () => {
            if(record.advantage === 0) return (<span>Please contact me to update my credit card details</span>);
            if(record.advantage === 1) return (<span>Please contact me to update my credit card details or membership package</span>);
        }

        const totalCostLabel = () => {
            if(fields.addon_rollover === 1 && record.advantage === 0 && record.years_consecutive >= 5) {
                return 'Your monthly instalments will be:';
            } else {
                return 'Total Purchase Amount:';
            }
        }

        return (
            <div className="body-wrap">

                { this.state.submitted === true ? (
                    <div className="main">
                        <div className="thanks">
                            <div className="thanks__inner">
                                <div className="thanks__title">Thank You!</div>
                                <p>{this.state.thanksMessage}</p>
                                <div className="thanks__banner">
                                    <div className="thanks__banner__inner"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                ) : (
                    <div>

                        <div className="player-bg"></div>

                        <header className="header">
                            {pageTitle()}
                        </header>

                        <form className="main" onSubmit={e => e.preventDefault()}>

                            <div className="current-package" data-selected={fields.addon_flexi !== 1}>
                                <div className="current-package__inner">
                                    {record.pc_desc_1}
                                    <Checkbox class="current-package__checkbox" checked={fields.addon_flexi !== 1} onClick={e => {}} />
                                </div>
                            </div>

                            {this._renderAddOns()}

                            {this._renderMemberChoice()}

                            <div className="form__submit">
                                <div className="form__submit__inner">
                                    {noteText()}
                                    <div className="form__submit__change-details">
                                        <Checkbox class="form__submit__change-details__checkbox white" checked={fields.change_details === 'yes'} onClick={this._toggleChangeDetails.bind(this)} />
                                        {updateText()}
                                    </div>
                                    <div className="form__submit__accept">
                                        <Checkbox class="form__submit__accept__checkbox white" checked={fields.accept === 'yes'} onClick={this._handleAcceptChange.bind(this)} />
                                        YESS, I agree to the terms &amp; conditions (<a href="https://campaigns.datacleanse.com.au/assets/essendon-football-club_2018-member-renewal/2018-EFC-MEMBERSHIP-TERMS-AND-CONDITIONS.PDF" target="_blank">click here to view</a>)
                                    </div>
                                    <button className="form__submit__button" onClick={e => this._handleSubmit(true, 'For renewing your membership and saying YESS to Don the Sash in 2018!')} disabled={this.state.loading}>{ this.state.loading ? 'Loading...' : buttonLabel() }</button>
                                </div>
                            </div>

                            <div className="total-addons">
                                <h2 className="total-addons__title">{totalCostLabel()}</h2>
                                {this._renderAddonTotal()}
                            </div>

                            <div className="no-thanks">
                                <button className="no-thanks__button" onClick={this._showNoThanks.bind(this)}>No Thanks</button>
                            </div>


                        </form>

                        <footer className="footer">
                            <div className="footer__inner">
                                <a href="#" target="_blank" className="footer__don-the-sash">Don The Sash</a>
                            </div>
                        </footer>

                    </div>
                )}

                <Modal classes="errors-modal" visible={this.state.errors.length > 0} title="Please check the following errors:" button={'Ok'} buttonAction={() => { this.setState({ errors: [] }) }} onClose={() => { this.setState({ errors: [] }) }} ref={(modal) => { this._modal = modal; }}>
                    <div className="errors-modal__list">
                        {this.state.errors.map((error, i) => <p key={`error_${i}`}>{error}</p>)}
                    </div>
                </Modal>

                <Modal classes="no-thanks-modal" visible={this.state.noThanksVisible} title="Please let us know your reason for not wanting to renew your membership this year" button={'Submit'} buttonAction={this._handleNoThanksSubmit.bind(this)} buttonDisabled={this.state.fields.no_reason === ''} onClose={this._handleNoThanksClose.bind(this)} ref={(modal) => { this._modal = modal; }}>
                    <ul className="no-thanks-modal__options">
                        {this._renderNoThanksOptions()}
                    </ul>
                </Modal>

            </div>
        );
    }

}

export default App;
