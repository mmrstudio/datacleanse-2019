<?php

    function hasPackage($package, $package_name) {
        return strpos($package_name, $package) !== false;
    }

    //print_r($record->toArray()); exit;

    $addons = [

        'rollover' => [
            'key' => 'rollover',
            'field' => 'addon_rollover',
            'class' => 'rollover',
            'name' => false,
            'description' => $record->years_consecutive < 5 ? 'YESS, I would like 10% off my membership. Sign me up to an automatic roll over payment plan.' : 'YESS, I would love to spread the cost of my membership over 10 months by signing up to an automatic roll over payment plan.',
            'price' => false,
            'monthly' => false,
            'options' => false,
            'default' => 0,
        ],
        'grandfinal' => [
            'key' => 'grandfinal',
            'field' => 'addon_grandfinal',
            'class' => 'rollover',
            'name' => false,
            'description' => 'I definitely want guaranteed access to a Grand Final ticket if Essendon Play.',
            'price' => 55,
            'monthly' => false,
            'options' => false,
            'default' => 0,
        ],
        'flexi' => [
            'key' => 'flexi',
            'field' => 'addon_flexi',
            'class' => 'rollover',
            'name' => 'Flexi 6',
            'description' => 'I would like to upgrade my membership to the Flexi 6. ',
            'price' => 180,
            'monthly' => false,
            'options' => false,
            'default' => 0,
        ],

    ];

    $member_addons = [];

    if(! $record->advantage) {
        $member_addons[] = $addons['rollover'];
    }

    if(hasPackage('Bronze', $record->pc_desc_1) || (hasPackage('Flexi', $record->pc_desc_1) && !hasPackage('Flexi 3', $record->pc_desc_1)) || hasPackage('BTB', $record->pc_desc_1) || hasPackage('Essendon Insider', $record->pc_desc_1) || hasPackage('Digital', $record->pc_desc_1)) {
        if(! $record->gf) $member_addons[] = $addons['grandfinal'];
    }

    if(hasPackage('Flexi 3', $record->pc_desc_1)) {
        $member_addons[] = $addons['flexi'];
    }

    //print_r($member_addons); exit;

    $merch_items = [
        [
            'key' => 'beanie',
            'field' => 'merch_beanie',
            'name' => 'Beanie',
            'price' => 15,
            'sizes' => false,
            'sizeLabel' => false,
            'sizeField' => false,
        ],
        [
            'key' => 'cap',
            'field' => 'merch_cap',
            'name' => 'Cap',
            'price' => 15,
            'sizes' => false,
            'sizeLabel' => false,
            'sizeField' => false,
        ],
        [
            'key' => 'hood_youth',
            'field' => 'merch_hood_youth',
            'name' => 'Hood Youth',
            'price' => 45,
            'sizes' => ['6','8','10','12','14','16'],
            'sizeLabel' => 'Size',
            'sizeField' => 'merch_hood_youth_size',
        ],
        [
            'key' => 'polo',
            'field' => 'merch_polo',
            'name' => 'Polo',
            'price' => 30,
            'sizes' => ['XX-Small', 'X-Small', 'Small', 'Medium', 'Large', 'X-Large', '2X-Large', '3X-Large', '4X-Large', '5X-Large', '7X-Large'],
            'sizeLabel' => 'Size',
            'sizeField' => 'merch_polo_size',
        ],
        [
            'key' => 'hood_adult',
            'field' => 'merch_hood_adult',
            'name' => 'Hood Adult',
            'price' => 50,
            'sizes' => ['XX-Small', 'X-Small', 'Small', 'Medium', 'Large', 'X-Large', '2X-Large', '3X-Large', '4X-Large', '5X-Large', '7X-Large'],
            'sizeLabel' => 'Size',
            'sizeField' => 'merch_hood_adult_size',
        ],
    ];

    $app_data = [
        'campaign' => $campaign,
        'record' => $record,
        'fields' => $hidden_fields,
        'allowedSelections' => 1,
        'processUrl' => $process_url,
        'addOns' => $member_addons,
        'merchItems' => $merch_items,
    ];

    //dd($app_data);
    //print_r($app_data); exit;

?><!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">

    <head>

    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    	<title>{{ $campaign->campaignTitle() }}</title>

    	<link href="{{ $campaign->asset('dist/bundle.css') }}" rel="stylesheet">
        <link rel="icon" href="{{ $campaign->asset('icon.ico') }}" />

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-33851616-15', 'auto');
          ga('send', 'pageview');

        </script>

        <link href="https://fonts.googleapis.com/css?family=Poppins:400,700" rel="stylesheet">

        <script>
            window._token = '{{ csrf_token() }}';
            window.appData = {!! json_encode($app_data) !!};
        </script>

    </head>

    <body>
        <div id="app"></div>
        <div class="incompatible" id="incompatible">
            <div class="incompatible__inner">
                <div class="incompatible__text">
                    <p class="title">Please upgrade your browser</p>
                    <p class="description">Unfortunately your browser doesn't support this site.<br>Please use one of the following broswers:<br>Google Chrome, Firefox, Safari, Internet Explorer 11 or Microsoft Edge</p>
                </div>
            </div>
        </div>
        <script src="{{ $campaign->asset('dist/bundle.js') }}"></script>
    </body>

</html>
