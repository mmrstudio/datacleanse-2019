
<div class="add-ons__tile">

    <div class="add-ons__tile__inner">

        <div class="add-ons__tile__image polo"></div>
        <div class="add-ons__tile__desc">It’s back due to popular demand in 2017 – this season’s member polo features a new design. Sizes XXS to 7XL. Get in fast to secure your size before they sell out!</div>
        <div class="add-ons__tile__note">Monthly payment option available.</div>
        <div class="add-ons__tile__options">

            <div class="add-ons__tile__options__select">
                <select name="addon_polo_size">
                    <option value="">Size</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == 'XXS') selected @endif>XXS</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == 'XS') selected @endif>XS</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == 'S') selected @endif>S</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == 'M') selected @endif>M</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == 'L') selected @endif>L</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == 'XL') selected @endif>XL</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == '2XL') selected @endif>2XL</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == '3XL') selected @endif>3XL</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == '4XL') selected @endif>4XL</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == '5XL') selected @endif>5XL</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == '6XL') selected @endif>6XL</option>
                    <option @if(old('addon_polo_size', $record->addon_polo_size) == '7XL') selected @endif>7XL</option>
                </select>
                <svg viewBox="0 0 18 10"><path d="M9,10c-0.4,0-0.8-0.1-1.1-0.4L0.4,2.4c-0.6-0.6-0.6-1.5,0-2C1-0.1,2-0.1,2.6,0.4L9,6.6l6.4-6.1c0.6-0.6,1.5-0.6,2.1,0 c0.6,0.6,0.6,1.5,0,2l-7.5,7.1C9.8,9.9,9.4,10,9,10z"/></svg> 
            </div>

            <label class="add-ons__tile__options__tick add-ons__tile__label">
                <input type="checkbox" name="addon_turbo" value="yes" {{ old('addon_turbo', $record->addon_turbo) === 'yes' ? 'checked' : '' }}>
                <span class="add-ons__tile__label__bg">
                    <span class="add-ons__tile__label__bg__inner">
                        <span class="add-ons__tile__label__text">$40</span>
                        <span class="add-ons__tile__label__checkbox">
                            <svg viewBox="0 0 40 40"><polygon points="14.1,31.7 6,23.3 8.6,20.9 14.2,26.7 31.7,9.1 34.2,11.6"/></svg>
                        </span>
                    </span>
                </span>
            </label>

        </div>

    </div>

</div>
