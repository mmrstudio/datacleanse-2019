<?php
    if(! isset($fields[$key])) return '';
    $field = $fields[$key];
    $id = isset($id) ? $id : camel_case($field->name . '_field');
    $label = isset($label) ? $label : $field->front_label;
    $control_label = isset($control_label) ? $control_label : false;
    $placeholder = isset($placeholder) ? $placeholder : $field->front_placeholder;
    $props = isset($props) ? implode(' ', $props) : false;
    $form_group_classes = ['form__group', 'form__group--' . $field->name];
    if($errors->has($field->name)) $form_group_classes[] = 'form__group--error';
    $field_classes = ['form__control', 'form__control--textarea', 'form__control--' . $field->name];
    $width = isset($width) ? $width : 'full';
?>

<div class="{{ implode(' ', $form_group_classes) }}" data-width="{{ $width }}" @if($id)id="{{ $id }}"@endif>
    <div class="form__group__controls">
        <textarea id="{{ $field->name }}" name="{{ $field->name }}" placeholder="{{ $placeholder }}" class="{{ implode(' ', $field_classes) }}" {{ $props }} rows="5">{{ old($field->name, $record->{$field->name}) }}</textarea>
    </div>
</div>
