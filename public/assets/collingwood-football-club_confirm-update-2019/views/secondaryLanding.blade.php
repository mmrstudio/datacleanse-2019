@extends($campaign->view('main'))

@section('content')
<div class="body-wrap__inner">

	<div class="main">

        <header class="header">
            <div class="header__logo">
                <a href="{{ $campaign->client_url }}/?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank"><img src="{{ $campaign->asset('images/header-logo.png') }}"></a>
            </div>
        </header>


        <form action="{{ $process_url }}" method="post" class="form">

			@include($campaign->view('common.errors'))

			<div class="form__inner">

                <div class="thanks">

                    <h1 class="thanks__title">THANK YOU!</h1>
                    <h2 class="thanks__sub-title" style="text-transform: uppercase">For confirming or updating your details! You are now in the running to win one of five free Memberships for 2018! </h2>
                    <p class="thanks__body">For your chance to win two tickets to the 2017 Crows Club Champion dinner, please complete this short fan survey.</p>

                </div>

            </div>

            <div class="form__inner" style="background-color: #002a5b;">

				<div class="form__fields form__fields--secondary">

                    <h2 class="form__heading heading" style="color:#fff; font-size: 3.125rem">Survey</h2>

                    <h3 class="form__sub-heading afc_survey_heading">1. Who is your favourite current Adelaide Crows AFL player?</h3>

					@include($campaign->view('fields.dropdown'), [
                        'label' => false,
						'key' => 'survey_favourite_player',
                        'options' => [
                            'Jonathon Beech' => 'Jonathon Beech',
                            'Brad Crouch' => 'Brad Crouch',
                            'Riley Knight' => 'Riley Knight',
                            'Josh Jenkins' => 'Josh Jenkins',
                            'Scott Thomspon' => 'Scott Thomspon',
                            'Jake Lever' => 'Jake Lever',
                            'Jordan Gallucci' => 'Jordan Gallucci',
                            'Jake Kelly' => 'Jake Kelly',
                            'Rory Sloane' => 'Rory Sloane',
                            'Harrison Wigg' => 'Harrison Wigg',
                            'Paul Seedsman' => 'Paul Seedsman',
                            'Daniel Talia' => 'Daniel Talia',
                            'Taylor Walker' => 'Taylor Walker',
                            'David Mackay' => 'David Mackay',
                            'Kyle Hartigan' => 'Kyle Hartigan',
                            'Luke Brown' => 'Luke Brown',
                            'Curtly Hampton' => 'Curtly Hampton',
                            'Eddie Betts' => 'Eddie Betts',
                            'Hugh Greenwood' => 'Hugh Greenwood',
                            'Rory Atkins' => 'Rory Atkins',
                            'Andrew Otten' => 'Andrew Otten',
                            'Charlie Cameron' => 'Charlie Cameron',
                            'Sam Jacobs' => 'Sam Jacobs',
                            'Kyle Cheney' => 'Kyle Cheney',
                            'Richard Doulgas' => 'Richard Doulgas',
                            'Tom Lynch' => 'Tom Lynch',
                            'Cameron Ellis-Yolmen' => 'Cameron Ellis-Yolmen',
                            'Rory Laird' => 'Rory Laird',
                            'Wayne Milera' => 'Wayne Milera',
                            'Myles Poholke' => 'Myles Poholke',
                            'Troy Menzel' => 'Troy Menzel',
                            'Brodie Smith' => 'Brodie Smith',
                            'Elliott Himmelberg' => 'Elliott Himmelberg',
                            'Matt Signorello' => 'Matt Signorello',
                            'Dean Gore' => 'Dean Gore',
                            'Paul Hunter' => 'Paul Hunter',
                            'Harry Dear' => 'Harry Dear',
                            'Tom Doedee' => 'Tom Doedee',
                            'Ben Davis' => 'Ben Davis',
                            'Mitch McGovern' => 'Mitch McGovern',
                            'Alex Keath' => 'Alex Keath',
                            'Reilly O’Brien' => 'Reilly O’Brien',
                            'Matthew Crouch' => 'Matthew Crouch',
                            'Ben Jarman' => 'Ben Jarman',
                        ],
					])

                    <h3 class="form__sub-heading afc_survey_heading">2. When it comes to the Adelaide Football Club I consider myself a...</h3>

                    <div class="form__group" data-width="full">
                        <div class="form__group__controls">
                            <ul class="afc_range">
                                <li>Casual Observer</li>
                                <li><label><input name="survey_consider" type="radio" value="1" {{ old('survey_consider', $record->survey_consider) == '1' ? 'checked' : '' }}><span>1</span></label></li>
                                <li><label><input name="survey_consider" type="radio" value="2" {{ old('survey_consider', $record->survey_consider) == '2' ? 'checked' : '' }}><span>2</span></label></li>
                                <li><label><input name="survey_consider" type="radio" value="3" {{ old('survey_consider', $record->survey_consider) == '3' ? 'checked' : '' }}><span>3</span></label></li>
                                <li><label><input name="survey_consider" type="radio" value="4" {{ old('survey_consider', $record->survey_consider) == '4' ? 'checked' : '' }}><span>4</span></label></li>
                                <li><label><input name="survey_consider" type="radio" value="5" {{ old('survey_consider', $record->survey_consider) == '5' ? 'checked' : '' }}><span>5</span></label></li>
                                <li><label><input name="survey_consider" type="radio" value="6" {{ old('survey_consider', $record->survey_consider) == '6' ? 'checked' : '' }}><span>6</span></label></li>
                                <li><label><input name="survey_consider" type="radio" value="7" {{ old('survey_consider', $record->survey_consider) == '7' ? 'checked' : '' }}><span>7</span></label></li>
                                <li>Hardcore Fanatic</li>
                            </ul>
                        </div>
                    </div>

                    <h3 class="form__sub-heading afc_survey_heading">3. Which of the following best represents your life stage? (tick one)</h3>

                    @include($campaign->view('fields.radio-group'), [
						'key' => 'survey_life_stage',
                        'name' => 'survey_life_stage',
                        'options' => [
                            'Single living alone',
                            'Family with children living at home (where youngest child is 6-14yrs)',
                            'Group of adults living in shared accommodation',
                            'Family with children living at home (where youngest child is over 15yrs)',
                            'Family without children',
                            'Single/Couple family with children who have left home',
                            'Family with children living at home (where youngest child is 5yrs or under)',
                            'Other family',
                        ],
                        'width' => 'half',
					])

                    <h3 class="form__sub-heading afc_survey_heading">4. Which of the following do you use (tick all that apply)</h3>

                    @include($campaign->view('fields.checkbox-group'), [
						'key' => 'survey_social',
                        'name' => 'survey_social[]',
                        'options' => [
                            'Facebook',
                            'Twitter',
                            'Instagram',
                            'Snapchat',
                            'Periscope',
                            'Pinterest',
                            'YouTube',
                            'Google Plus',
                            'None of the above',
                        ],
                        'width' => 'third',
					])

                    <h3 class="form__sub-heading afc_survey_heading">5. Considering the historic season of the Adelaide Crows in the inaugural AFL Women’s Competition, please choose the statement that best suits you:</h3>

                    @include($campaign->view('fields.radio-group'), [
						'key' => 'survey_aflw',
                        'name' => 'survey_aflw',
                        'options' => [
                            'I attended an Adelaide Crows AFL Women’s match in 2017',
                            'I did not attend an Adelaide Crows AFL Women’s match in 2017, but am open to attending next season',
                            'I support the Adelaide Crows in AFL Women’s Competition, but prefer to watch it on TV',
                            'I am not interested in the Adelaide Crows AFL Women’s Team',
                            'Other',
                        ],
                        'width' => 'full',
					])

				</div>

				<div class="form__submit">

					@foreach($hidden_fields as $name=>$value)
					<input type="hidden" name="{{ $name }}" value="{{ $value }}">
					@endforeach

					<button class="form__submit__button" type="submit">{{ $campaign->creative->button_text }}</button>

				</div>

			</div>

		</form>

	</div>

    @include($campaign->view('common.footer'))

</div>


@endsection
