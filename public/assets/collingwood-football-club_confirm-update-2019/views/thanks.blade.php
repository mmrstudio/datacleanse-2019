@extends($campaign->view('main'))

@section('content')

<header class="header">

		<a class="desktop_banner" href="{{ $campaign->client_url }}/?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank"><img src="{{ $campaign->asset('images/hero-banner.jpg') }}"></a>

		<a class="mobile_banner" href="{{ $campaign->client_url }}/?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank"><img src="{{ $campaign->asset('images/mobile-banner.jpg') }}"></a>

</header>
<div class="body-wrap__inner">

	<div class="main">

        <div class="form__inner">

            <div class="thanks">
							@if($campaign->thanks_title)
							<h1 class="thanks__title">{{ $campaign->thanks_title }}</h1>
							@endif

							@if($campaign->thanks_title)
							<h2 class="thanks__sub-title">{!! $campaign->thanks_sub_title !!}</h2>
							@endif

							@if($campaign->thanks_title)
							<p class="thanks__body">{!! $campaign->thanks_body !!}</p>
							@endif
            </div>

        </div>

	</div>

    @include($campaign->view('common.footer'))

</div>


@endsection
