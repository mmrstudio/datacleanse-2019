var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var webpack = require('webpack');

module.exports.getConfig = function(type) {

    var config = {
        development: {
            devtool: 'cheap-module-source-map',
            entry: './_dev/js/index.js',
            output: {
                path: __dirname,
                filename: 'bundle.js'
            },
            module: {
                rules: [
                    {
                        test: /\.js$/,
                        exclude: /(node_modules|bower_components)/,
                        use: {
                            loader: 'babel-loader',
                            options: {
                                presets: ['es2015', 'react']
                            }
                        }
                    },
                    {
                        test: /\.scss$/,
                        use: ExtractTextPlugin.extract({
                            fallback: 'style-loader',
                            //resolve-url-loader may be chained before sass-loader if necessary
                            use: ['css-loader', 'sass-loader']
                        })
                    },
                    { test: /\.(png|woff|woff2|eot|ttf|svg|gif|jpg)$/, loader: 'file-loader?name=[path][name].[ext]' }
                ]
            },
            resolve: {
                extensions: ['.js']
            },
            plugins: [
                new webpack.DefinePlugin({'process.env': { 'NODE_ENV': JSON.stringify('production') } }),
                new ExtractTextPlugin('bundle.css'),
                new CleanWebpackPlugin(['./dist'], { root: __dirname, verbose: true, dry: false }),
                new webpack.optimize.UglifyJsPlugin({
                  //include: /\.min\.js$/,
                  compress: {
                    warnings: false,
                    screw_ie8: true
                  },
                  comments: false,
                  sourceMap: false
                })
            ]
        }
    };

    return config[type];

}
