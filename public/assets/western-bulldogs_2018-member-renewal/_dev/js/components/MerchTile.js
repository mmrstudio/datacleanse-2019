import React from 'react';
import { plural } from '../utils';

class MerchTile extends React.Component {

    constructor(props){
        super(props);

        this._toggleMerchQty = this._toggleMerchQty.bind(this);
        this._incrementMerchQty = this._incrementMerchQty.bind(this);
        this._decrementMerchQty = this._decrementMerchQty.bind(this);

    }

    _toggleMerchQty() {
        this.props.onUpdate(this.props.merchKey, this.props.merchQty === 0 ? 1 : 0);
    }

    _incrementMerchQty() {
        if(this.props.totalSelections < this.props.allowedSelections) {
            this.props.onUpdate(this.props.merchKey, this.props.merchQty + 1);
        }
    }

    _decrementMerchQty() {
        if(this.props.merchQty > 0) {
            this.props.onUpdate(this.props.merchKey, this.props.merchQty - 1);
        }
    }

    render() {

        const decrementDisabled = this.props.merchQty <= 0;
        const incrementDisabled = this.props.totalSelections >= this.props.allowedSelections;

        const renderOptions = () => {

            //console.log('this.props.allowedSelections', this.props.allowedSelections);

            if(this.props.merchField !== false) {

                if(this.props.allowedSelections === 1) {
                    return (
                        <div className="member-choice__tile__checkbox">
                            <div className="member-choice__tile__checkbox__inner">
                                <svg viewBox="0 0 40 40"><polygon points="14.1,31.7 6,23.3 8.6,20.9 14.2,26.7 31.7,9.1 34.2,11.6"/></svg>
                            </div>
                        </div>
                    )
                } else {
                    return (
                       <div className="member-choice__tile__qty">
                           <button className="member-choice__tile__qty__button member-choice__tile__qty__button--decrement" disabled={decrementDisabled} onClick={this._decrementMerchQty}>
                               <svg viewBox="0 0 16 16"><path d="M15.7,6.1c-0.2-0.2-0.5-0.3-0.8-0.3H1.1c-0.3,0-0.6,0.1-0.8,0.3C0.1,6.3,0,6.6,0,6.9v2.2c0,0.3,0.1,0.6,0.3,0.8 c0.2,0.2,0.5,0.3,0.8,0.3h13.8c0.3,0,0.6-0.1,0.8-0.3C15.9,9.7,16,9.4,16,9.1V6.9C16,6.6,15.9,6.3,15.7,6.1z"/></svg>
                           </button>
                           <div className="member-choice__tile__qty__number">
                               <span>{this.props.merchQty}</span>
                           </div>
                           <button className="member-choice__tile__qty__button member-choice__tile__qty__button--increment" disabled={incrementDisabled} onClick={this._incrementMerchQty}>
                               <svg viewBox="0 0 16 16"><path d="M15.7,6.1c-0.2-0.2-0.5-0.3-0.8-0.3h-4.7V1.1c0-0.3-0.1-0.6-0.3-0.8C9.7,0.1,9.4,0,9.1,0H6.9C6.6,0,6.3,0.1,6.1,0.3 C5.9,0.5,5.8,0.8,5.8,1.1v4.7H1.1c-0.3,0-0.6,0.1-0.8,0.3C0.1,6.3,0,6.6,0,6.9v2.2c0,0.3,0.1,0.6,0.3,0.8c0.2,0.2,0.5,0.3,0.8,0.3 h4.7v4.7c0,0.3,0.1,0.6,0.3,0.8C6.3,15.9,6.6,16,6.9,16h2.2c0.3,0,0.6-0.1,0.8-0.3c0.2-0.2,0.3-0.5,0.3-0.8v-4.7h4.7 c0.3,0,0.6-0.1,0.8-0.3C15.9,9.7,16,9.4,16,9.1V6.9C16,6.6,15.9,6.3,15.7,6.1z"/></svg>
                           </button>
                       </div>
                   )
                }
            } else {
                return null;
            }

        }

        return (
            <div className="member-choice__tile" data-selected={this.props.merchQty > 0}>
                {this.props.allowedSelections === 1 ? <div className="member-choice__tile__overlay" onClick={this._toggleMerchQty}></div> : false}
                <div className={`member-choice__tile__image ${this.props.merchKey}`}></div>
                <div className="member-choice__tile__bottom">
                    {renderOptions()}
                    <div className="member-choice__tile__label">{this.props.label}</div>
                </div>
            </div>
        );
    }

}

MerchTile.contextTypes = {
    //router: React.PropTypes.object.isRequired
};

export default MerchTile;
