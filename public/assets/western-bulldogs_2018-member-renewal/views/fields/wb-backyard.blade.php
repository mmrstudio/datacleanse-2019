<div class="add-ons__tile">

    <div class="add-ons__tile__inner">

        <div class="add-ons__tile__image backyard"></div>
        <div class="add-ons__tile__desc">Your donation of just $55 provides a seat in the Bulldogs Backyard for nine children throughout the season. Each donor receives a special certificate of appreciation and makes an excellent gift idea or add-on to an existing membership.</div>
        <div class="add-ons__tile__note">Monthly payment option available.</div>
        <div class="add-ons__tile__options">

            <label class="add-ons__tile__options__tick add-ons__tile__label">
                <input type="checkbox" name="addon_backyard" value="yes" {{ old('addon_backyard', $record->addon_backyard) === 'yes' ? 'checked' : '' }}>
                <span class="add-ons__tile__label__bg">
                    <span class="add-ons__tile__label__bg__inner">
                        <span class="add-ons__tile__label__text">$55</span>
                        <span class="add-ons__tile__label__checkbox">
                            <svg viewBox="0 0 40 40"><polygon points="14.1,31.7 6,23.3 8.6,20.9 14.2,26.7 31.7,9.1 34.2,11.6"/></svg>
                        </span>
                    </span>
                </span>
            </label>

        </div>

    </div>

</div>
