<?php

    $get_member_package_info = DB::select('select _campaign_0039_lookup.* from _campaign_0039_lookup where _campaign_0039_lookup.event_name = ? and _campaign_0039_lookup.price_code = ?', [
        $record->event_name_1,
        $record->pc_1
    ]);

    $member_package_info = false;
    if($get_member_package_info)
    {
        $member_package_info = $get_member_package_info[0];
    }

    //print_r($member_package_info); exit;

    $addons = [

        'A1' => [
            'field' => 'addon_aflw_membership',
            'class' => 'aflw-membership',
            'name' => 'AFLW Membership',
            'description' => 'Women\'s football is now firmly entrenched in the fabric of the Club. Join us as we embark upon a second season.',
            'price' => '$50',
            'monthly' => true,
            'dropdown' => false
        ],

        'A2' => [
            'field' => 'addon_aflw_ambassador',
            'class' => 'aflw-ambassador',
            'name' => 'AFLW Ambassador',
            'description' => 'Show your pride in AFLW be joining as an AFLW Ambassador. Get access to an exclusive AFLW event as well as other benefits.',
            'price' => '$200',
            'monthly' => true,
            'dropdown' => false
        ],

        'A12' => [
            'field' => 'addon_aflw_thankyou',
            'class' => 'aflw-thankyou',
            'name' => 'AFLW Thank You',
            'description' => 'Women’s footy is #RealStrength. Without your support our women couldn\'t have realised their AFLW dreams. Thank you for being part of the journey.',
            'price' => false,
            'monthly' => false,
            'dropdown' => false
        ],

        'A11' => [
            'field' => 'addon_baby',
            'class' => 'baby',
            'name' => 'Baby Bulldog',
            'description' => 'You\'re never too young to be a member. Baby Bulldog memberships include: Baby beanie, Milestone Cards and Born a Bulldog Certificate.',
            'price' => '$40',
            'monthly' => true,
            'dropdown' => false
        ],

        'A8' => [
            'field' => 'addon_backyard',
            'class' => 'backyard',
            'name' => 'Bulldogs Backyard',
            'description' => 'Your donation of just $55 provides a seat in the Bulldogs Backyard for nine children throughout the season. Each donor receives a special certificate of appreciation.',
            'price' => '$55',
            'monthly' => true,
            'dropdown' => false
        ],

        'A6' => [
            'field' => 'addon_cap',
            'class' => 'cap',
            'name' => 'Member Cap',
            'description' => 'Show your pride with your 2018 Asics member cap.',
            'price' => '$20',
            'monthly' => true,
            'dropdown' => false
        ],

        'A9' => [
            'field' => 'addon_mug',
            'class' => 'mug',
            'name' => 'Member Mug',
            'description' => '2018 Exclusive Member mug - featuring Marcus Bontempelli.',
            'price' => '$15',
            'monthly' => true,
            'dropdown' => false
        ],

        'A10' => [
            'field' => 'addon_pet',
            'class' => 'pet',
            'name' => 'Pet Membership',
            'description' => 'The perfect gift for pets joining the pack. Includes certificate, pet sign and pet bag holder.',
            'price' => '$45',
            'monthly' => true,
            'dropdown' => false
        ],

        'A5' => [
            'field' => 'addon_polo',
            'class' => 'polo',
            'name' => 'Member Polo',
            'description' => 'The new Asics 2018 Polo is exclusive to members. Buy early to secure your size before they sell out.',
            'price' => '$40',
            'monthly' => true,
            'dropdown' => [
                'label' => 'Size',
                'field' => 'addon_polo_size',
                'options' => ['XXS', 'XS', 'S', 'M', 'L', 'XL', '2XL', '3XL', '4XL', '5XL', '7XL']
            ]
        ],

        'A4' => [
            'field' => 'addon_turbo',
            'class' => 'turbo',
            'name' => 'Turbo Pack',
            'description' => 'The complete member kit with all the essentials. Includes member-only polo, cap and mug. Buy early to secure your size before they sell out.',
            'price' => '$60',
            'monthly' => true,
            'dropdown' => [
                'label' => 'Size',
                'field' => 'addon_turbo_size',
                'options' => ['XXS', 'XS', 'S', 'M', 'L', 'XL', '2XL', '3XL', '4XL', '5XL', '7XL']
            ]
        ],

        'A3' => [
            'field' => 'addon_vest',
            'class' => 'vest',
            'name' => 'Member Vest',
            'description' => 'New to 2018 the Reversible Member Vest. The exclusive member only vest is the perfect winter warmer. Buy early to secure your size before they sell out.',
            'price' => '$80',
            'monthly' => true,
            'dropdown' => [
                'label' => 'Size',
                'field' => 'addon_vest_size',
                'options' => ['XXS', 'XS', 'S', 'M', 'L', 'XL', '2XL', '3XL', '4XL', '5XL', '7XL']
            ]
        ],

        'A7' => [
            'field' => 'addon_footscray',
            'class' => 'footscray',
            'name' => 'Footscray Membership',
            'description' => 'A Footscray Bulldogs Membership gives you access* to all home matches the Western Bulldogs standalone VFL team play at Victoria University Whitten Oval.',
            'price' => '$60',
            'monthly' => true,
            'dropdown' => false
        ],

    ];

    // append relevant addons
    $member_addons = [];
    if($member_package_info->addon_1) $member_addons[] = $addons[$member_package_info->addon_1];
    if($member_package_info->addon_2) $member_addons[] = $addons[$member_package_info->addon_2];
    if($member_package_info->addon_3) $member_addons[] = $addons[$member_package_info->addon_3];
    if($member_package_info->addon_4) $member_addons[] = $addons[$member_package_info->addon_4];
    if($member_package_info->addon_5) $member_addons[] = $addons[$member_package_info->addon_5];
    if($member_package_info->addon_6) $member_addons[] = $addons[$member_package_info->addon_6];
    //print_r($member_addons); exit;
    //$member_addons[] = $addons['A7'];

    $merch_items = [
        'B1' => [
            'key' => 'cap',
            'field' => 'merch_cap',
            'label' => [
                'one' => 'I want a cap',
                'multiple' => ['singular' => 'Cap', 'plural' => 'Caps']
            ]
        ],
        'B2' => [
            'key' => 'beanie',
            'field' => 'merch_beanie',
            'label' => [
                'one' => 'I want a beanie',
                'multiple' => ['singular' => 'Beanie', 'plural' => 'Beanies']
            ]
        ],
        'B3' => [
            'key' => 'scarf',
            'field' => 'merch_scarf',
            'label' => [
                'one' => 'I want a scarf',
                'multiple' => ['singular' => 'Scarf', 'plural' => 'Scarves']
            ]
        ],
        'B4' => [
            'key' => 'no_scarf',
            'field' => false,
            'label' => [
                'one' => 'Choose a cap or beanie',
                'multiple' => ['singular' => '', 'plural' => '']
            ]
        ]
    ];

    // append merch items
    $member_merch_items = [];
    if($member_package_info->choice_1) $member_merch_items[] = $merch_items[$member_package_info->choice_1];
    if($member_package_info->choice_2) $member_merch_items[] = $merch_items[$member_package_info->choice_2];
    if($member_package_info->choice_3) $member_merch_items[] = $merch_items[$member_package_info->choice_3];

    //$member_merch_items[] = $merch_items['B4'];
    //print_r($member_merch_items); exit;

    $app_data = [
        //'campaign' => $campaign,
        //'record' => $record,
        //'fields' => $hidden_fields,
        'allowedSelections' => $member_package_info->MC == 'Yes' ? $member_package_info->member_choice : 0,
        'memberPackage' => $member_package_info,
        'processUrl' => '/' . $process_url,
        'addOns' => $member_addons,
        'merchItems' => $member_merch_items,
    ];

    print_r($app_data); exit;

?><!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">

    <head>

    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    	<title>{{ $campaign->campaignTitle() }}</title>

    	<link href="{{ $campaign->asset('dist/bundle.css') }}" rel="stylesheet">
        <link rel="icon" href="{{ $campaign->asset('icon.ico') }}" />

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-33851616-15', 'auto');
          ga('send', 'pageview');

        </script>

        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700|Arvo:400italic,400,700,700italic' rel='stylesheet' type='text/css'>

        <script>
            window._token = '{{ csrf_token() }}';
            window.appData = {!! json_encode($app_data) !!};
        </script>

    </head>

    <body>
        <div id="app"></div>
        <div class="incompatible" id="incompatible">
            <div class="incompatible__inner">
                <div class="incompatible__text">
                    <p class="title">Please upgrade your browser</p>
                    <p class="description">Unfortunately your browser doesn't support this site.<br>Please use one of the following broswers:<br>Google Chrome, Firefox, Safari, Internet Explorer 11 or Microsoft Edge</p>
                </div>
            </div>
        </div>
        <script src="{{ $campaign->asset('dist/bundle.js') }}"></script>
    </body>

</html>
