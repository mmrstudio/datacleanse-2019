@extends($campaign->view('main'))

@section('content')

<header class="header header--thanks">

    @include($campaign->view('common.headerthankyou'))

   

</header>

<footer class="footer  thanks">

    @include($campaign->view('common.footer'))

</footer>

@endsection
