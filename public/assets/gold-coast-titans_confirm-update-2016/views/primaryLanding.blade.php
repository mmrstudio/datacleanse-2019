@extends($campaign->view('main'))

@section('content')

<header class="header">

    @include($campaign->view('common.header'))

    

</header>

<div class="main">

    <form action="{{ $process_url }}" method="post" class="form">

        @include($campaign->view('common.errors'))

        <div class="form__inner">

            <div class="form__intro">
                <div class="form__intro__logo"></div>
                @if($record->first_name != '')
                <div class="form__intro__greeting">Dear {{ $record->first_name }},</div>
                @endif
                <div class="form__intro__text">
                    As we prepare to launch 2017 season membership we want to make sure we have the correct details for you. <br><br>
Confirm or update your details below by midnight Wednesday 7th September and you could <strong>WIN</strong> a <strong>2017 signed jersey.</strong>
                </div>
            </div>

            <div class="form__title">
                <h1>Your Details</h1>
            </div>

            @include($campaign->view('fields.text'), [
                'key' => 'first_name',
               
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'last_name',
               
            ])


            @include($campaign->view('fields.address'), [
                'keys' => ['street_addr_1', 'street_addr_2'],
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'city',
                'id' => 'suburbField',
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'postcode',
            ])

            @include($campaign->view('fields.country'), [
                'key' => 'country',
            ])

            @include($campaign->view('fields.state'), [
                'key' => 'state',
            ])



            @include($campaign->view('fields.email'), [
                'key' => 'email_addr',
            ])

            @include($campaign->view('fields.dob'), [
                'key' => 'birth_date',
                'not_required' => false,
            ])

            @include($campaign->view('fields.text'), [
                'key' => 'phone_mobile',
            ])

            <div class="form__submit">

                @foreach($hidden_fields as $name=>$value)
                <input type="hidden" name="{{ $name }}" value="{{ $value }}">
                @endforeach

                <button class="form__submit__button" type="submit">Update</button>

            </div>

           
        </div>

    </form>

</div>

<footer class="footer">

    @include($campaign->view('common.footer'))

</footer>

@endsection
