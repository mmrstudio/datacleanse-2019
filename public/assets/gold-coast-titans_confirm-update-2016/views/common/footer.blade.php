	<img src="{{ $campaign->asset('images/footer-bg.jpg') }}" title="banner">

		<br><br><br>
    <p>Gold Coast Titans | (07) 5656 5656 | <a href="mailto:membersinfo@titans.com.au">membersinfo@titans.com.au</a></p>

    <ul class="footer__links">
        <li><a href="http://www.titans.com.au/" target="_blank">Gold Coast Titans</a></li>
        <li><a href="{{ $campaign->asset('Privacy_policy.pdf') }}" target="_blank">Privacy Policy</a></li>
        <li><a href="{{ $campaign->asset('Update_your_details_terms_and_conditions.pdf') }}" target="_blank">Terms &amp; Conditions</a></li>
        <li><a href="http://datacleanse.com.au" target="_blank">Product of Datacleanse</a></li>
    </ul>
    <br><br>
    <br><br>