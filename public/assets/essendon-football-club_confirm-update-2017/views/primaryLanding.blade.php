@extends($campaign->view('main'))

@section('content')
<div class="body-wrap__inner">

	<div class="main">

        <header class="header">
            <div class="header__logo">
                <a href="{{ $campaign->client_url }}/?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank"><img src="{{ $campaign->asset('images/header-logo.png') }}"></a>
            </div>
        </header>

		<form action="{{ $process_url }}" method="post" class="form">

			@include($campaign->view('common.errors'))

			<div class="form__inner">

				<div class="form__top">
					<h1 class="form__top__title title">{{ $campaign->title }}</h1>
                    @if($campaign->creative && $campaign->creative->prize_image)
					<div class="form__top__prize"><img src="{{ $campaign->asset('images/prize-image.jpg') }}"></div>
                    @endif
				</div>

				<div class="form__fields">

                    <h2 class="form__heading heading">Are your details correct?</h2>

					@include($campaign->view('fields.text'), [
						'key' => 'name_first',
                        'width' => 'half',
					])

                    @include($campaign->view('fields.text'), [
						'key' => 'name',
                        'width' => 'half',
					])

                    @include($campaign->view('fields.address'), [
						'keys' => ['street_addr_1', 'street_addr_2'],
					])

                    @include($campaign->view('fields.text'), [
						'key' => 'city',
						'id' => 'suburbField',
                        'width' => 'third',
					])

                    @include($campaign->view('fields.state'), [
						'key' => 'state',
                        'width' => 'third',
					])

                    @include($campaign->view('fields.text'), [
						'key' => 'zip',
						'id' => 'postcodeField',
                        'width' => 'third',
					])

                    @include($campaign->view('fields.country'), [
						'key' => 'country',
                        'width' => 'third',
					])

					@include($campaign->view('fields.text'), [
						'key' => 'phone_mobile',
                        'width' => 'third',
					])

                    @if($record->gender !== 'Supplied')
						@include($campaign->view('fields.gender'), [
							'key' => 'gender',
							'placeholder' => 'Please choose your gender...',
                            'width' => 'third',
						])
					@endif

                    @if($record->birthdate !== 'Supplied')
						@include($campaign->view('fields.dob'), [
							'key' => 'birthdate',
							'placeholder' => 'Please provide your DOB...',
                            'width' => 'half',
						])
					@endif

					@include($campaign->view('fields.email'), [
						'key' => 'email_addr',
                        'width' => 'half',
					])

                    <div class="form__fields__text">
                        <p>By clicking "Update/Confirm my details" you agree to the Competition Terms and Conditions and (unless you opt out via the checkboxes below) to Essendon Football Club sending you news, information, promotions and updates and providing your information to selected partners for the purpose of receiving offers approved by the club.</p>
                    </div>

                    @include($campaign->view('fields.checkbox'), [
						'key' => 'opt_out_club',
                        'width' => 'full',
                        'control_label' => 'I do not want to receive new information, promotions and updates from Essendon Football Club',
					])

                    @include($campaign->view('fields.checkbox'), [
						'key' => 'opt_out_partners',
                        'width' => 'full',
                        'control_label' => 'I do not want Essendon Football Club to provide my information to selected partners for the purpose of receiving offers approved by the Club.',
					])

				</div>

				<div class="form__submit">

					@foreach($hidden_fields as $name=>$value)
					<input type="hidden" name="{{ $name }}" value="{{ $value }}">
					@endforeach

					<button class="form__submit__button" type="submit">{{ $campaign->creative->button_text }}</button>

				</div>

			</div>

		</form>

	</div>

    @include($campaign->view('common.footer'))

</div>


@endsection
