@extends($campaign->view('main'))

@section('content')
<div class="body-wrap__inner">

	<div class="main">

        <header class="header">
            <div class="header__logo">
                <a href="{{ $campaign->client_url }}/?utm_medium=datacleanse-campaign&utm_source={{ $campaign->client->slug }}_{{ $campaign->slug }}" target="_blank"><img src="{{ $campaign->asset('images/header-logo.png') }}"></a>
            </div>
        </header>

        <div class="form__inner">

            <div class="thanks">
							@if($campaign->thanks_title)
							<h1 class="thanks__title">{{ $campaign->thanks_title }}</h1>
							@endif

							@if($campaign->thanks_title)
							<h2 class="thanks__sub-title">{!! $campaign->thanks_sub_title !!}</h2>
							@endif

							@if($campaign->thanks_title)
							<p class="thanks__body" style="text-transform:uppercase;">{{ $campaign->thanks_body }}</p>
							@endif
            </div>

        </div>

	</div>

    @include($campaign->view('common.footer'))

</div>


@endsection
