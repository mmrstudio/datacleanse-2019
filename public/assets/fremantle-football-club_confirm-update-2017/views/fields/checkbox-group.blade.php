<?php
    if(! isset($fields[$key])) return '';
    $field = $fields[$key];
    $name = isset($name) ? $name : $field->name;
    $id = isset($id) ? $id : camel_case($name . '_field');
    $label = isset($label) ? $label : $field->front_label;
    $value = isset($value) ? $value : '';
    $options = isset($options) ? $options : [];
    $field_value = $record->{$field->name} ? json_decode(old($field->name, $record->{$field->name})) : [];
    $control_label = isset($control_label) ? $control_label : false;
    $placeholder = isset($placeholder) ? $placeholder : $field->front_placeholder;
    $props = isset($props) ? implode(' ', $props) : false;
    $form_group_classes = ['form__group', 'form__group--radio', 'form__group--' . $field->name];
    if($errors->has($field->name)) $form_group_classes[] = 'form__group--error';
    $field_classes = ['form__control', 'form__control--radio', 'form__control--' . $field->name];
    $width = isset($width) ? $width : 'full';
?>

@foreach($options as $option)
<div class="{{ implode(' ', $form_group_classes) }}" data-width="{{ $width }}">
    <div class="form__group__controls">
        <label class="form__group__controls__label">
            <input name="{{ $name }}" type="checkbox" class="{{ implode(' ', $field_classes) }}" value="{{ $option }}" {{ in_array($option, $field_value) ? 'checked' : '' }} {{ $props }}>
            <span></span>
            {{ $option }}
            @if($option === 'Other')
            :
            <input id="{{ $field->name }}_other" name="{{ $field->name }}_other" type="text" placeholder="{{ $placeholder }}" class="form__control form__control--text" value="{{ old($field->name . '_other', $record->{$field->name . '_other'}) }}" {{ $props }}>
            <span class="form__group__border"></span>
            @endif
        </label>
    </div>
</div>
@endforeach
