<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Client;
use App\Campaign;

class DefaultsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // clients

        $mmr_client = new Client;
        $mmr_client->name = 'MMR Studio';
        $mmr_client->slug = 'mmr-studio';
        $mmr_client->cm_client_id = '9f22542c2f2758edb61983ba7b0c19be';
        $mmr_client->current_campaign = 0;
        $mmr_client->save();

        $demo_client = new Client;
        $demo_client->name = 'Demo Company';
        $demo_client->slug = 'demo-company';
        $demo_client->cm_client_id = '9f22542c2f2758edb61983ba7b0c19be';
        $demo_client->current_campaign = 1;
        $demo_client->save();

        // users

        $mmr_user = new User;
        $mmr_user->client_id = $mmr_client->id;
        $mmr_user->name = 'Web Department';
        $mmr_user->email = 'web@mmr.com.au';
        $mmr_user->password = bcrypt('hpi2010AFO');
        $mmr_user->current_campaign = 0;
        $mmr_user->save();

        $demo_user = new User;
        $demo_user->client_id = $demo_client->id;
        $demo_user->name = 'Demo User';
        $demo_user->email = 'demo@datacleanse.com.au';
        $demo_user->password = bcrypt('demouser');
        $demo_user->current_campaign = 1;
        $demo_user->save();

        // campaigns

        $demo_campaign = new Campaign;
        $demo_campaign->client_id = $demo_client->id;
        $demo_campaign->name = 'Demo Campaign';
        $demo_campaign->slug = 'demo-campaign';
        $demo_campaign->save();


    }
}
