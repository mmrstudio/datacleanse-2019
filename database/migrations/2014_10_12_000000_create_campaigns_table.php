<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id');
            $table->string('name');
            $table->string('slug');
            $table->string('input_table');
            $table->string('output_table');
            $table->integer('open')->default(1);
            $table->date('closed_at');
            $table->string('email_field');
            $table->string('mobile_field');
            $table->string('first_name_field');
            $table->string('last_name_field');
            $table->integer('secondary_step')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campaigns');
    }
}
