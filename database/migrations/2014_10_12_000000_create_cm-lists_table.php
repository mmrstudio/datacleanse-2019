<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cm_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id');
            $table->integer('campaign_id');
            $table->integer('send_out_id');
            $table->string('cm_list_id');
            $table->string('name');
            $table->date('imported_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cm_lists');
    }
}
