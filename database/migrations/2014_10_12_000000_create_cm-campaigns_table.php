<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cm_campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id');
            $table->integer('campaign_id');
            $table->integer('send_out_id');
            $table->integer('list_id');
            $table->string('name');
            $table->date('sent_at');
            $table->integer('recipients');
            $table->integer('opened');
            $table->integer('unopened');
            $table->integer('clicks');
            $table->integer('bounces');
            $table->integer('unsubscribes');
            $table->integer('complaints');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cm_campaigns');
    }
}
