<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id');
            $table->integer('campaign_id');
            $table->string('type');
            $table->string('name');
            $table->string('label');
            $table->integer('source');
            $table->integer('output');
            $table->integer('copy');
            $table->integer('secondary_step')->default(0);
            $table->integer('length')->default('255');
            $table->string('default');
            $table->text('format');
            $table->text('sanitize');
            $table->integer('required')->default('0');
            $table->string('validation');
            $table->text('validation_sanitize');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fields');
    }
}
