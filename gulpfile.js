
var gulp = require('gulp');
var fileinclude = require('gulp-file-include');
var uglify = require('gulp-uglify');
var replace = require('gulp-batch-replace');
var symlink = require('gulp-sym')
var rename = require("gulp-rename");
var shell = require('gulp-shell');
var cssmin = require('gulp-cssmin');
var autoprefixer = require('gulp-autoprefixer');
var notify = require('gulp-notify');
var changed = require('gulp-changed');
var compass = require('gulp-compass');

var webpack = require('webpack-stream');
var webpackConfig = require('./webpack.config.js').getConfig('development');


// var elixir = require('laravel-elixir');
//
// var gulp = require('gulp');
// var sass = require('gulp-sass');
// var plumber = require('gulp-plumber');
// var notify = require('gulp-notify');
// var fileinclude = require('gulp-file-include');
// var autoprefixer = require('gulp-autoprefixer');
// var cssmin = require('gulp-cssmin');
// var uglify = require('gulp-uglify');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */


gulp.task('watch', [], function () {
    gulp.watch('resources/assets/scss/**/*.scss', ['webpack']);
    gulp.watch('resources/assets/js/**/*.js', ['webpack']);

    // gulp.watch('public/campaigns/**/_dev/scss/*.scss').on('change', function(file) {
    //     var fileInput = file.path;
    //     var fileName = fileInput.split('/');
    //         fileName = fileName[fileName.length - 1];
    //     var filePath = fileInput.replace(new RegExp(fileName), '');
    //     var fileDest = fileInput.replace(/\/scss/, '');
    //         fileDest = fileDest.replace(new RegExp(fileName), '') + '../css/';
    //     var newFileName = fileName.replace(/\.scss/, '.css');
    //
    //     //var sassCommand = 'sass ' + fileInput + ' ' + fileDest + newFileName + ' --scss --sourcemap=none';
    //     //console.log(sassCommand);
    //
    //     return gulp.src(fileInput)
    //         //.pipe(shell([sassCommand]));
    //         .pipe(compass({
    //             "style": "compressed",
    //             "comments": true,
    //             "css": fileDest,
    //             "sass": filePath
    //         }))
    //         .on('error', function handleError() {
    //             this.emit('end');
    //         })
    //         .pipe(gulp.dest(fileDest));;
    //
    // });
    //
    // gulp.watch('public/campaigns/**/_dev/js/*.js').on('change', function(file) {
    //     var fileInput = file.path;
    //     var fileName = fileInput.split('/');
    //         fileName = fileName[fileName.length - 1];
    //     var fileDest = fileInput.replace(/\/js/, '');
    //         fileDest = fileDest.replace(new RegExp(fileName), '') + '../js/';
    //     var newFileName = fileName.replace(/__/, '');
    //
    //     console.log(newFileName);
    //
    //     return gulp.src(fileInput)
    //         .pipe(fileinclude({ prefix: ' @@', basepath: '@file' }))
    //         //.pipe(uglify())
    //         .pipe(rename(newFileName))
    //         .pipe(gulp.dest(fileDest));
    //
    // });

});

//error notification settings for plumber
var plumberErrorHandler = { errorHandler: notify.onError({
    title: 'Gulp',
    message: 'Error: <%= error.message %>'
  })
};

gulp.task('webpack', function() {

    // webpack
    return gulp.src('resources/assets/js/app.js')
        .pipe(webpack(webpackConfig))
        .on('error', function handleError() {
            this.emit('end');
        })
        .pipe(gulp.dest('public/dist/'));

});

// gulp.task('css', function() {
//
//     gulp.src('resources/assets/scss/app.scss')
//         .pipe(plumber(plumberErrorHandler))
//         .pipe(sass().on('error', sass.logError))
//         .pipe(gulp.dest('public/css/'));
//
// });
//
// gulp.task('js', function() {
//
//     gulp.src(['resources/assets/js/app.js'])
//         .pipe(fileinclude({ prefix: ' @@', basepath: '@file' }))
//         .pipe(gulp.dest('public/js/'));
//
// });
//
// gulp.task('publish', function() {
//
//     // autoprefix & minify
//     gulp.src('public/css/app.css')
//         .pipe(autoprefixer({ browsers: ['last 2 versions'], cascade: false }))
//         .pipe(cssmin())
//         .pipe(gulp.dest('public/css/'));
//
//     // uglify js
//     gulp.src(['public/js/app.js']).pipe(uglify()).pipe(gulp.dest('public/js/'));
//
// });
