@extends('layouts.app')

@section('content')

<div class="section-header">
    <div class="section-header__title">{{ $client->name }}</div>
</div>

<section class="main">

    @if($nav->show)
        @include('includes.sidebar-nav')
    @endif

    <main class="main__body">

        <h1 class="main__body__title">Edit Client</h1>

        <div class="main__body__inner form-wrap main__body__inner--grey">

            <div class="form-wrap__inner form-wrap__inner--full-width">

                <form class="form form--large" method="POST" action="{{ url('/client/edit/' . $client->id) }}">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="form__group{{ $errors->has('name') ? ' form__group--error' : '' }}">
                        <label for="campaignName">Name</label>
                        <input type="text" class="form__input" name="name" value="{{ old('name', $client->name) }}" id="campaignName">
                        @if ($errors->has('name'))
                        <div class="form__group__error-message">{{ $errors->first('name') }}</div>
                        @endif
                    </div>

                    <div class="form__group{{ $errors->has('slug') ? ' form__group--error' : '' }}">
                        <label for="campaignSlug">Slug</label>
                        <input type="text" class="form__input" name="slug" value="{{ old('slug', $client->slug) }}" id="campaignSlug">
                        @if ($errors->has('slug'))
                        <div class="form__group__error-message">{{ $errors->first('slug') }}</div>
                        @endif
                    </div>

                    <div class="form__group form__select">
                        <label for="clientField">Campaign Monitor Client</label>
                        <select name="client_id" id="clientField">
                            <option value="">Not Selected</option>
                            @foreach($cm_clients as $cm_client_id => $cm_client_name)
                            <option value="{{ $cm_client_id }}"{{ old('cm_client_id', $client->cm_client_id) == $cm_client_id ? ' selected' : '' }}>{{ $cm_client_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form__submit">
                        <button type="submit" class="button button--large">Save Client</button>
                    </div>

                </form>

            </div>

        </div>

    </main>

</section>

@endsection
