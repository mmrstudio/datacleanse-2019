@extends('layouts.app')

@section('content')

<div class="section-header">
    <div class="section-header__title">{{ $campaign->name }}</div>
</div>

<section class="main">

    @if($nav->show)
        @include('includes.sidebar-nav')
    @endif

    <main class="main__body">

        <h1 class="main__body__title">Edit Send Out - {{ $sendout->name }}</h1>

        <div class="main__body__inner form-wrap main__body__inner--grey">

            <div class="form-wrap__inner form-wrap__inner--full-width">

                <form class="form form--large" method="POST" action="{{ url('/send-out/' . $sendout->id) }}">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="form__group form__select">
                        <label for="clientField">Client</label>
                        <select name="client_id" id="clientField">
                            <option value="">Not Selected</option>
                            @foreach($clients as $client)
                            <option value="{{ $client->id }}"{{ old('client_id', $campaign->client_id) == $client->id ? ' selected' : '' }}>{{ $client->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form__group form__select">
                        <label for="campaignField">Campaign</label>
                        <select name="campaign_id" id="campaignField">
                            <option value="">Not Selected</option>
                            @foreach($campaigns as $campaign)
                            <option value="{{ $campaign->id }}"{{ old('campaign_id', $sendout->campaign_id) == $campaign->id ? ' selected' : '' }}>{{ $campaign->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form__group{{ $errors->has('name') ? ' form__group--error' : '' }}">
                        <label for="sendoutName">Name</label>
                        <input type="text" class="form__input" name="name" value="{{ old('name', $sendout->name) }}" id="sendoutName">
                        @if ($errors->has('name'))
                        <div class="form__group__error-message">{{ $errors->first('name') }}</div>
                        @endif
                    </div>

                    <div class="form__group form__select">
                        <label for="templateField">Email Template</label>
                        <select name="cm_template_id" id="templateField">
                            <option value="">Not Selected</option>
                            @foreach($templates as $template_id => $template_name)
                            <option value="{{ $template_id }}"{{ old('cm_template_id', $sendout->cm_template_id) == $template_id ? ' selected' : '' }}>{{ $template_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form__group{{ $errors->has('subject') ? ' form__group--error' : '' }}">
                        <label for="sendoutSubject">Subject Line</label>
                        <input type="text" class="form__input" name="subject" value="{{ old('subject', $sendout->subject) }}" id="sendoutSubject">
                        @if ($errors->has('subject'))
                        <div class="form__group__error-message">{{ $errors->first('subject') }}</div>
                        @endif
                    </div>

                    <div class="form__group{{ $errors->has('from_email') ? ' form__group--error' : '' }}">
                        <label for="sendoutFromEmail">From Email</label>
                        <input type="text" class="form__input" name="from_email" value="{{ old('from_email', $sendout->from_email) }}" id="sendoutFromEmail">
                        @if ($errors->has('from_email'))
                        <div class="form__group__error-message">{{ $errors->first('from_email') }}</div>
                        @endif
                    </div>

                    <div class="form__group{{ $errors->has('from_name') ? ' form__group--error' : '' }}">
                        <label for="sendoutFromName">From Name</label>
                        <input type="text" class="form__input" name="from_name" value="{{ old('from_name', $sendout->from_name) }}" id="sendoutFromName">
                        @if ($errors->has('from_name'))
                        <div class="form__group__error-message">{{ $errors->first('from_name') }}</div>
                        @endif
                    </div>

                    <div class="form__group{{ $errors->has('reply_email') ? ' form__group--error' : '' }}">
                        <label for="sendoutReplyEmail">Reply Email</label>
                        <input type="text" class="form__input" name="reply_email" value="{{ old('reply_email', $sendout->reply_email) }}" id="sendoutReplyEmail">
                        @if ($errors->has('reply_email'))
                        <div class="form__group__error-message">{{ $errors->first('reply_email') }}</div>
                        @endif
                    </div>

                    <div class="form__group{{ $errors->has('sms_message') ? ' form__group--error' : '' }}">
                        <label for="sendoutSMSMessage">SMS Message</label>
                        <textarea class="form__input" name="sms_message" id="sendoutSMSMessage">{{ old('sms_message', $sendout->sms_message) }}</textarea>
                        @if ($errors->has('sms_message'))
                        <div class="form__group__error-message">{{ $errors->first('sms_message') }}</div>
                        @endif
                    </div>

                    <div class="form__group{{ $errors->has('sms_opt_out') ? ' form__group--error' : '' }}">
                        <label for="sendoutSMSOptOut">SMS Opt Out</label>
                        <input type="text" class="form__input" name="sms_opt_out" value="{{ old('sms_opt_out', $sendout->sms_opt_out) }}" id="sendoutSMSOptOut">
                        @if ($errors->has('sms_opt_out'))
                        <div class="form__group__error-message">{{ $errors->first('sms_opt_out') }}</div>
                        @endif
                    </div>

                    <div class="form__submit">
                        <button type="submit" class="button button--large">Save Send Out</button>
                    </div>

                </form>

            </div>

        </div>

    </main>

</section>

@endsection
