@extends('layouts.app')

@section('content')

<div class="section-header">
    <div class="section-header__title">{{ $client ? $client->name : 'Campaigns' }}</div>
</div>

<section class="main">

    @if($nav->show)
        @include('includes.sidebar-nav')
    @endif

    <main class="main__body">
        Main
    </main>

</section>

@endsection
