<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>There’s a letter for you, from the animals at the zoo</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0 " />
<meta name="format-detection" content="telephone=no"/>

<style type="text/css">
body {
	margin: 0 !important;
	padding: 0 !important;
	-webkit-text-size-adjust: 100% !important;
	-ms-text-size-adjust: 100% !important;
	-webkit-font-smoothing: antialiased !important;
}

.visible-mobile {
  max-height: 0;
  width: 0;
  display: none;
}
img {
	border: 0 !important;
	outline: none !important;
}
p {
	Margin: 0px !important;
	Padding: 0px !important;
}
table {
	border-collapse: collapse;
	mso-table-lspace: 0px;
	mso-table-rspace: 0px;
}
td, a, span {
	border-collapse: collapse;
	mso-line-height-rule: exactly;
}
.ExternalClass * {
	line-height: 100%;
}
.em_defaultlink a {
	color: inherit !important;
	text-decoration: none !important;
}
span.MsoHyperlink {
	mso-style-priority: 99;
	color: inherit;
}
span.MsoHyperlinkFollowed {
	mso-style-priority: 99;
	color: inherit;
}
.em_text { font-family:Arial,sans-serif;font-size:18px; line-height: 23px; color:#1D1D1B;}
.em_text a { text-decoration:none; color:#1D1D1B;}
.em_text1, .em_text1 a { font-family: Helvetica, Arial, sans-serif; font-size:40px;  line-height: 48px; color:#49743D; text-decoration: none; font-weight:bold;  }
.em_text2, .em_text2 a { font-family: Helvetica, Arial, sans-serif; font-size: 13px; line-height: 22px; color:#49743D; text-decoration: none;
	}
.em_text3 { font-family: Helvetica, Arial, sans-serif; font-size: 12px; line-height: 22px; color: #1D1D1B;   }
.em_text3 a {
	color: #49743D;text-decoration: none;
}
.em_text4, .em_text4 a { font-family: Helvetica, Arial, sans-serif; font-size: 12px; line-height: 16px; color: #49743D; text-decoration: none; }




.preheader {
    display: none;
}
@media only screen and (min-width:660px) and (max-width:749px) {
	.em_main_table { width:100% !important; }
	.em_wrapper { width: 100% !important; }
	.em_hide { display: none !important; }
	.visible-mobile {
		display: block !important;
		max-height: none !important;
		width:100%!important;
		max-width: 100% !important;
		height: auto !important;

	  }
	.em_side{ padding: 0 50px !important; }
.em_aside { padding: 0 20px !important; /*Update the values as required*/ }
.em_aside2 { padding: 0 0px !important; /*Update the values as required*/ }
	img.visible-mobile{width: 100% !important;}


}

/*Stylesheet for the devices width between 481px to 660px*/
@media only screen and (min-width:481px) and (max-width:659px) {
.em_text1, .em_text1 a {font-size: 25px;}
.em_main_table { width:100% !important; }
.em_wrapper { width: 100% !important; }
.em_side{ padding: 0 50px !important; }
.em_aside { padding: 0 20px !important; /*Update the values as required*/ }
.em_aside2 { padding: 0 0px !important; /*Update the values as required*/ }
.em_aside3 { padding-left:20px!important;padding-right:20px!important; /*Update the values as required*/ }
.em_hide { display: none !important; }
.em_full_img { width: 100% !important; height: auto !important; max-width:none !important; }
.em_align_cent { text-align: center !important; }
.em_height { height:20px !important; font-size:0px !important; line-height:0px !important;}
.em_background { background-image:none  !important; height:auto !important; }
.em_pad { padding-top:20px !important;}
.em_side2 { width:20px !important;}
.em_side10 { width: 10px !important; /*Update the values as required*/ }
.em_pad_bottom {
	padding-bottom: 14px !important;
}

.em_center1{
	text-align: center!important;
	height: inherit!important;
	border-bottom: none!important;
	padding-top: 8px!important;
}
.em_center2{
	text-align: center!important;
	height: inherit!important;
	padding-bottom: 8px!important;
}

.visible-mobile {
		display: block !important;
		max-height: none !important;
		width:100%!important;
		max-width: 100% !important;
		height: auto !important;

	  }
}

/*Stylesheet for the devices width between 0px to 480px*/
@media only screen and (max-width:480px) {
.em_main_table { width: 100% !important; }
.em_wrapper { width: 100% !important; }
.em_side{ padding: 0 50px !important; }
.em_aside { padding: 0 20px !important; /*Update the values as required*/ }
.em_aside2 { padding: 0 0px !important; /*Update the values as required*/ }
.em_aside3 { padding-left:20px!important;padding-right:20px!important; /*Update the values as required*/ }
.em_hide { display: none !important; }
.em_full_img { width: 100% !important; height: auto !important; max-width:none !important; }
.em_align_cent { text-align: center !important; }
.em_height { height:20px !important; font-size:0px !important; line-height:0px !important;}
.em_background { background-image:none  !important; height:auto !important; }
.em_pad { padding-top:20px !important;}
.em_side2 { width:20px !important;}
.em_side10 { width: 10px !important; /*Update the values as required*/ }
.em_br {
	display: block;
}
.em_pad_bottom {
	padding-bottom: 14px !important;
}
.em_text1, .em_text1 a {font-size: 25px;}
.em_center1{
	text-align: center!important;
	height: inherit!important;
	border-bottom: none!important;
	padding-top: 8px!important;
}
.em_center2{
	text-align: center!important;
	height: inherit!important;
	padding-bottom: 8px!important;
}
.visible-mobile {
	display: block !important;
	max-height: none !important;
	width:100%!important;
	max-width: 100% !important;
	height: auto !important;

  }
}
</style>
<!--[if gte mso 9]>
<xml>
  <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
 </o:OfficeDocumentSettings>
</xml>
<![endif]-->




</head>


<body style="margin: 0px;padding: 0px;-webkit-text-size-adjust: 100% !important;-ms-text-size-adjust: 100% !important;-webkit-font-smoothing: antialiased !important;" bgcolor="#d3cdbf"><span class="preheader" style="display: none;">There’s a letter for you, from the animals at the zoo</span>

<!--Full width table start-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#d3cdbf" style="border-collapse: collapse;mso-table-lspace: 0px;mso-table-rspace: 0px;">
    	<tr>
        	<td>
            	<table width="750" cellpadding="0" cellspacing="0" border="0" align="center" class="em_main_table">

                   	<tr>
                   		<td valign="top" align="center">
                    		<table width="100%"  class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                       			<tr>

									<td align="center" class="em_hide">

										<img src="https://campaigns.datacleanse.com.au/assets/zoos-victoria_renew/email/friend/img_03.jpg" editable="true" width="750" height="201" border="0" style="display:block; max-width: 750px;" class="em_hide" alt="There’s a letter for you, from the animals at the zoo" >
									</td>

									<!--[if !mso]><!-->

									<td valign="top" colspan="1" class="visible-mobile">

											<img src="https://campaigns.datacleanse.com.au/assets/zoos-victoria_renew/email/friend/img_18.jpg" editable="true" width="750" height="201" border="0" style="display:block; max-width: 750px;"  class="visible-mobile" alt="There’s a letter for you, from the animals at the zoo" >


									</td>

									<!--<![endif]-->
								</tr>
							</table>
						</td>
					</tr>



				</table>
     		</td>
        </tr>
         <tr>
        	<td style="padding-bottom: 0px;" >
            	<table width="700" cellpadding="0" cellspacing="0" border="0" align="center" class="em_main_table">
                    <tr>
                    	<td  valign="top" colspan="1" bgcolor="#ffffff" class="em_aside" style="padding-left: 50px; padding-right: 50px;">
                    		<table width="600" style="width:600px;" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
								<tr>
									<td valign="top" align="center" style="" class="em_pad_bottom">
										<a href="https://zoos.mmrserver.com/?name={{ $name }}" title="Watch now"><img src="https://campaigns.datacleanse.com.au/assets/zoos-victoria_renew/email/friend/img_06.jpg" width="600" height="340" border="0" style="display:block; max-width: 600px;" class="em_full_img"></a>
									</td>
								</tr>
								<tr>
									<td  valign="top" colspan="1" bgcolor="#ffffff" >
										<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td align="center" style=" font-size:40px;color:#49743D; padding:23px 0 20px 0; ">
													<span class="em_text1">Hi {{ $name }}</span>
												</td>
											</tr>
											<tr>
												<td align="center" style="padding: 0; line-height: 20px;">
													<span class="em_text">{{ $sender }} wants you to watch your personalised video from the animals at the Zoo and read the very special letter they’ve written to you.</span><br><br>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
                  		</td>
                  	</tr>
                 	   <tr>
                    	<td  valign="top" colspan="1"  bgcolor="#ffffff">
                    		<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                    			<tr>

									<td valign="top" align="center" style="padding-top: 23px; padding-bottom: 35px;" >
										<table width="172" style="width:172px;" cellpadding="0" cellspacing="0" border="0" align="center">

											<tr>

												<td align="center">
													<a href="https://zoos.mmrserver.com/?name={{ $name }}" title="Watch now"><img src="https://campaigns.datacleanse.com.au/assets/zoos-victoria_renew/email/friend/img_10.jpg" width="172" height="41" border="0" style="display:block; max-width: 172px;" title="Watch now"></a>
												</td>


											</tr>


										</table>
									</td>
                   				</tr>
							</table>

                    	</td>
                    </tr>
                   	<tr>
                    	<td  valign="top" colspan="1"  bgcolor="#ffffff">
                    		<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">
                    			<tr>

									<td valign="top" align="center" style="padding-top: 0px; padding-bottom: 30px;" >
										<table width="246" style="width:246px;" cellpadding="0" cellspacing="0" border="0" align="center">

											<tr>

												<td align="center">
													<a href="https://www.zoo.org.au/"><img src="https://campaigns.datacleanse.com.au/assets/zoos-victoria_renew/email/friend/img_14.jpg" width="149" height="80" border="0" style="display:block; max-width: 149px;"  ></a>
												</td>
												<td align="center">
													<a href="https://www.zoo.org.au/"><img src="https://campaigns.datacleanse.com.au/assets/zoos-victoria_renew/email/friend/img_15.jpg" width="97" height="80" border="0" style="display:block; max-width: 97px;"  ></a>
												</td>


											</tr>


										</table>
									</td>
                   				</tr>
							</table>

                    	</td>
                    </tr>
                  	  <tr>
                    	<td  valign="top" colspan="1" >
                    		<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="center">

                        		<tr>
                        			<td align="center" style="padding: 25px 50px 43px 50px;" class="em_aside3">
                    					 <span class="em_text2" >
                    					 <a href="https://www.zoo.org.au/">© Zoos Victoria</a> &nbsp;&nbsp;&nbsp;&nbsp; This message was sent to you by {{ $sender }} ({{ $sender_email }})</span>
                    				</td>
								</tr>

                   			</table>
                    	</td>
                    </tr>

                 </table>

            </td>
       	</tr>
    </table>
   <!--Increase/decrease the number of (&nbsp;) as per the template width-->
<div class="em_hide" style="white-space:nowrap;font:20px courier;color:#ffffff;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
</body>
</html>
