@extends('layouts.app')

@section('content')

<div class="section-header">
    <div class="section-header__title">{{ $campaign['title'] }}</div>
    <div class="section-header__controls">
        <div class="button-switch">
            <div class="button-switch__label">Campaign Status</div>
            <div class="button-switch__options" data-state="on">
                <div class="button-switch__option button-switch__option--on">Open</div>
                <div class="button-switch__option button-switch__option--off">Closed</div>
            </div>
        </div>
        <a href="#" class="button icon icon-arrow-down section-header__controls__export">Export Campaign</a>
    </div>
</div>

<section class="main">

    @if($nav->show)
        @include('includes.sidebar-nav')
    @endif

    <main class="main__body">

        <h1 class="main__body__title">Traffic</h1>

        <div class="traffic">

            <div class="traffic__main">

                <div class="main__body__sub-title">
                    {{ $send_out['title'] }}
                    <div class="main__body__sub-title__button">
                        <button class="button button--dark-blue button--small icon icon-arrow-down">Export Traffic</button>
                    </div>
                </div>

                <div class="traffic__select-dimension">

                    <label class="form__select__label" for="trafficDimension">Dimension</label>
                    <select class="form__select form__select--select2" name="traffic_dimension" id="trafficDimension" data-minimum-results-for-search="Infinity">
                        @foreach($traffic['dimensions'] as $key => $dimension)
                        <option value="{{ $key }}" {{ $key == $traffic['dimension'] ? 'selected' : '' }}>{{ $dimension }}</option>
                        @endforeach
                    </select>

                </div>

                <div class="traffic__table">

                    <table class="table table--striped table--border-top">

                        @foreach($traffic['data'] as $dimension)
                        <tr>
                            <th>{{ $dimension['name'] }}</th>
                            <td class="text-right">{{ $dimension['value'] }}</td>
                        </tr>
                        @endforeach

                    </table>

                </div>

            </div>

            <div class="traffic__graph">

                <div class="main__body__sub-title">
                    Visitors by date
                </div>

                <div class="traffic__graph__chart">
                    <canvas id="overviewLineChart"></canvas>
                    <script>
                        var overviewLineChartSrcData = {
                            colour: 'green',
                            label: "Opens",
                            points: ["01 May", "02 May", "03 May", "04 May", "05 May", "06 May", "07 May"],
                            values: [200, 1200, 900, 750, 300, 80, 10]
                        };
                    </script>
                </div>

            </div>

        </div>

    </main>

</section>

@endsection
