@extends('layouts.app')

@section('content')

<div class="section-header">
    <div class="section-header__title">{{ $campaign->name }}</div>
</div>

<section class="main">

    @if($nav->show)
        @include('includes.sidebar-nav')
    @endif

    <main class="main__body">

        <h1 class="main__body__title">Edit Campaign</h1>

        <div class="main__body__inner form-wrap main__body__inner--grey">

            <div class="form-wrap__inner form-wrap__inner--full-width">

                <form class="form form--large" method="POST" action="{{ url('/campaign/' . $campaign->id) }}">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="form__group form__group--checkbox">
                        <div class="form__checkbox">
                            <label>
                                <input type="checkbox" name="open" value="1" {{ old('open', $campaign->open) == '1' ? 'checked' : '' }}>
                                <span></span>
                                Campaign Open
                            </label>
                        </div>
                    </div>

                    <div class="form__group form__select">
                        <label for="clientField">Client</label>
                        <select name="client_id" id="clientField">
                            <option value="">Not Selected</option>
                            @foreach($clients as $client)
                            <option value="{{ $client->id }}"{{ old('client_id', $campaign->client_id) == $client->id ? ' selected' : '' }}>{{ $client->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form__group{{ $errors->has('name') ? ' form__group--error' : '' }}">
                        <label for="campaignName">Name</label>
                        <input type="text" class="form__input" name="name" value="{{ old('name', $campaign->name) }}" id="campaignName">
                        @if ($errors->has('name'))
                        <div class="form__group__error-message">{{ $errors->first('name') }}</div>
                        @endif
                    </div>

                    <div class="form__group{{ $errors->has('slug') ? ' form__group--error' : '' }}">
                        <label for="campaignSlug">Slug</label>
                        <input type="text" class="form__input" name="slug" value="{{ old('slug', $campaign->slug) }}" id="campaignSlug">
                        @if ($errors->has('slug'))
                        <div class="form__group__error-message">{{ $errors->first('slug') }}</div>
                        @endif
                    </div>

                    <div class="form__group form__select">
                        <label for="emailField">Email Field</label>
                        <select name="email_field" id="emailField">
                            <option value="">Not Selected</option>
                            @foreach($campaign->fields(false) as $field)
                            <option value="{{ $field->name }}"{{ old('email_field', $campaign->email_field) == $field->name ? ' selected' : '' }}>{{ $field->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form__group form__select">
                        <label for="mobileField">Mobile Field</label>
                        <select name="mobile_field" id="mobileField">
                            <option value="">Not Selected</option>
                            @foreach($campaign->fields(false) as $field)
                            <option value="{{ $field->name }}"{{ old('mobile_field', $campaign->mobile_field) == $field->name ? ' selected' : '' }}>{{ $field->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form__group form__select">
                        <label for="firstNameField">First Name Field</label>
                        <select name="first_name_field" id="firstNameField">
                            <option value="">Not Selected</option>
                            @foreach($campaign->fields(false) as $field)
                            <option value="{{ $field->name }}"{{ old('first_name_field', $campaign->first_name_field) == $field->name ? ' selected' : '' }}>{{ $field->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form__group form__select">
                        <label for="lastNameField">Last Name Field</label>
                        <select name="last_name_field" id="lastNameField">
                            <option value="">Not Selected</option>
                            @foreach($campaign->fields(false) as $field)
                            <option value="{{ $field->name }}"{{ old('last_name_field', $campaign->last_name_field) == $field->name ? ' selected' : '' }}>{{ $field->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form__submit">
                        <button type="submit" class="button button--large">Save Campaign</button>
                    </div>

                </form>

            </div>

        </div>

    </main>

</section>

@endsection
