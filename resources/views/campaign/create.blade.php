@extends('layouts.app')

@section('content')

<div class="section-header">
    <div class="section-header__title">Create New Campaign</div>
</div>

<section class="main">

    @if($nav->show)
        @include('includes.sidebar-nav')
    @endif

    <main class="main__body">

        <h1 class="main__body__title">Create Campaign</h1>

        <div class="main__body__inner form-wrap main__body__inner--grey">

            <div class="form-wrap__inner form-wrap__inner--full-width">

                <form class="form form--large" method="POST" action="{{ route('campaign.store') }}">
                    {!! csrf_field() !!}

                    <div class="form__group form__group--checkbox">
                        <div class="form__checkbox">
                            <label>
                                <input type="checkbox" checked="checked" name="open" value="1" {{ old('open') == '1' ? 'checked' : '' }}>
                                <span></span>
                                Campaign Open
                            </label>
                        </div>
                    </div>

                    <div class="form__group form__select{{ $errors->has('client_id') ? ' form__group--error' : '' }}">
                        <label for="clientField">Client</label>
                        <select name="client_id" id="clientField">
                            <option value="">Not Selected</option>
                            @foreach($clients as $client)
                            <option value="{{ $client->id }}"{{ old('client_id') == $client->id ? ' selected' : '' }}>{{ $client->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('client_id'))
                        <div class="form__group__error-message">{{ $errors->first('client_id') }}</div>
                        @endif
                    </div>

                    <div class="form__group{{ $errors->has('name') ? ' form__group--error' : '' }}">
                        <label for="campaignName">Name</label>
                        <input type="text" class="form__input" name="name" value="{{ old('name','Confirm or Update '.date('Y')) }}" id="campaignName">
                        @if ($errors->has('name'))
                        <div class="form__group__error-message">{{ $errors->first('name') }}</div>
                        @endif
                    </div>

                    <div class="form__group{{ $errors->has('slug') ? ' form__group--error' : '' }}">
                        <label for="campaignSlug">Slug</label>
                        <input type="text" class="form__input" name="slug" value="{{ old('slug','confirm-update-'.date('Y')) }}" id="campaignSlug">
                        @if ($errors->has('slug'))
                        <div class="form__group__error-message">{{ $errors->first('slug') }}</div>
                        @endif
                    </div>

                    <div class="form__submit">
                        <button type="submit" class="button button--large">Create Campaign</button>
                    </div>

                </form>

            </div>

        </div>

    </main>

</section>

@endsection
