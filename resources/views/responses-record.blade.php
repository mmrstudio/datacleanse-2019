@extends('layouts.app')

@section('content')

<div class="section-header">
    <div class="section-header__title">{{ $campaign->client->name }} - {{ $campaign->name }}</div>
</div>

<section class="main">

    @if($nav->show)
        @include('includes.sidebar-nav')
    @endif

    <main class="main__body">

        <h1 class="main__body__title">View Individual Record</h1>

        <div class="responses">

            <div class="responses__inner">

                <div class="main__body__sub-title">
                    {{ $send_out_title }}
                </div>

                <div class="responses__table">

                    <table class="table table--striped table--border-top">

                        <thead>

                            <tr>
                                <th>Field</th>
                                <th>Old Value</th>
                                <th>New Value</th>
                            </tr>

                        </thead>

                        <tbody>

                            @foreach($form_fields as $field)
                            <tr>
                                <td><strong>{{ $field->front_label }}</strong> - {{ $field->name }}</td>
                                <td>{{ $input->{$field->name} }}</td>
                                <td>{{ $output->{$field->name} }}</td>
                            </tr>
                            @endforeach

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </main>

</section>

@endsection
