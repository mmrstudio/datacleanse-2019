@extends('layouts.app')

@section('content')

<div class="section-header">
    <div class="section-header__title">{{ $campaign->client->name }} - {{ $campaign->name }}</div>
</div>

<section class="main">

    @if($nav->show)
        @include('includes.sidebar-nav')
    @endif

    <main class="main__body">

        <h1 class="main__body__title">Responses</h1>

        <div class="responses">

            <div class="responses__inner">

                <div class="main__body__sub-title">
                    {{ $send_out_title }}
                    <div class="main__body__sub-title__button">
                        <a href="{{ url('responses/' . $campaign->id . '/refresh') }}" class="button button--dark-blue button--small">Refresh</a>
                    </div>
                </div>

                <div class="responses__select-category">

                    <div class="inline-dropdown">
                        <div class="inline-dropdown__label">Select dimension...</div>
                        <ul class="inline-dropdown__list">
                            @foreach($dropdown as $item)
                            <li class="inline-dropdown__list__link {{ $item['current'] ? 'inline-dropdown__list__link--active' : '' }}"><a href="{{ url($item['url']) }}">{{ $item['label'] }}</a></li>
                            @endforeach
                        </ul>
                    </div>

                </div>

                <div class="responses__table">

                    <table class="table table--striped table--border-top">

                        <thead>

                            <tr>
                                <th>Field</th>
                                <th class="text-right">Changed</th>
                                <th class="text-right">Unchanged</th>
                                <th class="text-right">Added</th>
                                <th class="text-right">Removed/Cleared</th>
                            </tr>

                        </thead>

                        <tbody>

                            @foreach($status as $field_name => $item)
                            <tr>
                                <td><strong>{{ $item['label'] }}</strong> ({{ $field_name }})</td>
                                <td class="text-right">{{ number_format($item['changed'], 0, '.', ',') }}<?php /* (<a href="{{ url('responses/compare/' . $field_name) }}">Compare</a>) */ ?></td>
                                <td class="text-right">{{ number_format($item['unchanged'], 0, '.', ',') }}</td>
                                <td class="text-right">{{ number_format($item['added'], 0, '.', ',') }}</td>
                                <td class="text-right">{{ number_format($item['removed'], 0, '.', ',') }}</td>
                            </tr>
                            @endforeach

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </main>

</section>

@endsection
