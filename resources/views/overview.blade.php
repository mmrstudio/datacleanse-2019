@extends('layouts.app')

@section('content')

<div class="section-header">
    <div class="section-header__title">{{ $campaign->client->name }} - {{ $campaign->name }}</div>
    <!--
    <div class="section-header__controls">
        <div class="button-switch">
            <div class="button-switch__label">Campaign Status</div>
            <div class="button-switch__options" data-state="on">
                <div class="button-switch__option button-switch__option--on">Open</div>
                <div class="button-switch__option button-switch__option--off">Closed</div>
            </div>
        </div>
        <a href="#" class="button icon icon-arrow-down section-header__controls__export">Export Campaign</a>
    </div>
    -->
</div>

<section class="main">

    @if($nav->show)
        @include('includes.sidebar-nav')
    @endif

    <main class="main__body">

        <h1 class="main__body__title">Overview</h1>

        <div class="overview">

            <div class="overview__stats">

                <div class="main__body__sub-title">
                    {{ $send_out_title }}
                    <!--
                    <div class="main__body__sub-title__button">
                        <button class="button button--dark-blue button--small icon icon-arrow-down">Export Overview</button>
                    </div>
                    -->

                    @if(! $campaign->locked)
                    <div class="main__body__sub-title__button">
                        (Last updated: {{ date('d/m/y g:iA', strtotime($campaign->updated_at)) }}) &nbsp;
                        <a href="{{ url('overview/' . $campaign->id . '/refresh') }}" class="button button--dark-blue button--small">Refresh Statistics</a>
                    </div>
                    @endif;

                </div>

                <div class="overview__stats__cols">

                    <div class="overview__stats__cols__col">

                        <table class="table table--lined table--stats">

                            @if(! $send_out)
                            <tr>
                                <th>Total Recipients</th>
                                <td>{{ number_format($stats['total_recipients']['total'], 0, '.', ',') }}</td>
                            </tr>
                            @endif

                            <tr>
                                <th>Emails Sent {{ ! $send_out ? ' (including reminders)' : '' }}</th>
                                <td>{{ number_format($stats['recipients']['total'], 0, '.', ',') }}</td>
                            </tr>

                            <tr>
                                <th>Opens</th>
                                <td class="color--dc-green">
                                    {{ number_format($stats['unique_opened']['total'], 0, '.', ',') }}
                                    <span>({{ $stats['unique_opened']['%'] }})</span>
                                </td>
                            </tr>

                            <tr>
                                <th>Clicks</th>
                                <td>
                                    {{ number_format($stats['clicks']['total'], 0, '.', ',') }}
                                    <span>({{ $stats['clicks']['%'] }})</span>
                                </td>
                            </tr>

                            <tr>
                                <th>{{ ! $send_out ? 'Total ' : '' }}Responses</th>
                                <td>
                                    {{ number_format($stats['responses']['total'], 0, '.', ',') }}
                                    <span>({{ $stats['responses']['%'] }})</span>
                                </td>
                            </tr>

                        </table>

                    </div>

                    <div class="overview__stats__cols__col overview__stats__cols__col--center">
                        <div class="overview__stats__doughnut">
                            <div class="overview__stats__doughnut__total">
                                <span>{{ number_format($stats['recipients']['total'], 0, '.', ',') }}</span><br>
                                Total
                            </div>
                            <canvas id="overviewDoughnutChart" width="200" height="200"></canvas>
                            <script>
                                var overviewDoughnutChartSrcData = [
                                    { label: 'Opens', value: {{ $stats['unique_opened']['total'] }}, colour: 'green'},
                                    { label: 'Unopened', value: {{ $stats['unopened']['total'] }}, colour: 'light-blue'},
                                    { label: 'Bounces', value: {{ $stats['bounces']['total'] }}, colour: 'purple'},
                                    { label: 'Unsubscribed', value: {{ $stats['unsubscribes']['total']}}, colour: 'pink'}
                                ];
                            </script>
                        </div>
                    </div>

                    <div class="overview__stats__cols__col">

                        <table class="table table--lined table--stats">

                            <tr>
                                <th>Unopened</th>
                                <td class="color--dc-light-blue">
                                    {{ number_format($stats['unopened']['total'], 0, '.', ',') }}
                                    <span>({{ $stats['unopened']['%'] }})</span>
                                </td>
                            </tr>

                            <tr>
                                <th>Bounces</th>
                                <td class="color--dc-purple">{{ number_format($stats['bounces']['total'], 0, '.', ',') }}</td>
                            </tr>

                            <tr>
                                <th>Unsubscribed</th>
                                <td class="color--dc-pink">{{ number_format($stats['unsubscribes']['total'], 0, '.', ',') }}</td>
                            </tr>

                            <tr>
                                <th>Marked as Spam</th>
                                <td>{{ number_format($stats['complaints']['total'], 0, '.', ',') }}</td>
                            </tr>

                        </table>

                    </div>

                </div>

            </div>

            <div class="overview__graph">

                <div class="main__body__sub-title">
                    {{ ucfirst($graph_metric) }} - {{ $send_out_title }} by date
                    <div class="main__body__sub-title__button">

                        <?php /*
                        <div class="button-group">
                            <div class="button-group__buttons">
                                <a href="#" class="button-group__button active">Week</a>
                                <a href="#" class="button-group__button">Month</a>
                                <a href="#" class="button-group__button">All</a>
                            </div>
                        </div>
                        */ ?>

                        <div class="button-group button-group--no-border">
                            <div class="button-group__buttons">
                                <a href="{{ url('overview?graph=responses') }}" class="button-group__button {{ $graph_metric == 'responses' ? 'active' : '' }}">Responses</a>
                                <a href="{{ url('overview?graph=opens') }}" class="button-group__button {{ $graph_metric == 'opens' ? 'active' : '' }}">Opens</a>
                                <a href="{{ url('overview?graph=clicks') }}" class="button-group__button {{ $graph_metric == 'clicks' ? 'active' : '' }}">Clicks</a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="overview__graph__chart" style="margin-bottom: 80px;">
                    <canvas id="overviewLineChart" width="200" height="50"></canvas>
                    <script>
                        var overviewLineChartSrcData = {
                            colour: 'green',
                            label: '{{ ucfirst($graph_metric) }}',
                            points: {!! json_encode($graph_data['labels']) !!},
                            values: {!! json_encode($graph_data['values']) !!}
                        };
                    </script>
                </div>

            </div>

        </div>

    </main>

</section>

@endsection
