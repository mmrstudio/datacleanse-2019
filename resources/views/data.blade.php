@extends('layouts.app')

@section('content')

<div class="section-header">
    <div class="section-header__title">{{ $campaign->client->name }} - {{ $campaign->name }}</div>
</div>

<section class="main">

    @if($nav->show)
        @include('includes.sidebar-nav')
    @endif

    <main class="main__body">

        <h1 class="main__body__title">All campaign data</h1>

        <div class="data">

            <div class="data__main">

                <?php /*
                <div class="main__body__sub-title">
                    This data will be available until 30/05/2016
                    <div class="main__body__sub-title__button">
                        <button class="button button--dark-blue button--small icon icon-arrow-down">Export Campaign</button>
                    </div>
                </div>
                */ ?>

                <div class="data__table-controls data__table-controls--top">

                    <div class="button-group">
                        <div class="button-group__label">View</div>
                        <div class="button-group__buttons" id="dataTableView">
                            <button class="button-group__button active" data-view="10">10</button>
                            <button class="button-group__button" data-view="50">50</button>
                            <button class="button-group__button" data-view="100">100</button>
                            <button class="button-group__button" data-view="250">250</button>
                            <button class="button-group__button" data-view="500">500</button>
                        </div>
                    </div>

                </div>

                <div class="data__table">

<table id="dataTable" class="display nowrap" cellspacing="0" width="100%" data-view="#dataTableView" data-page-nav="#dataTablePageNav">
    <thead>
        <tr>
            @foreach($columns as $column)
            <th data-name="{{ $column }}">{{ $column }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>

    </tbody>
</table>


                </div>

                <div class="data__table-controls data__table-controls--bottom">

                    <div class="button-group">
                        <div class="button-group__buttons" id="dataTablePageNav">
                            <button class="button-group__button icon icon-carret-left" data-page="previous">Previous</button>
                            <button class="button-group__button icon icon-carret-right" data-page="next">Next</button>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </main>

</section>

@endsection
