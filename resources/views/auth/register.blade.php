@extends('layouts.app')

@section('content')

<div class="section-header">
    <div class="section-header__title">Create User</div>
</div>

<div class="form-wrap">

    <div class="form-wrap__inner">

        <form class="form" method="POST" action="{{ url('/register') }}">
            {!! csrf_field() !!}

            <div class="form__group{{ $errors->has('name') ? ' form__group--error' : '' }}">
                <label for="userName">Name</label>
                <input type="text" class="form__input" name="name" value="{{ old('name') }}" id="userName">
                @if ($errors->has('name'))
                <div class="form__group__error-message">{{ $errors->first('name') }}</div>
                @endif
            </div>

            <div class="form__group{{ $errors->has('email') ? ' form__group--error' : '' }}">
                <label for="userEmail">Email</label>
                <input type="email" class="form__input" name="email" value="{{ old('email') }}" id="userEmail">
                @if ($errors->has('email'))
                <div class="form__group__error-message">{{ $errors->first('email') }}</div>
                @endif
            </div>

            <div class="form__group{{ $errors->has('client_name') ? ' form__group--error' : '' }}">
                <label for="clientName">Client</label>
                <select class="form__select" name="client_id" id="clientName">
                    <option value="NEW">New Client</option>
                    @foreach(App\Client::all() as $client)
                    <option value="{{ $client->id }}"{{ old('client_id') == $client->id ? ' selected' : '' }}>{{ $client->name }}</option>
                    @endforeach
                </select>
                <input type="text" class="form__input" name="client_name" value="{{ old('client_name') }}" placeholder="New Client Name">
                @if ($errors->has('client_name'))
                <div class="form__group__error-message">{{ $errors->first('client_name') }}</div>
                @endif
            </div>

            <div class="form__group{{ $errors->has('password') ? ' form__group--error' : '' }}">
                <label for="userPassword">Password</label>
                <input type="password" class="form__input" name="password" value="{{ old('password') }}" id="userPassword">
                @if ($errors->has('password'))
                <div class="form__group__error-message">{{ $errors->first('password') }}</div>
                @endif
            </div>

            <div class="form__group{{ $errors->has('password_confirmation') ? ' form__group--error' : '' }}">
                <label for="userPasswordConfirmation">Confirm Password</label>
                <input type="password" class="form__input" name="password_confirmation" value="{{ old('password_confirmation') }}" id="userPasswordConfirmation">
                @if ($errors->has('password_confirmation'))
                <div class="form__group__error-message">{{ $errors->first('password_confirmation') }}</div>
                @endif
            </div>

            <div class="form__submit">
                <button type="submit" class="button">Create User</button>
            </div>
        </form>

    </div>

</div>

@endsection
