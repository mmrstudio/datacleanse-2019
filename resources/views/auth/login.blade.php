@extends('layouts.app')

@section('content')

<div class="section-header">
    <div class="section-header__title">Login</div>
</div>

<div class="form-wrap">

    <div class="form-wrap__inner">

        <form class="form" method="POST" action="{{ url('/login') }}">
            {!! csrf_field() !!}

            <div class="form__group{{ $errors->has('email') ? ' form__group--error' : '' }}">
                <input type="email" class="form__input" name="email" value="{{ old('email') }}" placeholder="Username/email">
                @if ($errors->has('email'))
                <div class="form__group__error-message">{{ $errors->first('email') }}</div>
                @endif
            </div>

            <div class="form__group{{ $errors->has('password') ? ' form__group--error' : '' }}">
                <input type="password" class="form__input" name="password" placeholder="Password">
                @if ($errors->has('password'))
                <div class="form__group__error-message">{{ $errors->first('password') }}</div>
                @endif
            </div>

            <div class="form__group">
                <div class="form__checkbox">
                    <label>
                        <input type="checkbox" name="remember">
                        <span></span>
                        Remember Me
                    </label>
                </div>
            </div>

            <div class="form__submit">
                <button type="submit" class="button">Login</button>
                <a href="{{ url('/password/reset') }}" class="form__submit__link">Forgot password?</a>
            </div>
        </form>

    </div>

</div>

@endsection
