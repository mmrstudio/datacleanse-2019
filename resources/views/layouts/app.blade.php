<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Datacleanse</title>

    <!-- Styles -->
    <link href="{{ asset('dist/bundle.css') }}" rel="stylesheet">

</head>
<body id="dashboardBody">

    <div class="body-wrap">

        <header class="app-header {{ $nav->show ? 'app-header--has-nav' : '' }}">

            @if($nav->show)
            <button class="app-header__nav-toggle" id="navToggle">Menu</button>
            @endif

            <div class="app-header__logo">
                <a href="{{ url('/home') }}">Datacleanse</a>
            </div>

            @if (! Auth::guest())

            <div class="app-header__buttons">

                @can('mmr-admin')
                <div class="app-header__settings dropdown dropdown--right">
                    <button class="app-header__settings__button icon icon-gear">Settings</button>
                    <ul class="dropdown__list dropdown__list--right">
                        <li class="dropdown__list__link"><a href="{{ url('/users') }}">Users</a></li>
                        <li class="dropdown__list__link dropdown__list__link--indented"><a href="{{ url('/register') }}">Create User</a></li>
                        <li class="dropdown__list__link"><a href="{{ url('/clients') }}">Clients</a></li>
                        <li class="dropdown__list__link"><a href="{{ url('/campaigns') }}">Campaigns</a></li>
                    </ul>
                </div>
                @endcan

                <div class="app-header__user dropdown dropdown--right">
                    <button class="app-header__user__button icon icon-user">User</button>
                    <ul class="dropdown__list dropdown__list--right">
                        <li class="dropdown__list__info">
                            <strong>{{ Auth::user()->name }}</strong><br>
                            {{ Auth::user()->client->name }}
                        </li>
                        <?php /*
                        <li class="dropdown__list__link"><a href="{{ url('/logout') }}">Change Password</a></li>
                        */ ?>
                        <li class="dropdown__list__link dropdown__list__link--highlight"><a href="{{ url('/logout') }}">Logout</a></li>
                    </ul>
                </div>

            </div>

            @endif

        </header>

        @yield('content')

        <footer class="app-footer">

            <div class="app-footer__logo">
                <a href="{{ url('/home') }}">Datacleanse</a>
            </div>

            <?php /*
            <nav class="app-footer__nav">

                <ul class="app-footer__nav__list">
                    <li class="app-footer__nav__link"><a href="#">Contact &amp; Support</a></li>
                    <li class="app-footer__nav__link"><a href="#">Terms &amp; Conditions</a></li>
                    <li class="app-footer__nav__link"><a href="#">Privacy Policy</a></li>
                </ul>

            </nav>
            */ ?>

        </footer>

    </div>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('dist/bundle.js') }}"></script>

</body>
</html>
