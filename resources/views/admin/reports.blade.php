@extends('layouts.app')

@section('content')

<div class="section-header">
    <div class="section-header__title">Reports</div>
    <?php /*
    <div class="section-header__controls">
        <a href="{{ url('report/export/all') }}" class="button">Export All</a>
    </div>
    */ ?>
</div>

<section class="main">

    @if($nav->show)
        @include('includes.sidebar-nav')
    @endif

    <main class="main__body">
        <h1 class="main__body__title">All Reports</h1>
        <div class="main__body__inner">

            <table class="table table--striped">

                <thead>
                    <tr>
                        <th>Report</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach($reports as $report)
                    <tr>
                        <td><strong>{{ $report->name }}</strong></td>
                        <td class="text-right">
                            <a href="/report/{{ $report->id }}" class="button button--small">View</a>
                        </td>
                    </tr>
                    @endforeach

                </tbody>

            </table>

        </div>
    </main>

</section>

@endsection
