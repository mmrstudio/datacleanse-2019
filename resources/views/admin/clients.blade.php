@extends('layouts.app')

@section('content')

<div class="section-header">
    <div class="section-header__title">Clients</div>
    @can('mmr-admin')
    <div class="section-header__controls">
        <a href="{{ url('admin/campaigns/create') }}" class="button">New Client</a>
    </div>
    @endcan
</div>

<section class="main">

    <main class="main__body">
        <h1 class="main__body__title">All Campaigns</h1>
        <div class="main__body__inner">

            <table class="table table--striped">

                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Slug</th>
                        <th>Campaigns</th>
                        <th>Campaign Monitor ID</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach($clients as $client)
                    <tr>
                        <td><strong>{{ $client->name }}</strong></td>
                        <td>{{ $client->slug }}</td>
                        <td>{{ $client->campaigns->count() }}</td>
                        <td>{{ $client->cm_client_id }}</td>
                        <td class="text-right">
                            @can('mmr-admin')
                            <a href="/client/edit/{{ $client->id }}" class="button button--small">Edit</a>
                            @endcan
                        </td>
                    </tr>
                    @endforeach

                </tbody>

            </table>

        </div>
    </main>

</section>

@endsection
