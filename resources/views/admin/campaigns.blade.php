@extends('layouts.app')

@section('content')

<div class="section-header">
    <div class="section-header__title">Campaigns</div>
    @can('mmr-admin')
    <div class="section-header__controls">
        <a href="{{ route('campaign.create') }}" class="button">New Campaign</a>
    </div>
    @endcan
</div>

<section class="main">

    @if($nav->show)
        @include('includes.sidebar-nav')
    @endif

    <main class="main__body">
        <h1 class="main__body__title">All Campaigns</h1>
        <div class="main__body__inner">

            <table class="table table--striped">

                <thead>
                    <tr>
                        <th>Campaign</th>
                        <th>Client</th>
                        <th class="text-right">Emails Sent</th>
                        <th class="text-right">Recipients</th>
                        <th class="text-right">Responses</th>
                        <th class="text-right">Created</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach($campaigns as $campaign)
                    <tr>
                        <td>{{ $campaign->name }}</td>
                        <td><strong>{{ $campaign->client->name }}</strong></td>
                        <td class="text-right">{{ number_format($campaign->stats['recipients']['total'], 0, '.', ',') }}</td>
                        <td class="text-right">{{ number_format($campaign->stats['total_recipients']['total'], 0, '.', ',') }}</td>
                        <td class="text-right">{{ number_format($campaign->stats['responses']['total'], 0, '.', ',') }} ({{ $campaign->stats['responses']['%'] }})</td>
                        <td class="text-right">{{ $campaign->created_at }}</td>
                        <td class="text-right">
                            <a href="/campaign/set/{{ $campaign->id }}/overview" class="button button--small">Overview</a>
                            @can('mmr-admin')
                            <a href="/campaign/set/{{ $campaign->id }}/edit" class="button button--small">Edit</a>
                            @endcan
                        </td>
                    </tr>
                    @endforeach

                </tbody>

                @can('mmr-admin')
                <tfoot>
                    <tr>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th class="text-right">{{ number_format($totals['emails'], 0, '.', ',') }}</th>
                        <th class="text-right">{{ number_format($totals['recipients'], 0, '.', ',') }}</th>
                        <th class="text-right">{{ number_format($totals['responses'], 0, '.', ',') }} ({{ number_format(($totals['responses'] / $totals['recipients']) * 100, 0, '.', ',') }}%)</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                    </tr>
                </tfoot>
                @endcan

            </table>

        </div>
    </main>

</section>

@endsection
