@extends('layouts.app')

@section('content')

<div class="section-header">
    <div class="section-header__title">Campaigns</div>
    <div class="section-header__controls">
        <a href="{{ url('admin/campaigns/create') }}" class="button">New Campaign</a>
    </div>
</div>

<section class="main">

    @if($nav->show)
        @include('includes.sidebar-nav')
    @endif

    <main class="main__body">
        <h1 class="main__body__title">All Campaigns</h1>
        <div class="main__body__inner" id="campaignsViewAdmin"></div>
    </main>

</section>

@endsection
