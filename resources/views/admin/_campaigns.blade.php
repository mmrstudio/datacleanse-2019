@extends('layouts.app')

@section('content')

<div class="section-header">
    <div class="section-header__title">&nbsp;</div>
    <div class="section-header__controls">
        <a href="{{ url('admin/campaigns/create') }}" class="button">New Campaign</a>
    </div>
</div>

<section class="main">

    @if($nav->show)
        @include('includes.sidebar-nav')
    @endif

    <main class="main__body">

        <h1 class="main__body__title">Campaigns</h1>

        <div class="main__body__inner">

            <table class="table table--striped">

                <thead>
                    <tr>
                        <th>Client</th>
                        <th>Campaign</th>
                        <th>Created</th>
                    </tr>
                </thead>

                @foreach($campaigns as $campaign)
                <tr>
                    <td>{{ $campaign->client->name }}</td>
                    <td>{{ $campaign->name }}</td>
                    <td>{{ $campaign->created_at }}</td>
                </tr>
                @endforeach

            </table>


        </div>

    </main>

</section>

@endsection
