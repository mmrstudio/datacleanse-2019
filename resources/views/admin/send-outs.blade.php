@extends('layouts.app')

@section('content')

<div class="section-header">
    <div class="section-header__title">{{ $campaign->client->name }} - {{ $campaign->name }}</div>
    <div class="section-header__controls">
        <a href="{{ url('admin/campaigns/create') }}" class="button">New Send Out</a>
    </div>
</div>

<section class="main">

    @if($nav->show)
        @include('includes.sidebar-nav')
    @endif

    <main class="main__body">
        <h1 class="main__body__title">Send Outs</h1>
        <div class="main__body__inner" id="sendOutsView" data-campaign="{{ $campaign->id }}"></div>
    </main>

</section>

@endsection
