@extends('layouts.app')

@section('content')

<div class="section-header">
    <div class="section-header__title">Reports</div>
    <div class="section-header__controls">
        <a href="{{ url('report/export/' . $report->id) }}" class="button">Export</a>
    </div>
</div>

<section class="main">

    @if($nav->show)
        @include('includes.sidebar-nav')
    @endif

    <main class="main__body">
        <h1 class="main__body__title">{{ $report->name }} – {{ count($report_data) }} records</h1>

        <div class="data">

            <div class="data__main">

                <div class="data__table">

                    @if($report_data)
                    <table class="table table--striped">

                        <thead>
                            <tr>
                                @foreach($columns as $column)
                                <th>{{ $column }}</th>
                                @endforeach
                            </tr>
                        </thead>

                        <tbody>

                            @foreach($report_data as $row)
                            <tr>
                                @foreach($row as $column => $value)
                                <td>{{ $value }}</td>
                                @endforeach
                            </tr>
                            @endforeach

                        </tbody>

                    </table>
                    @else
                    <table class="table table--striped">
                        <tbody>
                            <tr>
                                <td>No data to show</td>
                            </tr>
                        </tbody>
                    </table>
                    @endif

                </div>

            </div>

        </div>

    </main>

</section>

@endsection
