@extends('layouts.app')

@section('content')

<div class="section-header">
    <div class="section-header__title">{{ $campaign['title'] }}</div>
    <div class="section-header__controls">
        <div class="button-switch">
            <div class="button-switch__label">Campaign Status</div>
            <div class="button-switch__options" data-state="on">
                <div class="button-switch__option button-switch__option--on">Open</div>
                <div class="button-switch__option button-switch__option--off">Closed</div>
            </div>
        </div>
        <a href="#" class="button icon icon-arrow-down section-header__controls__export">Export Campaign</a>
    </div>
</div>

<section class="main">

    @if($nav->show)
        @include('includes.sidebar-nav')
    @endif

    <main class="main__body">

        <h1 class="main__body__title">Responses</h1>

        <div class="responses">

            <div class="responses__inner">

                <div class="main__body__sub-title">
                    {{ $send_out['title'] }}
                    <div class="main__body__sub-title__button">
                        <button class="button button--dark-blue button--small icon icon-arrow-down">Export Responses</button>
                    </div>
                </div>

                <div class="responses__select-category">

                    <div class="inline-dropdown">
                        <div class="inline-dropdown__label">{{ $responses['category_name'] }}</div>
                        <ul class="inline-dropdown__list">
                            @foreach($responses['categories'] as $key => $category)
                            <li class="inline-dropdown__list__link {{ $key == $responses['category'] ? 'inline-dropdown__list__link--active' : '' }}"><a href="{{ url('/responses/category/' . $key . '/send-out/' . $send_out['id']) }}">{{ $category }}</a></li>
                            @endforeach
                        </ul>
                    </div>

                </div>

                <div class="responses__table">

                    <table class="table table--striped table--border-top">

                        @foreach($responses['data'] as $response)
                        <tr>
                            <th>
                                {{ $response['name'] }}
                                @if($responses['category'] == 'changed')
                                <span class="table__sub-link">(<a href="{{ url('responses/compare/' . $response['field'] . '/send-out/' . $send_out['id']) }}">Compare</a>)</span>
                                @endif
                            </th>
                            <td class="text-right">{{ $response['value'] }}</td>
                        </tr>
                        @endforeach

                    </table>

                </div>

            </div>

        </div>

    </main>

</section>

@endsection
