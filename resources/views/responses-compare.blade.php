@extends('layouts.app')

@section('content')

<div class="section-header">
    <div class="section-header__title">{{ $campaign->client->name }} - {{ $campaign->name }}</div>
</div>

<section class="main">

    @if($nav->show)
        @include('includes.sidebar-nav')
    @endif

    <main class="main__body">

        <h1 class="main__body__title">Responses</h1>

        <div class="responses">

            <div class="responses__inner">

                <div class="main__body__sub-title">
                    {{ $send_out_title }}
                </div>

                <div class="responses__select-category">

                    <div class="inline-dropdown">
                        <div class="inline-dropdown__label">Compare...</div>
                        <ul class="inline-dropdown__list">
                            @foreach($dropdown as $item)
                            <li class="inline-dropdown__list__link {{ $item['current'] ? 'inline-dropdown__list__link--active' : '' }}"><a href="{{ url($item['url']) }}">{{ $item['label'] }}</a></li>
                            @endforeach
                        </ul>
                    </div>

                </div>

                <div class="responses__table">

                    <table class="table table--striped table--border-top">

                        <thead>

                            <tr>
                                <th>{{ $id_field->front_label }} ({{ $id_field->name }})</th>
                                <th>Old Value</th>
                                <th>New Value</th>
                            </tr>

                        </thead>

                        <tbody>

                            @foreach($results as $result)
                            <tr>
                                <td>{{ $result->record_id }} (<a href="{{ url('responses/record/' . $result->output_id) }}">View</a>)</td>
                                <td>{{ $result->old }}</td>
                                <td>{{ $result->new }}</td>
                            </tr>
                            @endforeach

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </main>

</section>

@endsection
