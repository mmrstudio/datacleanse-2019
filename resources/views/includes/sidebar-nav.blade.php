
    <aside class="main__sidebar sidebar" id="sidebarNav" data-items='{!! json_encode($nav) !!}'>

        <?php /*
        <nav class="sidebar__nav" id="sidebarNav">

            <ul class="sidebar__nav__list">

                @foreach($nav->items as $item)
                <li class="sidebar__nav__link {{ $item['active'] ? 'sidebar__nav__link--active sidebar__nav__link--open' : '' }}">
                    <a href="{{ $item['url'] }}">{{ $item['title'] }}</a>
                    @if($item['sub_items'])
                    <button class="sidebar__nav__link__expand"></button>
                    <div class="sidebar__nav__sub-nav">
                        <ul class="sidebar__nav__sub-nav__list">
                            @foreach($item['sub_items'] as $id => $sub_item)
                            <li class="sidebar__nav__link sidebar__nav__sub-nav__link {{ $sub_item['active'] ? 'sidebar__nav__sub-nav__link--active' : '' }}"><a href="{{ $sub_item['url'] }}">{{ $sub_item['title'] }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </li>
                @endforeach

            </ul>

        </nav>
        */ ?>

    </aside>
