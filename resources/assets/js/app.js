
import React from 'react';
import ReactDOM from 'react-dom';
import 'whatwg-fetch';
import 'es6-promise';
import 'react-fastclick';
import 'chart.js';
import 'datatables';

let el = (id) => {
    return document.getElementById(id);
}

// import styles
import '../scss/app.scss';

// app components
import SidebarNav from './components/SidebarNav';
import CampaignsViewAdmin from './components/CampaignsViewAdmin';
import SendoutsViewAdmin from './components/SendoutsViewAdmin';

let sidebarNav = el('sidebarNav');
if(sidebarNav !== null) {
    let nav = JSON.parse(sidebarNav.getAttribute('data-items'));
    ReactDOM.render((<SidebarNav show={nav.show} items={nav.items} />), sidebarNav);
}

let campaignsViewAdmin = el('campaignsViewAdmin');
if(campaignsViewAdmin !== null) {
    ReactDOM.render((<CampaignsViewAdmin />), campaignsViewAdmin);
}

let sendOutsView = el('sendOutsView');
if(sendOutsView !== null) {
    ReactDOM.render((<SendoutsViewAdmin campaign={sendOutsView.getAttribute('data-campaign')} />), sendOutsView);
}

    //
    // @@include('../../../node_modules/chart.js/dist/Chart.bundle.js')
    // @@include('../../../node_modules/select2/dist/js/select2.full.js')
    // @@include('../../../node_modules/datatables/media/js/jquery.dataTables.js')
    //
    // function capitalize(s) {
    //     return s[0].toUpperCase() + s.slice(1);
    // }
    //
    // function isNumeric(n) {
    //     return !isNaN(parseFloat(n)) && isFinite(n);
    // }
    //
    // function gaTrack(trackData) {
    //     ga('send', trackData.track, trackData.category, trackData.action, trackData.label, trackData.value);
    // }
    //
    // function trackLink() {
    //
    //     var link = $(this);
    //     var trackData = {
    //         track: link.data('track'),
    //         category: link.data('track-category'),
    //         action: link.data('track-action'),
    //         label: link.data('track-label'),
    //         value: link.data('track-value') ? link.data('track-value') : 0
    //     }
    //
    //     gaTrack(trackData);
    //
    // }
    //
    // function initTrackLinks($root) {
    //     $root.find('[data-track]').on('click', trackLink);
    // }
    //
    $(document).ready(function() {

        var $b = $('body');

        //initTrackLinks($('body'));

        // sidebar nav toggle
        var navToggle = $('#navToggle');
        if(navToggle.length > 0) {

            navToggle.on('click', function() {
                $b.toggleClass('nav-open');
            });

        }

        // sidebar nav
        var sidebarNav = $('#sidebarNav');

        if(sidebarNav.length > 0) {

            var sidebarSubNav = sidebarNav.find('.sidebar__nav__sub-nav');

            sidebarSubNav.each(function() {

                var subNav = $(this);
                var subNavOpenClass = 'sidebar__nav__link--open';
                var subNavParent = subNav.parent();
                var subNavList = subNav.find('.sidebar__nav__sub-nav__list');
                var subNavToggle = subNavParent.find('.sidebar__nav__link__expand');

                if(subNavParent.hasClass(subNavOpenClass)) {
                    var subNavHeight = subNavList.height();
                    subNav.css({ 'height' : subNavHeight });
                }

                subNavToggle.on('click', function() {

                    if(subNavParent.hasClass(subNavOpenClass)) {
                        subNav.css({ 'height' : 0 });
                        subNavParent.removeClass(subNavOpenClass);
                    } else {
                        var subNavHeight = subNavList.height();
                        subNav.css({ 'height' : subNavHeight });
                        subNavParent.addClass(subNavOpenClass);
                    }

                });

            });

        }

        // select2 fields
        //$(".form__select--select2").select2();

        var chartColours = {
            'red' : '#ef6673',
            'green' : '#48cfae',
            'pink' : '#ec87c1',
            'blue' : '#0090ce',
            'light-blue' : '#61c7eb',
            'dark-blue' : '#3c3c4a',
            'purple' : '#ac92ed',
            'grey-1' : '#f6f7f9',
            'grey-2' : '#dcdce0',
            'grey-3' : '#9696a0'
        };

        var overviewDoughnutChartEl = $("#overviewDoughnutChart");

        if(overviewDoughnutChartEl.length > 0) {

            var overviewDoughnutChartDataValues = [];
            var overviewDoughnutChartDataColours = [];
            var overviewDoughnutChartDataLabels = [];

            $.each(overviewDoughnutChartSrcData, function(i, item) {
                overviewDoughnutChartDataLabels.push(item.label);
                overviewDoughnutChartDataValues.push(item.value);
                overviewDoughnutChartDataColours.push(chartColours[item.colour]);
            });

            var overviewDoughnutChart = new Chart(overviewDoughnutChartEl, {
                type: 'doughnut',
                data: {
                    labels: overviewDoughnutChartDataLabels,
                    datasets: [
                        {
                            data: overviewDoughnutChartDataValues,
                            backgroundColor: overviewDoughnutChartDataColours,
                            hoverBackgroundColor: overviewDoughnutChartDataColours
                        }]
                },
                options: {
                    elements: {
                        arc: {
                            borderWidth: 1
                        }
                    },
                    legend: false
                }
            });

        }
        
        var overviewLineChartEl = $("#overviewLineChart");

        if(overviewLineChartEl.length > 0) {

            var overviewLineChartData = {
                labels: overviewLineChartSrcData.points,
                datasets: [{
                    label: overviewLineChartSrcData.label,
                    fill: false,
                    tension: 0,
                    backgroundColor: "#fff",
                    borderWidth: 6,
                    borderColor: chartColours[overviewLineChartSrcData.colour],
                    pointRadius: 12,
                    pointBorderColor: chartColours[overviewLineChartSrcData.colour],
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 6,
                    pointHoverRadius: 6,
                    data: overviewLineChartSrcData.values
                }]
            }

            var overviewLineChart = new Chart(overviewLineChartEl, {
                type: 'line',
                data: overviewLineChartData,
                options: {
                    legend: false
                }
            });

        }

        var dataTableEl = $('#dataTable');
        var dataTable = dataTableEl.DataTable({
            "scrollX": true,
            "processing": true,
            "serverSide": true,
            "ajax": "/data/table"
        });

        var dataTableView = $(dataTableEl.data('view'));
        dataTableView.find('button').on('click', function(e) {
            e.preventDefault();
            dataTable.page.len($(this).data('view')).draw();
        });

        var dataTablePage = $(dataTableEl.data('page-nav'));
        dataTablePage.find('button').on('click', function(e) {
            e.preventDefault();
            dataTable.page($(this).data('page')).draw('page');
        });

        dataTable.on('init.dt draw.dt', function(e, settings) {
            dataTableView.find("button").removeClass('active');
            dataTableView.find("button[data-view='" + settings._iDisplayLength + "']").addClass('active');
        });

        $('.inline-dropdown').on('click', function(e) {
            $(this).toggleClass('inline-dropdown--active');
        });

    });
