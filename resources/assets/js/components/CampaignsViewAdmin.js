import React from 'react';
import moment from 'moment';

class CampaignsViewAdmin extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            campaigns: []
        }

        this._fetchJSON = this._fetchJSON.bind(this);

    }

    _fetchJSON(url, callback) {

        fetch(url, {credentials: 'same-origin'}).then(function(response) {
            return response.json().then(callback);
        });

    }

    componentWillMount() {

        // fetch intial campaign data from server
        this._fetchJSON('/campaign', (campaigns) => {
            this.setState({ campaigns : campaigns });
        });

    }

    render() {

        let campaignRows = [];
        if(this.state.campaigns.length > 0) {
            this.state.campaigns.map((campaign, i) => {
                campaignRows.push((
                    <tr key={i}>
                        <td>{campaign.client.name}</td>
                        <td><strong>{campaign.name}</strong></td>
                        <td>{moment(campaign.created_at, 'YYYY-MM-DD HH:mm:ss').format('LLL')}</td>
                        <td className="text-right">
                            <a href={'/campaign/set/' + campaign.id + '/overview'} className="button button--small">Overview</a>
                            <a href={'/campaign/set/' + campaign.id + '/edit'} className="button button--small">Edit</a>
                        </td>
                    </tr>
                ));
            });
        }


        return (
            <table className="table table--striped">

                <thead>
                    <tr>
                        <th>Client</th>
                        <th>Campaign</th>
                        <th>Created</th>
                    </tr>
                </thead>

                <tbody>
                    {campaignRows}
                </tbody>

            </table>
        );
    }

}

CampaignsViewAdmin.contextTypes = {

};

export default CampaignsViewAdmin;
