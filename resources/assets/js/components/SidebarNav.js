import React from 'react';
import moment from 'moment';

class SidebarNav extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            items : this.props.items
        }

        this._toggleNavItem = this._toggleNavItem.bind(this);

    }

    componentDidMount() {
        console.log('this.state.items', this.state.items);
    }

    _toggleNavItem(item) {
        let navItems = this.state.items;
        navItems[item].open = !navItems[item].open;
        this.setState({ items : navItems });
    }

    render() {

        let navItems = [];
        Object.keys(this.state.items).map((navItemKey) => {
            let navItem = this.state.items[navItemKey];
            let navItemClasses = ['sidebar__nav__link'];

            // set active class
            if(navItem.active) navItemClasses.push('sidebar__nav__link--active');

            // render sub nav?
            let subNav = false;
            if(navItem.sub_items) {

                let navSubItems = [];
                Object.keys(navItem.sub_items).map((subNavItemKey) => {
                    let subNavItem = navItem.sub_items[subNavItemKey];
                    let subNavItemClasses = ['sidebar__nav__link', 'sidebar__nav__sub-nav__link'];

                    // set active classes
                    if(subNavItem.active) subNavItemClasses.push('sidebar__nav__sub-nav__link--active');

                    navSubItems.push((
                        <li key={subNavItemKey} className={subNavItemClasses.join(' ')}>
                            <a href={subNavItem.url}>{subNavItem.title}</a>
                        </li>
                    ));

                });

                subNav = (
                    <div className="sidebar__nav__sub-nav">
                        <ul className="sidebar__nav__sub-nav__list">
                            {navSubItems}
                        </ul>
                    </div>
                );

            }

            navItems.push((
                <li key={navItemKey} className={navItemClasses.join(' ')} data-open={this.state.items[navItemKey].open}>
                    <a href={navItem.url}>{navItem.title}</a>
                    {navItem.sub_items ? (<button className="sidebar__nav__link__expand" onClick={() => { this._toggleNavItem(navItemKey) }}></button>) : false}
                    {subNav}
                </li>
            ));

        });

        return (
            <nav className="sidebar__nav">
                <ul className="sidebar__nav__list">
                    {navItems}
                </ul>
            </nav>
        );
    }

}

SidebarNav.contextTypes = {

};

export default SidebarNav;
