import React from 'react';
import moment from 'moment';

class SendoutsViewAdmin extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            sendouts: []
        }

        this._fetchJSON = this._fetchJSON.bind(this);

    }

    _fetchJSON(url, callback) {

        fetch(url, {credentials: 'same-origin'}).then(function(response) {
            return response.json().then(callback);
        });

    }

    componentWillMount() {

        let sendoutURL = this.props.campaign ? '/send-out/campaign/' + this.props.campaign : '/send-out';

        // fetch intial sendout data from server
        this._fetchJSON(sendoutURL, (sendouts) => {
            this.setState({ sendouts : sendouts });
        });

    }

    render() {

        let sendoutRows = [];
        if(this.state.sendouts.length > 0) {
            this.state.sendouts.map((sendout, i) => {
                sendoutRows.push((
                    <tr key={i}>
                        <td><strong>{sendout.name}</strong></td>
                        <td>{sendout.subject}</td>
                        <td>{moment(sendout.created_at, 'YYYY-MM-DD HH:mm:ss').format('LLL')}</td>
                        <td className="text-right">
                            { /* <a href={'/send-out/' + sendout.id + '/edit'} className="button button--inline button--red button--small">Delete</a> */ }
                            <a href={'/send-out/' + sendout.id + '/edit'} className="button button--inline button--small">Edit</a>
                        </td>
                    </tr>
                ));
            });
        }


        return (
            <table className="table table--striped">

                <thead>
                    <tr>
                        <th>Send Out</th>
                        <th>Subject</th>
                        <th>Created</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>

                <tbody>
                    {sendoutRows}
                </tbody>

            </table>
        );
    }

}

SendoutsViewAdmin.contextTypes = {

};

export default SendoutsViewAdmin;
