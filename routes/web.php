<?php

use App\Http\Controllers\FrontController;
use App\Campaign;
use App\Client;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// temp putting this up here so it frickin works
Route::get('report/export/{report_id}', 'CampaignController@reportExport');
Route::get('report/{report_id}', 'CampaignController@report');

$zoos_routes = function () {

    // require functions
    $project_functions = dirname(__FILE__) . '/../public/assets/zoos-victoria_renew/functions.php';
    if(is_file($project_functions)) require_once($project_functions);

    $client_slug = 'zoos-victoria';
    $campaign_slug = 'renew';

    Route::post('send/{hash}', function ($hash) {
        $send = sendToFriends();
        return response()->json(true);
    });

    Route::post('video', function () {
        $videoUrl = renderVideo();
        return response()->json($videoUrl);
    });

    Route::get('/', function () use ($client_slug, $campaign_slug) {
        $front = new FrontController;
        return $front->primaryLanding($client_slug, $campaign_slug);
    });

    Route::get('member/{hash}', function ($hash) use ($client_slug, $campaign_slug) {
        $front = new FrontController;
        return $front->primaryLanding($client_slug, $campaign_slug, $hash);
    });

};

// ZOOS VIC ROUTES
Route::group(['domain' => 'zoos.vhx.host'], $zoos_routes);
Route::group(['domain' => 'zoos.mmrserver.com'], $zoos_routes);
Route::group(['domain' => 'renewforthezoo.org.au'], $zoos_routes);


// FRONT/LANDING (public)

//Route::auth();
Auth::routes();

Route::get('pw/{pw}', function ($pw) {
    return $pw . ':<br>' . '<textarea style="width:100%;" rows="50">' . bcrypt($pw) . '</textarea>';
});

Route::get('/{client}/{campaign}/', 'FrontController@primaryLanding');
Route::get('/{client}/{campaign}/register', 'FrontController@primaryLanding');
Route::get('/{client}/{campaign}/update/{hash}', 'FrontController@primaryLanding');
Route::get('/{client}/{campaign}/update/{hash}/{sendout}', 'FrontController@primaryLanding');
Route::post('/{client}/{campaign}/update/{hash}', 'FrontController@primaryProcess');
Route::get('/{client}/{campaign}/confirm/{hash}', 'FrontController@confirm');
Route::get('/{client}/{campaign}/confirm/{hash}/{sendout}', 'FrontController@confirm');
Route::get('/{client}/{campaign}/step-2/{hash}', 'FrontController@secondaryLanding');
Route::post('/{client}/{campaign}/step-2/{hash}', 'FrontController@secondaryProcess');
Route::get('/{client}/{campaign}/thanks/{hash}', 'FrontController@thanks');
Route::get('/{client}/{campaign}/thanks', 'FrontController@thanks');
Route::get('/{client}/{campaign}/update', 'FrontController@primaryLanding');
Route::post('/{client}/{campaign}/update', 'FrontController@primaryProcessNew');
Route::get('/{client}/{campaign}/page/{view}', 'FrontController@page');

// TEST URL
Route::get('/{client}/{campaign}/test/{view_file}/{field_key}/{field_value}', 'FrontController@testData');

Route::post('/link', 'FrontController@createShortUrl');

// DASHBOARD (private)

Route::get('/', 'CampaignController@home');


Route::get('home', 'CampaignController@home');

// campaigns
Route::get('campaigns', 'CampaignController@home');

// campaign responses
Route::get('/responses/send-out/{send_out}', 'HomeController@responses');
Route::get('/responses/field/{field_name}', 'CampaignController@responsesField');
Route::get('/responses/field/{field_name}/send-out/{send_out}', 'CampaignController@responsesField');
Route::get('/responses/compare/{field}', 'CampaignController@responsesCompare');
Route::get('/responses/compare/{field}/send-out/{send_out}', 'HomeController@responses_compare');
Route::get('/responses/record/{record_id}', 'CampaignController@responsesRecord');
Route::get('/responses', 'CampaignController@responses');

// campaign traffic
Route::get('/traffic/send-out/{send_out}', 'HomeController@traffic');
Route::get('/traffic', 'HomeController@traffic');

// campaign data
//Route::get('/data/send-out/{send_out}', 'CampaignController@data');
Route::get('/data', 'CampaignController@data');
Route::get('/data/table', 'CampaignController@dataTable');

// campaigns
Route::get('clients', 'AdminController@clients');
Route::get('client/edit/{client_id}', 'AdminController@editClient');

// reports
Route::get('reports', 'CampaignController@reports');

// admin
//Route::get('/admin/campaigns', 'AdminController@campaigns');

// REST

// campaigns
Route::get('campaign/set/{campaign_id}', 'CampaignController@set');
Route::get('campaign/set/{campaign_id}/{redirect}', 'CampaignController@set');
Route::get('overview/send-out/{send_out}', 'CampaignController@overview');
Route::get('overview', 'CampaignController@overview');
Route::get('overview/{campaign_id}/refresh', 'CampaignController@refreshOverview');
Route::get('responses/{campaign_id}/refresh', 'CampaignController@refreshFieldStats');
Route::get('campaign/tables/create/input/{campaign_id}', 'CampaignController@createInputTable');
Route::get('campaign/tables', 'CampaignController@getTables');
Route::resource('campaign', 'CampaignController');
Route::resource('campaign.fields', 'CampaignFieldController');
Route::resource('campaign.tables', 'CampaignTableController');

Route::get('overview/{campaign_id}/analytics', 'CampaignController@analytics');
Route::get('overview/{campaign_id}/results', 'CampaignController@results');

// sendouts
Route::resource('send-out', 'SendoutController');
Route::get('send-out/campaign/{campaign_id}', 'SendoutController@index');
Route::get('campaign/{campaign_id}/send-outs', 'SendoutController@home');

//Auth
Route::get('/logout', function () {
    Auth::logout();
    return redirect('/login');
});

Route::get('file', 'fileController@index');
 Route::get('save', 'fileController@save');

 