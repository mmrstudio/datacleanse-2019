<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CMListMember extends Model
{

    protected $table = 'cm_list_members';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function exists($list_id, $input_id)
    {
        return CMListMember::where('list_id', $list_id)->where('input_id', $input_id)->count() > 0 ? true : false;
    }

}
