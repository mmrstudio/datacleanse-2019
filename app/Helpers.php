<?php

    function select($first_item, $list)
    {
        $merge = is_array($first_item) ? $first_item : ['' => $first_item];
        return array_merge($merge, $list);
    }

    function select_range($from, $to, $pad_length=false, $pad_str=false, $pad_direction=false) {
        $range = range($from, $to);
        $return = [];
        foreach($range as $val)
        {
            if($pad_length) $val = str_pad($val, $pad_length, $pad_str, $pad_direction);
            $return['_' . $val] = $val;
        }
        return $return;
    }

    function australian_states()
    {
        return [
            'ACT' => 'Australian Capital Territory',
            'NSW' => 'New South Wales',
            'NT' => 'Northern Territory',
            'QLD' => 'Queensland',
            'SA' => 'South Australia',
            'TAS' => 'Tasmania',
            'VIC' => 'Victoria',
            'WA' => 'Western Australia',
            //'-' => '-----------------',
            //'OTHER' => 'Other',
        ];
    }

    function nz_states()
    {
        return [
            'Auckland' => 'Auckland',
            'Bay of Plenty' => 'Bay of Plenty',
            'Canterbury' => 'Canterbury',
            'East Cape' => 'East Cape',
            'Hawkes Bay' => 'Hawkes Bay',
            'Manawatu-Whanganui' => 'Manawatu-Whanganui',
            'Marlborough' => 'Marlborough',
            'Nelson' => 'Nelson',
            'Northland' => 'Northland',
            'Otago' => 'Otago',
            'Southland' => 'Southland',
            'Taranaki' => 'Taranaki',
            'Tasman' => 'Tasman',
            'Waikato' => 'Waikato',
            'Wanganui' => 'Wanganui',
            'Wellington' => 'Wellington',
            'West Coast' => 'West Coast',
        ];
    }

    function genders()
    {
        return [
            'Male' => 'Male',
            'Female' => 'Female',
        ];
    }


    function countries($format='full')
    {
        $countries = [];

        $countries['full'] = [
            'AUSTRALIA' => 'Australia',
            'NEW ZEALAND' => 'New Zealand',
            '-' => '-----------------',
            'AFGHANISTAN' => 'Afghanistan',
            'ALBANIA' => 'Albania',
            'ALGERIA' => 'Algeria',
            'AMERICAN SAMOA' => 'American Samoa',
            'ANDORRA' => 'Andorra',
            'ANGOLA' => 'Angola',
            'ANGUILLA' => 'Anguilla',
            'ANTARCTICA' => 'Antarctica',
            'ANTIGUA AND BARBUDA' => 'Antigua and Barbuda',
            'ARGENTINA' => 'Argentina',
            'ARMENIA' => 'Armenia',
            'ARUBA' => 'Aruba',
            'AUSTRIA' => 'Austria',
            'AZERBAIJAN' => 'Azerbaijan',
            'BAHAMAS' => 'Bahamas',
            'BAHRAIN' => 'Bahrain',
            'BANGLADESH' => 'Bangladesh',
            'BARBADOS' => 'Barbados',
            'BELARUS' => 'Belarus',
            'BELGIUM' => 'Belgium',
            'BELIZE' => 'Belize',
            'BENIN' => 'Benin',
            'BERMUDA' => 'Bermuda',
            'BHUTAN' => 'Bhutan',
            'BOLIVIA' => 'Bolivia',
            'BOSNIA AND HERZEGOWINA' => 'Bosnia and Herzegowina',
            'BOTSWANA' => 'Botswana',
            'BOUVET ISLAND' => 'Bouvet Island',
            'BRAZIL' => 'Brazil',
            'BRITISH INDIAN OCEAN TERRITORY' => 'British Indian Ocean Territory',
            'BRUNEI DARUSSALAM' => 'Brunei Darussalam',
            'BULGARIA' => 'Bulgaria',
            'BURKINA FASO' => 'Burkina Faso',
            'BURUNDI' => 'Burundi',
            'CAMBODIA' => 'Cambodia',
            'CAMEROON' => 'Cameroon',
            'CANADA' => 'Canada',
            'CAPE VERDE' => 'Cape Verde',
            'CAYMAN ISLANDS' => 'Cayman Islands',
            'CENTRAL AFRICAN REPUBLIC' => 'Central African Republic',
            'CHAD' => 'Chad',
            'CHILE' => 'Chile',
            'CHINA' => 'China',
            'CHRISTMAS ISLAND' => 'Christmas Island',
            'COCOS (KEELING) ISLANDS' => 'Cocos (Keeling) Islands',
            'COLOMBIA' => 'Colombia',
            'COMOROS' => 'Comoros',
            'CONGO' => 'Congo',
            'CONGO, THE DEMOCRATIC REPUBLIC OF THE' => 'Congo, the Democratic Republic of the',
            'COOK ISLANDS' => 'Cook Islands',
            'COSTA RICA' => 'Costa Rica',
            'COTE DIVOIRE' => 'Cote d\'Ivoire',
            'CROATIA (HRVATSKA)' => 'Croatia (Hrvatska)',
            'CUBA' => 'Cuba',
            'CYPRUS' => 'Cyprus',
            'CZECH REPUBLIC' => 'Czech Republic',
            'DENMARK' => 'Denmark',
            'DJIBOUTI' => 'Djibouti',
            'DOMINICA' => 'Dominica',
            'DOMINICAN REPUBLIC' => 'Dominican Republic',
            'EAST TIMOR' => 'East Timor',
            'ECUADOR' => 'Ecuador',
            'EGYPT' => 'Egypt',
            'EL SALVADOR' => 'El Salvador',
            'EQUATORIAL GUINEA' => 'Equatorial Guinea',
            'ERITREA' => 'Eritrea',
            'ESTONIA' => 'Estonia',
            'ETHIOPIA' => 'Ethiopia',
            'FALKLAND ISLANDS (MALVINAS)' => 'Falkland Islands (Malvinas)',
            'FAROE ISLANDS' => 'Faroe Islands',
            'FIJI' => 'Fiji',
            'FINLAND' => 'Finland',
            'FRANCE' => 'France',
            'FRANCE METROPOLITAN' => 'France Metropolitan',
            'FRENCH GUIANA' => 'French Guiana',
            'FRENCH POLYNESIA' => 'French Polynesia',
            'FRENCH SOUTHERN TERRITORIES' => 'French Southern Territories',
            'GABON' => 'Gabon',
            'GAMBIA' => 'Gambia',
            'GEORGIA' => 'Georgia',
            'GERMANY' => 'Germany',
            'GHANA' => 'Ghana',
            'GIBRALTAR' => 'Gibraltar',
            'GREECE' => 'Greece',
            'GREENLAND' => 'Greenland',
            'GRENADA' => 'Grenada',
            'GUADELOUPE' => 'Guadeloupe',
            'GUAM' => 'Guam',
            'GUATEMALA' => 'Guatemala',
            'GUINEA' => 'Guinea',
            'GUINEA-BISSAU' => 'Guinea-Bissau',
            'GUYANA' => 'Guyana',
            'HAITI' => 'Haiti',
            'HEARD AND MC DONALD ISLANDS' => 'Heard and Mc Donald Islands',
            'HOLY SEE (VATICAN CITY STATE)' => 'Holy See (Vatican City State)',
            'HONDURAS' => 'Honduras',
            'HONG KONG' => 'Hong Kong',
            'HUNGARY' => 'Hungary',
            'ICELAND' => 'Iceland',
            'INDIA' => 'India',
            'INDONESIA' => 'Indonesia',
            'IRAN (ISLAMIC REPUBLIC OF)' => 'Iran (Islamic Republic of)',
            'IRAQ' => 'Iraq',
            'IRELAND' => 'Ireland',
            'ISRAEL' => 'Israel',
            'ITALY' => 'Italy',
            'JAMAICA' => 'Jamaica',
            'JAPAN' => 'Japan',
            'JORDAN' => 'Jordan',
            'KAZAKHSTAN' => 'Kazakhstan',
            'KENYA' => 'Kenya',
            'KIRIBATI' => 'Kiribati',
            'KOREA, DEMOCRATIC PEOPLES REPUBLIC OF' => 'Korea, Democratic People\'s Republic of',
            'KOREA, REPUBLIC OF' => 'Korea, Republic of',
            'KUWAIT' => 'Kuwait',
            'KYRGYZSTAN' => 'Kyrgyzstan',
            'LAO, PEOPLES DEMOCRATIC REPUBLIC' => 'Lao, People\'s Democratic Republic',
            'LATVIA' => 'Latvia',
            'LEBANON' => 'Lebanon',
            'LESOTHO' => 'Lesotho',
            'LIBERIA' => 'Liberia',
            'LIBYAN ARAB JAMAHIRIYA' => 'Libyan Arab Jamahiriya',
            'LIECHTENSTEIN' => 'Liechtenstein',
            'LITHUANIA' => 'Lithuania',
            'LUXEMBOURG' => 'Luxembourg',
            'MACAU' => 'Macau',
            'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF' => 'Macedonia, The Former Yugoslav Republic of',
            'MADAGASCAR' => 'Madagascar',
            'MALAWI' => 'Malawi',
            'MALAYSIA' => 'Malaysia',
            'MALDIVES' => 'Maldives',
            'MALI' => 'Mali',
            'MALTA' => 'Malta',
            'MARSHALL ISLANDS' => 'Marshall Islands',
            'MARTINIQUE' => 'Martinique',
            'MAURITANIA' => 'Mauritania',
            'MAURITIUS' => 'Mauritius',
            'MAYOTTE' => 'Mayotte',
            'MEXICO' => 'Mexico',
            'MICRONESIA, FEDERATED STATES OF' => 'Micronesia, Federated States of',
            'MOLDOVA, REPUBLIC OF' => 'Moldova, Republic of',
            'MONACO' => 'Monaco',
            'MONGOLIA' => 'Mongolia',
            'MONTSERRAT' => 'Montserrat',
            'MOROCCO' => 'Morocco',
            'MOZAMBIQUE' => 'Mozambique',
            'MYANMAR' => 'Myanmar',
            'NAMIBIA' => 'Namibia',
            'NAURU' => 'Nauru',
            'NEPAL' => 'Nepal',
            'NETHERLANDS' => 'Netherlands',
            'NETHERLANDS ANTILLES' => 'Netherlands Antilles',
            'NEW CALEDONIA' => 'New Caledonia',
            'NICARAGUA' => 'Nicaragua',
            'NIGER' => 'Niger',
            'NIGERIA' => 'Nigeria',
            'NIUE' => 'Niue',
            'NORFOLK ISLAND' => 'Norfolk Island',
            'NORTHERN MARIANA ISLANDS' => 'Northern Mariana Islands',
            'NORWAY' => 'Norway',
            'OMAN' => 'Oman',
            'PAKISTAN' => 'Pakistan',
            'PALAU' => 'Palau',
            'PANAMA' => 'Panama',
            'PAPUA NEW GUINEA' => 'Papua New Guinea',
            'PARAGUAY' => 'Paraguay',
            'PERU' => 'Peru',
            'PHILIPPINES' => 'Philippines',
            'PITCAIRN' => 'Pitcairn',
            'POLAND' => 'Poland',
            'PORTUGAL' => 'Portugal',
            'PUERTO RICO' => 'Puerto Rico',
            'QATAR' => 'Qatar',
            'REUNION' => 'Reunion',
            'ROMANIA' => 'Romania',
            'RUSSIAN FEDERATION' => 'Russian Federation',
            'RWANDA' => 'Rwanda',
            'SAINT KITTS AND NEVIS' => 'Saint Kitts and Nevis',
            'SAINT LUCIA' => 'Saint Lucia',
            'SAINT VINCENT AND THE GRENADINES' => 'Saint Vincent and the Grenadines',
            'SAMOA' => 'Samoa',
            'SAN MARINO' => 'San Marino',
            'SAO TOME AND PRINCIPE' => 'Sao Tome and Principe',
            'SAUDI ARABIA' => 'Saudi Arabia',
            'SENEGAL' => 'Senegal',
            'SEYCHELLES' => 'Seychelles',
            'SIERRA LEONE' => 'Sierra Leone',
            'SINGAPORE' => 'Singapore',
            'SLOVAKIA (SLOVAK REPUBLIC)' => 'Slovakia (Slovak Republic)',
            'SLOVENIA' => 'Slovenia',
            'SRI LANKA' => 'Sri Lanka',
            'SOLOMON ISLANDS' => 'Solomon Islands',
            'SOMALIA' => 'Somalia',
            'SOUTH AFRICA' => 'South Africa',
            'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS' => 'South Georgia and the South Sandwich Islands',
            'SPAIN' => 'Spain',
            'ST. HELENA' => 'St. Helena',
            'ST. PIERRE AND MIQUELON' => 'St. Pierre and Miquelon',
            'SUDAN' => 'Sudan',
            'SURINAME' => 'Suriname',
            'SVALBARD AND JAN MAYEN ISLANDS' => 'Svalbard and Jan Mayen Islands',
            'SWAZILAND' => 'Swaziland',
            'SWEDEN' => 'Sweden',
            'SWITZERLAND' => 'Switzerland',
            'SYRIAN ARAB REPUBLIC' => 'Syrian Arab Republic',
            'TAIWAN' => 'Taiwan, Province of China',
            'TAJIKISTAN' => 'Tajikistan',
            'TANZANIA, UNITED REPUBLIC OF' => 'Tanzania, United Republic of',
            'THAILAND' => 'Thailand',
            'TOGO' => 'Togo',
            'TOKELAU' => 'Tokelau',
            'TONGA' => 'Tonga',
            'TRINIDAD AND TOBAGO' => 'Trinidad and Tobago',
            'TUNISIA' => 'Tunisia',
            'TURKEY' => 'Turkey',
            'TURKMENISTAN' => 'Turkmenistan',
            'TURKS AND CAICOS ISLANDS' => 'Turks and Caicos Islands',
            'TUVALU' => 'Tuvalu',
            'UGANDA' => 'Uganda',
            'UKRAINE' => 'Ukraine',
            'UNITED ARAB EMIRATES' => 'United Arab Emirates',
            'UNITED KINGDOM' => 'United Kingdom',
            'USA' => 'United States',
            'UNITED STATES MINOR OUTLYING ISLANDS' => 'United States Minor Outlying Islands',
            'URUGUAY' => 'Uruguay',
            'UZBEKISTAN' => 'Uzbekistan',
            'VANUATU' => 'Vanuatu',
            'VENEZUELA' => 'Venezuela',
            'VIETNAM' => 'Vietnam',
            'VIRGIN ISLANDS (BRITISH)' => 'Virgin Islands (British)',
            'VIRGIN ISLANDS (U.S.)' => 'Virgin Islands (U.S.)',
            'WALLIS AND FUTUNA ISLANDS' => 'Wallis and Futuna Islands',
            'WESTERN SAHARA' => 'Western Sahara',
            'YEMEN' => 'Yemen',
            'ZAMBIA' => 'Zambia',
            'ZIMBABWE' => 'Zimbabwe',
        ];

        $countries['alpha-3'] = [
            'AUS' => 'Australia',
            'NZL' => 'New Zealand',
            '-' => '-----------------',
            'ABW'=>'Aruba',
            'AFG'=>'Afghanistan',
            'AGO'=>'Angola',
            'AIA'=>'Anguilla',
            'ALA'=>'Åland Islands',
            'ALB'=>'Albania',
            'AND'=>'Andorra',
            'ARE'=>'United Arab Emirates',
            'ARG'=>'Argentina',
            'ARM'=>'Armenia',
            'ASM'=>'American Samoa',
            'ATA'=>'Antarctica',
            'ATF'=>'French Southern Territories',
            'ATG'=>'Antigua and Barbuda',
            'AUT'=>'Austria',
            'AZE'=>'Azerbaijan',
            'BDI'=>'Burundi',
            'BEL'=>'Belgium',
            'BEN'=>'Benin',
            'BES'=>'Bonaire, Sint Eustatius and Saba',
            'BFA'=>'Burkina Faso',
            'BGD'=>'Bangladesh',
            'BGR'=>'Bulgaria',
            'BHR'=>'Bahrain',
            'BHS'=>'Bahamas',
            'BIH'=>'Bosnia and Herzegovina',
            'BLM'=>'Saint Barthélemy',
            'BLR'=>'Belarus',
            'BLZ'=>'Belize',
            'BMU'=>'Bermuda',
            'BOL'=>'Bolivia, Plurinational State of',
            'BRA'=>'Brazil',
            'BRB'=>'Barbados',
            'BRN'=>'Brunei Darussalam',
            'BTN'=>'Bhutan',
            'BVT'=>'Bouvet Island',
            'BWA'=>'Botswana',
            'CAF'=>'Central African Republic',
            'CAN'=>'Canada',
            'CCK'=>'Cocos (Keeling) Islands',
            'CHE'=>'Switzerland',
            'CHL'=>'Chile',
            'CHN'=>'China',
            'CIV'=>'Côte d\'Ivoire',
            'CMR'=>'Cameroon',
            'COD'=>'Congo, the Democratic Republic of the',
            'COG'=>'Congo',
            'COK'=>'Cook Islands',
            'COL'=>'Colombia',
            'COM'=>'Comoros',
            'CPV'=>'Cape Verde',
            'CRI'=>'Costa Rica',
            'CUB'=>'Cuba',
            'CUW'=>'Curaçao',
            'CXR'=>'Christmas Island',
            'CYM'=>'Cayman Islands',
            'CYP'=>'Cyprus',
            'CZE'=>'Czech Republic',
            'DEU'=>'Germany',
            'DJI'=>'Djibouti',
            'DMA'=>'Dominica',
            'DNK'=>'Denmark',
            'DOM'=>'Dominican Republic',
            'DZA'=>'Algeria',
            'ECU'=>'Ecuador',
            'EGY'=>'Egypt',
            'ERI'=>'Eritrea',
            'ESH'=>'Western Sahara',
            'ESP'=>'Spain',
            'EST'=>'Estonia',
            'ETH'=>'Ethiopia',
            'FIN'=>'Finland',
            'FJI'=>'Fiji',
            'FLK'=>'Falkland Islands (Malvinas)',
            'FRA'=>'France',
            'FRO'=>'Faroe Islands',
            'FSM'=>'Micronesia, Federated States of',
            'GAB'=>'Gabon',
            'GBR'=>'United Kingdom',
            'GEO'=>'Georgia',
            'GGY'=>'Guernsey',
            'GHA'=>'Ghana',
            'GIB'=>'Gibraltar',
            'GIN'=>'Guinea',
            'GLP'=>'Guadeloupe',
            'GMB'=>'Gambia',
            'GNB'=>'Guinea-Bissau',
            'GNQ'=>'Equatorial Guinea',
            'GRC'=>'Greece',
            'GRD'=>'Grenada',
            'GRL'=>'Greenland',
            'GTM'=>'Guatemala',
            'GUF'=>'French Guiana',
            'GUM'=>'Guam',
            'GUY'=>'Guyana',
            'HKG'=>'Hong Kong',
            'HMD'=>'Heard Island and McDonald Islands',
            'HND'=>'Honduras',
            'HRV'=>'Croatia',
            'HTI'=>'Haiti',
            'HUN'=>'Hungary',
            'IDN'=>'Indonesia',
            'IMN'=>'Isle of Man',
            'IND'=>'India',
            'IOT'=>'British Indian Ocean Territory',
            'IRL'=>'Ireland',
            'IRN'=>'Iran, Islamic Republic of',
            'IRQ'=>'Iraq',
            'ISL'=>'Iceland',
            'ISR'=>'Israel',
            'ITA'=>'Italy',
            'JAM'=>'Jamaica',
            'JEY'=>'Jersey',
            'JOR'=>'Jordan',
            'JPN'=>'Japan',
            'KAZ'=>'Kazakhstan',
            'KEN'=>'Kenya',
            'KGZ'=>'Kyrgyzstan',
            'KHM'=>'Cambodia',
            'KIR'=>'Kiribati',
            'KNA'=>'Saint Kitts and Nevis',
            'KOR'=>'Korea, Republic of',
            'KWT'=>'Kuwait',
            'LAO'=>'Lao People\'s Democratic Republic',
            'LBN'=>'Lebanon',
            'LBR'=>'Liberia',
            'LBY'=>'Libya',
            'LCA'=>'Saint Lucia',
            'LIE'=>'Liechtenstein',
            'LKA'=>'Sri Lanka',
            'LSO'=>'Lesotho',
            'LTU'=>'Lithuania',
            'LUX'=>'Luxembourg',
            'LVA'=>'Latvia',
            'MAC'=>'Macao',
            'MAF'=>'Saint Martin (French part)',
            'MAR'=>'Morocco',
            'MCO'=>'Monaco',
            'MDA'=>'Moldova, Republic of',
            'MDG'=>'Madagascar',
            'MDV'=>'Maldives',
            'MEX'=>'Mexico',
            'MHL'=>'Marshall Islands',
            'MKD'=>'Macedonia, the former Yugoslav Republic of',
            'MLI'=>'Mali',
            'MLT'=>'Malta',
            'MMR'=>'Myanmar',
            'MNE'=>'Montenegro',
            'MNG'=>'Mongolia',
            'MNP'=>'Northern Mariana Islands',
            'MOZ'=>'Mozambique',
            'MRT'=>'Mauritania',
            'MSR'=>'Montserrat',
            'MTQ'=>'Martinique',
            'MUS'=>'Mauritius',
            'MWI'=>'Malawi',
            'MYS'=>'Malaysia',
            'MYT'=>'Mayotte',
            'NAM'=>'Namibia',
            'NCL'=>'New Caledonia',
            'NER'=>'Niger',
            'NFK'=>'Norfolk Island',
            'NGA'=>'Nigeria',
            'NIC'=>'Nicaragua',
            'NIU'=>'Niue',
            'NLD'=>'Netherlands',
            'NOR'=>'Norway',
            'NPL'=>'Nepal',
            'NRU'=>'Nauru',
            'OMN'=>'Oman',
            'PAK'=>'Pakistan',
            'PAN'=>'Panama',
            'PCN'=>'Pitcairn',
            'PER'=>'Peru',
            'PHL'=>'Philippines',
            'PLW'=>'Palau',
            'PNG'=>'Papua New Guinea',
            'POL'=>'Poland',
            'PRI'=>'Puerto Rico',
            'PRK'=>'Korea, Democratic People\'s Republic of',
            'PRT'=>'Portugal',
            'PRY'=>'Paraguay',
            'PSE'=>'Palestinian Territory, Occupied',
            'PYF'=>'French Polynesia',
            'QAT'=>'Qatar',
            'REU'=>'Réunion',
            'ROU'=>'Romania',
            'RUS'=>'Russian Federation',
            'RWA'=>'Rwanda',
            'SAU'=>'Saudi Arabia',
            'SDN'=>'Sudan',
            'SEN'=>'Senegal',
            'SGP'=>'Singapore',
            'SGS'=>'South Georgia and the South Sandwich Islands',
            'SHN'=>'Saint Helena, Ascension and Tristan da Cunha',
            'SJM'=>'Svalbard and Jan Mayen',
            'SLB'=>'Solomon Islands',
            'SLE'=>'Sierra Leone',
            'SLV'=>'El Salvador',
            'SMR'=>'San Marino',
            'SOM'=>'Somalia',
            'SPM'=>'Saint Pierre and Miquelon',
            'SRB'=>'Serbia',
            'SSD'=>'South Sudan',
            'STP'=>'Sao Tome and Principe',
            'SUR'=>'Suriname',
            'SVK'=>'Slovakia',
            'SVN'=>'Slovenia',
            'SWE'=>'Sweden',
            'SWZ'=>'Swaziland',
            'SXM'=>'Sint Maarten (Dutch part)',
            'SYC'=>'Seychelles',
            'SYR'=>'Syrian Arab Republic',
            'TCA'=>'Turks and Caicos Islands',
            'TCD'=>'Chad',
            'TGO'=>'Togo',
            'THA'=>'Thailand',
            'TJK'=>'Tajikistan',
            'TKL'=>'Tokelau',
            'TKM'=>'Turkmenistan',
            'TLS'=>'Timor-Leste',
            'TON'=>'Tonga',
            'TTO'=>'Trinidad and Tobago',
            'TUN'=>'Tunisia',
            'TUR'=>'Turkey',
            'TUV'=>'Tuvalu',
            'TWN'=>'Taiwan, Province of China',
            'TZA'=>'Tanzania, United Republic of',
            'UGA'=>'Uganda',
            'UKR'=>'Ukraine',
            'UMI'=>'United States Minor Outlying Islands',
            'URY'=>'Uruguay',
            'USA'=>'United States',
            'UZB'=>'Uzbekistan',
            'VAT'=>'Holy See (Vatican City State)',
            'VCT'=>'Saint Vincent and the Grenadines',
            'VEN'=>'Venezuela, Bolivarian Republic of',
            'VGB'=>'Virgin Islands, British',
            'VIR'=>'Virgin Islands, U.S.',
            'VNM'=>'Viet Nam',
            'VUT'=>'Vanuatu',
            'WLF'=>'Wallis and Futuna',
            'WSM'=>'Samoa',
            'YEM'=>'Yemen',
            'ZAF'=>'South Africa',
            'ZMB'=>'Zambia',
            'ZWE'=>'Zimbabwe',
        ];

        return $countries[$format];
    }

    function afl_clubs()
    {
        return [
            'Adelaide Crows' => 'Adelaide Crows',
            'Brisbane Lions' => 'Brisbane Lions',
            'Carlton' => 'Carlton',
            'Collingwood' => 'Collingwood',
            'Essendon' => 'Essendon',
            'Fremantle' => 'Fremantle',
            'Geelong Cats' => 'Geelong Cats',
            'Gold Coast SUNS' => 'Gold Coast SUNS',
            'GWS GIANTS' => 'GWS GIANTS',
            'Hawthorn' => 'Hawthorn',
            'Kangaroos' => 'Kangaroos',
            'Melbourne' => 'Melbourne',
            'Port Adelaide' => 'Port Adelaide',
            'Richmond' => 'Richmond',
            'St Kilda' => 'St Kilda',
            'Sydney Swans' => 'Sydney Swans',
            'West Coast Eagles' => 'West Coast Eagles',
            'Western Bulldogs' => 'Western Bulldogs',
        ];
    }

function titles()
{
    return [
        'Mr' => 'Mr',
        'Mrs' => 'Mrs',
        'Ms' => 'Ms',
        'Miss' => 'Miss',
        'Master' => 'Master',
        'Dr' => 'Dr',
        'Professor' => 'Professor',
        'Sister' => 'Sister',
        'Rev' => 'Rev',
        'Father' => 'Father',
        'Sir' => 'Sir',
        'Cmdr' => 'Cmdr',
        'Judge' => 'Judge',
    ];
}

function now($time=false, $hours_minutes=true) {
    $format = $hours_minutes ? 'Y-m-d H:i:s' : 'Y-m-d';
    return $time ? date($format, $time) : date($format);
}

function db_tables() {

    $tables = [];
    $get_tables = \DB::select('SHOW TABLES');

    foreach ($get_tables as $table)
    {
        foreach ($table as $key => $table_name)
        {
            $tables[] = $table_name;
        }
    }

    return $tables;

}

function db_campaign_tables($campaign_id=false) {

    $tables = [];
    $get_tables = db_tables();

    foreach ($get_tables as $table_name)
    {
        if($campaign_id)
        {

            $campaign_id = str_pad($campaign_id, 4, '0', STR_PAD_LEFT);

            if(strpos($table_name, '_' . $campaign_id . '_') !== false)
            {
                $tables[] = $table_name;
            }

        }
        else
        {

            if(substr($table_name, 0, 1) == '_')
            {
                $tables[] = $table_name;
            }

        }

    }

    return $tables;

}

function count_lines($file) {
    return count(file($file));
}

function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ceil(((float)$usec + (float)$sec));
}

function field($type, $field, $label, $placeholder, $required, $record, $entry_type, $errors)
{

    switch($type)
    {
        case 'text' :

            ?>

            <div class="form-group <?php echo $errors->has($field) ? 'has-error' : '' ?>">
                <label class="control-label" for="first_name">
                    <?php echo $label; ?>
                    <?php if($required) : ?>
                    <span class="required">*</span>
                    <?php endif; ?>
                </label>
                <div class="controls">
                    <input id="<?php echo $field; ?>" name="<?php echo $field; ?>" type="text" placeholder="<?php echo $placeholder; ?>" class="form-control" value="<?php old($field, $record->{$field}) ?>" <?php echo $entry_type == 'update' ? 'disabled' : '' ?>>
                </div>
            </div>

            <?php

            break;
    }

    exit;

}

function pc($value, $of, $decimals=2)
{
    return number_format(($value / $of) * 100, $decimals);
}

function wp_html_split( $input ) {
	return preg_split( get_html_split_regex(), $input, -1, PREG_SPLIT_DELIM_CAPTURE );
}

function wp_replace_in_html_tags( $haystack, $replace_pairs ) {
	// Find all elements.
	$textarr = wp_html_split( $haystack );
	$changed = false;

	// Optimize when searching for one item.
	if ( 1 === count( $replace_pairs ) ) {
		// Extract $needle and $replace.
		foreach ( $replace_pairs as $needle => $replace );

		// Loop through delimiters (elements) only.
		for ( $i = 1, $c = count( $textarr ); $i < $c; $i += 2 ) {
			if ( false !== strpos( $textarr[$i], $needle ) ) {
				$textarr[$i] = str_replace( $needle, $replace, $textarr[$i] );
				$changed = true;
			}
		}
	} else {
		// Extract all $needles.
		$needles = array_keys( $replace_pairs );

		// Loop through delimiters (elements) only.
		for ( $i = 1, $c = count( $textarr ); $i < $c; $i += 2 ) {
			foreach ( $needles as $needle ) {
				if ( false !== strpos( $textarr[$i], $needle ) ) {
					$textarr[$i] = strtr( $textarr[$i], $replace_pairs );
					$changed = true;
					// After one strtr() break out of the foreach loop and look at next element.
					break;
				}
			}
		}
	}

	if ( $changed ) {
		$haystack = implode( $textarr );
	}

	return $haystack;
}

function get_html_split_regex() {
	static $regex;

	if ( ! isset( $regex ) ) {
		$comments =
			  '!'           // Start of comment, after the <.
			. '(?:'         // Unroll the loop: Consume everything until --> is found.
			.     '-(?!->)' // Dash not followed by end of comment.
			.     '[^\-]*+' // Consume non-dashes.
			. ')*+'         // Loop possessively.
			. '(?:-->)?';   // End of comment. If not found, match all input.

		$cdata =
			  '!\[CDATA\['  // Start of comment, after the <.
			. '[^\]]*+'     // Consume non-].
			. '(?:'         // Unroll the loop: Consume everything until ]]> is found.
			.     '](?!]>)' // One ] not followed by end of comment.
			.     '[^\]]*+' // Consume non-].
			. ')*+'         // Loop possessively.
			. '(?:]]>)?';   // End of comment. If not found, match all input.

		$escaped =
			  '(?='           // Is the element escaped?
			.    '!--'
			. '|'
			.    '!\[CDATA\['
			. ')'
			. '(?(?=!-)'      // If yes, which type?
			.     $comments
			. '|'
			.     $cdata
			. ')';

		$regex =
			  '/('              // Capture the entire match.
			.     '<'           // Find start of element.
			.     '(?'          // Conditional expression follows.
			.         $escaped  // Find end of escaped element.
			.     '|'           // ... else ...
			.         '[^>]*>?' // Find end of normal element.
			.     ')'
			. ')/';
	}

	return $regex;
}

function _autop_newline_preservation_helper( $matches ) {
	return str_replace( "\n", "<WPPreserveNewline />", $matches[0] );
}

function wpautop( $pee, $br = true ) {
	$pre_tags = array();

	if ( trim($pee) === '' )
		return '';

	// Just to make things a little easier, pad the end.
	$pee = $pee . "\n";

	/*
	 * Pre tags shouldn't be touched by autop.
	 * Replace pre tags with placeholders and bring them back after autop.
	 */
	if ( strpos($pee, '<pre') !== false ) {
		$pee_parts = explode( '</pre>', $pee );
		$last_pee = array_pop($pee_parts);
		$pee = '';
		$i = 0;

		foreach ( $pee_parts as $pee_part ) {
			$start = strpos($pee_part, '<pre');

			// Malformed html?
			if ( $start === false ) {
				$pee .= $pee_part;
				continue;
			}

			$name = "<pre wp-pre-tag-$i></pre>";
			$pre_tags[$name] = substr( $pee_part, $start ) . '</pre>';

			$pee .= substr( $pee_part, 0, $start ) . $name;
			$i++;
		}

		$pee .= $last_pee;
	}
	// Change multiple <br>s into two line breaks, which will turn into paragraphs.
	$pee = preg_replace('|<br\s*/?>\s*<br\s*/?>|', "\n\n", $pee);

	$allblocks = '(?:table|thead|tfoot|caption|col|colgroup|tbody|tr|td|th|div|dl|dd|dt|ul|ol|li|pre|form|map|area|blockquote|address|math|style|p|h[1-6]|hr|fieldset|legend|section|article|aside|hgroup|header|footer|nav|figure|figcaption|details|menu|summary)';

	// Add a single line break above block-level opening tags.
	$pee = preg_replace('!(<' . $allblocks . '[\s/>])!', "\n$1", $pee);

	// Add a double line break below block-level closing tags.
	$pee = preg_replace('!(</' . $allblocks . '>)!', "$1\n\n", $pee);

	// Standardize newline characters to "\n".
	$pee = str_replace(array("\r\n", "\r"), "\n", $pee);

	// Find newlines in all elements and add placeholders.
	$pee = wp_replace_in_html_tags( $pee, array( "\n" => " <!-- wpnl --> " ) );

	// Collapse line breaks before and after <option> elements so they don't get autop'd.
	if ( strpos( $pee, '<option' ) !== false ) {
		$pee = preg_replace( '|\s*<option|', '<option', $pee );
		$pee = preg_replace( '|</option>\s*|', '</option>', $pee );
	}

	/*
	 * Collapse line breaks inside <object> elements, before <param> and <embed> elements
	 * so they don't get autop'd.
	 */
	if ( strpos( $pee, '</object>' ) !== false ) {
		$pee = preg_replace( '|(<object[^>]*>)\s*|', '$1', $pee );
		$pee = preg_replace( '|\s*</object>|', '</object>', $pee );
		$pee = preg_replace( '%\s*(</?(?:param|embed)[^>]*>)\s*%', '$1', $pee );
	}

	/*
	 * Collapse line breaks inside <audio> and <video> elements,
	 * before and after <source> and <track> elements.
	 */
	if ( strpos( $pee, '<source' ) !== false || strpos( $pee, '<track' ) !== false ) {
		$pee = preg_replace( '%([<\[](?:audio|video)[^>\]]*[>\]])\s*%', '$1', $pee );
		$pee = preg_replace( '%\s*([<\[]/(?:audio|video)[>\]])%', '$1', $pee );
		$pee = preg_replace( '%\s*(<(?:source|track)[^>]*>)\s*%', '$1', $pee );
	}

	// Remove more than two contiguous line breaks.
	$pee = preg_replace("/\n\n+/", "\n\n", $pee);

	// Split up the contents into an array of strings, separated by double line breaks.
	$pees = preg_split('/\n\s*\n/', $pee, -1, PREG_SPLIT_NO_EMPTY);

	// Reset $pee prior to rebuilding.
	$pee = '';

	// Rebuild the content as a string, wrapping every bit with a <p>.
	foreach ( $pees as $tinkle ) {
		$pee .= '<p>' . trim($tinkle, "\n") . "</p>\n";
	}

	// Under certain strange conditions it could create a P of entirely whitespace.
	$pee = preg_replace('|<p>\s*</p>|', '', $pee);

	// Add a closing <p> inside <div>, <address>, or <form> tag if missing.
	$pee = preg_replace('!<p>([^<]+)</(div|address|form)>!', "<p>$1</p></$2>", $pee);

	// If an opening or closing block element tag is wrapped in a <p>, unwrap it.
	$pee = preg_replace('!<p>\s*(</?' . $allblocks . '[^>]*>)\s*</p>!', "$1", $pee);

	// In some cases <li> may get wrapped in <p>, fix them.
	$pee = preg_replace("|<p>(<li.+?)</p>|", "$1", $pee);

	// If a <blockquote> is wrapped with a <p>, move it inside the <blockquote>.
	$pee = preg_replace('|<p><blockquote([^>]*)>|i', "<blockquote$1><p>", $pee);
	$pee = str_replace('</blockquote></p>', '</p></blockquote>', $pee);

	// If an opening or closing block element tag is preceded by an opening <p> tag, remove it.
	$pee = preg_replace('!<p>\s*(</?' . $allblocks . '[^>]*>)!', "$1", $pee);

	// If an opening or closing block element tag is followed by a closing <p> tag, remove it.
	$pee = preg_replace('!(</?' . $allblocks . '[^>]*>)\s*</p>!', "$1", $pee);

	// Optionally insert line breaks.
	if ( $br ) {
		// Replace newlines that shouldn't be touched with a placeholder.
		$pee = preg_replace_callback('/<(script|style).*?<\/\\1>/s', '_autop_newline_preservation_helper', $pee);

		// Normalize <br>
		$pee = str_replace( array( '<br>', '<br/>' ), '<br />', $pee );

		// Replace any new line characters that aren't preceded by a <br /> with a <br />.
		$pee = preg_replace('|(?<!<br />)\s*\n|', "<br />\n", $pee);

		// Replace newline placeholders with newlines.
		$pee = str_replace('<WPPreserveNewline />', "\n", $pee);
	}

	// If a <br /> tag is after an opening or closing block tag, remove it.
	$pee = preg_replace('!(</?' . $allblocks . '[^>]*>)\s*<br />!', "$1", $pee);

	// If a <br /> tag is before a subset of opening or closing block tags, remove it.
	$pee = preg_replace('!<br />(\s*</?(?:p|li|div|dl|dd|dt|th|pre|td|ul|ol)[^>]*>)!', '$1', $pee);
	$pee = preg_replace( "|\n</p>$|", '</p>', $pee );

	// Replace placeholder <pre> tags with their original content.
	if ( !empty($pre_tags) )
		$pee = str_replace(array_keys($pre_tags), array_values($pre_tags), $pee);

	// Restore newlines in all elements.
	if ( false !== strpos( $pee, '<!-- wpnl -->' ) ) {
		$pee = str_replace( array( ' <!-- wpnl --> ', '<!-- wpnl -->' ), "\n", $pee );
	}

	return $pee;
}
