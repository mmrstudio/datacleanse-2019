<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Input;

class InputValidate extends Command
{
    protected $signature = 'input:validate {validate_what} {campaign_id} {batch}';
    protected $description = 'Validates the input data for given data type (email|mobile), campaign and batch';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $validate_what = $this->argument('validate_what');
        $campaign_id = $this->argument('campaign_id');
        $batch = $this->argument('batch');

        // create the table
        return Input::validate($validate_what, $campaign_id, $batch, $this);
    }
}
