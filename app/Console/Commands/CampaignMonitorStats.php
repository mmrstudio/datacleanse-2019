<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use DB;

use App\CMCampaign;
use App\Campaign;
use App\CampaignMonitor;

class CampaignMonitorStats extends Command
{
    protected $signature = 'cm:stats';
    protected $description = 'Retrieves stats for given sendout id';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        //$send_out_id = $this->argument('send_out_id');

        $cm_campaigns = CMCampaign::all();
        $campaign_stats = [];

        $bar = $this->output->createProgressBar(count($cm_campaigns));

        foreach($cm_campaigns as $cm_campaign)
        {
            $campaign_stats[$cm_campaign->cm_campaign_id] = $cm_campaign->syncStats();
            $bar->advance();
        }

        print_r($campaign_stats);

        $this->info("\nDone.\n");

        return;

    }
}
