<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Illuminate\Database\Eloquent\Model;

use DB;
use Schema;
use App\Input;
use App\Campaign;
use App\Link;

//use GuzzleHttp\Psr7\Request;

class URLShorten extends Command {

    private $client;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'urls:shorten';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates dtcl.nz short links for given batch';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->client = new \GuzzleHttp\Client();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {

        ini_set('memory_limit','8000M');

        $batch = $this->option('batch');
        $campaign_id = $this->option('campaign');

        // get campaign
        $campaign = Campaign::find($campaign_id);
        if(! $campaign) return;
        //print_r($campaign->toArray()); exit;

        $table_name = Campaign::tableName('input', $campaign_id);
        //echo $table_name; exit;

        //DB::enableQueryLog();

        $get_source = DB::table($table_name)->whereNull('short_url')->where('import_batch', '=', $batch)->where('invalid_mobile', '=', '0')->where('send_method', '=', 'sms')->get();

        //print_r(DB::getQueryLog()); exit;

        $i = 1;
        $num_source = count($get_source);
        //print_r($num_source); exit;

        $bar = $this->output->createProgressBar($num_source);

        foreach($get_source as $source)
        {
            $dc_url = 'https://campaigns.datacleanse.com.au/';
            $url = $dc_url . $campaign->url('update', $source->hash, false);

            //echo $url . "\n";

            $short_url = $this->shortenURL($url);

            DB::table($table_name)->where('id', $source->id)->update(['short_url' => $short_url]);

            if($i % 100 == 0) {
                //$this->info('Converted... ' . $i . ' of ' . $num_source);
                $bar->advance(100);
            }

            //$bar->advance();

            $i++;

        }

        echo "\n";

    }

    protected function shortenURL($url)
    {
        //$this->info('Converting... ' . $url);

        // $request = $this->client->post('http://dtcl.nz/link', [
        //     'headers' => ['X-Authorization' => 'f83573ace4098a04c7a3bec7301390b9aea3dfd9'],
        //     'form_params' => ['url' => $url],
        // ]);
        //
        // $response = json_decode($request->getBody());

        $response = Link::createFromUrl($url, false);

        //print_r($response); exit;

        //return $response->short_url;
        return $response['short_url'];

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['batch', null, InputOption::VALUE_REQUIRED, 'Batch Number', null],
            ['campaign', null, InputOption::VALUE_REQUIRED, 'Campaign ID', null]
        ];
    }

}
