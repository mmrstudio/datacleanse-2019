<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use DB;

use App\Campaign;

class InputImport extends Command
{
    protected $signature = 'import {campaign_id}';
    protected $description = 'Applies hash and import batch to given campaign';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $campaign_id = $this->argument('campaign_id');

        // import file exists?
        //if(! is_file($import_file)) return $this->error('File "' . $file . '" does not exist');

        // get campaign
        $campaign = Campaign::with('client')->find($campaign_id);

        // campaign exists?
        if(! $campaign) return $this->error('Could not find campaign with ID of "' . $campaign_id . '"');

        // set batch?
        $set_batch = $this->confirm('Set batch name?');
        $update_batch = false;

        // batch name
        if($set_batch)
        {
            $batch_name = $this->ask('Enter a name for this batch');
        }
        else
        {
            $update_batch = $this->confirm('Update existing batch?');

            if($update_batch)
            {
                $update_batch_name = $this->ask('Enter a name for batch to update');
            }
        }

        $send_method = $this->ask('Enter send method (email|sms)');

        // ask to proceed
        if(! $this->confirm('Process imported data for "' . $campaign->client->name . ' - ' . $campaign->name  . '"?')) return;

        $table_name = Campaign::tableName('input', $campaign_id);

        // do import
        //$campaign->import($import_file, $batch_name, $this);

        // add hash
        $uniqid = uniqid();
        if($update_batch)
        {
            DB::update("update $table_name set hash = MD5(CONCAT(id,'$uniqid')) where send_method = '$send_method' and import_batch = '$update_batch_name'");
        }
        else
        {
            DB::update("update $table_name set hash = MD5(CONCAT(id,'$uniqid')) where send_method = '$send_method'");
        }

        // add import batch
        if($set_batch)
        {
            DB::update("update $table_name set import_batch = ? where send_method = ?", [$batch_name,$send_method]);
        }

    }
}
