<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use DB;

use App\Input;
use App\Sendout;
use App\Campaign;
use App\CMList;
use App\CMListMember;
use App\CMCampaign;
use App\Field;
use App\CampaignMonitor;

class CampaignMonitorPrepare extends Command
{
    protected $signature = 'cm:prepare {sendout_id}';
    protected $description = 'Generates campaign monitor lists and campaigns for given send out';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $sendout_id = $this->argument('sendout_id');

        // get sendout
        $sendout = Sendout::with('client', 'campaign')->find($sendout_id);
        if(! $sendout) return $this->error('Could not find sendout with ID of "' . $sendout . '"');

        // get table
        $table_name = $sendout->campaign->inputTable();

        $available_batches_arr = [];
        $available_batches = DB::table($table_name)->select('import_batch')->groupBy('import_batch')->get();
        if($available_batches) foreach($available_batches as $row) $available_batches_arr[] = $row->import_batch;

        // get batch
        $batch = $this->ask('Enter batch (' . implode($available_batches_arr, ', ') . ')');

        // test mode?
        $test_mode = false;
        if($this->confirm('Test mode?')) $test_mode = true;

        $list_name_base = $sendout->client->slug . '_' . $sendout->campaign->slug . '_' . $sendout->slug . '_' . $batch;

        if(! $test_mode)
        {
            $max_duplicates = DB::table($table_name)
                                ->where('import_batch', $batch)
                                ->where(function ($query) {
                                    $query->whereNull('output_id')
                                          ->orWhere('output_id', '0');
                                 })
                                ->max('mailing_list');
        }
        else
        {
            $max_duplicates = 1;
        }

        //echo $max_duplicates . "\n"; exit;

        for($i=0; $i < $max_duplicates; $i++)
        {
            $list_number = str_pad($i+1, 2, '0', STR_PAD_LEFT);
            $list_name = $list_name_base . '_' . $list_number;

            if($test_mode) $list_name = $list_name . '_TEST';

            $this->info($list_name);

            if(! CMList::exists($list_name))
            {
                $list = new CMList;
                $list->fill([
                    'client_id' => $sendout->client->id,
                    'campaign_id' => $sendout->campaign->id,
                    'send_out_id' => $sendout->id,
                    'name' => $list_name,
                    'batch' => $batch,
                    'list_number' => $i+1,
                    'test' => $test_mode ? '1' : '0',
                ]);
                $list->save();

                // reload list record
                $list = CMList::with('client', 'campaign', 'sendout')->find($list->id);

                $this->line(' - list prepared');
            }
            else
            {
                $this->line(' - list exists');
                $list = CMList::name($list_name);
            }

            // create list on campaign monitor
            if($list->list_imported == '0')
            {
                CampaignMonitor::createList($list, $this);
            }

            // prepare campaign record
            if($list->list_imported == '0')
            {

                if(! CMCampaign::exists($list_name))
                {
                    $campaign = new CMCampaign;
                    $campaign->fill([
                        'client_id' => $sendout->client->id,
                        'campaign_id' => $sendout->campaign->id,
                        'send_out_id' => $sendout->id,
                        'list_id' => $list->id,
                        'name' => $list_name,
                    ]);
                    $campaign->save();

                    $this->line(' - campaign prepared');

                    // reload list record
                    $list = CMCampaign::with('client', 'campaign', 'sendout')->find($campaign->id);
                }
                else
                {
                    $this->line(' - campaign exists');
                    $campaign = CMCampaign::name($list_name);
                }

                $custom_subject = false;
                if($test_mode)
                {
                    $custom_subject = "{$campaign->sendout->subject} ({$batch})";
                }

                // create campaign on campaign monitor
                if(! $campaign->campaign_created)
                {
                    CampaignMonitor::createCampaign($campaign, $this, $custom_subject);
                }

            }

        }

        $this->info("\nDone.\n");

        return;

    }
}
