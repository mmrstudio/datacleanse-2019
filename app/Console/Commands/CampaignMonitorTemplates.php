<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Campaign;
use App\Client;
use App\CampaignMonitor;

class CampaignMonitorTemplates extends Command
{
    protected $signature = 'cm:templates {client_id}';
    protected $description = 'Lists Campaign Monitor templates for given client';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $client_id = $this->argument('client_id');
        $client = Client::find($client_id);

        if(! $client) return $this->error('Client not found');

        $templates = CampaignMonitor::templates($client->cm_client_id, false);

        foreach($templates as $template_id => $template_name)
        {
            $this->info($template_id . '  ' . $template_name);
        }

        return;
    }
}
