<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Input;

class InputExclude extends Command
{
    protected $signature = 'input:exclude {campaign_id} {batch}';
    protected $description = 'Validates the input data for given data type (email|mobile), campaign and batch';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $campaign_id = $this->argument('campaign_id');
        $batch = $this->argument('batch');

        return Input::exportExcludes($campaign_id, $batch, $this);

    }
}
