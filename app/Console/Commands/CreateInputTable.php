<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Campaign;

class CreateInputTable extends Command
{
    protected $signature = 'create:inputTable {campaign_id}';
    protected $description = 'Creates the input table for a campaign from defined fields';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $campaign_id = $this->argument('campaign_id');
        $campaign = Campaign::find($campaign_id);

        if(!$campaign) return $this->error('Campaign ID invalid');

        // create the table
        if($this->confirm('Create INPUT table for "' . $campaign->client->name . ' - ' . $campaign->name  . '"?'))
        {
            $table_name = Campaign::createInputTable($campaign_id, false);
            return $this->info('Created input table for campaign ID ' . $campaign_id . '... ' . $table_name . "\n");
        }

        return '';
    }
}
