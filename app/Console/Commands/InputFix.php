<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use DB;

use App\Input;
use App\Campaign;

class InputFix extends Command
{
    protected $signature = 'input:fix {fix_what} {campaign_id}';
    protected $description = 'Evenly splits records across multiple mailing lists';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        ini_set('memory_limit','2048M');

        $fix_what = $this->argument('fix_what');
        $campaign_id = $this->argument('campaign_id');
        $campaign = Campaign::find($campaign_id);

        $this->info("\n" . $campaign->client->name . ' - ' . $campaign->name . "\n");
        $this->info("Fields:");

        $input_table = Campaign::tableName('input', $campaign_id);

        $fields = [];
        $get_fields = DB::table('fields')
                        ->select('name')
                        ->where('campaign_id', $campaign_id)
                        ->where('input', '1')
                        //->where('form', '1')
                        ->get();

        foreach($get_fields as $key => $field)
        {
            $fields[$key+1] = $field->name;
            $this->line($key+1 . '. ' . $field->name);
        }

        $field_key = $this->ask('Choose field');
        $selected_field = $fields[$field_key];

        $this->info('Updating: ' . $selected_field . '...');

        if($fix_what == 'phone')
        {
            $get_fields = DB::table($input_table)
                            ->select('id', $selected_field)
                            ->whereNotNull($selected_field)
                            ->get();

            $bar = $this->output->createProgressBar(count($get_fields));

            foreach($get_fields as $field)
            {
                $phone = $field->{$selected_field};

                if(strlen($phone) === 9 && in_array(substr($phone, 0, 1), ['2','3','4','7','8']))
                {
                    $phone = '0' . $phone;

                    DB::table($input_table)->where('id', $field->id)->update([$selected_field => $phone]);
                }

                $bar->advance();
            }

        }

        $this->info("\n\n".'Done.'."\n");

        //print_r($fields); exit;

    }
}
