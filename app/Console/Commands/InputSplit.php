<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use DB;

use App\Input;
use App\Campaign;

class InputSplit extends Command
{
    protected $signature = 'input:split {split_what} {campaign_id} {batch}';
    protected $description = 'Evenly splits records across multiple mailing lists';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        ini_set('memory_limit','2048M');

        $split_what = $this->argument('split_what');
        $campaign_id = $this->argument('campaign_id');
        $batch = $this->argument('batch');

        $table_name = Campaign::tableName('input', $campaign_id);

        $this->info('Starting split...');

        switch($split_what)
        {
            case 'email' :
                $invalid_col = 'invalid_email';
                $duplicates_col = 'email_duplicates';
                $send_method = 'email';
                break;

            case 'sms' :
                $invalid_col = 'invalid_mobile';
                $duplicates_col = 'mobile_duplicates';
                $send_method = 'sms';
                break;

        }

        $mailing_list = 1;

        $count_records = DB::table($table_name)->where('import_batch', '=', $batch)->where($invalid_col, '=', '0')->where('send_method', '=', $send_method)->count();

        $lists = $this->ask('How many lists? ('.$count_records.' records)');

        $records_per_list = ceil($count_records / $lists);

        $this->info('Emails per list: ' . $records_per_list);

        $bar = $this->output->createProgressBar($count_records);

        $input = DB::table($table_name)->where('import_batch', '=', $batch)->where($invalid_col, '=', '0')->where('send_method', '=', $send_method)->chunk($records_per_list, function($records) use ($table_name, &$mailing_list, $bar) {

            //$this->info('Splitting list: ' . $mailing_list);

            foreach($records as $record)
            {
                DB::table($table_name)->where('id', $record->id)->update(['mailing_list' => $mailing_list]);
                $bar->advance();
            }

            $mailing_list++;

        });


        exit;


        // get mailing lists
        $mailing_lists = [];
        $get_mailing_lists = DB::table($table_name)->select(DB::raw("mailing_list, count(mailing_list) as 'records'"))->where('import_batch', '=', $batch)->where($invalid_col, '=', '0')->where('send_method', '=', $send_method)->havingRaw('COUNT(mailing_list) > 5')->groupBy('mailing_list')->get();

        // process lists
        if(count($get_mailing_lists) > 0)
        {
            foreach($get_mailing_lists as $list)
            {
                $mailing_lists[] = [
                    'name' => $list->mailing_list,
                    'records' => $list->records,
                ];
            }
        }

        //print_r($mailing_lists); exit;

        // get numbers
        $total_records = DB::table($table_name)->select('id')->where('import_batch', '=', $batch)->where($invalid_col, '=', '0')->where('send_method', '=', $send_method)->count();
        $total_lists = count($mailing_lists);
        $per_list = ceil($total_records / $total_lists);
        $this->info('Mailing lists: ' . $total_lists);
        $this->info('Total Records: ' . $total_records);
        $this->info('Records Per List: ' . $per_list);

        // determine distributable records
        $count_distributable = DB::table($table_name)->select('id')->where('import_batch', '=', $batch)->where('mailing_list', '=', $mailing_lists[0]['name'])->where($invalid_col, '=', '0')->where('send_method', '=', $send_method)->where($duplicates_col, '=', '1')->count();
        $count_non_distributable = DB::table($table_name)->select('id')->where('import_batch', '=', $batch)->where('mailing_list', '=', $mailing_lists[0]['name'])->where($invalid_col, '=', '0')->where('send_method', '=', $send_method)->where($duplicates_col, '>', '1')->count();

        $leave_behind = $per_list - $count_non_distributable;

        // extract distributable records
        $distributable = [];
        $get_distributable = DB::table($table_name)->select('id')->where('import_batch', '=', $batch)->where('mailing_list', '=', $mailing_lists[0]['name'])->where($duplicates_col, '=', '1')->where($invalid_col, '=', '0')->where('send_method', '=', $send_method)->take($count_distributable - $leave_behind)->skip($leave_behind)->get();
        foreach($get_distributable as $record) $distributable[] = $record->id;

        $total_distributable = count($distributable);

        $this->info('Distributable Records: ' . $count_distributable);
        $this->info('Non-Distributable Records: ' . $count_non_distributable);
        $this->info('Leave Behind: ' . $leave_behind);

        //echo '$count_non_distributable ' . $count_non_distributable . "\n";
        //echo '$count_distributable ' . $count_distributable . "\n";
        //echo '$leave_behind ' . $leave_behind . "\n";
        //exit;
        //print_r(count($distributable)); exit;

        // determine how many for each list
        $from = 0;
        $distributable_index = 0;
        $distributable_remaining = $total_distributable;

        foreach($mailing_lists as $key => &$list)
        {
            $list['take'] = $per_list - $list['records'];
            $list['take'] = ($list['take'] > $distributable_remaining) ? $distributable_remaining : $list['take'];
            $list['from'] = ($key > 0) ? $from + $list['take'] : 0;
            $from = ($key > 0) ? $from + $list['take'] + 1 : $from;
            $list['ids'] = [];

            $this->info('Distributable Remaining: ' . $distributable_remaining);

            $this->info('List '.$list['name'].':');

            // divide distributabel ids
            for($i=0; $i<$list['take']; $i++)
            {
                if(isset($distributable[$distributable_index]))
                {
                    $list['ids'][] = $distributable[$distributable_index];
                    $distributable_index++;
                    $distributable_remaining--;
                }
            }

            $list['num_ids'] = count($list['ids']);
            $list['total'] = $list['records'] + $list['take'];

            $this->info('    Existing: ' . $list['records']);
            $this->info('    Take: ' . $list['take']);
            $this->info('    From: ' . $list['from']);
            $this->info('    IDs: ' . $list['num_ids']);
            $this->info('    Total After Distribution: ' . $list['total']);

        }

        //print_r($mailing_lists);
        //exit;

        // update mailing list col
        foreach($mailing_lists as $mailing_list)
        {
            $this->info('Distributing list ' . $mailing_list['name'] . ': ' . ($mailing_list['take'] > 0 ? '+' : '') . $mailing_list['take'] . ' records');

            if(count($mailing_list['ids']) > 0)
            {
                DB::table($table_name)->whereIn('id', $mailing_list['ids'])->update(['mailing_list' => $mailing_list['name']]);
            }
        }

    }
}
