<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use DB;

use App\Input;
use App\Sendout;
use App\Campaign;
use App\CMList;
use App\CMListMember;
use App\CMCampaign;
use App\Field;
use App\CampaignMonitor;

class CampaignMonitorDelete extends Command
{
    protected $signature = 'cm:delete {sendout_id}';
    protected $description = 'Deletes generated CM lists and campaigns for sendout';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $sendout_id = $this->argument('sendout_id');
        $sendout = Sendout::with('client', 'campaign')->find($sendout_id);

        if(! $this->confirm('Delete campaigns for:' . "\n " . $sendout->campaign->client->name . ' - ' . $sendout->campaign->name  . ' - ' . $sendout->name . '?')) return;

        // test mode?
        //$test_mode = false;
        //if($this->confirm('Delete TEST only?')) $test_mode = true;

        // get campaigns & lists
        $campaigns = CMCampaign::where('send_out_id', $sendout_id)->get();
        $lists = CMList::where('send_out_id', $sendout_id)->get();

        foreach($campaigns as $campaign)
        {
            $this->info("Campaign: $campaign->name");
            CampaignMonitor::deleteCampaign($campaign->cm_campaign_id, $this);
        }

        foreach($lists as $list)
        {
            $this->info("List: $list->name");
            CampaignMonitor::deleteList($list->cm_list_id, $this);
        }

        // delete lists
        DB::table('cm_lists')->where('send_out_id', $sendout_id)->delete();
        DB::table('cm_campaigns')->where('send_out_id', $sendout_id)->delete();

        $this->info("\nDone.\n");

        return;

    }
}
