<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Campaign;

class CreateTables extends Command
{
    protected $signature = 'create:tables {campaign_id}';
    protected $description = 'Creates the input and output table for a campaign from defined fields';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $campaign_id = $this->argument('campaign_id');

        // create the tables
        $table_name = Campaign::createInputTable($campaign_id, false);
        $table_name = Campaign::createOutputTable($campaign_id, false);

        return $this->info('Created tables for campaign ID ' . $campaign_id . "\n");
    }
}
