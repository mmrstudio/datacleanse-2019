<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Input;

class InputExport extends Command
{
    protected $signature = 'input:export {export_what} {campaign_id} {sendout_id} {batch}';
    protected $description = 'Validates the input data for given data type (email|mobile), campaign and batch';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $export_what = $this->argument('export_what');
        $campaign_id = $this->argument('campaign_id');
        $sendout_id = $this->argument('sendout_id');
        $batch = $this->argument('batch');

        switch($export_what)
        {
            case 'email' :
                return Input::exportEmails($campaign_id, $sendout_id, $batch, $this);
                break;

            case 'sms' :
                return Input::exportSMS($sendout_id, $batch, $this);
                break;
        }

    }
}
