<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use DB;

use App\Input;
use App\Campaign;

class Clear extends Command
{
    protected $signature = 'clear {campaign_id}';
    protected $description = 'Clears output table and resets output state on input records';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        ini_set('memory_limit','2048M');

        $campaign_id = $this->argument('campaign_id');
        $campaign = Campaign::find($campaign_id);

        // ask to proceed
        if(! $this->confirm('Clear output for "' . $campaign->client->name . ' - ' . $campaign->name  . '"?')) return;

        $input_table = Campaign::tableName('input', $campaign_id);
        $output_table = Campaign::tableName('output', $campaign_id);

        dd($output_table);

        // clear output
        DB::table($output_table)->truncate();

        $this->info("\n\n".'Done.'."\n");

        //print_r($fields); exit;

    }
}
