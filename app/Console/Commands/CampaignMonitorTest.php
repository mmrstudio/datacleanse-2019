<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use DB;

use App\Input;
use App\Sendout;
use App\Campaign;
use App\CMList;
use App\CMListMember;
use App\CMCampaign;
use App\Field;
use App\CampaignMonitor;

class CampaignMonitorPrepare extends Command
{
    protected $signature = 'cm:test {sendout_id}';
    protected $description = 'Generates a test campaign and list in Campaign Monitor for sendout';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $sendout_id = $this->argument('sendout_id');

        // get sendout
        $sendout = Sendout::with('client', 'campaign')->find($sendout_id);
        if(! $sendout) return $this->error('Could not find sendout with ID of "' . $sendout . '"');

        // get batch
        $batch = $this->ask('Enter batch');

        // test mode?
        $test_mode = true;

        $table_name = $sendout->campaign->inputTable();
        $list_name_base = $sendout->client->slug . '_' . $sendout->campaign->slug . '_' . $sendout->slug . '_' . $batch;

        if(! $test_mode)
        {
            $max_duplicates = DB::table($table_name)
                                ->where('import_batch', $batch)
                                ->where(function ($query) {
                                    $query->whereNull('output_id')
                                          ->orWhere('output_id', '0');
                                 })
                                ->max('mailing_list');
        }
        else
        {
            $max_duplicates = 1;
        }

        //echo $max_duplicates . "\n"; exit;

        for($i=0; $i < $max_duplicates; $i++)
        {
            $list_number = str_pad($i+1, 2, '0', STR_PAD_LEFT);
            $list_name = $list_name_base . '_' . $list_number;

            if($test_mode) $list_name = $list_name . '_TEST';

            $this->info($list_name);

            if(! CMList::exists($list_name))
            {
                $list = new CMList;
                $list->fill([
                    'client_id' => $sendout->client->id,
                    'campaign_id' => $sendout->campaign->id,
                    'send_out_id' => $sendout->id,
                    'name' => $list_name,
                    'batch' => $batch,
                    'list_number' => $i+1,
                    'test' => $test_mode ? '1' : '0',
                ]);
                $list->save();

                // reload list record
                $list = CMList::with('client', 'campaign', 'sendout')->find($list->id);

                $this->line(' - list prepared');
            }
            else
            {
                $this->line(' - list exists');
                $list = CMList::name($list_name);
            }

            // create list on campaign monitor
            if($list->list_imported == '0')
            {
                CampaignMonitor::createList($list, $this);
            }

            // prepare campaign record
            if($list->list_imported == '0')
            {

                if(! CMCampaign::exists($list_name))
                {
                    $campaign = new CMCampaign;
                    $campaign->fill([
                        'client_id' => $sendout->client->id,
                        'campaign_id' => $sendout->campaign->id,
                        'send_out_id' => $sendout->id,
                        'list_id' => $list->id,
                        'name' => $list_name,
                    ]);
                    $campaign->save();

                    $this->line(' - campaign prepared');

                    // reload list record
                    $list = CMCampaign::with('client', 'campaign', 'sendout')->find($campaign->id);
                }
                else
                {
                    $this->line(' - campaign exists');
                    $campaign = CMCampaign::name($list_name);
                }

                // create campaign on campaign monitor
                if(! $campaign->campaign_created)
                {
                    CampaignMonitor::createCampaign($campaign, $this);
                }

            }

        }

        // IMPORT

        foreach($sendout->lists as $list)
        {
            $member_ids = [];
            $chunk = 1;

            $this->info('Importing list: ' . $list->name);

            $input_records = DB::table($table_name)
                               ->select('id')
                               ->where('import_batch', $list->batch)
                               ->where('invalid_email', '0')
                               ->where(function ($query) {
                                   $query->whereNull('output_id')
                                         ->orWhere('output_id', '0');
                                })
                               ->where('send_method', 'email')
                               ->where('mailing_list', $list->list_number);

            if($list->test == '1')
            {
                $test_num = 10;

                $input_records->limit($test_num);
                $input_records->orderByRaw("RAND()");
            }

            $input_records = $input_records->get();

            foreach($input_records as $member) $member_ids[] = $member->id;
            //foreach($list->members as $member) $member_ids[] = $member->input_id;

            $member_count = DB::table($table_name)->whereIn('id', $member_ids)->count();
            $chunk_count = ceil($member_count / $chunk_size);

            //print_r($list->fields()); exit;

            $input = DB::table($table_name)->whereIn('id', $member_ids)->chunk($chunk_size, function($records) use ($sendout, $list, &$chunk, $chunk_count, $member_count) {

                //print_r($records); exit;

                $this->line(' - chunk ' . $chunk . ' of ' . $chunk_count . ' (' . $member_count . ' records)');

                $import_data = [];

                foreach($records as $record)
                {
                    $format_email = $sendout->campaign->formatEmail(false);
                    //print_r($format_email); exit;
                    foreach($format_email as $field => $format)
                    {
                        if(is_callable($format))
                        {
                            $record = $format($field, $record);
                        }
                    }

                    //print_r($record); exit;

                    $import_record = [
                        'EmailAddress' => $record->{$sendout->campaign->email_field},
                        'Name' => $record->{$sendout->campaign->first_name_field} . ' ' . $record->{$sendout->campaign->last_name_field},
                        'CustomFields' => [
                            ['Key' => 'firstname', 'Value' => $record->{$sendout->campaign->first_name_field}],
                            ['Key' => 'lastname', 'Value' => $record->{$sendout->campaign->last_name_field}],
                        ],
                    ];

                    foreach($list->fields(false) as $field_key => $field_name)
                    {
                        $import_record['CustomFields'][] = ['Key' => $field_key, 'Value' => $record->{$field_name}];
                    }

                    $dc_url = 'https://campaigns.datacleanse.com.au/';
                    $import_record['CustomFields'][] = ['Key' => '_update_url', 'Value' => $dc_url . $sendout->campaign->url('update', $record->hash, false) . '/' . $sendout->id];
                    $import_record['CustomFields'][] = ['Key' => '_confirm_url', 'Value' => $dc_url . $sendout->campaign->url('confirm', $record->hash, false) . '/' . $sendout->id];

                    //print_r($import_record); exit;

                    $import_data[] = $import_record;

                }

                //print_r($import_data); exit;

                if(! $list->list_imported)
                {
                    CampaignMonitor::importList($list, $import_data, $this);
                }

                $chunk++;

            });

            $list->list_imported = '1';
            $list->save();

        }

        $this->info("\nDone.\n");

        return;

    }
}
