<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Excel;

use App\Report;
use App\Campaign;

class CampaignReport extends Command
{
    protected $signature = 'report {campaign_id}';
    protected $description = 'Processes the input data for given campaign and batch';

    public function __construct()
    {
        parent::__construct();
    }

    public function runReport($report_name, $campaign)
    {
        $report_data = $campaign->report($report_name);
        if($report_data)
        {
            $save_report = CampaignReport::save($report_name, $report_data, $campaign);
            $this->info('Saved: ' . $save_report . '.csv');
        }
    }

    public function save($report_name, $report_data, $campaign)
    {
        $export_path = storage_path('reports');
        $export_file = $campaign->client->slug . '_' . $campaign->slug . '_REPORT_' . $report_name . '_' . date('Ymd');

        Excel::create($export_file, function($excel) use ($report_data) {

            $excel->sheet('data', function($sheet) use ($report_data) {
                $sheet->fromArray($report_data);
            });

        })->store('csv', $export_path);

        return $export_file;
    }

    public function handle()
    {
        $campaign_id = $this->argument('campaign_id');
        $campaign = Campaign::find($campaign_id);

        if(! $campaign) return $this->error('Camapign not found');

        $reports = [];
        $report_names = [];
        $get_report_names = Report::where('campaign_id', $campaign_id)->get();
        if($get_report_names)
        {
            $num = 2;
            foreach($get_report_names as $report)
            {
                $reports[$num] = $report->name;
                $report_names[$num] = '   ' . $num . '. ' . $report->name;
                $num++;
            }
        }

        $report_num = $this->ask('Which report? (enter number)' . "\n   1. All\n" . implode("\n", $report_names) . "\n");

        if($report_num === '1') // All
        {
            foreach($reports as $report_name)
            {
                $this->runReport($report_name, $campaign);
            }

        }
        else
        {
            $report_name = $reports[$report_num];
            $this->runReport($report_name, $campaign);
        }

    }
}
