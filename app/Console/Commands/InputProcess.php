<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Input;

class InputProcess extends Command
{
    protected $signature = 'input:process {process_what} {campaign_id} {batch}';
    protected $description = 'Processes the input data for given campaign and batch';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $process_what = $this->argument('process_what');
        $campaign_id = $this->argument('campaign_id');
        $batch = $this->argument('batch');

        switch($process_what)
        {
            case 'email' :
                return Input::processEmails($campaign_id, $batch, $this);
                break;

            case 'sms' :
                return Input::processSMS($campaign_id, $batch, $this);
                break;
        }

    }
}
