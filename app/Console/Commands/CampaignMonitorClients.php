<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Campaign;
use App\CampaignMonitor;

class CampaignMonitorClients extends Command
{
    protected $signature = 'cm:clients';
    protected $description = 'Lists Campaign Monitor clients';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $clients = CampaignMonitor::clients(false);

        foreach($clients as $client_id => $client_name)
        {
            $this->info($client_id . '  ' . $client_name);
        }

        return;
    }
}
