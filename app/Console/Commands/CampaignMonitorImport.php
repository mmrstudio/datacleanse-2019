<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use DB;

use App\Input;
use App\Sendout;
use App\Campaign;
use App\CMList;
use App\CMListMember;
use App\CMCampaign;
use App\Field;
use App\CampaignMonitor;

class CampaignMonitorImport extends Command
{
    protected $signature = 'cm:import {sendout_id}';
    protected $description = 'Imports input data to campaign monitor for given send out';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $sendout_id = $this->argument('sendout_id');

        // test mode?
        $test_mode = false;
        if($this->confirm('Test mode?')) $test_mode = true;

        // skip imported?
        if(!$test_mode)
        {
            $skip_imported = false;
            if($this->confirm('Skip already imported lists?')) $skip_imported = true;
        }
        else
        {
            $skip_imported = true;
        }

        // get sendout
        $sendout = Sendout::with('client', 'campaign', 'lists')->find($sendout_id);
        if(! $sendout) return $this->error('Could not find sendout with ID of "' . $sendout . '"');

        $chunk_size = 999;
        $table_name = $sendout->campaign->inputTable();

        //print_r($sendout->toArray());

        foreach($sendout->lists as $list)
        {
            $member_ids = [];
            $chunk = 1;

            if($list->test == '1' && !$test_mode)
            {
                $this->info('Skipping test list: ' . $list->name);
                continue; // skip test if not test mode
            }

            if($list->list_imported == '1' && $skip_imported)
            {
                $this->info('Skipping imported list: ' . $list->name);
                continue; // skip test if not test mode
            }

            if($list->test == '1' && !$test_mode)
            {
                $this->info('Skipping test list');
                continue; // skip test if not test mode
            }

            $limit = 999999;
            $order = '`id` ASC';

            if($list->test == '1')
            {
                $limit = $this->ask('This list is a test. How many records to import?');
                $order = "RAND()";
                $chunk_size = $limit;
            }

            $this->info('Importing list: ' . $list->name);

            $input_records = DB::table($table_name)
                               ->where('import_batch', $list->batch)
                               ->where('invalid_email', '0')
                               ->where(function ($query) {
                                   $query->whereNull('output_id')
                                         ->orWhere('output_id', '0');
                                })
                               ->where('send_method', 'email')
                               ->where('mailing_list', $list->list_number)
                               ->take($limit)
                               ->orderByRaw($order);



            $this->line(' - retrieving records...');

            $input_records = $input_records->get();

            foreach($input_records as $member) $member_ids[] = $member->id;

            $this->line(' - counting records...');

            $member_count = count($member_ids);
            $chunk_count = ceil($member_count / $chunk_size);

            $this->line(' - starting import');

            $input = DB::table($table_name)->whereIn('id', $member_ids)->chunk($chunk_size, function($records) use ($sendout, $list, &$chunk, $chunk_count, $member_count) {
            //$input = $importable_records->chunk($chunk_size, function($records) use ($sendout, $list, &$chunk, $chunk_count, $member_count) {

                //print_r($records); exit;

                $this->line(' --- chunk ' . $chunk . ' of ' . $chunk_count . ' (' . $member_count . ' records)');

                $import_data = [];

                foreach($records as $record)
                {
                    $format_email = $sendout->campaign->formatEmail(false);
                    //print_r($format_email); exit;
                    foreach($format_email as $field => $format)
                    {
                        if(is_callable($format))
                        {
                            $record = $format($field, $record);
                        }
                    }

                    //print_r($record); exit;

                    $import_record = [
                        'EmailAddress' => $record->{$sendout->campaign->email_field},
                        'Name' => $record->{$sendout->campaign->first_name_field} . ' ' . $record->{$sendout->campaign->last_name_field},
                        'CustomFields' => [
                            ['Key' => 'firstname', 'Value' => $record->{$sendout->campaign->first_name_field}],
                            ['Key' => 'lastname', 'Value' => $record->{$sendout->campaign->last_name_field}],
                        ],
                    ];

                    //print_r($record); exit;

                    foreach($list->fields(false) as $field_key => $field_name)
                    {
                        $import_record['CustomFields'][] = ['Key' => $field_key, 'Value' => $record->{$field_name}];
                    }

                    $dc_url = 'https://campaigns.datacleanse.com.au/';
                    $import_record['CustomFields'][] = ['Key' => '_update_url', 'Value' => $dc_url . $sendout->campaign->url('update', $record->hash, false) . '/' . $sendout->id];
                    $import_record['CustomFields'][] = ['Key' => '_confirm_url', 'Value' => $dc_url . $sendout->campaign->url('confirm', $record->hash, false) . '/' . $sendout->id];

                    //print_r($import_record); exit;

                    $import_data[] = $import_record;

                }

                $this->line(' ----- prepared chunk. Sending...');

                //print_r($import_data); exit;

                if(! $list->list_imported)
                {
                    CampaignMonitor::importList($list, $import_data, $this);
                }

                $this->line(' ------- sent');

                $chunk++;

            });

            $list->list_imported = '1';
            $list->save();

        }

        $this->info("\nDone.\n");

        return;

    }
}



// function($field, $record) {
// 	if($record->years_consecutive_2017 == 1) {
// 		$record->{$field->name} = $record->years_consecutive_2017. ' year';
// 	} else {
// 		$record->{$field->name} = $record->years_consecutive_2017 . ' years';
// 	}
// 	return $record;
// }
//
// function($field, $record) {
//     $years = str_split(str_pad($record->years_consecutive_2017, 2, '0', STR_PAD_LEFT));
// 	$record->{$field->name} = $years[0] == '0' ? '_' : $years[0];
// 	return $record;
// }
//
// function($field, $record) {
//     $years = str_split(str_pad($record->years_consecutive_2017, 2, '0', STR_PAD_LEFT));
// 	$record->{$field->name} = $years[1] == '0' ? '_' : $years[1];
// 	return $record;
// }
