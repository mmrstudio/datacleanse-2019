<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\CreateTables',
        'App\Console\Commands\CreateInputTable',
        'App\Console\Commands\CreateOutputTable',
        'App\Console\Commands\InputImport',
        'App\Console\Commands\InputProcess',
        'App\Console\Commands\InputValidate',
        'App\Console\Commands\InputDistribute',
        'App\Console\Commands\InputSplit',
        'App\Console\Commands\InputExport',
        'App\Console\Commands\InputExclude',
        'App\Console\Commands\InputFix',
        'App\Console\Commands\URLShorten',
        'App\Console\Commands\CampaignMonitorClients',
        'App\Console\Commands\CampaignMonitorTemplates',
        'App\Console\Commands\CampaignMonitorPrepare',
        'App\Console\Commands\CampaignMonitorImport',
        'App\Console\Commands\CampaignMonitorStats',
        'App\Console\Commands\CampaignMonitorDelete',
        'App\Console\Commands\CampaignReport',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }
}
