<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignCreative extends Model
{
    protected $table = 'campaigns_creative';

    public function process() {

        if($this)
        {
            // title size
            $title_size = explode('/', $this->title_size);
            $this->title_size = [
                'desktop' => $title_size[0],
                'mobile' => isset($title_size[1]) ? $title_size[1] : '1',
            ];

            // heading size
            $heading_size = explode('/', $this->heading_size);
            $this->heading_size = [
                'desktop' => $heading_size[0],
                'mobile' => isset($heading_size[1]) ? $heading_size[1] : '1',
            ];

            // body size
            $body_size = explode('/', $this->body_size);
            $this->body_size = [
                'desktop' => $body_size[0],
                'mobile' => isset($body_size[1]) ? $body_size[1] : '1',
            ];

            // button size
            $button_size = explode('/', $this->button_size);
            $this->button_size = [
                'desktop' => $button_size[0],
                'mobile' => isset($button_size[1]) ? $button_size[1] : '1',
            ];

            // footer size
            $footer_size = explode('/', $this->footer_size);
            $this->footer_size = [
                'desktop' => $footer_size[0],
                'mobile' => isset($footer_size[1]) ? $footer_size[1] : '1',
            ];

            // field size
            $field_size = explode('/', $this->field_size);
            $this->field_size = [
                'desktop' => $field_size[0],
                'mobile' => isset($field_size[1]) ? $field_size[1] : '1',
            ];

            // thanks

            $thanks_title_size = explode('/', $this->thanks_title_size);
            $this->thanks_title_size = [
                'desktop' => $thanks_title_size[0],
                'mobile' => isset($thanks_title_size[1]) ? $thanks_title_size[1] : '1',
            ];

            $thanks_sub_title_size = explode('/', $this->thanks_sub_title_size);
            $this->thanks_sub_title_size = [
                'desktop' => $thanks_sub_title_size[0],
                'mobile' => isset($thanks_sub_title_size[1]) ? $thanks_sub_title_size[1] : '1',
            ];

            $thanks_body_size = explode('/', $this->thanks_body_size);
            $this->thanks_body_size = [
                'desktop' => $thanks_body_size[0],
                'mobile' => isset($thanks_body_size[1]) ? $thanks_body_size[1] : '1',
            ];

            // intro size
            $intro_size = explode('/', $this->intro_size);
            $this->intro_size = [
                'desktop' => $intro_size[0],
                'mobile' => isset($intro_size[1]) ? $intro_size[1] : '1',
            ];

        }

        return $this;
    }

}
