<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Auth;

class Client extends Model
{

    public function users()
    {
        return $this->hasMany('App\User', 'client_id', 'id');
    }

    public function campaigns()
    {
        return $this->hasMany('App\Campaign', 'client_id', 'id');
    }

    public static function current() {

        $client = false;
        $user = Auth::user();

        if($user) $client = $user->client;

        return $client;

    }

}
