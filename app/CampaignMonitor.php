<?php

namespace App;

use Cache;
use Log;

class CampaignMonitor
{

    public static function auth() {
        return ['api_key' => env('CM_API_KEY')];
    }

	public static function clients($use_cached=true)
	{
        // get directly from API
        if(! $use_cached) return self::CMLoadClients();

        // load from cache (or from API)
        $clients = Cache::remember('cmClients', 60*24, function() {
            return self::CMLoadClients();
        });

		return $clients;
	}

    public static function templates($client_id, $use_cached=true)
	{
        // get directly from API
        if(! $use_cached) return self::CMLoadTemplates($client_id);

        // load from cache (or from API)
        $clients = Cache::remember('cmTemplates_' . $client_id, 60*24, function() {
            return self::CMLoadTemplates($client_id);
        });

		return $clients;
	}

	public static function campaignStats($campaign_id, $use_cached=true)
	{
        // get directly from API
        if(! $use_cached) return self::CMLoadCampaignStats($campaign_id);

        // load from cache (or from API)
        $clients = Cache::remember('cmCampaignStats', 60*24, function() use ($campaign_id) {
            return self::CMLoadCampaignStats($campaign_id);
        });

		return $clients;
	}

    public static function CMLoadClients() {

        $clients = [];

        $campaign_monitor = new \CS_REST_General(self::auth());
        $get_clients = $campaign_monitor->get_clients();

        if($get_clients->was_successful())
        {
            $clients_response = $get_clients->response;

            foreach($clients_response as $client)
            {
                $clients[$client->ClientID] = $client->Name;
            }

        }

        return $clients;

    }

    public static function CMLoadTemplates($client_id) {

        $templates = [];

        $campaign_monitor = new \CS_REST_Clients($client_id, self::auth());
        $get_templates = $campaign_monitor->get_templates();

        if($get_templates->was_successful())
        {
            $templates_response = $get_templates->response;

            foreach($templates_response as $template)
            {
                $templates[$template->TemplateID] = $template->Name;
            }

        }

        return $templates;

    }

    public static function CMLoadCampaignStats($campaign_id) {

        $campaign_monitor = new \CS_REST_Campaigns($campaign_id, self::auth());
        $get_campaign_stats = $campaign_monitor->get_summary();

        if($get_campaign_stats->was_successful())
        {
            $stats_response = $get_campaign_stats->response;

            return $stats_response;

        }

        return false;

    }

    public static function createList($list, $cli=false)
    {
        $campaign_monitor = new \CS_REST_Lists(NULL, self::auth());

        if($cli) $cli->line(' - creating CM list...');

        $createList = $campaign_monitor->create($list->client->cm_client_id, [
            'Title' => $list->name,
            'ConfirmedOptIn' => false,
            'UnsubscribeSetting' => 'AllClientLists'
        ]);

        Log::info('CampaignMonitor::createList: ' . $list->name);
        Log::info((array)$createList->response);

        if($createList->was_successful())
        {
            $list->cm_list_id = $createList->response;
            $list->list_created = '1';
            $list->save();

            if($cli) $cli->line('   - created list (ID: ' . $list->cm_list_id . ')');

            // Create custom fields
            $list = CampaignMonitor::createCustomFields($list, $cli);

        }
        else
        {
            print_r($createList->response);

            if($createList->response->Code == '250')
            {
                // Create custom fields
                if($list->cm_list_id && ! $list->fields_created)
                    $list = CampaignMonitor::createCustomFields($list, $cli);

            }

        }

        return $list;

    }

    public static function createCustomFields($list, $cli) {

        if($cli) $cli->line('   - creating custom fields...');

        // Get list
        $cm_list = new \CS_REST_Lists($list->cm_list_id, self::auth());

        $fields_created = true;

        foreach($list->fields() as $field_key => $field_name)
        {

            $createField = $cm_list->create_custom_field([
                'FieldName' => $field_key,
                'DataType' => 'Text',
            ]);

            if($createField->was_successful())
            {
                if($cli) $cli->line('     - ' . var_dump($createField->response));
            }
            else
            {
                if($cli) $cli->error('     - ' . var_dump($createField->response));
                $fields_created = false;
            }

        }

        Log::info('CampaignMonitor::createCustomFields: ' . $list->name);
        Log::info($list->fields());

        $list->fields_created = ($fields_created) ? '1' : '0';
        $list->save();

        return $list;

    }

    public static function createCampaign($campaign, $cli=false, $custom_subject=false)
    {
        $campaign_data = [
            'Subject' => $custom_subject ? $custom_subject : $campaign->sendout->subject,
            'Name' => $campaign->name,
            'FromName' => $campaign->sendout->from_name,
            'FromEmail' => $campaign->sendout->from_email,
            'ReplyTo' => $campaign->sendout->reply_email,
            'ListIDs' => [$campaign->cmList->cm_list_id],
            'TemplateID' => $campaign->sendout->cm_template_id,
            'TemplateContent' => [
                'Singlelines' => [],
                'Multilines' => [],
                'Images' => [],
                'Repeaters' => [],
            ],
        ];

        //print_r($campaign_data); exit;

        $CS_REST_Campaigns = new \CS_REST_Campaigns(NULL, self::auth());

        $createCampaign = $CS_REST_Campaigns->create_from_template($campaign->client->cm_client_id, $campaign_data);

        if($createCampaign->was_successful())
        {
            if($cli) $cli->line('   - created campaign (ID: ' . $createCampaign->response . ')');

            $campaign->campaign_created = '1';
            $campaign->cm_campaign_id = $createCampaign->response;
            $campaign->save();
        }
        else
        {
            if($cli) $cli->error('   - could not create campaign');
            print_r($createCampaign->response);
        }

        Log::info('CampaignMonitor::createCampaign: ' . $campaign->name);
        Log::info($createCampaign->response);

        return $campaign;

    }

    public static function deleteCampaign($campaign_id, $cli=false) {
        if($cli) $cli->line('Deleting campaign: ' . $campaign_id);
        $CS_REST_Campaigns = new \CS_REST_Campaigns($campaign_id, self::auth());
        return $CS_REST_Campaigns->delete();
    }

    public static function importList($list, $importData, $cli=false) {

        if($cli) $cli->line('   - sending to CM');

        $CS_REST_Subscribers = new \CS_REST_Subscribers($list->cm_list_id, self::auth());

        $importSubscribers = $CS_REST_Subscribers->import($importData, false);

        // if(! $importSubscribers->was_successful())
        // {
        //     if($cli) $cli->comment('   - imported with issues');
        //     print_r($importSubscribers->response->ResultData->FailureDetails);
        // }
        // else
        // {
        //     if($cli) $cli->line('   - imported successfully');
        // }

        if($cli) $cli->line('   - imported');

        Log::info('CampaignMonitor::importList: ' . $list->name);
        Log::info((array)$importSubscribers->response);

        return $list;
    }

    public static function deleteList($list_id, $cli=false) {
        if($cli) $cli->line('Deleting list: ' . $list_id);
        $CS_REST_Lists = new \CS_REST_Lists($list_id, self::auth());
        return $CS_REST_Lists->delete();
    }

    public static function CMLoadCampaignOpens($campaign_id, $cm_campaign) {

        $campaign_monitor = new \CS_REST_Campaigns($campaign_id, self::auth());

        $opens = [];

        $request = [
            'page' => 1,
            'page_size' => 1000,
            'num_pages' => 0,
        ];

        $process = [];

        $get_campaign_opens = $campaign_monitor->get_opens(null, $request['page'], $request['page_size']);

        //dd($get_campaign_opens);

        if($get_campaign_opens->was_successful())
        {
            $request['num_pages'] = $get_campaign_opens->response->NumberOfPages;

            $process[] = $request;
            $process[] = $get_campaign_opens;

            // go to page 2
            $request['page']++;

            // loop pages
            while($request['page'] <= ($request['num_pages'] + 1))
            {
                // loop responses
                foreach($get_campaign_opens->response->Results as $result)
                {
                    $result_date = explode(' ', $result->Date);
                    $opens[$result_date[0]] = isset($opens[$result_date[0]]) ? $opens[$result_date[0]] + 1 : 1;
                }

                // increment page
                $request['page']++;

                // request next page
                if($request['page'] <= $request['num_pages'])
                {
                    $get_campaign_opens = $campaign_monitor->get_opens(null, $request['page'], $request['page_size']);

                    $process[] = $request;
                    $process[] = $get_campaign_opens;
                }
            }

            //dd($opens);
            //dd($process);

            return $opens;

        }

        return [];

    }

    public static function CMLoadCampaignClicks($campaign_id, $cm_campaign) {

        $campaign_monitor = new \CS_REST_Campaigns($campaign_id, self::auth());

        $clicks = [];

        $request = [
            'page' => 1,
            'page_size' => 1000,
            'num_pages' => 0,
        ];

        $process = [];

        $get_campaign_clicks = $campaign_monitor->get_clicks(null, $request['page'], $request['page_size']);

        //dd($get_campaign_clicks);

        if($get_campaign_clicks->was_successful())
        {
            $request['num_pages'] = $get_campaign_clicks->response->NumberOfPages;

            $process[] = $request;
            $process[] = $get_campaign_clicks;

            // go to page 2
            $request['page']++;

            // loop pages
            while($request['page'] <= ($request['num_pages'] + 1))
            {
                // loop responses
                foreach($get_campaign_clicks->response->Results as $result)
                {
                    $result_date = explode(' ', $result->Date);
                    $clicks[$result_date[0]] = isset($clicks[$result_date[0]]) ? $clicks[$result_date[0]] + 1 : 1;
                }

                // increment page
                $request['page']++;

                // request next page
                if($request['page'] <= $request['num_pages'])
                {
                    $get_campaign_clicks = $campaign_monitor->get_clicks(null, $request['page'], $request['page_size']);

                    $process[] = $request;
                    $process[] = $get_campaign_clicks;
                }
            }

            return $clicks;

        }

        return [];

    }

}
