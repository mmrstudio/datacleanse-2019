<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sendout extends Model
{

    protected $table = 'send_outs';
    protected $fillable = ['client_id', 'campaign_id', 'name', 'cm_template_id', 'subject', 'from_email', 'from_name', 'reply_email', 'sms_message', 'sms_opt_out'];

    public function client()
    {
        return $this->hasOne('App\Client', 'id', 'client_id');
    }

    public function campaign()
    {
        return $this->hasOne('App\Campaign', 'id', 'campaign_id');
    }

    public function lists()
    {
        return $this->hasMany('App\CMList', 'send_out_id', 'id');
    }

}
