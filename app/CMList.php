<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Field;

class CMList extends Model
{

    protected $table = 'cm_lists';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function client()
    {
        return $this->hasOne('App\Client', 'id', 'client_id');
    }

    public function campaign()
    {
        return $this->hasOne('App\Campaign', 'id', 'campaign_id');
    }

    public function sendout()
    {
        return $this->hasOne('App\Sendout', 'id', 'send_out_id');
    }

    public function members()
    {
        return $this->hasMany('App\CMListMember', 'list_id', 'id');
    }

    public function fields($urls=true)
    {
        $list_fields = [];
        $fields = Field::where('email', '1')->where('campaign_id', $this->campaign->id)->get();

        foreach($fields as $field) $list_fields['_' . $field->name] = $field->name;
        // {
        //     $field_name = $field->trim > 0 ? $field->name . '_trim' : $field->name;
        //     $list_fields[] = $field_name;
        // }

        // add url fields
        if($urls)
        {
            $list_fields['_confirm_url'] = 'confirm_url';
            $list_fields['_update_url'] = 'update_url';
        }

        return $list_fields;
    }

    public static function name($list_name)
    {
        return CMList::with('client', 'campaign', 'sendout')->where('name', $list_name)->first();
    }

    public static function exists($list_name)
    {
        return CMList::where('name', $list_name)->count() > 0 ? true : false;
    }

}
