<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{

    public function getOptions()
    {
        $options = [];

        if($this->type === 'enum' || $this->type === 'gender')
        {
            $options_arr = explode(',', $this->options);
            foreach($options_arr as $option)
            {
                $option_arr = explode(':', $option);
                $options[$option_arr[0]] = $option_arr[1];
            }
        }

        return $options;
    }

}
