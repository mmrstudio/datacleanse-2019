<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;
use Excel;

use App\Field;
use App\Campaign;
use App\Sendout;

class Input extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function lists()
    {
        return $this->belongsToMany('CMList', 'cm_list_members', 'input_id', 'list_id');
    }

    public static function hash($hash)
    {
        return Input::where('hash', '=', $hash)->first();
    }

    public static function copy($campaign, $form_data, $input)
    {

        if($input)
        {
            $fields = Field::where('campaign_id', $campaign->id)->where('copy', '1')->get();

            foreach($fields as $field)
            {
                if(! isset($form_data[$field->name]))
                {
                    $form_data[$field->name] = ($input && $input->{$field->name}) ? $input->{$field->name} : '';
                }
            }

        }

        return $form_data;

    }

    public static function emptyRecord($campaign_id)
    {
        $empty_record = new Input;
        $field_definitions = [];

        $fields = Field::where('campaign_id', $campaign_id)->where('form', '1')->get();

        foreach($fields as $field)
        {
            $field_definitions[$field->name] = $field->default;
        }

        $empty_record->fill($field_definitions);

        //dd($empty_record);

        return $empty_record;
    }

    public static function validate($validate_what, $campaign_id, $batch=false, $cli=false)
    {
    	ini_set('memory_limit','1024M');

		// get campaign
        $campaign = Campaign::find($campaign_id);
        if(! $campaign) return;

        $table_name = Campaign::tableName('input', $campaign_id);

        $validation_errors = [];
        $error_ids = [];

        switch($validate_what)
        {
            case 'email' :
                $send_method = 'email';
                $validate_column = $campaign->email_field;
                $invalid_col = 'invalid_email';
                break;

            case 'sms' :
                $send_method = 'sms';
                $validate_column = $campaign->mobile_field;
                $invalid_col = 'invalid_mobile';
                break;
        }

        $source = DB::table($table_name)->select(['id', $validate_column])->where('import_batch', $batch)->where('send_method', $send_method)->get();
        $num_source = count($source);
        $i = 1;

        DB::table($table_name)->where('import_batch', $batch)->update([$invalid_col => '0']);

        // validate data
        foreach($source as $record)
        {
            switch($validate_what)
            {
                case 'email' : $valid_record = filter_var($record->{$campaign->email_field}, FILTER_VALIDATE_EMAIL); break;
                case 'sms' : $valid_record = strlen(trim($record->{$campaign->mobile_field})) >= 10; break;
            }

            if (! $valid_record)
            {

                switch($validate_what)
                {
                    case 'email' :

                        $validation_errors[] = 'Invalid Email (' . $record->id . '): ' . $record->{$campaign->email_field};
                        if(! in_array($record->id, $error_ids)) $error_ids[] = $record->id;

                        // mark record as invalid email
                        DB::table($table_name)->where($campaign->email_field, $record->{$campaign->email_field})->where('import_batch', $batch)->update(['invalid_email' => '1']);

                        break;

                    case 'sms' :

                        $validation_errors[] = 'Invalid Mobile (' . $record->id . '): ' . $record->{$campaign->mobile_field};
                        if(! in_array($record->id, $error_ids)) $error_ids[] = $record->id;

                        // mark record as invalid email
                        DB::table($table_name)->where($campaign->mobile_field, $record->{$campaign->mobile_field})->where('import_batch', $batch)->update(['invalid_mobile' => '1']);

                        break;
                }

            }

            if($i % 1000 == 0)
            {
                if($cli) $cli->info('Validating ' . $i . ' of ' . $num_source);
            }

            $i++;

        }

        if(count($validation_errors) > 0)
        {
            if($cli) $cli->error('VALIDATION ERRORS:');

            foreach($validation_errors as $error)
            {
                if($cli) $cli->error($error);
            }

            if($cli) $cli->error('IDs: ' . implode(',', $error_ids));

            return;

        }

        if($cli) $cli->info('No validation errors were encountered');

        return;

    }

    public static function processEmails($campaign_id, $batch=false, $cli=false)
    {
        ini_set('memory_limit','2048M');

        // get campaign
        $campaign = Campaign::find($campaign_id);
        if(! $campaign) return;

        $table_name = Campaign::tableName('input', $campaign_id);

        $singles = 0;
        $duplicates = 0;

        // get all emails
        $emails = DB::table($table_name)->select('id', $campaign->email_field)->where('import_batch', $batch)->where('invalid_email', '0')->where('send_method', 'email')->orderBy($campaign->email_field, 'asc')->get();
        $num_emails_to_process = count($emails);
        $i = 1;

        if($cli) $cli->info('Processing...');

        foreach($emails as $record)
        {

            // count num emails
            $num_emails = DB::table($table_name)->select($campaign->email_field)->where('import_batch', $batch)->where('send_method', 'email')->where($campaign->email_field, $record->{$campaign->email_field})->count();

            if($num_emails === 1) $singles++;
            if($num_emails > 1) $duplicates++;

            // save number of duplicates against each record
            DB::table($table_name)->where('id', $record->id)->update(['email_duplicates' => $num_emails]);

            // divide into first mailing list
            if($num_emails === 1)
            {
                //$mailing_list = $batch . '_1';
                DB::table($table_name)->where('id', $record->id)->update(['mailing_list' => '1']);
            }

            if($i % 1000 == 0)
            {
                if($cli) $cli->info('    ' . $i . ' of ' . $num_emails_to_process);
            }

            $i++;

        }

        // get max number of duplicates
        $max_duplicates = DB::table($table_name)->select(DB::raw('COUNT(*) as count'))->where('import_batch', $batch)->where('send_method', 'email')->groupBy($campaign->email_field)->orderBy('count', 'desc')->take(1)->get();

        $max_duplicates = (count($max_duplicates) > 0) ? $max_duplicates[0]->count : 0;

		if($cli) $cli->info('Total records: ' . count($emails));
		if($cli) $cli->info('Records with NO duplicates: ' . $singles);
		if($cli) $cli->info('Records with duplicates: ' . $duplicates);
		if($cli) $cli->info('Max duplicates: ' . $max_duplicates);

        // DIVIDE

        // get duplicate emails
        $duplicate_emails = DB::table($table_name)->select($campaign->email_field)->where('import_batch', $batch)->where('send_method', 'email')->where('email_duplicates', '>', 1)->groupBy($campaign->email_field)->get();
        $num_duplicates_to_process = count($emails);
        $i = 1;

        if($cli) $cli->info('Allocating...');

        // loop through each then assign mailing list number
        foreach($duplicate_emails as $duplicate_email)
        {
            $list_num = 1;

            // get all of same
            $get_duplicated = DB::table($table_name)->select('id', $campaign->email_field)->where('import_batch', $batch)->where('send_method', 'email')->where($campaign->email_field, '=', $duplicate_email->{$campaign->email_field})->get();

            foreach($get_duplicated as $dup)
            {
                //$mailing_list = $batch . '_' . $list_num;
                DB::table($table_name)->where('id', $dup->id)->update(['mailing_list' => $list_num]);
                $list_num++;
            }

            if($i % 1000 == 0)
            {
                if($cli) $cli->info('    ' . $i . ' of ' . $num_duplicates_to_process);
            }

            $i++;

        }

        return;

    }

    public static function processSMS($campaign_id, $batch=false, $cli=false)
    {
        ini_set('memory_limit','2048M');

        // get campaign
        $campaign = Campaign::find($campaign_id);
        if(! $campaign) return;

        $table_name = Campaign::tableName('input', $campaign_id);

        $singles = 0;
        $duplicates = 0;

        // get all mobiles
        $mobiles = DB::table($table_name)->select('id', $campaign->mobile_field)->where('import_batch', $batch)->where('invalid_mobile', '0')->where('send_method', 'sms')->orderBy($campaign->mobile_field, 'asc')->get();
        $num_mobiles_to_process = count($mobiles);
        $i = 1;

        if($cli) $cli->info('Processing...');

        foreach($mobiles as $record)
        {

            // count num mobiles
            $num_mobiles = DB::table($table_name)->select($campaign->mobile_field)->where('import_batch', $batch)->where('send_method', 'sms')->where($campaign->mobile_field, $record->{$campaign->mobile_field})->count();

            if($num_mobiles === 1) $singles++;
            if($num_mobiles > 1) $duplicates++;

            // save number of duplicates against each record
            DB::table($table_name)->where('id', $record->id)->update(['mobile_duplicates' => $num_mobiles]);

            // divide into first mailing list
            if($num_mobiles === 1)
            {
                $mailing_list = '1';
                DB::table($table_name)->where('id', $record->id)->update(['mailing_list' => $mailing_list]);
            }

            if($i % 1000 == 0)
            {
                if($cli) $cli->info('    ' . $i . ' of ' . $num_mobiles_to_process);
            }

            $i++;

        }

        // get max number of duplicates
        $max_duplicates = DB::table($table_name)->select(DB::raw('COUNT(*) as count'))->where('import_batch', $batch)->where('send_method', 'sms')->groupBy($campaign->mobile_field)->orderBy('count', 'desc')->take(1)->get();

        $max_duplicates = (count($max_duplicates) > 0) ? $max_duplicates[0]->count : 0;

		if($cli) $cli->info('Total records: ' . count($mobiles));
		if($cli) $cli->info('Records with NO duplicates: ' . $singles);
		if($cli) $cli->info('Records with duplicates: ' . $duplicates);
		if($cli) $cli->info('Max duplicates: ' . $max_duplicates);

        // DIVIDE

        // get duplicate mobiles
        $duplicate_mobiles = DB::table($table_name)->select($campaign->mobile_field)->where('import_batch', $batch)->where('send_method', 'sms')->where('mobile_duplicates', '>', 1)->groupBy($campaign->mobile_field)->get();
        $num_duplicates_to_process = count($mobiles);
        $i = 1;

        if($cli) $cli->info('Allocating...');

        // loop through each then assign mailing list number
        foreach($duplicate_mobiles as $duplicate_mobile)
        {
            $list_num = 1;

            // get all of same
            $get_duplicated = DB::table($table_name)->select('id', $campaign->mobile_field)->where('import_batch', $batch)->where('send_method', 'sms')->where($campaign->mobile_field, '=', $duplicate_mobile->{$campaign->mobile_field})->get();

            foreach($get_duplicated as $dup)
            {
                $mailing_list = $list_num;
                DB::table($table_name)->where('id', $dup->id)->update(['mailing_list' => $mailing_list]);
                $list_num++;
            }

            if($i % 1000 == 0)
            {
                if($cli) $cli->info('    ' . $i . ' of ' . $num_duplicates_to_process);
            }

            $i++;

        }

        return;

    }

    public static function exportEmails($campaign_id, $sendout_id, $batch=false, $cli=false)
    {
    	ini_set('memory_limit','1024M');

        // get campaign
        $campaign = Campaign::find($campaign_id);
        if(! $campaign) return;

        // get sendout
        $sendout = Sendout::with('client', 'campaign', 'lists')->find($sendout_id);
        if(! $sendout && $cli) return $cli->error('Could not find sendout with ID of "' . $sendout_id . '"');

        $table_name = Campaign::tableName('input', $campaign_id);

        $export_path = storage_path('export');
        $validation_errors = [];
        $error_ids = [];

        if($cli) $cli->info('Starting...');

        // get mailing lists
        $mailing_lists = [];
        $get_mailing_lists = DB::table($table_name)->select('mailing_list')->where('import_batch', '=', $batch)->where('mailing_list', '<>', '')->groupBy('mailing_list')->get();

        $fields = [];
        $get_fields = Field::where('campaign_id', $campaign->id)->where('email', 1)->get();
        $columns = [];

        foreach($get_fields as $field)
        {
            if($field->input)
            {
                $columns[] = $field->name;
            }

            $fields[$field->name] = $field;
        }

        $columns[] = 'hash';

        //print_r($fields); exit;
        //print_r($columns); exit;

        if(count($get_mailing_lists) > 0)
        {

            foreach($get_mailing_lists as $list) $mailing_lists[] = $list->mailing_list;

            //print_r($mailing_lists); exit;

            // generate each list
            foreach($mailing_lists as $list)
            {
                $export_file = $campaign->client->slug . '_' . $campaign->slug . '_EMAIL_' . $batch . '_' . $list;
                $export_data = [];

                if($cli) $cli->info('Exporting... ' . $list);

                //$sourceData = Source::where('mailing_list', $list)->get();
                $sourceData = DB::table($table_name)
                                ->where('import_batch', $batch)
                                ->where('mailing_list', $list)
                                ->where('send_method', 'email')
                                ->where('invalid_email', '0')
                                ->where(function ($query) {
                                    $query->whereNull('output_id')
                                          ->orWhere('output_id', '0');
                                      })
                                ->limit(200)
                                ->inRandomOrder()
                                ->get();

                //print_r($sourceData); exit;

                foreach($sourceData as $record)
                {

                    $format_email = $campaign->formatEmail(false);
                    //print_r($format_email);
                    foreach($format_email as $field_key => $field_format)
                    {
                        if(is_callable($field_format))
                        {
                            $record = $field_format($field_key, $record);
                        }
                    }

                    if($record === false) continue;

                    $export_row = [
                        'EmailAddress' => $record->{$campaign->email_field},
                        'Name' => $record->{$campaign->first_name_field} . ' ' . $record->{$campaign->last_name_field},
                    ];

                    foreach($fields as $field)
                    {
                        $key = $field->name;

                        if(isset($record->{$key}))
                        {
                            $value = ($record->{$key} && strlen($record->{$key}) > 0) ? $record->{$key} : '';

                            if($field->trim > 0)
                            {
                                $key = $key . '_trim';
                                $value = (strlen($value) > $field->trim) ? substr($value, 0, $field->trim-3).'...' : $value;
                            }

                            $export_row['_' . $key] = $value;

                        }
                        else
                        {
                            $export_row['_' . $key] = '';
                        }

                    }

                    $dc_url = 'https://campaigns.datacleanse.com.au/';

                    $export_row['_confirm_url'] = $dc_url . $campaign->url('update', $record->hash, false) . '/' . $sendout->id;
                    $export_row['_update_url'] = $dc_url . $campaign->url('update', $record->hash, false) . '/' . $sendout->id;

                    //print_r($export_row);

                    $export_data[] = $export_row;

                }

                //exit;
                //print_r($export_data); exit;

                Excel::create($export_file, function($excel) use($export_data) {

                    $excel->sheet('data', function($sheet) use($export_data) {
                        $sheet->fromArray($export_data);
                    });

                })->store('csv', $export_path);

                if($cli) $cli->info('Exported: ' . $export_file . '.csv');

            }

        }
        else
        {
            if($cli) $cli->error('There are no lists to export!');
        }

    }

    public static function exportSMS($sendout_id, $batch=false, $cli=false)
    {
        $export_path = storage_path('export');
        $validation_errors = [];
        $error_ids = [];

        // get sendout
        $sendout = Sendout::find($sendout_id);
        if(! $sendout) return;

        // get campaign
        $campaign = Campaign::find($sendout->campaign_id);
        if(! $campaign) return;

        $table_name = Campaign::tableName('input', $campaign->id);

        if($cli) $cli->info('Starting...');

        $mailing_lists = [];
        $get_mailing_lists = DB::table($table_name)->select('mailing_list')->where('import_batch', '<>', '')->where('import_batch', '=', $batch)->where('invalid_mobile', '=', '0')->where('send_method', '=', 'sms')->groupBy('mailing_list')->get();

        if(count($get_mailing_lists) > 0)
        {

            foreach($get_mailing_lists as $list) $mailing_lists[] = $list->mailing_list;

            // generate each list
            foreach($mailing_lists as $list)
            {
                //$export_file = $list;
                $export_file = $campaign->client->slug . '_' . $campaign->slug . '_' . $batch . '_SMS_' . $list;

                if($cli) $cli->info('Exporting... ' . $list);

                $data = [];
                $get_list = DB::table($table_name)->where('mailing_list', '=', $list)->where('invalid_mobile', '=', '0')->where('send_method', '=', 'sms')->where('import_batch', '=', $batch)->get();

                //print_r($get_list); exit;

                foreach($get_list as $record)
                {

                    $short_url = str_replace('http://', '', $record->short_url);

                    $first_name = substr($record->{$campaign->first_name_field}, 0, 20);
                    $first_name = explode(' ', $first_name);
                    $first_name = $first_name[0];

                    $name_length = strlen($first_name);

                    $sms_message = $sendout->sms_message;
                    $sms_message = str_replace('(_first_name_field_)', $first_name, $sms_message);

                    $sms_message = str_replace('(____link_____)', $short_url, $sms_message);
                    $sms_message = str_replace('(_optout_)', $sendout->sms_opt_out, $sms_message);

                    //echo $sms_message . "\n";

                    $message_length = strlen($sms_message);

                    if($name_length > 0 && $message_length > 0)
                    {

                        $data[] = [
                            'Mobile No' => str_replace(' ', '', $record->{$campaign->mobile_field}),
                            'First Name' => $record->{$campaign->first_name_field},
                            'Custom 1' => $sms_message,
                            'Length' => $message_length,
                        ];

                    }
                }

                //print_r($data); exit;

                Excel::create($export_file, function($excel) use($data) {

                    $excel->sheet('data', function($sheet) use($data) {
                        $sheet->fromArray($data);
                    });

                })->store('csv', $export_path);

                if($cli) $cli->info('Exported: ' . $export_file . '.csv');

            }

        }
        else
        {
            if($cli) $cli->error('There are no lists to export!');
        }
    }

    public static function exportExcludes($campaign_id, $batch=false, $cli=false)
    {
    	ini_set('memory_limit','1024M');

        // get campaign
        $campaign = Campaign::find($campaign_id);
        if(! $campaign) return;

        $table_name = Campaign::tableName('output', $campaign_id);

        $export_path = storage_path('export');
        $validation_errors = [];
        $error_ids = [];

        if($cli) $cli->info('Exporting...');

        $export_data = [];

        $sourceData = DB::table($table_name)
                        ->select($campaign->email_field, 'input_id')
                        ->get();

        //print_r($fields); exit;
        //print_r($columns); exit;

        $export_file = $campaign->client->slug . '_' . $campaign->slug . '_EXCLUDES_' . $batch;

        if(count($sourceData) > 0)
        {

            // generate each list
            foreach($sourceData as $record)
            {
                $export_data[] = [
                    'email' => $record->{$campaign->email_field},
                    'input_id' => $record->input_id
                ];

            }

            Excel::create($export_file, function($excel) use($export_data) {

                $excel->sheet('data', function($sheet) use($export_data) {
                    $sheet->fromArray($export_data);
                });

            })->store('csv', $export_path);

            if($cli) $cli->info('Exported: ' . $export_file . '.csv');

        }
        else
        {
            if($cli) $cli->error('There are no lists to export!');
        }

    }


}
