<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class CampaignStats extends Model
{
    protected $table = 'campaigns_stats';

    public static function summary($campaign_id, $dimension, $metric=false, $sendout=false)
    {
        DB::enableQueryLog();

        $select = ['date', 'dimension'];
        $group_by = ['date', 'dimension'];

        $summary_query = CampaignStats::select();
        $summary_query->where('campaign_id', $campaign_id);
        $summary_query->where('dimension', $dimension);

        if($metric) {
            $summary_query->where('metric', $metric);
            $group_by[] = 'metric';
            $select[] = 'metric';
        }

        if($sendout)
        {
            $summary_query->where('sendout_id', $sendout);
        }

        $summary_query->groupBy($group_by);

        $select[] = DB::raw('SUM(value) as value');

        $get_data = $summary_query->select($select);
        $get_data = $summary_query->get();

        //dd(DB::getQueryLog());

        //dd($get_data->toArray());

        return $get_data;
    }

    public static function total($campaign_id, $dimension, $metric=false, $sendout=false)
    {
        DB::enableQueryLog();

        $get_data = false;

        $select = ['dimension'];
        $group_by = ['dimension'];

        $summary_query = CampaignStats::select();
        $summary_query->where('campaign_id', $campaign_id);
        $summary_query->where('dimension', $dimension);

        if($metric) {
            $summary_query->where('metric', $metric);
            $group_by[] = 'metric';
            $select[] = 'metric';
        }

        if($sendout)
        {
            $summary_query->where('sendout_id', $sendout);
        }

        $summary_query->groupBy($group_by);

        $select[] = DB::raw('SUM(value) as value');

        $get_data = $summary_query->select($select);
        $get_data = $summary_query->first();

        //dd(DB::getQueryLog());
        //dd($get_data->toArray());

        return $get_data;
    }

    public static function dates($campaign_id)
    {
        $campaign_dates = CampaignStats::select('date')->where('campaign_id', $campaign_id)->groupBy('date')->get();

        dd($campaign_dates);

        return $campaign_dates;
    }

}
