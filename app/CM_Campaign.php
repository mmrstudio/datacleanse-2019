<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\CampaignMonitor;

class CM_Campaign extends Model
{

    protected $table = 'cm_campaigns';

    public function stats($sync=false)
    {
        $stats = [];

        // update stats from CM?
        if($sync) $this->syncStats();

        dd($this->toArray());

        return $stats;
    }

    public function syncStats()
    {
        // load stats
        $cm_stats = CampaignMonitor::CMLoadCampaignStats($this->cm_campaign_id);

        if($cm_stats)
        {
            //dd($cm_stats);

            $this->recipients = $cm_stats->Recipients;
            $this->opened = $cm_stats->TotalOpened;
            $this->unique_opened = $cm_stats->UniqueOpened;
            $this->clicks = $cm_stats->Clicks;
            $this->bounces = $cm_stats->Bounced;
            $this->unsubscribes = $cm_stats->Unsubscribed;
            $this->complaints = $cm_stats->SpamComplaints;
            $this->forwards = $cm_stats->Forwards;
            $this->likes = $cm_stats->Likes;
            $this->mentions = $cm_stats->Mentions;
            $this->web_version = $cm_stats->WebVersionURL;
            $this->world_view = $cm_stats->WorldviewURL;
            $this->save();

            // force update timestamp
            $this->touch();

            // reload model
            $this->fresh();

        }

        //return $stats;
    }

    public static function stat($column, $send_out_id=false, $campaign_id=false)
    {
        $get_stat = CM_Campaign::query();

        if($send_out_id) $get_stat->where('send_out_id', $send_out_id);
        if($campaign_id) $get_stat->where('campaign_id', $campaign_id);

        return $get_stat->sum($column);
    }

}
