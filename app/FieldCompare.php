<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FieldCompare extends Model
{
    protected $table = 'fields_compare';
}
