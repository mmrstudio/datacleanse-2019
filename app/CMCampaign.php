<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Field;

class CMCampaign extends Model
{

    protected $table = 'cm_campaigns';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function client()
    {
        return $this->hasOne('App\Client', 'id', 'client_id');
    }

    public function campaign()
    {
        return $this->hasOne('App\Campaign', 'id', 'campaign_id');
    }

    public function sendout()
    {
        return $this->hasOne('App\Sendout', 'id', 'send_out_id');
    }

    public function cmList()
    {
        return $this->hasOne('App\CMList', 'id', 'list_id');
    }

    public static function name($campaign_name)
    {
        return CMCampaign::with('client', 'campaign', 'sendout')->where('name', $campaign_name)->first();
    }

    public static function exists($campaign_name)
    {
        return CMCampaign::where('name', $campaign_name)->count() > 0 ? true : false;
    }

    public function stats($sync=false)
    {
        $stats = [];

        // update stats from CM?
        if($sync) $this->syncStats();

        dd($this->toArray());

        return $stats;
    }

    public function syncStats()
    {
        // load stats
        $cm_stats = CampaignMonitor::CMLoadCampaignStats($this->cm_campaign_id);

        if($cm_stats)
        {
            //dd($cm_stats);

            $this->recipients = $cm_stats->Recipients;
            $this->opened = $cm_stats->TotalOpened;
            $this->unique_opened = $cm_stats->UniqueOpened;
            $this->clicks = $cm_stats->Clicks;
            $this->bounces = $cm_stats->Bounced;
            $this->unsubscribes = $cm_stats->Unsubscribed;
            $this->complaints = $cm_stats->SpamComplaints;
            $this->forwards = $cm_stats->Forwards;
            $this->likes = $cm_stats->Likes;
            $this->mentions = $cm_stats->Mentions;
            $this->web_version = $cm_stats->WebVersionURL;
            $this->world_view = $cm_stats->WorldviewURL;
            $this->save();

            // force update timestamp
            $this->touch();

            // reload model
            $this->fresh();

        }

        return array_only($this->toArray(), ['recipients', 'opened', 'unique_opened', 'clicks', 'bounces', 'unsubscribes', 'complaints', 'forwards', 'likes', 'mentions']);
    }

    public static function stat($column, $send_out_id=false, $campaign_id=false)
    {
        $get_stat = CM_Campaign::query();

        if($send_out_id) $get_stat->where('send_out_id', $send_out_id);
        if($campaign_id) $get_stat->where('campaign_id', $campaign_id);

        return $get_stat->sum($column);
    }

    public function syncOpens()
    {
        //echo $this->cm_campaign_id . '<br>';
        //echo $this->name . '<br>';

        // load stats
        $opens = CampaignMonitor::CMLoadCampaignOpens($this->cm_campaign_id, $this);

        //print_r($opens);

        return $opens;
    }

    public function syncClicks()
    {
        //echo $this->cm_campaign_id . '<br>';
        //echo $this->name . '<br>';

        // load stats
        $clicks = CampaignMonitor::CMLoadCampaignClicks($this->cm_campaign_id, $this);

        //print_r($clicks);

        return $clicks;
    }

}
