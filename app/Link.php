<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;
use Validator;

class Link extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $connection = 'dtclnz';

    public static function createFromUrl($url, $expires) {

        $hashids = new \Hashids\Hashids('datacleanse', 6);

        // validate URL
        $validator = Validator::make(['url' => $url], ['url' => ['url']]);
        if($validator->fails()) return ['created' => false, 'error' => 'Invalid URL'];

        // remove whitespace
        $url = trim($url);

        // check if URL already exists (and hasn't expired)
        $check_link = Link::where('url', $url)->whereRaw('expires_at >= curdate()')->first();

        if($check_link)
        {

            if(! $check_link->hash)
            {
                $hash = $hashids->encode($check_link->id);
                $check_link->hash = $hash;
                $check_link->save();
            }

            // return original
            return [
                'created' => true,
                'hash' => $check_link->hash,
                'url' => $check_link->url,
                'short_url' => url($check_link->hash),
                'id' => $check_link->id,
            ];

        }
        else
        {

            // create link record
            $link = new Link;
            $link->url = $url;

            if($expires) {
                $expires = ($expires !== 'never') ? $expires : false;
            } else {
                $expires = '1 year';
            }

            if($expires) {

                $expiry_date = new \DateTime();
                $expiry_date->add(date_interval_create_from_date_string($expires));
                $expiry_date = $expiry_date->format('Y-m-d h:i:s');

                $link->expires_at = $expiry_date;

            }

            $link->save();

            // generate hash
            $hash = $hashids->encode($link->id . rand(1,99));
            $duplicate_hash = true;

            // while($duplicate_hash === true)
            // {
            //     $check_hash = DB::connection('dtclnz')->table('links')->where('hash', $hash)->count();
            //
            //     if($check_hash > 0)
            //     {
            //         $hash = $hashids->encode($link->id . rand(1000,9999));
            //     }
            //     else
            //     {
            //         $duplicate_hash = false;
            //     }
            //
            // }

            $link->hash = $hash;
            $link->save();

            return [
                'created' => true,
                'hash' => $link->hash,
                'url' => $link->url,
                'short_url' => url($link->hash),
            ];

        }

    }

}
