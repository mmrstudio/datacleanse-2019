<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;
use Gate;
use Auth;
use Schema;
use Excel;
use View;
use LaravelAnalytics;

use App\CM_Campaign;
use App\Input;
use App\Output;
use App\Field;
use App\CampaignStats;
use App\FieldCompare;
use App\Report;

class Campaign extends Model
{
    protected $fillable = ['open', 'client_id', 'name', 'slug', 'email_field', 'mobile_field', 'first_name_field', 'last_name_field'];

    public function __construct() {
        View::addNamespace('public', public_path('assets'));
    }

    public function client()
    {
        return $this->hasOne('App\Client', 'id', 'client_id');
    }

    public function creative()
    {
        return $this->hasOne('App\CampaignCreative', 'campaign_id', 'id');
    }

    public function send_outs()
    {
        return $this->hasMany('App\Sendout', 'campaign_id', 'id');
    }

    public function fields($campaign_step=1)
    {
        $get_fields = Field::where('campaign_id', $this->id);
        if($campaign_step !== false)
        {
            $get_fields->where('step', $campaign_step);
        }
        return $get_fields->get();
        //return $this->hasMany('App\Field', 'campaign_id', 'id');
    }

    public function dataColumns()
    {
        $columns = [];
        $fields = Field::where('campaign_id', $this->id)->where('output', 1)->get();
        foreach($fields as $field)
        {
            $columns[] = $field->name;
        }

        $columns[] = 'created_at';
        //$columns = array_merge($columns, ['created_at']);

        return $columns;
    }

    public function url($screen=false, $hash=false, $full_url=true)
    {
        $url_segments = [$this->client->slug, $this->slug];
        if($screen) $url_segments[] = $screen;
        if($hash) $url_segments[] = $hash;

        $url_segments = implode('/', $url_segments);

        return $full_url ? url($url_segments) : $url_segments;
    }

    public function campaignTitle($include_client=true)
    {
        $title = $include_client ? $this->client->name . ' - ' : '';
        $title .= $this->title;
        return $title;
    }

    public static function current()
    {

        $campaign = false;
        $user = Auth::user();

        if($user) $campaign = $user->campaign;

        return $campaign;

    }

    public static function setCurrent($campaign_id=0)
    {

        $user = Auth::user();
        $user->current_campaign = $campaign_id;
        $user->save();

        return $campaign_id;

    }

    public static function nav($campaign_id=false)
    {
        // get current campaign
        $campaign = Campaign::current();

        // if campaign not currently set, return empty nav
        if(! $campaign) return [];

        $campaign_nav = [
            'overview' => [
                'url' => url('overview'),
                'title' => 'Overview (All)',
                'active' => false,
                'sub_items' => $campaign->sendoutNav('overview'),
                'open' => false,
            ],
            'responses' => [
                'url' => url('responses'),
                'title' => 'Responses',
                'active' => false,
                //'sub_items' => $campaign->sendoutNav('responses'),
                'sub_items' => false,
                'open' => false,
            ],
            // 'traffic' => [
            //     'url' => url('traffic'),
            //     'title' => 'Traffic (All)',
            //     'active' => false,
            //     'sub_items' => $campaign->sendoutNav('traffic'),
            //     'open' => false,
            // ],
            // 'data' => [
            //     'url' => url('data'),
            //     'title' => 'Data',
            //     'active' => false,
            //     'sub_items' => false,
            //     'open' => false,
            // ],
        ];

        // append reports option
        if(count($campaign->reports()) > 0)
        {
            $campaign_nav['reports'] = [
                'url' => url('reports'),
                'title' => 'Reports',
                'active' => false,
                //'sub_items' => $campaign->reportsNav(),
                'open' => false,
            ];
        }

        // include admin controls?
        if(Gate::allows('mmr-admin'))
        {
            $campaign_admin_nav['admin'] = [
                'url' => url('campaign/' . $campaign->id . '/edit'),
                'title' => 'Campaign Admin',
                'active' => false,
                'sub_items' => [
                    'edit' => [
                        'url' => url('campaign/' . $campaign->id . '/edit'),
                        'title' => 'Edit Campaign',
                        'active' => false,
                    ],
                    'fields' => [
                        'url' => url('campaign/' . $campaign->id . '/fields'),
                        'title' => 'Fields',
                        'active' => false,
                    ],
                    'sendouts' => [
                        'url' => url('campaign/' . $campaign->id . '/send-outs'),
                        'title' => 'Send Outs',
                        'active' => false,
                    ],
                    'data' => [
                        'url' => url('campaign/' . $campaign->id . '/input'),
                        'title' => 'Input Data',
                        'active' => false,
                    ],
                ],
            ];

            $campaign_nav = array_merge($campaign_admin_nav, $campaign_nav);

        }

        $campaigns_item = [
            'campaigns' => [
                'url' => url('campaigns'),
                'title' => 'All Campaigns',
                'active' => false,
                'sub_items' => false,
                'open' => false,
            ]
        ];

        $campaign_nav = array_merge($campaigns_item, $campaign_nav);

        return $campaign_nav;

    }

    public function sendoutNav($section)
    {

        $send_out_nav = [];
        $send_outs = Campaign::with('send_outs')->find($this->id)->send_outs;

        if($send_outs)
        {
            foreach($send_outs as $send_out)
            {
                if($send_out->overview)
                {
                    $send_out_nav[$send_out->id] = [
                        'url' => url("$section/send-out/$send_out->id"),
                        'title' => $send_out->name,
                        'active' => false,
                    ];
                }
            }
        }

        return $send_out_nav;

    }

    public function reportsNav()
    {
        $reports_nav = [[
            'url' => url("report/export/all"),
            'title' => 'Export All',
            'active' => false,
        ]];
        return $reports_nav;

    }

    public function assetDirectory()
    {
        return $this->client->slug . '_' . $this->slug;
    }

    public function asset($asset)
    {
        return url('assets/' . $this->assetDirectory() . '/' . $asset);
    }

    public function view($view_name)
    {
        return 'public::' . $this->assetDirectory() . '.views.' . $view_name;
    }

    public static function tableName($table, $campaign_id)
    {
        return '_campaign_' . str_pad($campaign_id, 4, '0', STR_PAD_LEFT) . '_' . $table;
    }

    public function inputTable()
    {
        return '_campaign_' . str_pad($this->id, 4, '0', STR_PAD_LEFT) . '_input';
    }

    public function outputTable()
    {
        return '_campaign_' . str_pad($this->id, 4, '0', STR_PAD_LEFT) . '_output';
    }

    public static function createInputTable($campaign_id, $backup=true)
    {
        // generate table name
        $table_name = Campaign::tableName('input', $campaign_id);

        // get defined fields
        $fields = Field::where('campaign_id', $campaign_id)->where('input', '1')->orderBy('order', 'ASC')->get();

        //dd($fields->toArray());

        // backup existing table?
        if($backup)
        {
            if(Schema::hasTable($table_name))
            {
                $backup_name = 'z' . date('U') . $table_name . '_backup';
                Schema::rename($table_name, $backup_name);
            }
        }
        else
        {
            Schema::dropIfExists($table_name);
        }

        // create new input table with default fields
        Schema::create($table_name, function($table) use ($fields, $table_name)
		{
			$table->increments('id');

            // add custom fields to table
            foreach($fields as $field)
            {
                switch($field->type)
                {
                    case 'string' : case 'country' : case 'state' : case 'postcode' : case 'suburb' : case 'dob' : case 'gender' : case 'enum' : default : $col = $table->string($field->name, $field->length)->nullable(); break;
                    case 'email' : $col = $table->string($field->name, $field->length)->index($field->name)->nullable(); break;
                    case 'integer' : $col = $table->integer($field->name)->nullable(); break;
                    case 'float' : $col = $table->float($field->name)->nullable(); break;
                    case 'text' : $col = $table->text($field->name)->nullable(); break;
                    case 'date' : $col = $table->date($field->name)->nullable(); break;
                    case 'datetime' : $col = $table->dateTime($field->name)->nullable(); break;
                }

                // set default value
                if($field->default) $col->default($field->default);

            }

            // add default fields
			$table->string('send_method', 255)->default('email')->index('send_method')->nullable();
			$table->string('hash', 255)->nullable();
			$table->string('short_url', 255)->nullable();
			$table->string('import_batch')->index('import_batch')->nullable();
            $table->integer('mailing_list')->default('1')->nullable();
			$table->integer('email_duplicates')->default('1')->nullable();
			$table->integer('mobile_duplicates')->default('1')->nullable();
			$table->integer('invalid_email')->default('0')->index('invalid_email')->nullable();
			$table->integer('invalid_mobile')->default('0')->nullable();
			$table->integer('output_id')->nullable();
			$table->dateTime('output_at')->nullable();
			$table->timestamps();

		});

        return $table_name;

    }

    public static function createOutputTable($campaign_id, $backup=true)
    {
        // generate table name
        $table_name = Campaign::tableName('output', $campaign_id);

        // get defined fields
        $fields = Field::where('campaign_id', $campaign_id)->where('output', '1')->orderBy('order', 'ASC')->get();

        //dd($fields->toArray());

        // backup existing table?
        if($backup)
        {
            if(Schema::hasTable($table_name))
            {
                $backup_name = 'z' . date('U') . $table_name . '_backup';
                Schema::rename($table_name, $backup_name);
            }
        }
        else
        {
            Schema::dropIfExists($table_name);
        }

        // create new input table with default fields
        Schema::create($table_name, function($table) use ($fields, $table_name)
		{
			$table->increments('id');

            // add custom fields to table
            foreach($fields as $field)
            {
                switch($field->type)
                {
                    case 'string' : case 'email' : case 'country' : case 'state' : case 'postcode' : case 'suburb' : case 'dob' : case 'gender' : case 'enum' : default : $col = $table->string($field->name, $field->length)->nullable(); break;
                    case 'integer' : $col = $table->integer($field->name)->nullable(); break;
                    case 'float' : $col = $table->float($field->name)->nullable(); break;
                    case 'text' : $col = $table->text($field->name)->nullable(); break;
                    case 'date' : $col = $table->date($field->name)->nullable(); break;
                    case 'datetime' : $col = $table->dateTime($field->name)->nullable(); break;
                }

                // set default value
                if($field->default) $col->default($field->default);

            }

            // add default fields
			$table->string('send_method', 255)->nullable();
			$table->string('hash', 255)->index('hash')->nullable();
			$table->string('import_batch')->nullable();
            $table->integer('mailing_list')->default('0')->nullable();
            $table->integer('sendout')->default('0')->nullable();
            $table->integer('step')->default('1')->nullable();
            $table->string('entry_type')->nullable();
			$table->integer('input_id')->nullable();
			$table->timestamps();

		});

        return $table_name;

    }

    public function import($import_file, $batch_name, $cli=false)
    {
        Model::unguard();
        ini_set('memory_limit','8000M');

        $table_name = Campaign::tableName('input', $this->id);

        $chunk_size = 500;
        $total_records = count_lines($import_file) - 1; // minus header row
        $total_chunks = ceil($total_records / $chunk_size);
        $current_chunk = 1;
        $imported_records = 0;

        // time tracking
        $time_start = microtime_float();
        $time_current = $time_start;
        $time_elapsed = 0;

        Excel::filter('chunk')->load($import_file)->chunk($chunk_size, function($rows) use ($batch_name, $cli, $total_records, $total_chunks, &$current_chunk, &$imported_records, $chunk_size, $time_start, &$time_current, &$time_elapsed, $table_name) {

            $imported_this_chunk = $imported_records + $chunk_size;

            if($cli)
            {
                $time_now = microtime_float();
                $time_diff = $time_now - $time_current;

                $time_elapsed = $time_elapsed + $time_diff;
                $time_current = $time_now;

                $time_diff_str = $time_diff > 60 ? ($time_diff / 60) . 'm' : $time_diff . 's';
                $time_elapsed_str = $time_elapsed > 60 ? ($time_elapsed / 60) . 'm' : $time_elapsed . 's';

                // calculate time remaining
                $time_per_record = $time_diff / $chunk_size;
                $time_total = $time_per_record * $total_records;
                $time_this_chunk = $imported_this_chunk * $time_per_record;
                $time_remaining = $time_total - $time_this_chunk;

                $time_remaining_str = $time_remaining > 60 ? ceil($time_remaining / 60) . 'm' : ceil($time_remaining) . 's';

                $cli->info(" - Importing $imported_records - $imported_this_chunk of $total_records (chunk $current_chunk of $total_chunks) - Time elapsed: $time_diff_str / $time_elapsed_str (Estimated $time_remaining_str remaining)");

            }

            foreach($rows as $row)
            {
                $input = new Input;
                $input->setTable($table_name);
                $input->fill($row->toArray());
                $input->import_batch = $batch_name;
                $input->save();

                // generate hash
                $input->hash = md5(implode($input->toArray()));
                $input->save();

                $imported_records++;
            }

            $current_chunk++;

        });

        if($cli) $cli->info("\nImport complete!\n");

        return $imported_records;

    }

    public function input()
    {
        $input_table = new Input;
        $input_table->setTable(Campaign::tableName('input', $this->id));
        return $input_table;
    }

    public function output()
    {
        $output_table = new Output;
        $output_table->setTable(Campaign::tableName('output', $this->id));
        return $output_table;
    }

    public function validationRules($campaign_step=1)
    {
        $rules = [];

        foreach($this->fields($campaign_step) as $field)
        {

            if($field->type === 'state' || $field->type === 'suburb')
            {
                $country_field = Field::where('campaign_id', $this->id)->where('type', 'country')->first();

                if(strlen($country_field->options) > 0)
                {
                    $country_formats = explode(',', $country_field->options);
                    $country_aus = $country_formats[0];
                    $country_nz = $country_formats[1];
                }
                else
                {
                    $country_aus = 'AUSTRALIA';
                    $country_nz = 'NEW ZEALAND';
                }

                $rules[$field->name] = function($input, $rules) use ($field, $country_field, $country_aus, $country_nz) {

                    if(isset($input[$field->name]) && ($input[$country_field->name] == $country_aus || $input[$country_field->name] == $country_nz))
                    {
                        $rules[$field->name] = 'required';
                    }
                    else
                    {
                        $rules[$field->name] = '';
                    }

                    return $rules;

                };

            }
            elseif($field->type === 'postcode')
            {
                $country_field = Field::where('campaign_id', $this->id)->where('type', 'country')->first();

                if(strlen($country_field->options) > 0)
                {
                    $country_formats = explode(',', $country_field->options);
                    $country_aus = $country_formats[0];
                    $country_nz = $country_formats[1];
                }
                else
                {
                    $country_aus = 'AUSTRALIA';
                    $country_nz = 'NEW ZEALAND';
                }

                $rules[$field->name] = function($input, $rules) use ($field, $country_field, $country_aus) {

                    if(isset($input[$field->name]) && $input[$country_field->name] == $country_aus)
                    {
                        $rules[$field->name] = 'required';
                    }
                    else
                    {
                        $rules[$field->name] = '';
                    }

                    return $rules;

                };

            }
            elseif(strlen($field->validation_callable) > 0)
            {
                eval('$validation_rule = ' . $field->validation_callable . ';');

                if(is_callable($validation_rule))
                {
                    $rules[$field->name] = $validation_rule;
                }
            }
            else
            {
                $field_rules = [];

                if($field->required) $field_rules[] = 'required';
                if($field->validation != '') $field_rules[] = $field->validation;
                if($field->type === 'email' && $field->required) $field_rules[] = 'email';
                //if($field->type === 'dob' && $field->required) $field_rules[] = 'date_format:"' . $field->format_input . '"';

                if($field->type === 'gender' && $field->required) {
                    $genders = [];
                    $genders_arr = explode(',', $field->options);
                    foreach($genders_arr as $gender)
                    {
                        $gender_arr = explode(':', $gender);
                        $genders[] = $gender_arr[0];
                    }
                    $field_rules[] = 'in:' . implode(',', $genders);
                }

                if(count($field_rules) > 0)
                {
                    $rules[$field->name] = implode('|', $field_rules);
                }

            }

        }

        //dd($rules);

        return $rules;

    }

    public function validationMessages($campaign_step=1)
    {
        $messages = [];

        foreach($this->fields($campaign_step) as $field)
        {
            $field_label = $field->front_label ? $field->front_label : $field->label;

            // required
            if($field->required) $messages[$field->name . '.required'] = '"' . $field_label . '" is required';

            // email
            if($field->required && $field->type === 'email') $messages[$field->name . '.email'] = '"' . $field_label . '" must be a valid email address';
            if($field->required && $field->type === 'dob') $messages[$field->name . '.date_format'] = '"' . $field_label . '" must be a valid date';
            if($field->required && $field->type === 'gender') $messages[$field->name . '.in'] = '"' . $field_label . '" is invalid';

            if($field->validation_messages)
            {
                $field_messages = explode(',', $field->validation_messages);
                foreach($field_messages as $field_message)
                {
                    $field_message = explode(':', $field_message);
                    $messages[$field->name . '.' . $field_message[0]] = $field_message[1];
                }
            }

        }

        return $messages;

    }

    public function validationSanitize($campaign_step=1)
    {
        $sanitize = [];

        foreach($this->fields($campaign_step) as $field)
        {
            if($field->type === 'country')
            {
                $sanitize[$field->name] = function($input) use ($field) {
                    $input[$field->name] = strtoupper($input[$field->name]);
                    return $input;
                };
            }

            if($field->type === 'dob')
            {
                $sanitize[$field->name] = function($input) use ($field) {

                    $dob = \DateTime::createFromFormat('Y-m-d', $input[$field->name]);

                    if($dob !== false)
                    {
                        $input[$field->name] = $dob->format($field->format_output);
                    }

                    return $input;

                };
            }

            if(strlen($field->validation_sanitize) > 0)
            {
                eval('$sanitation_rule = ' . $field->validation_sanitize . ';');

                if(is_callable($sanitation_rule))
                {
                    $sanitize[$field->name] = $sanitation_rule;
                }

            }
        }

        //dd($sanitize);

        return $sanitize;

    }

    public function formatInput($campaign_step=1)
    {
        $format = [];

        //dd($this->fields->toArray());

        foreach($this->fields($campaign_step) as $field)
        {

            switch($field->type)
            {

                case 'dob' :

                    $format[$field->name] = function($value) use ($field) {

                        if($value !== '')
                        {
                            $dob = \DateTime::createFromFormat($field->format_input, $value);

                            if($dob)
                            {
                                $value = $dob->format('d/m/Y');
                            }
                            else
                            {
                                $value = '';
                            }

                        }
                        else
                        {
                            $value = '';
                        }

                        return $value;

                    };

                    break;

                case 'country' :

                    $format[$field->name] = function($value) use ($field) {
                        $value = trim($value) === '' ? $field->default : $value;
                        return strtoupper($value);
                    };

                    break;

                case 'gender' :

                    $format[$field->name] = function($value) use ($field) {
                        // $gender_format = explode('/', $field->format_input);
                        // $genders = [
                        //     $gender_format[0] => 'Male',
                        //     $gender_format[1] => 'Female',
                        // ];
                        //dd($genders);
                        //return isset($genders[$value]) ? $genders[$value] : '';
                        return $value;
                    };

                    break;

                default :

                    if(strlen($field->format_input) > 0)
                    {
                        eval('$format_input = ' . $field->format_input . ';');

                        if(is_callable($format_input))
                        {
                            $format[$field->name] = $format_input;
                        }

                    }

                    break;

            }

        }

        //dd($format);

        return $format;

    }

    public function formatOutput($campaign_step=1, $confirm=false)
    {
        $format = [];
        $fields = $this->fields($campaign_step);

        foreach($fields as $field)
        {

            switch($field->type)
            {

                case 'dob' :

                    $format[$field->name] = function($form_data) use ($field, $confirm) {

                        if(isset($form_data[$field->name]))
                        {

                            if($form_data[$field->name] !== '')
                            {
                                // if confirm, use input date field format, otherwise use datepicker format (d/m/Y)
                                $input_date_format = $confirm ? $field->format_output : 'd/m/Y';

                                $dob = \DateTime::createFromFormat($input_date_format, $form_data[$field->name]);

                                if($dob)
                                {
                                    $form_data[$field->name] = $dob->format($field->format_output);
                                }
                                else
                                {
                                    $form_data[$field->name] = '';
                                }

                            }
                            else
                            {
                                $form_data[$field->name] = '';
                            }

                        }

                        return $form_data;

                    };

                    break;

                case 'gender' :

                    $format[$field->name] = function($form_data) use ($field, $confirm) {
                        // $gender_format = explode('/', $field->format_input);
                        // $genders = [
                        //     'Male' => $gender_format[0],
                        //     'Female' => $gender_format[1],
                        // ];
                        // $form_data[$field->name] = $genders[$form_data[$field->name]];
                        return $form_data;
                    };

                    break;

                default :

                    if(strlen($field->format_output) > 0)
                    {
                        eval('$format_output = ' . $field->format_output . ';');

                        if(is_callable($format_output))
                        {
                            $format[$field->name] = $format_output;
                        }

                    }

                    break;

            }

        }

        //dd($format);

        return $format;

    }

    public function formatEmail($campaign_step=1)
    {
        $format = [];

        foreach($this->fields($campaign_step) as $field)
        {

            if(strlen($field->format_email) > 0)
            {
                $eval_code = '$format_email = ' . $field->format_email . ';';
                eval($eval_code);

                if(is_callable($format_email))
                {
                    $format[$field->name] = $format_email;
                }

            }

        }

        //dd($format);

        return $format;

    }

    public function firstSendRecipients($send_method)
    {
        $recipients = 0;
        $first_send = Sendout::where('campaign_id', $this->id)->orderBy('id', 'ASC')->first();
        $first_send_id = $first_send ? $first_send->id : false;

        if($send_method == 'email')
        {
            $recipients = CM_Campaign::stat('recipients', $first_send_id, $this->id);
        }

        return $recipients;
    }

    public static function stats($campaign_id, $send_out_id=false)
    {

        $stats = [];
        $input_table = Campaign::tableName('input', $campaign_id);
        $output_table = Campaign::tableName('output', $campaign_id);

        // get campaign
        $campaign = Campaign::find($campaign_id);

        // get first send responses
        $first_send = Sendout::where('campaign_id', $campaign_id)->orderBy('id', 'ASC')->first();
        $first_send_id = $first_send ? $first_send->id : false;
        $first_send_recipients = CM_Campaign::stat('recipients', $first_send_id, $campaign_id);

        // get total recipients
        $stats['total_recipients'] = [
            'total' => $campaign->recipients > 0 ? $campaign->recipients : $first_send_recipients,
            '%' => '100%',
        ];

        // get recipients
        $stats['recipients'] = [
            'total' => CM_Campaign::stat('recipients', $send_out_id, $campaign_id),
            '%' => '100%',
        ];

        if($send_out_id)
        {
            $count_responses = DB::table($output_table)->where('sendout', $send_out_id)->count();
            $count_responses_pc = $stats['recipients']['total'] > 0 ? number_format(($count_responses / $stats['recipients']['total']) * 100, 2) . '%' : '';
        }
        else
        {
            $count_responses = DB::table($output_table)->count();
            $total_recipients = $campaign->recipients > 0 ? $campaign->recipients : $first_send_recipients;

            $count_responses_pc = $stats['recipients']['total'] > 0 ? number_format(($count_responses / $total_recipients) * 100, 2) . '%' : '';
        }

        $stats['responses'] = [
            'total' => $count_responses,
            '%' => $count_responses_pc,
        ];

        // statistic columns
        $columns = ['opened', 'unique_opened', 'clicks', 'bounces', 'unsubscribes', 'complaints', 'forwards', 'likes', 'mentions'];

        foreach($columns as $column)
        {
            $stat_value = CM_Campaign::stat($column, $send_out_id, $campaign_id);

            $stats[$column] = [
                'total' => $stat_value,
                '%' => $stats['recipients']['total'] > 0 ? number_format(($stat_value / $stats['recipients']['total']) * 100, 2) . '%' : '',
            ];
        }

        // get unopened
        $unopened = $stats['recipients']['total'] - $stats['unique_opened']['total'];
        $stats['unopened'] = [
            'total' => $unopened,
            '%' => $stats['recipients']['total'] > 0 ? number_format(($unopened / $stats['recipients']['total']) * 100, 2) . '%' : '',
        ];

        return $stats;

    }

    public function analytics($args=[])
    {
        $response = [];
        $page_filter = 'ga:pagePath=~^' . str_replace('/', '\\/', '/' . $this->url(false, false, false) . '/');
        $filters = [];

        $response['dimension'] = isset($args['dimension']) && $args['dimension'] ? $args['dimension'] : 'ga:pagePath';
        $response['metric'] = isset($args['metric']) && $args['metric'] ? $args['metric'] : 'ga:sessions';

        if(isset($args['day']) && $args['day'])
        {
            $response['start_date'] = new \DateTime($args['day']);
            $response['end_date'] = new \DateTime($args['day']);
        }
        else
        {
            $response['start_date'] = isset($args['start_date']) && $args['start_date'] ? new \DateTime($args['start_date']) : new \DateTime($this->created_at);
            $response['end_date'] = isset($args['end_date']) && $args['end_date'] ? new \DateTime($args['end_date']) : new \DateTime();
        }

        if(isset($args['sendout']))
        {
            $page_filter .= '(update|confirm)\/(.*?)\/' . $args['sendout'];
        }

        $filters[] = $page_filter;

        $response['max_results'] = isset($args['max_results']) && $args['max_results'] ? intval($args['max_results']) : 6;
        $response['sort'] = isset($args['sort']) && $args['sort'] ? $args['sort'] : '-' . $response['metric'];

        if(isset($args['filters'])) $filters[] = $args['filters'];

        $ga_query = [
            'dimensions' => $response['dimension'],
            'sort' => $response['sort'],
            'filters' => implode(',', $filters),
            'max-results' => $response['max_results'],
        ];

        $response['ga_query'] = $ga_query;

        //dd($response);
        //dd($ga_query);

        $ga_results = LaravelAnalytics::performQuery($response['start_date'], $response['end_date'], $response['metric'], $ga_query);

        $total_sessions = $ga_results->totalsForAllResults['ga:sessions'];
        $combined_sessions = 0;

        $response['start_date'] = $response['start_date']->format('Y-m-d');
        $response['end_date'] = $response['end_date']->format('Y-m-d');
        $response['total_sessions'] = intval($ga_results->totalsForAllResults['ga:sessions']);
        $response['total_results'] = intval($ga_results->totalResults);
        $response['sessions'] = [];

        if($ga_results->totalResults > 0)
        {

            foreach($ga_results->rows as $row)
            {
                $response['sessions'][] = [
                    'label' => $row[0],
                    'value' => intval($row[1]),
                ];

                $combined_sessions = $combined_sessions + $row[1];
            }

            foreach($response['sessions'] as &$metric)
            {
                $metric['%'] = number_format(($metric['value'] / $total_sessions) * 100, 2);
            }

            $other_metrics = $total_sessions - $combined_sessions;

            $response['sessions'][] = [
                'label' => 'Others',
                'value' => $other_metrics,
                '%' => number_format(($other_metrics / $total_sessions) * 100, 2),
            ];

        }

        //print_r($response); exit;

        //dd($ga_results);
        //dd($response);

        return $response;

    }

    public function traffic($args=[])
    {


        return $response;

    }

    public function results($send_out_id=false)
    {

        $stats = [];
        $input_table = Campaign::tableName('input', $this->id);
        $output_table = Campaign::tableName('output', $this->id);

        // get campaign
        $campaign_id = $this->id;
        $campaign = $this;

        // get first send responses
        $first_send_recipients = $this->firstSendRecipients('email') + $this->firstSendRecipients('sms');

        // get total recipients
        $stats['recipients'] = [
            'label' => 'Total Recipients',
            'value' => $campaign->recipients > 0 ? $campaign->recipients : $first_send_recipients,
            '%' => '100',
        ];

        // get recipients
        $stats['sent'] = [
            'label' => 'Total Sent',
            'value' => CM_Campaign::stat('recipients', $send_out_id, $campaign_id),
            '%' => '100',
        ];

        if($send_out_id)
        {
            $count_responses = DB::table($output_table)->where('sendout', $send_out_id)->count();
            $count_responses_pc = $stats['recipients']['value'] > 0 ? pc($count_responses, $stats['recipients']['value']) : '';
        }
        else
        {
            $count_responses = DB::table($output_table)->count();
            $total_recipients = $campaign->recipients > 0 ? $campaign->recipients : $first_send_recipients;

            $count_responses_pc = $stats['recipients']['value'] > 0 ? pc($count_responses, $total_recipients) : '';
        }

        $stats['responses'] = [
            'label' => 'Total Responses',
            'value' => $count_responses,
            '%' => $count_responses_pc,
        ];

        // EMAILS

        $stats['emails'] = [];

        // statistic columns
        $columns = ['opened', 'unique_opened', 'clicks', 'bounces', 'unsubscribes', 'complaints', 'forwards', 'likes', 'mentions'];

        foreach($columns as $column)
        {
            $stat_value = CM_Campaign::stat($column, $send_out_id, $campaign_id);

            $stats['emails'][$column] = [
                'label' => str_replace('_', ' ', title_case($column)),
                'value' => $stat_value,
                '%' => $stats['recipients']['value'] > 0 ? pc($stat_value, $stats['recipients']['value']) : '',
            ];
        }

        // get unopened
        $unopened = $stats['recipients']['value'] - $stats['emails']['unique_opened']['value'];
        $stats['emails']['unopened'] = [
            'label' => 'Unopened',
            'value' => $unopened,
            '%' => $stats['recipients']['value'] > 0 ? pc($unopened, $stats['recipients']['value']) : '',
        ];

        return $stats;

    }

    public function syncOpens()
    {
        $cm_campaigns = CMCampaign::where('campaign_id', $this->id)->get();
        $opens = [];

        foreach($cm_campaigns as $cm_campaign)
        {
            $campaign_opens = $cm_campaign->syncOpens();

            //print_r($campaign_opens);

            foreach($campaign_opens as $date => $count)
            {
                if(isset($opens[$date]))
                {
                    $opens[$date]['count'] = $opens[$date]['count'] + $count;
                }
                else
                {
                    $opens[$date] = [
                        'count' => $count,
                        'sendout' => $cm_campaign->send_out_id,
                    ];
                }
            }

        }

        //print_r($opens); exit;

        // delete existing
        CampaignStats::where('client_id', $this->client->id)->where('campaign_id', $this->id)->where('dimension', 'campaign')->where('metric', 'opens')->delete();

        foreach($opens as $date => $open)
        {
            $stat = new CampaignStats;
            $stat->client_id = $this->client->id;
            $stat->campaign_id = $this->id;
            $stat->sendout_id = $open['sendout'];
            $stat->dimension = 'campaign';
            $stat->metric = 'opens';
            $stat->value = $open['count'];
            $stat->date = $date;
            $stat->save();
        }

    }

    public function syncClicks()
    {
        $cm_campaigns = CMCampaign::where('campaign_id', $this->id)->get();
        $clicks = [];

        foreach($cm_campaigns as $cm_campaign)
        {
            $campaign_clicks = $cm_campaign->syncClicks();

            //print_r($campaign_clicks);

            foreach($campaign_clicks as $date => $count)
            {
                if(isset($clicks[$date]))
                {
                    $clicks[$date]['count'] = $clicks[$date]['count'] + $count;
                }
                else
                {
                    $clicks[$date] = [
                        'count' => $count,
                        'sendout' => $cm_campaign->send_out_id,
                    ];
                }
            }

        }

        //dd($clicks);

        // delete existing
        CampaignStats::where('client_id', $this->client->id)->where('campaign_id', $this->id)->where('dimension', 'campaign')->where('metric', 'clicks')->delete();

        foreach($clicks as $date => $click)
        {
            $stat = new CampaignStats;
            $stat->client_id = $this->client->id;
            $stat->campaign_id = $this->id;
            $stat->sendout_id = $click['sendout'];
            $stat->dimension = 'campaign';
            $stat->metric = 'clicks';
            $stat->value = $click['count'];
            $stat->date = $date;
            $stat->save();
        }

    }

    public function syncResponses()
    {
        $responses = [];

        $input_table = $this->inputTable();
        $output_table = $this->outputTable();

        $has_entry_type = Schema::hasColumn($output_table, 'entry_type');

        if($has_entry_type)
        {
            $get_responses = DB::select("select CONCAT(YEAR(`created_at`), '-', MONTH(`created_at`),'-', DAY(`created_at`)) AS 'date', COUNT(`id`) AS 'count', `entry_type`, `sendout` FROM `$output_table` GROUP BY `entry_type`, `sendout`, CONCAT(YEAR(`created_at`), '-', MONTH(`created_at`), '-', DAY(`created_at`)) ORDER BY CONCAT(YEAR(`created_at`), '-', MONTH(`created_at`), '-', DAY(`created_at`)) ASC;");
        }
        else
        {
            $get_responses = DB::select("select CONCAT(YEAR(`created_at`), '-', MONTH(`created_at`),'-', DAY(`created_at`)) AS 'date', COUNT(`id`) AS 'count', `sendout` FROM `$output_table` GROUP BY `sendout`, CONCAT(YEAR(`created_at`), '-', MONTH(`created_at`), '-', DAY(`created_at`)) ORDER BY CONCAT(YEAR(`created_at`), '-', MONTH(`created_at`), '-', DAY(`created_at`)) ASC;");
        }

        // delete existing
        CampaignStats::where('client_id', $this->client->id)->where('campaign_id', $this->id)->where('dimension', 'responses')->delete();

        foreach($get_responses as $responses)
        {
            $stat = new CampaignStats;
            $stat->client_id = $this->client->id;
            $stat->campaign_id = $this->id;
            $stat->sendout_id = $responses->sendout;
            $stat->dimension = 'responses';
            $stat->metric = $has_entry_type ? $responses->entry_type : 'update';
            $stat->value = $responses->count;
            $stat->date = $responses->date;
            $stat->save();
        }

        //dd($get_responses);

    }

    public function syncFieldStats()
    {
        $input_table = $this->inputTable();
        $output_table = $this->outputTable();

        // changed/unchaged
        $status_fields = Field::where('form', '1')->where('campaign_id', $this->id)->get();
        foreach($status_fields as $field)
        {
            $input_column = $field->compare_to ? $field->compare_to : $field->name;
            $output_column = $field->name;

            if(Schema::hasColumn($input_table, $input_column) && Schema::hasColumn($output_table, $output_column))
            {
                $field->changed = count(DB::select("select `$input_table`.`id` as 'input_id', `$output_table`.`id` as 'output_id', `$input_table`.`$input_column` as 'old', `$output_table`.`$output_column` as 'new' from $input_table left join $output_table on `$input_table`.`id` = `$output_table`.`input_id` where `$input_table`.`$input_column` <> '' and `$input_table`.`$input_column` <> `$output_table`.`$output_column`"));
                $field->unchanged = count(DB::select("select `$input_table`.`id` as 'input_id', `$output_table`.`id` as 'output_id', `$input_table`.`$input_column` as 'old', `$output_table`.`$output_column` as 'new' from $input_table left join $output_table on `$input_table`.`id` = `$output_table`.`input_id` where `$input_table`.`$input_column` <> '' and `$input_table`.`$input_column` = `$output_table`.`$output_column`"));
                $field->added = count(DB::select("select `$input_table`.`id` as 'input_id', `$output_table`.`id` as 'output_id', `$input_table`.`$input_column` as 'old', `$output_table`.`$output_column` as 'new' from $input_table left join $output_table on `$input_table`.`id` = `$output_table`.`input_id` where `$input_table`.`$input_column` = '' and `$output_table`.`$output_column` <> ''"));
                $field->removed = count(DB::select("select `$input_table`.`id` as 'input_id', `$output_table`.`id` as 'output_id', `$input_table`.`$input_column` as 'old', `$output_table`.`$output_column` as 'new' from $input_table left join $output_table on `$input_table`.`id` = `$output_table`.`input_id` where `$input_table`.`$input_column` <> '' and `$output_table`.`$output_column` = ''"));
                $field->save();
            }
        }

        // field comparisons
        $compare_fields = Field::where('compare', '1')->where('campaign_id', $this->id)->get();

        // delete existing
        FieldCompare::where('client_id', $this->client->id)->where('campaign_id', $this->id)->delete();

        foreach($compare_fields as $field)
        {
            $output_column = $field->name;

            if(Schema::hasColumn($output_table, $output_column))
            {
                $results_sql = "select `$output_table`.`$output_column` as 'value', count(`$output_table`.`$output_column`) as 'responses' from `$output_table` where `$output_table`.`$output_column` <> '' group by `$output_table`.`$output_column` order by count(`$output_table`.`$output_column`) desc limit 150";
                //echo $results_sql;
                $results = DB::select($results_sql);

                foreach($results as $result)
                {
                    $compare = new FieldCompare;
                    $compare->client_id = $this->client->id;
                    $compare->campaign_id = $this->id;
                    $compare->field_id = $field->id;
                    $compare->value = $result->value;
                    $compare->responses = $result->responses;
                    $compare->save();
                }

            }
        }

    }

    public function report($report_name)
    {
        $report_data = false;
        $report = Report::where('campaign_id', $this->id)->where('name', $report_name)->first();

        if($report)
        {
            $run_report = DB::select($report->sql, json_decode($report->params));

            if($run_report)
            {
                $report_data = [];

                foreach($run_report as $row)
                {
                    $report_data[] = (array) $row;
                }
            }

        }

        return $report_data;

    }

    public function reports($campaign_id=false)
    {
        $campaign_id = $campaign_id ? $campaign_id : $this->id;
        $reports = Report::where('campaign_id', $campaign_id)->get();
        return $reports;
    }

    public function getIntro($record) {
        $intro_text = $this->intro;
        
        $fields = $this->fields()->toArray();
        $field_values = array_reduce($fields, function($collect, $field) use ($record) {
            $collect[$field['name']] = $record->{$field['name']};
            return $collect;
        }, []);

        //dd($field_values);

        foreach($field_values as $field_key => $value)
        {
            $intro_text = str_replace('[' . $field_key . ']', $value, $intro_text);
        }

        $intro_text = wpautop($intro_text);

        return $intro_text;
    }

}
