<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

use App\Campaign;

class ProcessRequest extends Request {

    private $campaign;

	public function authorize()
	{
        return true;
	}

    public function sanitize()
    {
        $input = $this->all();
        //dd($input);

        $validation_sanitize = $this->campaign->validationSanitize($input['step']);
        //dd($validation_sanitize);

        foreach($validation_sanitize as $key => $callback)
        {
            if(is_callable($callback))
            {
                if(isset($input[$key]))
                {
                    $input = $callback($input);
                }
            }
        }

        $this->replace($input);

        //dd($this->all());

        return $this->all();

    }

	public function rules()
	{
        // get campaign
        $this->campaign = Campaign::find($this->get('campaign'));

        $this->sanitize();

        $input = $this->all();
        //dd($input);

        //dd(date_parse_from_format('d/m/Y', $input['date_of_birth']));

        // get original input record
        if(isset($input['input']))
        {
            $source_input = $input['input'] !== '0' ? $this->campaign->input()->find($input['input']) : false;
        }
        else
        {
            $source_input = false;
        }

        $rules = [];

        $validation_rules = $this->campaign->validationRules($input['step']);
        //dd($validation_rules);

        foreach($validation_rules as $field => $rule)
        {
            if(is_callable($rule))
            {
                $rules = $rule($input, $rules, $source_input);
            }
            else
            {
                $rules[$field] = $rule;
            }
        }

        //dd($rules);

        return $rules;

	}

    public function messages()
    {
        $input = $this->all();

        $messages = $this->campaign->validationMessages($input['step']);

        //dd($messages);

        return $messages;
    }

}
