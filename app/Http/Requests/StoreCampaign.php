<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Gate;

class StoreCampaign extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('mmr-admin');
        //$userId = auth()->user()->id;
        //return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'max:32',
                Rule::unique('campaigns')->where(function ($query) {
                    $query->where('client_id','=', $this->input('client_id'));
                })
            ],
            'slug' => [
                'required',
                'max:32',
                Rule::unique('campaigns')->where(function ($query) {
                    $query->where('client_id','=', $this->input('client_id'));
                })
            ],
            'client_id' => 'required',
        ];
    }
}
