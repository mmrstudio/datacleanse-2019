<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Form;
use Validator;
use Schema;

use App\Http\Requests;
use App\Http\Requests\ProcessRequest;

use App\Client;
use App\Campaign;
use App\Field;
use App\Input;
use App\Link;

class FrontController extends Controller
{

    public function primaryLanding($client_slug, $campaign_slug, $hash=false, $sendout=false)
    {

        $record = false;
        $input = false;

        // get the client
        $client = Client::where('slug', $client_slug)->first();
        if(! $client) abort(404);

        // get the campaign
        $campaign = Campaign::with('client')->with('creative')->where('client_id', $client->id)->where('slug', $campaign_slug)->first();
        if(! $campaign) abort(404);

        // process creative
        if($campaign->creative) $campaign->creative = $campaign->creative->process();

        // populate fields array
        $fields = [];
        foreach($campaign->fields(1) as $field) $fields[$field->name] = $field;

        // get the input record
        if($hash)
        {
            $input = $campaign->input()->where('hash', $hash)->first();
            if(! $input) abort(404);

            // get output record (if it exists)
            $output = $campaign->output()->where('input_id', $input->id)->first();

            // set record
            $record = $output ? $output : $input;

            // generate process url
            $process_url = $campaign->url('update', $hash);

            // set entry type
            $entry_type = 'update';
        }
        else
        {
            $output = false;

            // generate process url
            $process_url = $campaign->url('update');

            // generate empty set
            $input = Input::emptyRecord($campaign->id);
            $record = $input;

            // set entry type
            $entry_type = 'new';
        }

        //dd($record->toArray());

        // format record data
        foreach($campaign->formatInput() as $field => $callback)
        {
            if(is_callable($callback))
            {
                $record->{$field} = $callback($record->{$field});
            }
        }

        //dd($record);
        //dd($record->toArray());

        $hidden_fields = [
            'id' => $entry_type === 'update' ? $record->id : '0',
            'input' => $input ? $input->id : '0',
            'campaign' => $campaign->id,
            'hash' => $entry_type === 'update' ? $hash : '',
            'entry_type' => $entry_type,
            'update' => $output ? $record->input_id : '0',
            'sendout' => $sendout ? $sendout : '0',
            'step' => '1',
            '_token' => csrf_token(),
        ];

        // determine view
        if($campaign->open)
        {
            $view = $campaign->view('primaryLanding');
        }
        else
        {
            if(view()->exists($campaign->view('closed')))
            {
                $view = $campaign->view('closed');
            }
            else
            {
                $view = 'closed';
            }
        }

        return view($view, [
            'campaign' => $campaign,
            'client' => $client,
            'fields' => $fields,
            'input' => $input,
            'output' => $output,
            'record' => $record,
            'process_url' => $process_url,
            'hidden_fields' => $hidden_fields,
            'entry_type' => $entry_type,
        ]);

    }

    public function secondaryLanding($client_slug, $campaign_slug, $hash=false)
    {

        $record = false;

        // get the client
        $client = Client::where('slug', $client_slug)->first();
        if(! $client) abort(404);

        // get the campaign
        $campaign = Campaign::with('client')->where('client_id', $client->id)->where('slug', $campaign_slug)->first();
        if(! $campaign) abort(404);

        // process creative
        if($campaign->creative) $campaign->creative = $campaign->creative->process();
        //dd($campaign->toArray());

        // populate fields array
        $fields = [];
        foreach($campaign->fields(2) as $field) $fields[$field->name] = $field;
        //dd($fields);

        // get the input record
        if($hash)
        {
            // get output record (if it exists)
            $record = $campaign->output()->where('hash', $hash)->first();

            // check record
            if(! $record)
            {
                $redirect_url = $campaign->url('update', $hash);
                return redirect($redirect_url);
            }

            // generate process url
            $process_url = $campaign->url('step-2', $hash);

        }
        else
        {
            abort(404);
        }

        //dd($record->toArray());

        // format record data
        foreach($campaign->formatInput() as $field => $callback)
        {
            if(is_callable($callback))
            {
                $record->{$field} = $callback($record->{$field});
            }
        }

        //dd($record);
        //dd($record->toArray());

        $hidden_fields = [
            'id' => $record->id,
            'campaign' => $campaign->id,
            'hash' => $hash,
            'step' => '2',
            '_token' => csrf_token(),
        ];

         if($campaign->open)
        {
            $view = $campaign->view('secondaryLanding');
        }
        
        return view($campaign->view('secondaryLanding'), [
            'campaign' => $campaign,
            'client' => $client,
            'output' => $record,
            'record' => $record,
            'process_url' => $process_url,
            'hidden_fields' => $hidden_fields,
            'fields' => $fields,
        ]);

    }

    public function confirm($client_slug, $campaign_slug, $hash=false, $sendout=false)
    {

        $record = false;

        // check hash
        if(! $hash) abort(404);

        // get the client
        $client = Client::where('slug', $client_slug)->first();
        if(! $client) abort(404);

        // get the campaign
        $campaign = Campaign::with('client')->where('client_id', $client->id)->where('slug', $campaign_slug)->first();
        if(! $campaign) abort(404);

        // process creative
        if($campaign->creative) $campaign->creative = $campaign->creative->process();
        //dd($campaign->toArray());

        // populate fields array
        $fields = [];
        foreach($campaign->fields() as $field) $fields[$field->name] = $field;
        //dd($fields);

        // get the input record
        $input = $campaign->input()->where('hash', $hash)->first();
        if(! $input) abort(404);

        // generate redirect URL
        if($campaign->steps > 1)
        {
            $redirect_url = $campaign->url('step-2', $hash);
        }
        else
        {
            $redirect_url = $campaign->url('thanks', $hash);
        }

        // get output record (if it exists)
        $output = $campaign->output()->where('input_id', $input->id)->first();

        //dd($input->toArray());

        // if output exists, go straight to thanks
        if($output)
        {
            return redirect($redirect_url);
        }

        $record = $input->toArray();
        //dd($record);

        // sanitize input
        foreach($campaign->validationSanitize() as $key => $callback)
        {
            if(is_callable($callback))
            {
                $record = $callback($record);
            }
        }

        //dd($record);

        // validation rules
        $validation_rules = [];

        foreach($campaign->validationRules() as $field => $rule)
        {
            if(is_callable($rule))
            {
                $validation_rules = $rule($input, $validation_rules, $input);
            }
            else
            {
                $validation_rules[$field] = $rule;
            }
        }
        //dd($validation_rules);

        // validation messages
        $validation_messages = $campaign->validationMessages();
        //dd($validation_messages);

        //dd($record);
        //dd($input->toArray());

        // validate
        $validator = Validator::make($record, $validation_rules, $validation_messages);

        if($validator->passes())
        {
            // get relative field keys
            $field_keys = [];
            foreach($fields as $field)
            {
                if($field->form) $field_keys[] = $field->name;
            }

            //dd($field_keys);

            // set form data
            $form_data = array_only($record, $field_keys);

            //dd($form_data);

            // format form data
            foreach($campaign->formatOutput(1, true) as $field => $callback)
            {
                if(is_callable($callback))
                {
                    $form_data = $callback($form_data);
                }
            }

            //dd($form_data);

            // copy data from source (not shown on form)
            $form_data = Input::copy($campaign, $form_data, $input);

            //dd($form_data);

            // set additional columns in output data
            $form_data['input_id']      = $input->id;
            $form_data['hash']          = $input->hash;
            $form_data['import_batch']  = $input->import_batch;
            $form_data['mailing_list']  = $input->mailing_list;
            $form_data['send_method']   = $input->send_method;
            $form_data['sendout']       = $sendout;
            $form_data['entry_type']    = 'confirm';

            //dd($form_data);

            // create new output
            $output = $campaign->output();
            $output->fill($form_data);
            $output->save();

            // save output id & time
            DB::table(Campaign::tableName('input', $campaign->id))
              ->where('hash', $hash)
              ->update(['output_id' => $output->id, 'output_at' => now()]);

            // redirect to thanks
            return redirect($redirect_url);

        }
        else
        {
            $redirect_url = $campaign->url('update', $hash . '/' . $sendout);
            //dd($redirect_url);
            return redirect($redirect_url)->withErrors($validator);
        }


    }

    public function primaryProcessNew($client_slug, $campaign_slug, ProcessRequest $request) {
        return $this->primaryProcess($client_slug, $campaign_slug, false, $request);
    }

    public function primaryProcess($client_slug, $campaign_slug, $hash=false, ProcessRequest $request)
    {

        // get the client
        $client = Client::where('slug', $client_slug)->first();
        if(! $client) abort(404);

        // get the campaign
        $campaign = Campaign::where('client_id', $client->id)->where('slug', $campaign_slug)->first();
        if(! $campaign) abort(404);

        // process creative
        if($campaign->creative) $campaign->creative = $campaign->creative->process();
        //dd($campaign->toArray());

        $request_array = $request->toArray();

        // save primary record
        $output = $this->_saveRecordToDb($campaign, $request_array);

        // has secondary records?
        $secondary_accounts = $request->get('secondary_accounts', false);
        if($secondary_accounts)
        {
            $secondary_accounts = json_decode($secondary_accounts);

            foreach($secondary_accounts as &$secondary_account)
            {
                $secondary_account->entry_type = $request_array['entry_type'];
                $secondary_account = (array)$secondary_account;
                foreach($secondary_account as $key => $value)
                {
                    $secondary_account[$key] = is_array($value) ? json_encode($value) : $value;
                }
            }

            //dd($secondary_accounts);

            foreach($secondary_accounts as $save_secondary_account)
            {
                $this->_saveRecordToDb($campaign, $save_secondary_account);
            }
        }

        //dd($secondary_accounts);

        // update AFC 2020 member choice stock amounts (hacky af, TO BE REMOVED ASAP AFTER CAMPAIGN CLOSE!) - NM 11/08/2019
        if($campaign->id === 78 && $output)
        {

            $upsells_table = '_campaign_0078_upsells';

            // delete previous items
            DB::table($upsells_table)->where('output_id', $output->id)->delete();

            // save hoodies
            if($output->upsell_hoodie)
            {
                $upsell_hoodie = json_decode($output->upsell_hoodie);
                if(count($upsell_hoodie) > 0)
                {
                    foreach($upsell_hoodie as $hoodie_size)
                    {
                        DB::table($upsells_table)->insert(['output_id' => $output->id, 'item' => 'hoodie', 'option' => $hoodie_size]);
                    }
                }
            }

            // save polos
            if($output->upsell_polo)
            {
                $upsell_polo = json_decode($output->upsell_polo);
                if(count($upsell_polo) > 0)
                {
                    foreach($upsell_polo as $polo_size)
                    {
                        DB::table($upsells_table)->insert(['output_id' => $output->id, 'item' => 'polo', 'option' => $polo_size]);
                    }
                }
            }

        }

        if($campaign->steps == 1 && $hash)
        {
            $redirect_url = $campaign->url('thanks', $hash);
        }
        elseif($campaign->steps == 2 && $hash)
        {
            $redirect_url = $campaign->url('step-2', $hash);
        }
        else
        {
            $redirect_url = $campaign->url('thanks');
        }

        if($request->get('_fetch', false))
        {
            return response()->json(true);
        }
        else
        {
            return redirect($redirect_url);
        }

    }

    private function _saveRecordToDb($campaign, $request)
    {
        //dd($request);

        $id = $request['id'];
        $hash = $request['hash'];
        $entry_type = $request['entry_type'];

        // get form data
        $form_data = array_except($request, ['id', 'input', 'campaign', 'hash', 'entry_type', 'update', '_token', '_fetch', 'secondary_accounts']);
        //dd($form_data);

        if($entry_type === 'update' && $id && $hash)
        {

            // get source record
            $input = $campaign->input()->where('hash', $hash)->first();

            // return error if source not found
            if(! $input) abort(404);

            $output = $campaign->output()->where('input_id', $input->id)->first();

        }
        else
        {
            $input = false;
            $output = false;
        }

        //dd($form_data);
        //print_r($form_data); exit;

        //dd($campaign->formatOutput());

        // format form data
        foreach($campaign->formatOutput() as $field => $callback)
        {
            if(is_callable($callback))
            {
                $form_data = $callback($form_data);
            }
        }

        //dd($form_data);
        //print_r($form_data); exit;

        if($output)
        {
            // update existing output record

            DB::table(Campaign::tableName('output', $campaign->id))
              ->where('hash', $hash)
              ->update($form_data);

        }
        else
        {
            // create a new output record

            // copy data from source (not shown on form)
            if($entry_type === 'update')
            {
                $form_data = Input::copy($campaign, $form_data, $input);
            }

            //dd($form_data);

            // set additional columns in output data
            $form_data['input_id']      = ($input) ? $input->id : 0;
            $form_data['hash']          = ($input) ? $input->hash : '';
            $form_data['import_batch']  = ($input) ? $input->import_batch : 'NEW';
            $form_data['mailing_list']  = ($input) ? $input->mailing_list : '0';
            $form_data['send_method']   = ($input) ? $input->send_method : '';
            $form_data['entry_type']    = $entry_type;

            //dd($input);
            //dd($form_data);
            //print_r($form_data); exit;

            // create new output
            $output = $campaign->output();
            $output->fill($form_data);
            $output->save();

            if($entry_type === 'update')
            {
                // save output id & time
                DB::table(Campaign::tableName('input', $campaign->id))
                  ->where('hash', $hash)
                  ->update(['output_id' => $output->id, 'output_at' => now()]);

            }

        }

        return $output;

    }

    public function secondaryProcess($client_slug, $campaign_slug, $hash=false, ProcessRequest $request)
    {

        // get the client
        $client = Client::where('slug', $client_slug)->first();
        if(! $client) abort(404);

        // get the campaign
        $campaign = Campaign::where('client_id', $client->id)->where('slug', $campaign_slug)->first();
        if(! $campaign) abort(404);

        // process creative
        if($campaign->creative) $campaign->creative = $campaign->creative->process();
        //dd($campaign->toArray());

        $id = $request->get('id');
        $hash = $request->get('hash');

        $output = $campaign->output()->where('hash', $hash)->first();

        // check record
        if(! $output)
        {
            $redirect_url = $campaign->url('update', $hash);
            return redirect($redirect_url);
        }

        // get form data
        $form_data = $request->except(['id', 'campaign', 'hash', 'entry_type', 'update', '_token']);
        //dd($form_data);

        // format form data
        $format_out = $campaign->formatOutput($form_data['step']);
        //dd($format_out);
        foreach($format_out as $field => $callback)
        {
            if(is_callable($callback))
            {
                $form_data = $callback($form_data);
            }
        }

        // convert arrays to json
        foreach($form_data as $key => $value)
        {
            if(is_array($value))
            {
                $form_data[$key] = json_encode($value);
            }
        }

        //dd($form_data);

        DB::table(Campaign::tableName('output', $campaign->id))
          ->where('hash', $hash)
          ->update($form_data);

        $redirect_url = $campaign->url('thanks', $hash);

        return redirect($redirect_url);

    }

    public function thanks($client_slug, $campaign_slug, $hash=false)
    {

        $record = false;

        // get the client
        $client = Client::where('slug', $client_slug)->first();
        if(! $client) abort(404);

        // get the campaign
        $campaign = Campaign::where('client_id', $client->id)->where('slug', $campaign_slug)->first();
        if(! $campaign) abort(404);

        // process creative
        if($campaign->creative) $campaign->creative = $campaign->creative->process();
        //dd($campaign->toArray());

        // get the input record
        if($hash)
        {

            // load input record from campaign input table
            $input = $campaign->input()->where('hash', $hash)->first();
            if(! $input) abort(404);

            // get output record (if it exists)
            $output = $campaign->output()->where('input_id', $input->id)->first();

            // set record
            $record = $output ? $output : $input;

            // set entry type
            $entry_type = 'update';
        }
        else
        {
            $record = false;
        }

        //dd($output->toArray());

        // send thank you SMS
        if($campaign->send_thanks_sms && Schema::hasColumn($campaign->outputTable(), 'sms_sent') && $record)
        {
            if(!$record->sms_sent)
            {
                // check & format number
                $mobile = preg_replace('/[^0-9,.]/', '', $record->{$campaign->mobile_field});
                if(strlen($mobile) == 9 && substr($mobile, 1) == '4') $mobile = '+61' . $mobile; // prepend 0
                $mobile_valid = preg_match_all('/^\D*(?:[+]?[6][1]|0)\D*[4](\D*\d){8}\D*$/', $mobile);

                if($mobile_valid)
                {

                    $guzzle = $this->client = new \GuzzleHttp\Client();

                    $clicksend = [
                        'headers' => [
                            'Authorization' => 'Basic bW1yc3R1ZGlvOmFjNjAxNzUyYmMyZTUzMTAyMGI4MGJmNWZiOGUxYWJkNDkxZGNmNTU5NjgyMDU3MjMxMTRiYmI2MjhjNWE4YjYw',
                            'Content-Type' => 'application/json',
                        ],
                        'body' => [
                            'messages' => [[
                                'source' => 'datacleanse',
                                //'to' => '0400565382',
                                'to' => $record->{$campaign->mobile_field},
                                'from' => $campaign->thanks_sms_from,
                                'body' => str_replace('(_first_name_field_)', $record->{$campaign->first_name_field}, $campaign->thanks_sms),
                            ]]
                        ]
                    ];

                    //dd($clicksend);

                    $clicksend_data = [
                        'headers' => $clicksend['headers'],
                        'body' => json_encode($clicksend['body']),
                    ];

                    //dd($clicksend_data);

                    // send the sms
                    try {
                        $send_sms = $guzzle->post('https://rest.clicksend.com/v3/sms/send', $clicksend_data);
                    } catch (\GuzzleHttp\Exception\ClientException $e) {
                        //dd($e->getResponse()->getBody()->getContents());
                    }

                    //dd($output);

                    // save SMS state
                    DB::table($campaign->outputTable())->where('id', $output->id)->update([
                        'sms_sent' => 1,
                        'sms_data' => json_encode($clicksend),
                    ]);

                }

            }
        }

        return view($campaign->view('thanks'), [
            'campaign' => $campaign,
            'client' => $client,
            'record' => $record,
        ]);

    }

    public function page($client_slug, $campaign_slug, $view)
    {
        // get the client
        $client = Client::where('slug', $client_slug)->first();
        if(! $client) abort(404);

        // get the campaign
        $campaign = Campaign::where('client_id', $client->id)->where('slug', $campaign_slug)->first();
        if(! $campaign) abort(404);

        // process creative
        if($campaign->creative) $campaign->creative = $campaign->creative->process();
        //dd($campaign->toArray());

        return view($campaign->view($view));
    }

    public function createShortUrl(Request $request)
	{
        $url = $request->get('url', false);
        if(! $url) return response()->json(['created' => false, 'error' => 'No URL specified']);

        $expires = $request->get('expires', false);

        $link_reponse = Link::createFromUrl($url, $expires);

        return response()->json($link_reponse);
    }
    
    public function testData($client_slug, $campaign_slug, $view_file=false, $field_key=false, $field_value=false)
    {

        $record = false;
        $input = false;
        $sendout = false;

        // get the client
        $client = Client::where('slug', $client_slug)->first();
        if(! $client) abort(404);

        // get the campaign
        $campaign = Campaign::with('client')->with('creative')->where('client_id', $client->id)->where('slug', $campaign_slug)->first();
        if(! $campaign) abort(404);

        // process creative
        if($campaign->creative) $campaign->creative = $campaign->creative->process();
        //dd($campaign->toArray());

        // populate fields array
        $fields = [];
        foreach($campaign->fields(1) as $field) $fields[$field->name] = $field;
        //dd($fields);

        // get the input record
        if($field_key && $field_value)
        {
            // load input record from campaign input table
            $input = $campaign->input()->where($field_key, $field_value)->first();
            if(! $input) abort(404);

            // get output record (if it exists)
            $output = $campaign->output()->where('input_id', $input->id)->first();

            // set record
            $record = $output ? $output : $input;

            // generate process url
            //$process_url = url($client->slug . '/' . $campaign->slug . '/update/' . $hash);
            $process_url = $campaign->url('test', $field_key . '/' . $field_value);

            $record->hash = !$record->hash ? 'TEST' : $record->hash;
            $record->confirm_url = $campaign->url('confirm/' . $record->hash);
            $record->update_url = $campaign->url('update/' . $record->hash);

            // set entry type
            $entry_type = 'update';
        }
        else
        {
            $output = false;

            // generate process url
            //$process_url = url($client->slug . '/' . $campaign->slug . '/update');
            $process_url = $campaign->url('update');

            // generate empty set
            $input = Input::emptyRecord($campaign->id);
            $record = $input;

            // set entry type
            $entry_type = 'new';
        }

        //dd($record->toArray());

        // format record data
        foreach($campaign->formatInput() as $field => $callback)
        {
            if(is_callable($callback))
            {
                $record->{$field} = $callback($record->{$field});
            }
        }

        //dd($record);
        //dd($record->toArray());

        $hidden_fields = [
            'id' => $entry_type === 'update' ? $record->id : '0',
            'input' => $input ? $input->id : '0',
            'campaign' => $campaign->id,
            'hash' => $entry_type === 'update' ? $record->hash : '',
            'entry_type' => $entry_type,
            'update' => $output ? $record->input_id : '0',
            'sendout' => $sendout ? $sendout : '0',
            'step' => '1',
            '_token' => csrf_token(),
        ];

        // determine view
        if($campaign->open)
        {
            $view = $campaign->view('test-' . $view_file);
        }
        else
        {
            if(view()->exists($campaign->view('closed')))
            {
                $view = $campaign->view('closed');
            }
            else
            {
                $view = 'closed';
            }
        }

        return view($view, [
            'campaign' => $campaign,
            'client' => $client,
            'input' => $input,
            'output' => $output,
            'record' => $record,
            'process_url' => $process_url,
            'hidden_fields' => $hidden_fields,
            'entry_type' => $entry_type,
            'fields' => $fields,
        ]);

    }


}
