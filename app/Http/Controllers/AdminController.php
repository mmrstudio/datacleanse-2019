<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;

use App\Client;
use App\Campaign;
use App\Sendout;
use App\Field;
use App\Nav;
use App\CampaignMonitor;
use App\CM_Campaign;
use App\CMCampaign;

use DB;
use Schema;
use Gate;

class AdminController extends Controller
{

    public $nav = [];

    public function __construct()
    {
        $this->middleware('auth');

        $this->nav = [
            'show' => true,
            'items' => [
                'users' => [
                    'url' => url('admin/users'),
                    'title' => 'Users',
                    'active' => false,
                    'sub_items' => [
                        'create' => [
                            'url' => url('admin/users/create'),
                            'title' => 'Add Campaign',
                            'active' => false,
                        ],
                    ],
                ],
                'clients' => [
                    'url' => url('admin/clients'),
                    'title' => 'Clients',
                    'active' => false,
                    'sub_items' => [
                        'create' => [
                            'url' => url('admin/clients/create'),
                            'title' => 'Add Client',
                            'active' => false,
                        ],
                    ],
                ],
                'campaigns' => [
                    'url' => url('admin/campaigns'),
                    'title' => 'Campaigns',
                    'active' => false,
                    'sub_items' => [
                        'create' => [
                            'url' => url('admin/campaigns/create'),
                            'title' => 'Add Campaign',
                            'active' => false,
                        ],
                    ],
                ],

            ]
        ];

    }

    public function campaigns()
    {
        // activate nav item
        $this->nav['items']['campaigns']['active'] = true;

        // get campaigns
        $campaigns = Campaign::with('client')->get();

        //dd($campaigns->toArray());

        return view('admin.campaigns', [
            'nav' => (object) $this->nav,
            'campaigns' => $campaigns,
        ]);
    }

    public function clients()
    {
        if(Gate::denies('mmr-admin')) return abort(404);

        // nav
        $nav = new Nav(false);

        $clients = Client::all();

        //dd($clients->toArray());

        return view('admin.clients', [
            'nav' => $nav,
            'clients' => $clients,
        ]);
    }

    public function editClient($client_id)
    {
        if(Gate::denies('mmr-admin')) return abort(404);

        // get client
        $client = Client::find($client_id);

        $cm_clients = CampaignMonitor::clients();

        return view('client.edit', [
            'client' => $client,
            'cm_clients' => $cm_clients,
        ]);

    }

}
