<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Campaign;
use App\Field;

class CampaignTableController extends Controller
{

    public function index($campaign_id)
    {
        $tables = db_campaign_tables($campaign_id);
        return response()->json($tables);
    }

}
