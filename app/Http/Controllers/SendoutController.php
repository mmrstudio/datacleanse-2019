<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Client;
use App\Campaign;
use App\Sendout;
use App\Field;
use App\Nav;
use App\CampaignMonitor;
use App\CM_Campaign;

use Schema;
use Gate;

class SendoutController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($campaign_id=false)
    {
        $get_sendouts = Sendout::with('client', 'campaign');
        if($campaign_id) $get_sendouts->where('campaign_id', $campaign_id);
        $sendouts = $get_sendouts->get();

        return response()->json($sendouts);
    }

    public function home($campaign_id)
    {
        // nav
        $nav = new Nav;
        $nav->setItems(Campaign::nav());
        $nav->setActive('admin');
        $nav->setActive('admin.sendouts');

        // get campaign
        $campaign = Campaign::with('client')->find($campaign_id);

        return view('admin.send-outs', [
            'campaign' => $campaign,
            'nav' => $nav,
            'client' => Client::current(),
        ]);
    }

    public function edit($sendout_id)
    {
        // nav
        $nav = new Nav;
        $nav->setItems(Campaign::nav());
        $nav->setActive('admin');
        $nav->setActive('admin.sendouts');

        // get sendout
        $sendout = Sendout::with('campaign', 'client')->find($sendout_id);

        //dd($sendout->toArray());

        if($sendout)
        {

            // get campaign
            $campaign = $sendout->campaign;

            // get CM templates
            $templates = CampaignMonitor::templates($sendout->client->cm_client_id, false);

            return view('send-out.edit', [
                'nav' => $nav,
                'sendout' => $sendout,
                'campaign' => $campaign,
                'clients' => Client::all(),
                'campaigns' => Campaign::where('client_id', $sendout->client_id)->get(),
                'templates' => $templates,
            ]);

        }

    }

    public function update($sendout_id, Request $request)
    {
        $sendout = Sendout::find($sendout_id);

        if($sendout)
        {
            $sendout->update($request->except('_token', '_method'));
        }

        return redirect("campaign/$sendout->campaign_id/send-outs");

    }

}
