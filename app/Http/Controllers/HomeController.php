<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;

use App\Campaign;

class HomeController extends Controller
{

    public $client = false;
    public $campaign = false;
    public $nav = [];

    public function __construct()
    {
        $this->middleware('auth');

        $send_outs = [
            [
                'url' => '',
                'title' => 'Send Out 1',
                'active' => false,
            ],
            [
                'url' => '',
                'title' => 'Send Out 2',
                'active' => false,
            ],
        ];

        $this->nav = Campaign::nav();

        $this->campaign = [
            'title' => 'Campaign Title',
        ];

        $this->client = [
            'name' => 'Client Name',
        ];

    }

    public function index()
    {
        return 'campaigns';

        $this->nav['show'] = false;

        return view('campaigns', [
            'nav' => $this->nav,
            'client' => $this->client ? (object) $this->client : false,
            'campaign' => $this->campaign ? (object) $this->campaign : false,
        ]);
    }

    public function responses($category=false, $send_out_id=false)
    {
        // activate nav item
        $this->nav['items']['responses']['active'] = true;

        // activate current send out nav item
        if($send_out_id && isset($this->nav['items']['responses']['sub_items'][$send_out_id])) $this->nav['items']['responses']['sub_items'][$send_out_id]['active'] = true;

        $send_out = [
            'id' => $send_out_id ? $send_out_id : 'all',
            'title' => $send_out_id != 'all' ? 'Send Out ' . $send_out_id : 'All send outs',
        ];

        $responses = [
            'categories' => [
                'changed' => 'Changed Fields',
                'unchanged' => 'Unchanged Fields',
                'renew' => 'Renew Membership',
                'favourite_player' => 'Favourite Player',
            ],
            'category' => 'changed',
            'category_name' => 'Changed Fields',
            'data' => [
                [
                    'field' => 'street',
                    'name' => 'Street',
                    'value' => 530,
                ],
                [
                    'field' => 'suburb',
                    'name' => 'Suburb',
                    'value' => 503,
                ],
                [
                    'field' => 'phone',
                    'name' => 'Phone',
                    'value' => 356,
                ],
                [
                    'field' => 'mobile',
                    'name' => 'Mobile',
                    'value' => 125,
                ],
                [
                    'field' => 'email',
                    'name' => 'Email Address',
                    'value' => 100,
                ],
                [
                    'field' => 'dob',
                    'name' => 'Date of birth',
                    'value' => 50,
                ],
            ],
        ];

        $view_data = [
            'nav' => $this->nav,
            'client' => $this->client ? $this->client : false,
            'campaign' => $this->campaign ? $this->campaign : false,
            'send_out' => $send_out,
            'responses' => $responses,
        ];

        //dd($view_data);

        return view('responses', $view_data);
    }

    public function responses_compare($field, $send_out_id=false)
    {
        // activate nav item
        $this->nav['items']['responses']['active'] = true;

        if($send_out_id && isset($this->nav['items']['responses']['sub_items'][$send_out_id])) $this->nav['items']['responses']['sub_items'][$send_out_id]['active'] = true;

        $send_out = [
            'id' => $send_out_id ? $send_out_id : 'all',
            'title' => $send_out_id != 'all' ? 'Send Out ' . $send_out_id : 'All send outs',
        ];

        $responses = [
            'id_label' => 'Account ID',
            'data' => [
                ['id' => '663160325-0', 'old' => 'cwilson0@issuu.com', 'new' => 'eday0@wp.com'],
                ['id' => '966480140-2', 'old' => 'kfrazier1@histats.com', 'new' => 'mking1@shinystat.com'],
                ['id' => '012461171-0', 'old' => 'mgutierrez2@dyndns.org', 'new' => 'gmeyer2@slate.com'],
                ['id' => '314274988-8', 'old' => 'jbryant3@webmd.com', 'new' => 'drichards3@mtv.com'],
                ['id' => '894065212-6', 'old' => 'cfowler4@google.ca', 'new' => 'jrivera4@opera.com'],
                ['id' => '559018856-3', 'old' => 'tgarcia5@usda.gov', 'new' => 'jhudson5@irs.gov'],
                ['id' => '639706877-3', 'old' => 'jrose6@sciencedaily.com', 'new' => 'djackson6@51.la'],
                ['id' => '130112584-9', 'old' => 'ppeters7@symantec.com', 'new' => 'chenry7@google.cn'],
                ['id' => '405579078-3', 'old' => 'dkelly8@mashable.com', 'new' => 'wboyd8@foxnews.com'],
                ['id' => '234926643-5', 'old' => 'drobertson9@ucoz.ru', 'new' => 'dsimmons9@behance.net'],
                ['id' => '666729255-7', 'old' => 'tboyda@taobao.com', 'new' => 'dfostera@yellowpages.com'],
                ['id' => '806923902-6', 'old' => 'jgomezb@go.com', 'new' => 'chernandezb@hhs.gov'],
                ['id' => '774712019-2', 'old' => 'jfowlerc@360.cn', 'new' => 'kwalkerc@go.com'],
                ['id' => '594824606-X', 'old' => 'wthompsond@homestead.com', 'new' => 'lbryantd@dot.gov'],
                ['id' => '289070828-4', 'old' => 'rbennette@nasa.gov', 'new' => 'jlarsone@smh.com.au'],
            ],
            'field' => [
                'id' => 'email',
                'label' => 'Email Address',
            ],
            'fields' => [
                'email' => 'Email Address',
                'street' => 'Street',
                'suburb' => 'Suburb',
                'phone' => 'Phone',
                'mobile' => 'Mobile',
                'dob' => 'Date of birth',
            ],
        ];

        $view_data = [
            'nav' => $this->nav,
            'client' => $this->client ? $this->client : false,
            'campaign' => $this->campaign ? $this->campaign : false,
            'responses' => $responses,
            'send_out' => $send_out,
        ];

        return view('responses-compare', $view_data);
    }

    public function traffic($send_out_id=false)
    {
        // activate nav item
        $this->nav['items']['traffic']['active'] = true;

        // activate current send out nav item
        if($send_out_id && isset($this->nav['items']['traffic']['sub_items'][$send_out_id])) $this->nav['items']['traffic']['sub_items'][$send_out_id]['active'] = true;

        $send_out = [
            'id' => $send_out_id ? $send_out_id : false,
            'title' => 'Send Out 1',
        ];

        $traffic = [
            'dimension' => 'states',
            'dimensions' => [
                'countries' => 'Countries',
                'states' => 'States',
                'devices' => 'Devices',
                'os' => 'Operating Systems',
            ],
            'data' => [
                [
                    'name' => 'Victoria',
                    'value' => 530 . ' (' . number_format((530/2000)*100, 2) . '%)',
                ],
                [
                    'name' => 'New South Wales',
                    'value' => 503 . ' (' . number_format((503/2000)*100, 2) . '%)',
                ],
                [
                    'name' => 'Queensland',
                    'value' => 356 . ' (' . number_format((356/2000)*100, 2) . '%)',
                ],
                [
                    'name' => 'Western Australia',
                    'value' => 125 . ' (' . number_format((125/2000)*100, 2) . '%)',
                ],
                [
                    'name' => 'South Australia',
                    'value' => 100 . ' (' . number_format((100/2000)*100, 2) . '%)',
                ],
                [
                    'name' => 'Tasmania',
                    'value' => 50 . ' (' . number_format((50/2000)*100, 2) . '%)',
                ],
                [
                    'name' => 'ACT',
                    'value' => 45 . ' (' . number_format((45/2000)*100, 2) . '%)',
                ],
            ],
        ];

        $view_data = [
            'nav' => $this->nav,
            'client' => $this->client ? $this->client : false,
            'campaign' => $this->campaign ? $this->campaign : false,
            'send_out' => $send_out,
            'traffic' => $traffic,
        ];

        return view('traffic', $view_data);
    }

    public function data($send_out_id=false)
    {
        // activate nav item
        $this->nav['items']['data']['active'] = true;

        // activate current send out nav item
        if($send_out_id && isset($this->nav['items']['data']['sub_items'][$send_out_id])) $this->nav['items']['data']['sub_items'][$send_out_id]['active'] = true;

        $send_out = [
            'id' => $send_out_id ? $send_out_id : false,
            'title' => 'Send Out 1',
        ];

        $view_data = [
            'nav' => $this->nav,
            'client' => $this->client ? $this->client : false,
            'campaign' => $this->campaign ? $this->campaign : false,
            'send_out' => $send_out,
        ];

        return view('data', $view_data);
    }

}
