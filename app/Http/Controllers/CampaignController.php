<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\StoreCampaign;

use App\Client;
use App\Campaign;
use App\Sendout;
use App\Field;
use App\Nav;
use App\CampaignMonitor;
use App\CM_Campaign;
use App\CMCampaign;
use App\FieldCompare;
use App\CampaignStats;
use App\Report;

use DB;
use Schema;
use Gate;
use Excel;

class CampaignController extends Controller
{
    private $request;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    public function home()
    {
        // nav
        $nav = new Nav(false);

        // which campaigns view to show?
        //$campaigns_view = Gate::denies('mmr-admin') ? 'campaigns' : 'admin.campaigns';
        $campaigns_view = 'admin.campaigns';

        // get campaigns
        $campaigns = Campaign::with('client');

        $client = Client::current();

        if(Gate::denies('mmr-admin'))
        {
            $campaigns->where('client_id', $client->id);
        }

        $campaigns = $campaigns->get();

        //dd($campaigns->toArray());

        $totals = [
            'recipients' => 0,
            'emails' => 0,
            'responses' => 0,
        ];

        foreach($campaigns as $campaign)
        {
            $campaign->stats = Campaign::stats($campaign->id, false);

            $totals['recipients'] += $campaign->stats['total_recipients']['total'];
            $totals['emails'] += $campaign->stats['recipients']['total'];
            $totals['responses'] += $campaign->stats['responses']['total'];
        }

        //dd($campaigns->toArray());

        return view($campaigns_view, [
            'nav' => $nav,
            'client' => Client::current(),
            'campaigns' => $campaigns,
            'totals' => $totals,
        ]);
    }

    public function index()
    {
        $campaigns = Campaign::with('client');

        $client = Client::current();
        if($client->id > 1)
        {
            $campaigns->where('client_id', $client->id);
        }

        $campaigns = $campaigns->get();
        return response()->json($campaigns);
    }

    public function set($campaign_id=0, $redirect=false)
    {
        $campaign_id = Campaign::setCurrent($campaign_id);

        if($redirect)
        {
            switch($redirect)
            {
                default : case 'overview' :
                    $redirect_path = 'overview';
                    break;
                case 'edit' :
                    $redirect_path = 'campaign/ ' . $campaign_id . '/edit';
                    break;
            }
        }
        else
        {
            $redirect_path = 'overview';
        }

        return redirect($redirect_path);
    }

    public function refreshOverview($campaign_id)
    {
        $campaign = Campaign::find($campaign_id);
        $cm_campaigns = CMCampaign::where('campaign_id', $campaign_id)->get();

        if($campaign->locked)
        {
            echo "This campaign is locked for updates.";
            exit;
        }

        // CM campaign overview stats
        $campaign_stats = [];
        foreach($cm_campaigns as $cm_campaign) $cm_campaign->syncStats();
        //dd($campaign_stats);

        // CM opens
        $campaign->syncOpens();

        // CM clicks
        $campaign->syncClicks();

        // CM opens
        $campaign->syncResponses();

        // Field stats
        $campaign->syncFieldStats();

        $campaign->touch();

        return redirect('overview');
    }

    public function refreshFieldStats($campaign_id)
    {
        $campaign = Campaign::find($campaign_id);
        $campaign->syncFieldStats();

        return redirect('responses');
    }

    public function overview($send_out_id=false)
    {
        // get current campaign
        $campaign = Campaign::current();

        // if no campain selected, redirect to campaigns screen
        if(! $campaign) return redirect('campaigns');

        // nav
        $nav = new Nav;
        $nav->setItems(Campaign::nav());
        $nav->setActive('overview');

        // set active send out
        if($send_out_id) $nav->setActive("overview.$send_out_id");

        // get campaign overview stats
        $stats = Campaign::stats($campaign->id, $send_out_id);
        //dd($stats);

        // get sendout info
        $send_out = $send_out_id ? Sendout::find($send_out_id) : false;

        // get graph data
        $graph_metric = $this->request->get('graph', 'responses');

        $graph_data = [
            'labels' => [],
            'values' => [],
        ];

        $graph_source = [
            'responses' => CampaignStats::summary($campaign->id, 'responses', false, $send_out_id),
            'opens' => CampaignStats::summary($campaign->id, 'campaign', 'opens', $send_out_id),
            'clicks' => CampaignStats::summary($campaign->id, 'campaign', 'clicks', $send_out_id),
        ];

        foreach($graph_source[$graph_metric] as $metric)
        {
            $graph_data['labels'][] = date('d/m/y', strtotime($metric->date));
            $graph_data['values'][] = $metric->value;
        }

        $view_data = [
            'nav' => $nav,
            'campaign' => $campaign,
            'send_out_title' => $send_out ? $send_out->name : 'All Sendouts',
            'send_out' => $send_out,
            'stats' => $stats,
            'graph_metric' => $graph_metric,
            'graph_source' => $graph_source,
            'graph_data' => $graph_data,
        ];

        //dd($view_data);

        return view('overview', $view_data);
    }

    public function traffic($send_out_id=false)
    {
        // get current campaign
        $campaign = Campaign::current();

        // if no campain selected, redirect to campaigns screen
        if(! $campaign) return redirect('campaigns');

        // nav
        $nav = new Nav;
        $nav->setItems(Campaign::nav());
        $nav->setActive('traffic');

        // set active send out
        if($send_out_id) $nav->setActive("traffic.$send_out_id");

        // get campaign overview stats
        $stats = Campaign::stats($campaign->id, $send_out_id);
        //dd($stats);

        // get sendout info
        $send_out = $send_out_id ? Sendout::find($send_out_id) : false;

        // get graph data
        // $graph_metric = $this->request->get('graph', 'responses');
        //
        // $graph_data = [
        //     'labels' => [],
        //     'values' => [],
        // ];
        //
        // $graph_source = [
        //     'responses' => CampaignStats::summary($campaign->id, 'responses', false, $send_out_id),
        //     'opens' => CampaignStats::summary($campaign->id, 'campaign', 'opens', $send_out_id),
        //     'clicks' => CampaignStats::summary($campaign->id, 'campaign', 'clicks', $send_out_id),
        // ];
        //
        // foreach($graph_source[$graph_metric] as $metric)
        // {
        //     $graph_data['labels'][] = date('d/m/y', strtotime($metric->date));
        //     $graph_data['values'][] = $metric->value;
        // }

        $view_data = [
            'nav' => $nav,
            'campaign' => $campaign,
            'send_out_title' => $send_out ? $send_out->name : 'All Sendouts',
            'send_out' => $send_out,
            'stats' => $stats,
            'graph_metric' => $graph_metric,
            'graph_source' => $graph_source,
            'graph_data' => $graph_data,
        ];

        dd($view_data);

        return view('overview', $view_data);
    }

    public function edit($campaign_id)
    {
        abort_if(Gate::denies('mmr-admin'), 403);

        // nav
        $nav = new Nav;
        $nav->setItems(Campaign::nav());
        $nav->setActive('admin');
        $nav->setActive('admin.edit');

        // get campaign
        $campaign = Campaign::find($campaign_id);

        //$campaign->fields = [];

        return view('campaign.edit', [
            'nav' => $nav,
            'campaign' => $campaign,
            'clients' => Client::all(),
            'tables' => db_campaign_tables($campaign_id),
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_if(Gate::denies('mmr-admin'), 403);

        // nav
        $nav = new Nav;
        $nav->setItems(Campaign::nav());
        $nav->setActive('admin');
        $nav->setActive('admin.create');

        return view('campaign.create', [
            'nav' => $nav,
            'clients' => Client::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCampaign $request)
    {
        abort_if(Gate::denies('mmr-admin'), 403);

        $campaign = new Campaign;
        $campaign->client_id = $request->input('client_id');
        $campaign->name = $request->input('name');
        $campaign->slug = $request->input('slug');
        $campaign->open = ($request->get('open', false) === '1')?'1':'0';
        $campaign->closed_at = '0';
        $campaign->save();

        //return redirect("campaign/$campaign_id/edit");

        return redirect()->route('campaign.edit', ['id' => $campaign->id]);
    }

    public function update($campaign_id, Request $request)
    {
        $campaign = Campaign::find($campaign_id);

        if($campaign)
        {
            $campaign->update($request->except('open', '_token', '_method'));

            // change open state
            if($request->get('open', false) === '1')
            {
                $campaign->open = '1';
                $campaign->closed_at = '0';
            }
            else
            {
                $campaign->open = '0';
                $campaign->closed_at = now();
            }

            $campaign->save();

        }

        return redirect("campaign/$campaign_id/edit");

    }

    public function data()
    {

        // get current campaign
        $campaign = Campaign::current();

        // if no campain selected, redirect to campaigns screen
        if(! $campaign) return redirect('campaigns');

        // nav
        $nav = new Nav;
        $nav->setItems(Campaign::nav());
        $nav->setActive('data');

        // get table columns
        $columns = $campaign->dataColumns();
        //dd($columns);

        $view_data = [
            'nav' => $nav,
            'campaign' => $campaign,
            'columns' => $columns,
        ];

        //dd($view_data);

        return view('data', $view_data);
    }

    public function getTables()
    {
        $tables = db_campaign_tables();
        return response()->json($tables);
    }

    public function createTables()
    {
        $tables = db_campaign_tables();
        return response()->json($tables);
    }

    public function createInputTable($campaign_id, $backup=true)
    {
        return Campaign::createInputTable($campaign_id, $backup);
    }

    public function responses($send_out_id=false)
    {

        // get current campaign
        $campaign = Campaign::current();

        // if no campain selected, redirect to campaigns screen
        if(! $campaign) return redirect('campaigns');

        // nav
        $nav = new Nav;
        $nav->setItems(Campaign::nav());
        $nav->setActive('responses');

        // set active send out
        if($send_out_id) $nav->setActive("responses.$send_out_id");

        // get sendout info
        $send_out = $send_out_id ? Sendout::find($send_out_id) : false;

        $input_table = $campaign->inputTable();
        $output_table = $campaign->outputTable();

        $status = [];
        $status_fields = Field::where('form', '1')->where('campaign_id', $campaign->id)->get();

        // dropdown categories
        $dropdown = [
            ['url' => 'responses', 'label' => 'Summary', 'current' => true],
            ['url' => 'responses/field/entry_type', 'label' => 'Confirm/Update', 'current' => false]
        ];
        $result_fields = Field::where('compare', '1')->where('campaign_id', $campaign->id)->get();
        foreach($result_fields as $result_field) $dropdown[] = ['url' => 'responses/field/' . $result_field->name, 'label' => $result_field->front_label, 'current' => false];

        //DB::connection()->enableQueryLog();

        foreach($status_fields as $field)
        {
            $input_column = $field->compare_to ? $field->compare_to : $field->name;
            $output_column = $field->name;

            if(Schema::hasColumn($input_table, $input_column) && Schema::hasColumn($output_table, $output_column))
            {
                $status[$field->name]['label'] = $field->front_label;
                $status[$field->name]['changed'] = $field->changed;
                $status[$field->name]['unchanged'] = $field->unchanged;
                $status[$field->name]['added'] = $field->added;
                $status[$field->name]['removed'] = $field->removed;
            }
        }

        //dd(DB::getQueryLog());
        //dd($status);

        $view_data = [
            'nav' => $nav,
            'campaign' => $campaign,
            //'send_out_title' => $send_out ? $send_out->name : 'All Sendouts',
            'send_out_title' => 'Summary',
            'status' => $status,
            'dropdown' => $dropdown,
        ];

        //dd($view_data);

        return view('responses', $view_data);
    }

    public function responsesField($field_name, $send_out_id=false)
    {
        // get current campaign
        $campaign = Campaign::current();

        // if no campain selected, redirect to campaigns screen
        if(! $campaign) return redirect('campaigns');

        // nav
        $nav = new Nav;
        $nav->setItems(Campaign::nav());
        $nav->setActive('responses');

        // set active send out
        if($send_out_id) $nav->setActive("responses.$send_out_id");

        // get sendout info
        $send_out = $send_out_id ? Sendout::find($send_out_id) : false;

        $output_table = $campaign->outputTable();

        // get the field
        $field = Field::where('name', $field_name)->where('campaign_id', $campaign->id)->first();

        // dropdown categories
        $dropdown = [
            ['url' => 'responses', 'label' => 'Summary', 'current' => false],
            ['url' => 'responses/field/entry_type', 'label' => 'Confirm/Update', 'current' => $field_name === 'entry_type']
        ];
        $result_fields = Field::where('compare', '1')->where('campaign_id', $campaign->id)->get();
        foreach($result_fields as $result_field)
        {
            $dropdown[] = [
                'url' => 'responses/field/' . $result_field->name,
                'label' => $result_field->front_label,
                'current' => $field && $field->name === $result_field->name,
            ];
        }

        $results = [];

        if($field_name === 'entry_type')
        {
            $input_column = false;
            $output_column = 'entry_type';
            $send_out_title = 'Confirm/Update';
            $results = [];

            $confirms = CampaignStats::total($campaign->id, 'responses', 'confirm', false);
            $updates = CampaignStats::total($campaign->id, 'responses', 'update', false);

            $confirms_total = $confirms ? $confirms->value : 0;
            $updates_total = $updates ? $updates->value : 0;
            $total = $confirms_total + $updates_total;

            $confirms_pc = number_format(($confirms_total / $total) * 100, 2) . '%';
            $updates_pc = number_format(($updates_total / $total) * 100, 2) . '%';

            $results[] = (object) [
                'value' => 'Confirmed',
                'responses' => number_format($confirms_total, 0) . " ($confirms_pc)",
            ];

            $results[] = (object) [
                'value' => 'Updated',
                'responses' => number_format($updates_total, 0) . " ($updates_pc)",
            ];

        }
        else
        {
            $input_column = $field->name;
            $output_column = $field->name;
            $send_out_title = $field->front_label;

            // get field comparisons
            $field_comparisons = FieldCompare::where('field_id', $field->id)->get();
            $results = $field_comparisons;

            // foreach($field_comparisons as $comparison)
            // {
            //     $results[] = [
            //         'value' => $comparison->value,
            //         'responses' => $comparison->responses,
            //     ];
            // }

        }

        $view_data = [
            'nav' => $nav,
            'campaign' => $campaign,
            'sendout' => $send_out,
            'send_out_title' => $send_out_title,
            'field' => $field,
            'results' => $results,
            'dropdown' => $dropdown,
        ];

        //dd($view_data);

        return view('responses-field', $view_data);

    }

    public function responsesCompare($field_name, $send_out_id=false)
    {
        // get current campaign
        $campaign = Campaign::current();

        // if no campain selected, redirect to campaigns screen
        if(! $campaign) return redirect('campaigns');

        // nav
        $nav = new Nav;
        $nav->setItems(Campaign::nav());
        $nav->setActive('responses');

        // set active send out
        if($send_out_id) $nav->setActive("responses.$send_out_id");

        // get sendout info
        $send_out = $send_out_id ? Sendout::find($send_out_id) : false;

        $input_table = $campaign->inputTable();
        $output_table = $campaign->outputTable();

        // get the field
        $field = Field::where('name', $field_name)->where('campaign_id', $campaign->id)->first();

        // dropdown categories
        $dropdown = [['url' => 'responses', 'label' => 'Summary', 'current' => false]];
        $result_fields = Field::where('form', '1')->where('campaign_id', $campaign->id)->get();
        foreach($result_fields as $result_field) $dropdown[] = ['url' => 'responses/compare/' . $result_field->name, 'label' => $result_field->front_label . " ($result_field->name)", 'current' => $field->name === $result_field->name];

        // get ID field
        $id_field = Field::where('name', $campaign->id_field)->where('campaign_id', $campaign->id)->first();

        $input_column = $field->compare_to ? $field->compare_to : $field->name;
        $output_column = $field->name;

        // get the results
        if(Schema::hasColumn($input_table, $input_column) && Schema::hasColumn($output_table, $output_column))
        {
            $results_sql = "select `$input_table`.`id` as 'input_id', `$output_table`.`id` as 'output_id', `$output_table`.`$campaign->id_field` as 'record_id', `$input_table`.`$input_column` as 'old', `$output_table`.`$output_column` as 'new' from `$input_table` left join `$output_table` on `$input_table`.`id` = `$output_table`.`input_id` where `$input_table`.`$input_column` <> '' and `$input_table`.`$input_column` <> `$output_table`.`$output_column`";
            //dd($results_sql);
            $results = DB::select($results_sql);
        }

        $view_data = [
            'nav' => $nav,
            'campaign' => $campaign,
            'sendout' => $send_out,
            'send_out_title' => $field->front_label . ' (' . $field->name . ')',
            'field' => $field,
            'results' => $results,
            'dropdown' => $dropdown,
            'id_field' => $id_field,
        ];

        //dd($view_data);

        return view('responses-compare', $view_data);

    }

    public function responsesRecord($output_id, $send_out_id=false)
    {
        // get current campaign
        $campaign = Campaign::current();

        // if no campain selected, redirect to campaigns screen
        if(! $campaign) return redirect('campaigns');

        // nav
        $nav = new Nav;
        $nav->setItems(Campaign::nav());
        $nav->setActive('responses');

        // set active send out
        if($send_out_id) $nav->setActive("responses.$send_out_id");

        // get sendout info
        $send_out = $send_out_id ? Sendout::find($send_out_id) : false;

        $input_table = $campaign->inputTable();
        $output_table = $campaign->outputTable();

        // get ID field
        $id_field = Field::where('name', $campaign->id_field)->where('campaign_id', $campaign->id)->first();

        // get form fields
        $select_fields = ['id', $id_field->name];
        $form_fields = Field::where('form', '1')->where('input', '1')->where('output', '1')->where('campaign_id', $campaign->id)->get();
        foreach($form_fields as $field) $select_fields[] = $field->name;

        // comparison data
        $comparison_data = [];

        // get records
        $output = DB::table($output_table)->select(array_merge($select_fields, ['input_id']))->where('id', $output_id)->first();
        $input = DB::table($input_table)->select($select_fields)->where('id', $output->input_id)->first();

        //dd($input);

        $view_data = [
            'nav' => $nav,
            'campaign' => $campaign,
            'sendout' => $send_out,
            'send_out_title' => $id_field->front_label . ': ' . $output->{$id_field->name},
            'form_fields' => $form_fields,
            'input' => $input,
            'output' => $output,
            'id_field' => $id_field,
        ];

        //dd($view_data);

        return view('responses-record', $view_data);

    }

    public function dataTable(Request $request)
    {
        $campaign = Campaign::current();
        //dd($campaign->toArray());

        $columns = $request->get('columns');
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');

        $order = $request->get('order');
        $order_by = [
            'column' => $columns[$order[0]['column']]['name'],
            'direction' => $order[0]['dir'],
        ];

        $select_fields = [];
        foreach($columns as $column)
        {
            $select_fields[] = $column['name'];
        }

        $table_name = $campaign->outputTable();

        $total_records = DB::table($table_name)->count();
        $records = DB::table($table_name)->select($select_fields)->skip($start)->take($length)->orderBy($order_by['column'], $order_by['direction'])->get();

        $return_data = [
            'draw' => $draw,
            'recordsTotal' => $total_records,
            'recordsFiltered' => $total_records,
            'data' => [],
        ];

        foreach($records as $record)
        {
            $return_data['data'][] = array_flatten((array) $record);
        }

        //dd($request->all());
        //print_r($return_data); exit;

        return response()->json($return_data);

    }

    public function analytics($campaign_id, Request $request)
    {
        $campaign = Campaign::find($campaign_id);
        $analytics_args = $request->only(['dimension', 'metric', 'start_date', 'end_date', 'day', 'filters', 'max_results', 'sendout']);

        $campaign_analytics = $campaign->analytics($analytics_args);
        //dd($campaign_analytics);

        return response()->json($campaign_analytics);

    }

    public function results($campaign_id, Request $request)
    {
        $campaign = Campaign::find($campaign_id);
        $campaign_results = $campaign->results();
        dd($campaign_results);

        return response()->json($campaign_results);

    }

    public function reports()
    {
        // get current campaign
        $campaign = Campaign::current();

        // if no campain selected, redirect to campaigns screen
        if(! $campaign) return redirect('campaigns');

        // get reports
        $reports = $campaign->reports();

        // nav
        $nav = new Nav;
        $nav->setItems(Campaign::nav());
        $nav->setActive('reports');

        $view_data = [
            'nav' => $nav,
            'campaign' => $campaign,
            'reports' => $reports ? $reports : [],
        ];

        //dd($view_data);

        return view('admin.reports', $view_data);

    }

    public function report($report_id)
    {
        // get report
        $report = Report::find($report_id);

        // get campaign
        $campaign = Campaign::find($report->campaign_id);

        // get report data
        $report_data = $campaign->report($report->name);

        // get table info
        $columns = [];
        if($report_data)
        {
            foreach($report_data[0] as $key => $value)
            {
                $columns[] = $key;
            }
        }

        // nav
        $nav = new Nav;
        $nav->setItems(Campaign::nav());
        $nav->setActive('reports');

        $view_data = [
            'nav' => $nav,
            'campaign' => $campaign,
            'report' => $report,
            'columns' => $columns,
            'report_data' => $report_data,
        ];

        //dd($view_data);

        return view('admin.report', $view_data);

    }

    public function reportExport($report_id)
    {
        // get report
        $report = Report::find($report_id);

        // get campaign
        $campaign = Campaign::find($report->campaign_id);

        // get report data
        $report_data = $campaign->report($report->name);

        $filename = studly_case($campaign->client->name) . '_Report_' . studly_case($report->name) . '_' . date('Y-m-d_Hi');

        Excel::create($filename, function($excel) use ($report_data, $report) {

            $excel->setTitle($report->name);

            $excel->sheet($report->name, function($sheet) use ($report_data) {
                $sheet->fromArray($report_data);
            });

        })->download('xls');

    }

}
