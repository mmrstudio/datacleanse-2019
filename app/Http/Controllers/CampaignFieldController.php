<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Campaign;
use App\Field;

class CampaignFieldController extends Controller
{

    public function index($campaign_id)
    {
        $fields = Campaign::find($campaign_id)->fields;
        return response()->json($fields);
    }

    public function show($campaign_id, $field_id)
    {
        $field = Field::find($field_id);
        if(! $field) return response()->json('', 404);
        return response()->json($field);
    }

    public function destroy($campaign_id, $field_id)
    {
        $field = Field::find($field_id);

        if(! $field) return response()->json('', 404);

        return response()->json($field->delete());

    }

}
