<?php

namespace App;

class Nav
{

    public $show = false;
    public $items = [];

    public function __construct($show=true) {
        $this->show = $show;
    }

    public function show() {
        $this->show = true;
    }

    public function hide() {
        $this->show = false;
    }

    public function setItems($items) {
        $this->items = $items;
    }

    public function setActive($path) {

        $path_segments = explode('.', $path);

        if(count($path_segments) == 1)
        {
            $nav_item = $path_segments[0];
        }
        else
        {
            $nav_item = $path_segments[0] . '.sub_items.' . $path_segments[1];
        }

        array_set($this->items, $nav_item . '.active', true);
        array_set($this->items, $nav_item . '.open', true);

    }

}
