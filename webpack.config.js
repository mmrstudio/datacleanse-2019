var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var webpack = require('webpack');

module.exports.getConfig = function(type) {

    var config = {
        //devtool: 'cheap-module-source-map',
        entry: './resources/assets/js/app.js',
        output: {
            path: __dirname,
            filename: 'bundle.js'
        },
        debug : true,
        module: {
            loaders: [
                {
                    test: /\.js$/,
                    loader: ['babel'],
                    exclude: /node_modules/,
                    include: path.join(__dirname, 'resources/assets/js'),
                    query: {
                        presets: ['react', 'es2015']
                    }
                },
                { test: /\.scss$/, loader: ExtractTextPlugin.extract('css!sass') },
                { test: /\.(png|woff|woff2|eot|ttf|svg|gif|jpg)$/, loader: 'file-loader' }
            ]
        },
        resolve: {
            extensions: ['', '.js']
        },
        plugins: [
            //new webpack.DefinePlugin({'process.env': { 'NODE_ENV': JSON.stringify('production') } }),
            new ExtractTextPlugin('bundle.css'),
            new CleanWebpackPlugin(['public/dist'], { root: __dirname, verbose: true, dry: false })
        ]
    };

    return config;

}
